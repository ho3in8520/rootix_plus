<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Unit implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $units = [
            'usdt',
            'btt',
            'win',
            'jst',
        ];
        if (in_array($value, $units))
            return 1;
        return 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid request';
    }
}
