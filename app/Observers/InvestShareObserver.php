<?php

namespace App\Observers;

use App\Jobs\InviterProfit;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\Invest_Shares;

class InvestShareObserver
{
    /**
     * Handle the Invest_Shares "created" event.
     *
     * @param \App\Models\Invest_Shares $invest_shares
     * @return void
     */
    public function created(Invest_Shares $invest_shares)
    {
        /*
         * transactions12 => خرید invest جدید
         * transactions13 => خرید shares جدید
         * transactions8 => سود حاصل از خرید فرد زیر مجموعه
         * profit_inviter => درصد سودی که باید به دعوت کننده فرد برسه
         */

        $buy_or_update = $invest_shares->wasChanged('initial_value') ? 'update' : 'buy';
        $amount = $invest_shares->wasChanged('initial_value') ?
            $invest_shares->initial_value - $invest_shares->getOriginal('initial_value') :
            $invest_shares->initial_value;

        $type = $invest_shares->type;

        $inviter = auth()->user()->referral;
        $inviter_id = $inviter ? $inviter->id : '';
        $USDT_assets = Asset::query()
            ->whereIn('user_id', [auth()->user()->id, $inviter_id])
            ->where('unit', 'usdt')
            ->get(['id', 'amount', 'user_id']);

        $USDT_asset = $USDT_assets->where('user_id', auth()->user()->id)->first();
        $inviter_USDT_asset = $USDT_assets->where('user_id', $inviter_id)->first();

        $buy_extra_filed = $type == 'invest' ? 12 : 13;

        $base_data = BaseData::query()
            ->where(function ($query) use ($buy_extra_filed) {
                $query
                    ->where('type', 'transactions')
                    ->whereIn('extra_field1', [$buy_extra_filed, 8]);
            })
            ->orWhere('type', 'profit_inviter')
            ->get(['id', 'extra_field1', 'type']);
        $finance_transaction = [];
        $date = date('Y-m-d H:i:s');
        $transactions_buy = $base_data->where('type', 'transactions')->where('extra_field1', $buy_extra_filed)->first();
        $profit_inviter = $base_data->where('type', 'profit_inviter')->first();


        $USDT_asset->amount = $USDT_asset->amount - $amount;
        $USDT_asset->save();


        $des = $buy_or_update == 'buy' ?
            "Buy new $type with Initial Value {$amount} USDT" :
            "Increase initial value of $type to {$amount} USDT";

        $finance_transaction[] = [
            'financeable_id' => $USDT_asset->id,
            'financeable_type' => 'App\Models\Asset',
            'refer_id' => $invest_shares->id,
            'user_id' => $USDT_asset->user_id, // کاربر کاربر
            'type' => 1, // کاهش
            'transact_type' => $transactions_buy->id, // ایدی از بیس دیتا
            'amount' => $amount, // مقدار سود اضافه شده سی تی ار
            'description' => $des,
            'created_at' => $date,
            'updated_at' => $date
        ];

        $finance_transaction[] = [
            'financeable_id' => $invest_shares->id,
            'financeable_type' => 'App\Models\Invest_Shares',
            'refer_id' => null,
            'user_id' => $invest_shares->user_id, // کاربر کاربر
            'type' => 2, // افزایش
            'transact_type' => $transactions_buy->id, // ایدی از بیس دیتا
            'amount' => $amount, // مقدار خرید یا اپدیت
            'description' => $des,
            'created_at' => $date,
            'updated_at' => $date
        ];
        Finance_transaction::insert($finance_transaction);
        // اضافه کردن سود به کاربر دعوت کننده + لاگ مربوطه در جدول finance_transaction
        if ($inviter_USDT_asset && $profit_inviter) {
            $des = "Profit to Buy $type of subsets";
            InviterProfit::dispatch(auth()->user(), $amount, $invest_shares->id, $des, $type);
        }
    }

    /**
     * Handle the Invest_Shares "updated" event.
     *
     * @param \App\Models\Invest_Shares $invest_Shares
     * @return void
     */
    public function updated(Invest_Shares $invest_Shares)
    {
        if ($invest_Shares->wasChanged('initial_value')) {
            $this->created($invest_Shares);
        }
    }

}
