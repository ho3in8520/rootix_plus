<?php

namespace App\Observers;

use App\Models\Asset;
use App\Models\User;
use App\Models\UserGuard;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function created(User $user)
    {
        $array = [
            ['user_id' => $user->id, 'unit' => 'rial', 'name' => 'Rial', 'amount' => 0, 'type_token' => 20, 'logo' => 'rls'],
            ['user_id' => $user->id, 'unit' => 'usdt', 'name' => 'USDT', 'amount' => 0, 'type_token' => 20, 'logo' => 'usdt'],
            ['user_id' => $user->id, 'unit' => 'usdt', 'name' => 'JUST', 'amount' => 0, 'type_token' => 20, 'logo' => 'jst'],
            ['user_id' => $user->id, 'unit' => 'usdt', 'name' => 'WINkLink', 'amount' => 0, 'type_token' => 20, 'logo' => 'win'],
            ['user_id' => $user->id, 'unit' => 'btt', 'name' => 'BitTorrent', 'amount' => 0, 'type_token' => 10, 'logo' => 'btt'],
        ];
        Asset::insert($array);

        $user->userGuard()->create();
    }

    /**
     * Handle the User "updated" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
