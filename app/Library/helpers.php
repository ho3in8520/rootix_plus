<?php

use App\Models\BaseData;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Http;
use Morilog\Jalali\Jalalian;
use PragmaRX\Google2FA\Google2FA;
use PragmaRX\Google2FALaravel\Support\Authenticator;

//use Kavenegar;


function get_price_token($unit = 'btt')
{
    $response2 = Http::withoutVerifying()->withHeaders(['content-type: application/json'])->post('https://api.nobitex.ir/market/stats', [
        'srcCurrency' => 'rls',
        'dstCurrency' => 'usdt'
    ]);
    return $response2['global'];
}

function toFixed($number)
{
    return number_format(floor($number * 1000) / 1000, 3);
}

function verifyGatewayIrankish(array $param)
{

    $terminalID = '08052518';
    $password = '5688823446637855';
    $acceptorId = "992180008052518";
    $pub_key = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqjnFp+hLOuHrNj1baxi0r7RZP
mrxzAVpa68SBLFSyBZM0AoRQdZUF9InsaDvxqmoRud1kPolopBVfZd++DJkAaD4q
HGL6oDv7Hq5N7xcK3u3blq0g0BCXxs1q/W4jUvfvN81Y/kx0DNO9nQ5dKDS5yj+g
xLgfAdM8GN0GpWnRNQIDAQAB
-----END PUBLIC KEY-----';

    $data = array_merge($param, [
        "terminalId" => $terminalID,
    ]);

    $data_string = json_encode($data);
    $ch = curl_init('https://ikc.shaparak.ir/api/v3/confirmation/purchase');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    $result = curl_exec($ch);
    $result = json_decode($result);

    return $result;
}

function smsVerify($message, $number, $mode)
{
    try {

        $receptor = $number;//Receptors numbers

        Kavenegar::VerifyLookup($receptor, $message, '', '', $mode);
    } catch (\Kavenegar\Exceptions\ApiException $e) {
        // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
        echo $e->errorMessage();
    } catch (\Kavenegar\Exceptions\HttpException $e) {
        // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
        echo $e->errorMessage();
    }
//    \sms()->send($message)->to($number)->dispatch();
}

function google2faVerify(\Illuminate\Http\Request $request)
{
    $authenticator = app(Authenticator::class)->boot($request);
    if ($authenticator->isAuthenticated()) {
        return true;
    }
    return $authenticator->makeRequestOneTimePasswordResponse();
}

function upload($file, $type)
{
    $guard = '';
    if (\Illuminate\Support\Facades\Auth::guard('admin')->check()) {
        $guard = 'admin';
    }
    $path = \Illuminate\Support\Facades\Storage::disk('uploaded')->put(auth($guard)->user()->id . '/' . $type, $file);
    return $path;
}

function getImage($path, $mode = null)
{
    $ex = explode('/', $path);
    return route("image.view", ['type' => $ex[0], 'id' => $ex[1], 'file' => $ex[2]]);
}

function amountTransactionDays()
{
    $transaction = \App\Models\BaseData::where('type', 'transactions')->where('extra_field1', 1)->first();
//    $result= \App\Models\Asset::select([
//        '*',
//        'total_amount_transaction'=>\App\Models\Finance_transaction::select([\Illuminate\Support\Facades\DB::raw('ifnull(sum(amount),0) as total')])
//            ->where('created_at',\Carbon\Carbon::now()->format('Y-m-d'))->where('transact_type',$transaction->id)->limit(1)
//    ])->where('user_id',auth()->user()->id)->where('unit','rial')->first();
    $result = \App\Models\Finance_transaction::select([\Illuminate\Support\Facades\DB::raw('ifnull(sum(amount),0) as total')])
        ->where('created_at', \Carbon\Carbon::now()->format('Y-m-d'))->where('transact_type', $transaction->id)->first();
    return $result;
}

function transactionLimit()
{
    $array = [
        0 => 0,
        1 => 200000000,
        2 => 500000000,
        3 => 5000000000,
        4 => 10000000000,
    ];
    $result = $array[auth()->user()->step_complate];
    return $result;
}

function convertStepToPersian($data)
{
    $step = [0, 1, 2, 3, 4, 5, 6];
    $persian = [trans('attributes.number.zero'),
        trans('attributes.number.one'),
        trans('attributes.number.two'),
        trans('attributes.number.three'),
        trans('attributes.number.four'),
        trans('attributes.number.five'),
        trans('attributes.number.six')];
    $result = str_replace($step, $persian, $data);
    return $result;
}

function jdate_from_gregorian($input, $format = '%A, %d %B %Y | H:i:s')
{
    return \Morilog\Jalali\Jalalian::fromDateTime($input)->format($format);
}

function jalali_to_timestamp($date, $spliter = '/', $first = true)
{
    if ($first == true) {
        $h = "00";
        $i = "00";
        $s = "00";
    } else {
        $h = "23";
        $i = "59";
        $s = "59";
    }

    list($y, $m, $d) = explode($spliter, $date);
    return (new \Morilog\Jalali\Jalalian($y, $m, $d, $h, $i, $s))->toArray()['timestamp'];
}

function faTOen($string) {
    return strtr($string, array('۰'=>'0', '۱'=>'1', '۲'=>'2', '۳'=>'3', '۴'=>'4', '۵'=>'5', '۶'=>'6', '۷'=>'7', '۸'=>'8', '۹'=>'9', '٠'=>'0', '١'=>'1', '٢'=>'2', '٣'=>'3', '٤'=>'4', '٥'=>'5', '٦'=>'6', '٧'=>'7', '٨'=>'8', '٩'=>'9'));
}

/*
 * تبدیل تاریخ شمسی به میلادی
 */
function jalali_to_gregorian($date,$splitter='/') {
    return \Morilog\Jalali\CalendarUtils::createCarbonFromFormat('Y/m/d H:i:s', faTOen($date));
}
function showData($view, array $array = [])
{
    if (request()->ajax()) {
        $temp = ["status" => "100", "FormatHtml" => $view->renderSections()['content']];
        $temp = array_merge($temp, $array);
        return json_encode($temp);
    } else
        return $view;
}

function tether_to_ctr($tether)
{
    $ctr_price = \App\Models\BaseData::query()
        ->active()
        ->where('type', 'currency_price')
        ->where('name', 'ctr')
        ->first(['extra_field1']);
    if (!is_null($ctr_price))
        return $ctr_price->extra_field1 * $tether;
    return 0;
}

// تبدیل تتر به btt
function tether_to_btt($tether)
{
    $price_currency = \App\Models\GlobalMarketApi::price_currency()
        ->first(['value']);
    if (!is_null($price_currency) && !is_null($price_currency->value))
        return $tether / $price_currency->BttToUsdt;
    return 0;
}

// برگرداندن پارمتر های مورد نیاز کنسل کردن اینوست : تاریخ، درصد خسارت
function parameter_cancel_invest()
{
    $base_data = BaseData::query()->whereIn('type', ['date_cancel_invest', 'percent_cancel_invest'])->get(['extra_field1', 'type']);
    $date = $base_data->where('type', 'date_cancel_invest')->first();
    $percent = $base_data->where('type', 'percent_cancel_invest')->first();

    $date = $date ? $date->extra_field1 : null;
    $percent = $percent ? $percent->extra_field1 : null;
    $now = Date::now()->format('Y-m-d');
    return [
        'date' => $now == $date ? true : ($date > $now ? $date : false),
        'percent' => $now != $date ? $percent : false
    ];
}

function convertEn($string)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);

    $englishNumbersOnly = str_replace($persian, $num, $convertedPersianNums);

    return $englishNumbersOnly;
}


function banks_name()
{
    return ['Tejarat', 'Sepah', 'Mellat', 'Melli', 'Maskan', 'Keshavarzi', 'PostBank', 'Others'];
}

// گرفتن رنگ متناسب با ارز ها
function currency_color($currency_name = null)
{
    $currency_color = [
        'btc' => '#f7931a',
        'eth' => '#627eea',
        'usdt' => '#26a17b',
        'doge' => '#c3a634',
        'xrp' => '#23292f',
        'ltc' => '#335d9d',
    ];
    if (!is_null($currency_name)) {
        if (key_exists($currency_name, $currency_color))
            return $currency_color[$currency_name];
        else
            return false;
    }
    return $currency_color;
}


function convertToParameter($address, $amount)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "api.ctrproject.com/convert-data",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "address=" . $address . "&amount=" . $amount . "",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);
    if ($httpcode == 200) {
        return $response;
    }
    return false;
}

function contractUnit($unit)
{
    $array = [
        'usdt' => ['type_token' => 20, 'contract' => 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t'],
        'trx' => ['type_token' => 20, 'contract' => 'TNUC9Qb1rRpS5CbWLmNMxXBjyFoydXjWFR'],
        'btt' => ['type_token' => 10, 'contract' => '1002000'],
        'jst' => ['type_token' => 20, 'contract' => 'TCFLL5dx5ZJdKnWuesXxi1VPwjLVmWZZy9'],
        'win' => ['type_token' => 20, 'contract' => 'TLa2f6VPqDgRE67v1736s7bJ8Ray5wYjU7'],
    ];
    return $array[$unit];
}

//تبدیل نوع فایل اپلود شده به فارسی
function convert_upload_file_to_persian($type): string
{
    switch ($type) {
        case 'phone_bill':
            return 'قبض تلفن';
        case 'national':
            return 'کارت ملی';
        default:
            return '--';
    }
}

function currency_change($change)
{
    $sign = substr($change['dayChange'], 0, 1);
    $value = $change['dayChange'];
    if ($sign == '-') {
        $sign_class = 'red-currency';
        $sign = '-';
        $value = substr($value, 1);
    } else {
        $sign_class = 'green-currency';
        $sign = '+';
    }

    return [$sign_class, $sign, $value];

}

function currency_change1($key, $value)
{
//    dd($key,$value);
    $fa_name = '';
    $en_name = '';
    $day_change = $value['dayChange'];
    switch ($key) {
        case 'btc':
            $fa_name = 'Bitcoin';
            $en_name = 'BTC';
            break;
        case 'eth':
            $fa_name = 'Ethereum';
            $en_name = 'ETH';
            break;
        case 'usdt':
            $fa_name = 'Theter';
            $en_name = 'USDT';
            break;
        case 'trx':
            $fa_name = 'Tron';
            $en_name = 'TRX';
            break;
        case 'doge':
            $fa_name = 'Dogecoin';
            $en_name = 'DOGE';
            break;
    }
    $sign = substr($day_change, 0, 1);
    if ($sign == '-') {
        $sign_class = 'red-currency';
        $sign = '-';
        $day_change = substr($day_change, 1);
    } else {
        $sign_class = 'green-currency';
        $sign = '+';
    }

    return [$sign_class, $sign, $day_change, $fa_name, $en_name];

}

function rila_to_usdt($array, $currency)
{
    $rial = $array[$currency]['dayHigh'];
    $usdt = $array['usdt']['dayHigh'];

    return $rial / $usdt;
}

// چک کردن درست بودن یا درست نبودن کد گوگل اتنتیکاتور
function getCurrentOtp($code)
{
    $google2fa = new Google2FA();
    $user = auth('web')->user();
    $otp = $google2fa->getCurrentOtp($user->google2fa_secret);
    if ($code == $otp) {
        return true;
    }
    return false;
}

function index($data, $loop)
{
    return ($data->currentPage() - 1) * $data->perPage() + 1 + $loop->index;
}

function getPrefix()
{
    $temp = ltrim(request()->route()->getPrefix(), "/");
    return str_replace('/', '.', $temp);
}

function getCurrentGuard()
{
    $prefix = getPrefix();
    if ($prefix == "admin") {
        return 'admin';
    } else {
        return 'web';
    }
}

function getCurrentUser()
{
    $guard = getCurrentGuard();
    if ($guard == "admin") {
        return auth('admin')->user();
    } elseif ($guard == "web") {
        return auth('web')->user();
    }
}

function getCategory($id){
    $category=\App\Models\PostCategory::findOrFail($id);
    return $category->name;
}

function countNotificationsNotView()
{
    $user = getCurrentUser();
    $result = \App\Models\Notification::where('user_id', $user->user_guard_id)->whereDoesntHave('notification_view')
        ->orWhere('user_id', 0)->whereDoesntHave('notification_view')
        ->count();
    return $result;
}

function getNotificationsNotView()
{
    $user = getCurrentUser();
    $result = \App\Models\Notification::where('user_id', $user->user_guard_id)->whereDoesntHave('notification_view')
        ->orWhere('user_id', 0)->whereDoesntHave('notification_view')
        ->get();
    return $result;
}
