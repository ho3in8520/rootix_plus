<?php


namespace App\Library;

use IEXBase\TronAPI\Tron;

class TronApi2 extends Tron
{
    public function getBalanceUsdt(string $address)
    {
        return $this->manager->request('wallet/triggersmartcontract', [
            'owner_address' => '41031fcd5ce8f93b1ccbfb5ca892018b4efde35e68',
            'contract_address' => '41a614f803b6fd780986a42c78ec9c7f77e6ded13c',
            'call_value' => 0,
            'fee_limit' => 150000000,
            'function_selector' => "balanceOf(address)",
            'parameter' => $address,
        ]);

    }

    public function getBalanceCtr(string $address)
    {
        return $this->manager->request('wallet/triggersmartcontract', [
            'owner_address' => '41031fcd5ce8f93b1ccbfb5ca892018b4efde35e68',
            'contract_address' => '41491cb84918bfac04d9a741329e716322c39dacad',
            'call_value' => 0,
            'fee_limit' => 1000000,
            'function_selector' => "balanceOf(address)",
            'parameter' => $address,
        ]);

    }

    public function getTransactionByAccountTrc20(string $address, $failed = 0)
    {
        return $this->manager->request('v1/accounts/' . $address . '/transactions/trc20?only_unconfirmed=' . $failed, [
        ], 'get');

    }

    public function getTransactionByAccountTrc10(string $address, $failed = 0)
    {
        return $this->manager->request('v1/accounts/' . $address . '/transactions?only_unconfirmed=' . $failed, [
        ], 'get');

    }

    public function getAccountInfo(string $address)
    {
        return $this->manager->request('v1/accounts/' . $address, [
        ], 'get');

    }

    public function triggerSmartContract($parameters, $contract_address, $owner_address)
    {
//        $contract_address=$this->toHex($contract_address);
        $transaction = $this->manager->request('wallet/triggersmartcontract', [
            'owner_address' => $owner_address,
            'contract_address' => $contract_address,
            'call_value' => 0,
            'fee_limit' => 150000000,
            'function_selector' => "transfer(address,uint256)",
            'parameter' => $parameters,
        ]);

        $transaction = $transaction['transaction'];

        $signedTransaction = $this->signTransaction($transaction, null);

        $response = $this->sendRawTransaction($signedTransaction);

        return array_merge($response, $signedTransaction);
    }

    public function triggerSmartContractWithdraw($parameters, $contract_address, $owner_address)
    {
        $contract_address = $this->toHex($contract_address);
        $transaction = $this->manager->request('wallet/triggersmartcontract', [
            'owner_address' => $owner_address,
            'contract_address' => $contract_address,
            'call_value' => 0,
            'fee_limit' => 150000000,
            'function_selector' => "transfer(address,uint256)",
            'parameter' => $parameters,
        ]);

        $transaction = $transaction['transaction'];

        $signedTransaction = $this->signTransaction($transaction, null);

        $response = $this->sendRawTransaction($signedTransaction);

        return array_merge($response, $signedTransaction);
    }

}
