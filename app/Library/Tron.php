<?php

use IEXBase\TronAPI\Tron;

function Tron($private_key = 'a9425deb2d9f01aa148b9d04d632d0b98f9f91191e2acbc1e5aaec9148753102'){
    $fullNode = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');
    $solidityNode = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');
    $eventServer = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');

    try {
        //'d40ea0e75b754ef6e59cc8d6589ee9192664e90d0a0173a5e7adfba2faa5e2ad'
        $tron = new \App\Library\TronApi2($fullNode, $solidityNode, $eventServer,null,null,$private_key);

    } catch (\IEXBase\TronAPI\Exception\TronException $e) {
        exit($e->getMessage());
    }

    return $tron;
}

