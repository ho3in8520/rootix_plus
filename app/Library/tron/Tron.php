<?php

namespace App\Library\tron;

use App\Models\Asset;
use App\Models\TransactionTron;
use Illuminate\Support\Facades\DB;

trait Tron
{
    private $unit = [
        '1002000' => 'btt',
        'TGdnjZ5QFY71eDrywzw856FpVh7xMBsF4k' => 'ctr',
        'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t' => 'usdt'
    ];

    public function getInformationAccount($addressBase58)
    {
        try {
            return Tron()->getAccountInfo($addressBase58);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getAmounts($address, $trc = 'all')
    {
        $amount = [];
        if ($trc == 'all') {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {
                if (isset($this->unit[$item['key']])) {
                    $amount[$this->unit[$item['key']]] = $item['value'];
                }
            }
            //// create array trc20
            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                if (isset($this->unit[array_keys($item)[0]])) {
                    $amount[$this->unit[array_keys($item)[0]]] = (int)$item[array_keys($item)[0]];
                }
            }
        } elseif ($trc == 10) {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {
                if (isset($this->unit[$item['key']])) {
                    $amount[$this->unit[$item['key']]] = $item['value'];
                }
            }
        } elseif ($trc == 20) {
            //// create array trc20
            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                if (isset($this->unit[array_keys($item)[0]])) {
                    $amount[$this->unit[array_keys($item)[0]]] = (int)$item[array_keys($item)[0]];
                }
            }
        }
        return $amount;
    }

    public function checkAmountUnit(string $address)
    {
        $hash = [];
        $transactions = Tron()->getTransactionByAccountTrc20($address);
        if (count($transactions['data']) > 0):
            foreach ($transactions['data'] as $transaction) {
                if ($transaction['token_info']['symbol'] == 'USDT' && $transaction['to'] == $address) {
                    $hash['usdt'][$transaction['transaction_id']] = $transaction['value'] / 1000000;
                }
            }
            $model = TransactionTron::whereIn('first_hash', array_keys($hash['usdt']))->get()->toArray();

            foreach ($hash['usdt'] as $key => $val) {
                foreach ($model as $item) {
                    if ($key == $item['first_hash']) {
                        unset($hash['usdt'][$key]);
                    }
                }
            }
            if (!empty($hash['usdt'])) {
                $this->updateAssets($hash);
            }
        endif;
    }

    private function updateAssets($data)
    {
        $array = [];
        $user = auth()->user();
        DB::beginTransaction();
        try {
            foreach ($data as $k => $unit) {
                $model = Asset::where('unit', $k)->where('user_id', $user->id)->first();
                $amount = 0;
                foreach ($unit as $item) {
                    $model->amount += $item;
                }
                $model->save();
            }
            DB::commit();
            $this->addTransactionTron($data);
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    private function addTransactionTron($data)
    {
        $array = [];
        $user = auth()->user();
        DB::beginTransaction();
        try {
            foreach ($data as $k => $unit) {
                foreach ($unit as $key => $item) {
                    array_push($array, ['first_hash' => $key, 'user_id' => $user->id, 'amount' => $item, 'unit' => $k, 'amount_commission' => $this->commission(), 'status' => 1]);
                }
            }
            TransactionTron::insert($array);
            DB::commit();
            return $this->createParameterJs();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    private function createParameterJs()
    {
        $result = TransactionTron::where('status', 1)->get();
        foreach ($result as $row) {
            $address = env('ADDRESS_TRON_HEX');
            $amount = $row->amount*1000000;
            $id_transact = $row->id;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "api.ctrproject.com/convert-data",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "address=" . $address . "&amount=" . $amount . "",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
            if ($httpcode == 200) {
                $row->ethers_data = $response;
                $row->save();
            }
        }
    }

    private function commission()
    {
        return 3;
    }

    public function getAmountsAdmin($address, $trc = 'all')
    {
        $amount = [];
        if ($trc == 'all') {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {
                if (isset($this->unit[$item['key']])) {
                    $amount[$this->unit[$item['key']]] = $item['value'];
                }
            }
            //// create array trc20
            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                if (isset($this->unit[array_keys($item)[0]])) {
                    $amount[$this->unit[array_keys($item)[0]]] = (int)$item[array_keys($item)[0]];
                }
            }
        } elseif ($trc == 10) {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {
                if (isset($this->unit[$item['key']])) {
                    $amount[$this->unit[$item['key']]] = $item['value'];
                }
            }
        } elseif ($trc == 20) {
            //// create array trc20
            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                if (isset($this->unit[array_keys($item)[0]])) {
                    $amount[$this->unit[array_keys($item)[0]]] = (int)$item[array_keys($item)[0]];
                }
            }
        }
        return $amount;
    }
}
