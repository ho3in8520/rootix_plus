<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CurrencyTransferAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = \App\Models\TransactionTron::select(['user_id', 'status', 'unit', 'id', 'amount_commission', 'commission_hash', 'transfer_hash', 'ethers_data', 'amount',
            'address' => \App\Models\Asset::select('token')->whereColumn('assets.user_id', 'transaction_trons.user_id')->whereNotNull('token')->limit(1),
        ])->whereIn('status', [0, 1, 2, 3, 4, 5, 6])->get();

        foreach ($result as $item) {
            if ($item->status == 0) {

            } elseif ($item->status == 1) {
                \Illuminate\Support\Facades\DB::beginTransaction();
                try {
                    $transfer = Tron()->sendTransaction(json_decode($item->address)->address_hex, $item->amount_commission, '', env('ADDRESS_TRON_BASE'));
                    $item->commission_hash = $transfer['txID'];
                    $item->status = 2;
                    $item->save();
                    \Illuminate\Support\Facades\DB::commit();
                } catch (Exception $e) {
                    \Illuminate\Support\Facades\DB::rollBack();
                }
            } elseif ($item->status == 2) {
                try {
                    $result = \Tron()->getTransaction($item->commission_hash);
                    if ($result['ret'][0]['contractRet'] == 'SUCCESS') {
                        $item->status = 4;
                    } else {
                        $item->status = 3;
                    }
                } catch (Exception $e) {
                    $item->status = 3;
                }
                $item->save();
            } elseif ($item->status == 3) {
                /// fail fail withdraw admin
                $item->status = 3;
                $item->save();
            } elseif ($item->status == 4) {
                \Illuminate\Support\Facades\DB::beginTransaction();
                try {
                    $transfer = Tron(json_decode($item->address)->private_key)->triggerSmartContract($item->ethers_data, 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t', json_decode($item->address)->address_hex);
                    $item->transfer_hash = $transfer['txID'];
                    $item->status = 5;
                    $item->save();
                    \Illuminate\Support\Facades\DB::commit();
                } catch (Exception $e) {
                    \Illuminate\Support\Facades\DB::rollBack();
                }
            } elseif ($item->status == 5) {
                try {
                    $result = \Tron()->getTransaction($item->transfer_hash);
                    if ($result['ret'][0]['contractRet'] == 'SUCCESS') {
                        $item->status = 7;
                    } else {
                        $item->status = 6;
                    }
                } catch (Exception $e) {
                    $item->status = 6;
                }
                $item->save();
            }
        }
        return 1;
    }
}
