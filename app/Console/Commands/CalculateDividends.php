<?php

namespace App\Console\Commands;

use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\Invest_Shares;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CalculateDividends extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dividends:calc {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate dividends of users';

    private $id;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $invest_Shares_id = intval($this->argument('id')) > 0 ? $this->argument('id') : false;
        $shares = Invest_Shares::query()
            ->join('base_data', function ($join) {
                $join
                    ->on('invest_shares.type', '=', 'base_data.type')
                    ->where(function ($query) {
                        $query
                            ->where(function ($query) {
                                $query
                                    ->where('initial_value','>=',DB::raw('base_data.extra_field1'))
                                    ->where('initial_value','<',DB::raw('base_data.extra_field2'));
                            })
                            ->orWhere(function ($query) {
                                $query
                                    ->where('invest_shares.initial_value', '>=', DB::raw('base_data.extra_field1'))
                                    ->where('base_data.extra_field2', '-1');
                            });
                    });
            })
            ->join('assets', function ($join) {
                $join
                    ->on('invest_shares.user_id', '=', 'assets.user_id')
                    ->where(function ($q) {
                        $q->where(function ($q) {
                            return $q
                                ->where('invest_shares.type', 'invest')
                                ->where('assets.unit', 'ctr');
                        })
                            ->orWhere(function ($q) {
                            return $q
                                ->where('invest_shares.type', 'shares')
                                ->where('assets.unit', 'btt');
                        });

                    });
            })
            ->where('invest_shares.status', 1)
            ->when($invest_Shares_id, function ($query, $invest_Shares_id) {
                return $query->where('invest_shares.id', $invest_Shares_id);
            })
            ->get([
                'invest_shares.id',
                'invest_shares.type',
                'invest_shares.initial_value',
                'invest_shares.profit',
                'invest_shares.user_id',

                'base_data.extra_field3',

                'assets.id as asset_id',
                'assets.name as asset_name',
                'assets.unit as asset_unit',
                'assets.amount as asset_amount',
            ]);
        $base_data_profit = BaseData::query()
            ->where('type', 'transactions')
            ->whereIn('extra_field1', [5, 6])
            ->get();

        $finance_transaction = [];
        $invest_shares = [];
        $assets = [];
        foreach ($shares as $share) {
            $value = $share->initial_value;
            $percent = $share->extra_field3;
            $profit = 0;
            $currency_profit = $share->type == 'invest' ? 'ctr' : 'btt';
            switch ($share->type) {
                case 'invest':
                    $profit = tether_to_ctr($percent / 100 * $value);
                    break;
                case 'shares':
                    $profit = tether_to_btt($percent / 100 * $value);
            }
            $invest_shares[] = [
                'id' => $share->id,
                'user_id' => $share->user_id,
                'profit' => $share->profit + $profit,
                'initial_value' => $share->initial_value,
            ];

            $assets[] = [
                'id' => $share->asset_id,
                'user_id' => $share->user_id,
                'name' => $share->asset_name,
                'unit' => $share->asset_unit,
                'amount' => $share->asset_amount + $profit,
            ];


            // Finance Transactions Row
            $des = "Your profit of {$share->type} increased by $profit {$currency_profit}";
            $date = date('Y-m-d H:i:s');

            $base_data_id = $share->type == 'invest' ? $base_data_profit->where('extra_field1', 5)->first() : $base_data_profit->where('extra_field1', 6)->first();
            $finance_transaction[] = [
                'financeable_id' => $share->id,
                'financeable_type' => 'App\Models\Invest_Shares',
                'refer_id' => null,
                'user_id' => $share->user_id, // کاربر کاربر
                'type' => 2, // افزایش
                'transact_type' => $base_data_id->id, // ایدی از بیس دیتا
                'amount' => $profit, // مقدار سود اضافه شده سی تی ار
                'description' => $des,
                'created_at' => $date,
                'updated_at' => $date
            ];
            $finance_transaction[] = [
                'financeable_id' => $share->asset_id,
                'financeable_type' => 'App\Models\Asset',
                'refer_id' => $share->id,
                'user_id' => $share->user_id, // کاربر کاربر
                'type' => 2, // افزایش
                'transact_type' => $base_data_id->id, // ایدی از بیس دیتا
                'amount' => $profit, // مقدار سود اضافه شده سی تی ار
                'description' => $des,
                'created_at' => $date,
                'updated_at' => $date
            ];
        } //-- End foreach

        Invest_Shares::query()
            ->upsert($invest_shares, ['id'], ['profit']);

        Asset::query()
            ->upsert($assets, ['id'], ['amount']);

        Finance_transaction::query()
            ->insert($finance_transaction);
    }
}
