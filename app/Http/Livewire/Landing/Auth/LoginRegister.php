<?php

namespace App\Http\Livewire\Landing\Auth;

use App\Models\User;
use App\Rules\StrengthPasswordFormat;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use ZxcvbnPhp\Zxcvbn;

class LoginRegister extends Component
{
    public $viewMode = 'login';
    public $email;
    public $password;
    public $password_confirmation;
    public $country = 101;
    public $countries;
    public $passwordStrength = 0;
    public $recaptcha;
    protected $listeners=['recaptcha'=>'setRecaptcha'];
    public function render()
    {
        return view('livewire.landing.auth.login-register');
    }

    public function login()
    {

        $this->validate([
            'email' => 'required|email',
            'password' => 'required',
            'recaptcha' =>'recaptcha|required'
        ]);

        if (Auth::attempt(['email' => $this->email, 'password' => $this->password]))
            return $this->emit('landingRegisterSuccess', ['status' => 100, 'msg' => 'ورود موفق']);
        return $this->emit('landingRegisterSuccess', ['status' => 500, 'msg' => 'اطلاعات شما با سوابق ما ما مطاقبت ندارد']);
    }

    public function register()
    {
        $this->validate([
            'email' => 'required|email|unique:users',
            'password' => ['required','min:6','confirmed',new StrengthPasswordFormat()],
            'country' => 'required|exists:countries,id',
            'recaptcha' =>'recaptcha|required'
        ]);
        try {
            $user = User::create([
                'email' => $this->email,
                'password' => Hash::make($this->password),
                'country_id' => $this->country,
                'code' => rand(1000, 9999),
            ]);
            Auth::login($user);
            return $this->emit('landingRegisterSuccess', ['status' => 100, 'msg' => 'ثبت نام شما با موفقیت انجام گردید']);
        } catch (\Exception $e) {
            return $this->emit('landingRegisterSuccess', ['status' => 500, 'msg' => 'عملیات ثبت نام با مشکل مواجه شد']);
        }
    }

    public function changeForm($form)
    {
        switch ($form) {
            case 'login';
                $this->viewMode = 'login';
                break;
            case 'register';
                $this->viewMode = 'register';
                break;
            default;
                $this->viewMode = 'login';
                break;
        }
        $this->reset(['password','email','passwordStrength']);
    }

    public function updatedPassword($password)
    {
        $zxcvbn = app(Zxcvbn::class)->passwordStrength($password);
        $this->passwordStrength = $zxcvbn['score'];
    }

    public function setRecaptcha($val)
    {
        $this->recaptcha=$val;
    }

}
