<?php

namespace App\Http\Livewire\User\Profile;

use App\Models\User;
use Livewire\Component;

class Form extends Component
{
    public $user;
    public $first_name;
    public $last_name;
    public $email;
    public $mobile;

    public function render()
    {
        $this->first_name = $this->user->first_name;
        $this->last_name = $this->user->last_name;
        $this->email = $this->user->email;
        $this->mobile = $this->user->mobile;
        return view('livewire.user.profile.form');
    }

    public function submit()
    {
        $this->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email,' . $this->user->id . ' ,id',
            'mobile' => 'required',
        ]);
        $user = User::where('id', $this->user->id)->update([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'mobile' => $this->mobile,
        ]);
        if ($user)
            return $this->emit('swal', ['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'time' => 3000]);
        return $this->emit('swal', ['status' => 500, 'msg' => 'خطا در انجام عملیات']);
    }
}
