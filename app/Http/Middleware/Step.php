<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Step
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \auth()->user();
        $array = [
            0 => 'step.verify-mobile',
            1 => 'step.verify-email',
            2 => 'step.bank',
            3 => 'step.information',
            4 => 'step.finish',
        ];
        $routes = [
            'step.verify-token.email'
        ];
        $prefix = str_replace('/', '', $request->route()->getPrefix());
        if (Auth::check() && $user->is_complete_steps == 0 && $request->isMethod('get')) {
            if ($user->step < 2 && $request->route()->getName() != $array[$user->step]) {
                if (in_array($request->route()->getName(), $routes)) {
                    return $next($request);
                }
                return redirect()->route($array[$user->step]);
            } elseif ($user->step >= 2 && $prefix == 'step') {
                if (in_array($request->route()->getName(), $array) && $request->route()->getName() == $array[$user->step]) {
                    return $next($request);
                }
                return redirect()->route($array[$user->step]);
            }
        }
        return $next($request);
    }
}
