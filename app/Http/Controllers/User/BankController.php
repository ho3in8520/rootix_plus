<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\BaseData;
use App\Rules\ShebaFormat;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = Bank::query()->where('user_id', auth()->user()->id)->get();
        return view('user.bank.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.bank.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', Rule::in(banks_name())],
            'card_number' => 'required|numeric|digits:16',
            'account_number' => 'required|numeric',
            'sheba_number' => ['required', new ShebaFormat()],
        ]);
        $request['user_id'] = auth()->user()->id;
        Bank::create($request->only(['name', 'card_number', 'account_number', 'sheba_number', 'user_id']));
        return response()->json(['status' => '100', 'msg' => 'حساب بانکی با موفقیت اضافه شد']);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Bank $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Bank $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        return view('user.bank.create', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Bank $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        $request->validate([
            'name' => ['required', Rule::in(banks_name())],
            'card_number' => 'required|numeric|digits:16',
            'account_number' => 'required|numeric',
            'sheba_number' => ['required', new ShebaFormat()],
        ]);
        $request['status']= 2;
        $request['reject_reason']= null;
        $bank->update($request->only(['name', 'card_number', 'account_number', 'sheba_number','status','reject_reason']));
        return response()->json(['status' => '100', 'msg' => 'حساب بانکی با موفقیت ویرایش شد']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Bank $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $bank->delete();
        return redirect()->back()->with('flash', ['type' => 'success', 'msg' => 'با موفقیت حذف شد!']);
    }
}
