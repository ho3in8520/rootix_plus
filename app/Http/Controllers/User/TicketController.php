<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Notification;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = TicketMaster::where('user_id',auth()->user()->id)->orderBy('created_at','desc')->paginate(10);

        return view('user.ticket.index',compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('user.ticket.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('file')) {
            $this->validate($request, [
                'title' => 'required|string',
                'role_id' => 'required',
                'priority' => 'required',
                'description' => 'required|string',
                'file'=>'required|mimes:jpeg,png,jpg,zip,pdf,rar|max:10240',
            ]);
        }else{
            $this->validate($request,[
                'title' =>'required|string',
                'role_id' =>'required',
                'priority' =>'required',
                'description' =>'required|string',
            ]);
        }

        try{
            $ticket_master = new TicketMaster();
            $ticket_master->user_id = auth()->user()->id;
            $ticket_master->title = $request->input('title');
            $ticket_master->priority = $request->input('priority');
            $ticket_master->role_id = $request->input('role_id');
            $ticket_master->status = 1;
            $ticket_master->save();

            $ticket_detail = new TicketDetail();
            $ticket_detail->user_id = auth()->user()->id;
            $ticket_detail->type = 1;
            $ticket_detail->master_id = $ticket_master->id;
            $ticket_detail->description = $request->input('description');
            $ticket_detail->save();
            if($request->has('file')){
                $ticket_detail->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('file'), 'ticket'),
                ]);
            }
            switch ($ticket_master->priority){
                case 1:
                    $color='info';
                    break;
                case 2:
                    $color='warning';
                    break;
                case 3:
                    $color='danger';
                    break;
            }
            $admin=Admin::query()->first();
            Notification::query()->create([
                    'user_id' => $admin->userGuard->id,
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'icon' => 'fas fa-user-alt',
                    'color' => $color,
                    'started_at' => $ticket_master->created_at,
                    'view' => 0,
                ]);
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید','url'=>route('ticket.show',$ticket_master->id)]);
        }catch (\Exception $e){
            dd($e);
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است']);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = TicketMaster::where('id',$id)->firstOrFail();
        if ($ticket->status != 4){
            $roles = Role::all();
            $role = Role::where('id',$ticket->role_id)->first();
            return view('user.ticket.show',compact('ticket','roles','role'));
        }
        return redirect()->back()->with('flash',['type' =>'danger','msg'=>'این تیکت بسته شده است']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.ticket.show');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function comment(Request $request, $id)
    {
        if($request->has('file')){
            $this->validate($request, [
                'description' => 'required|string',
                'file'=>'required|mimes:jpeg,png,jpg,zip,pdf,rar|max:10240',
            ]);
        }else {
            $this->validate($request, [
                'description' => 'required|string',
            ]);
        }
        TicketMaster::where('id',$id)->update(['status'=>1]);
        $ticket_detail = new TicketDetail();
        $ticket_detail->user_id = auth()->user()->id;
        $ticket_detail->type = 1;
        $ticket_detail->master_id = $id;
        $ticket_detail->description = $request->input('description');
        $ticket_detail->save();
        if($request->has('file')){
            $ticket_detail->files()->create([
                'user_id' => auth()->user()->id,
                'path' => upload($request->file('file'), 'ticket'),
            ]);
        }
        return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
    }
}
