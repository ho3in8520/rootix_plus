<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $transactions= Finance_transaction::query()->where('user_id',auth()->user()->id);

        in_array($request->type,['1','2'])?$transactions->where('type', $request->type):'';

        $request->unit ?
            $transactions->whereHasMorph('financeable', [Asset::class], function ($query) use ($request) {
                $query->where('unit', $request->unit);
            }) : '';

        $request->date_from? $transactions->where('created_at', '>=', $request->date_from) : '';
        $request->date_to? $transactions->where('created_at', '<', $request->date_to) : '';
        $request->amount_from && $request->has('amount_from') && !empty($request->amount_from)? $transactions->where('amount', '>=', $request->amount_from) : '';
        $request->amount_to && $request->has('amount_to') && !empty($request->amount_to)? $transactions->where('amount', '<', $request->amount_to) : '';
        $request->destination_address && $request->has('destination_address') && !empty($request->destination_address)? $transactions->where('extra_field1', '=', $request->destination_address) : '';
        $transactions->orderBy('created_at','desc');
        $transactions = $transactions->paginate(10);


        return showData(view('user.transactions.index', compact('transactions')));
    }
}
