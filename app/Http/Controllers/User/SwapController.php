<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\GlobalMarketApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class SwapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'unit1'=>'required|string',
            'unit2'=>'required|string',
            'input1'=>'required|between:0,99,999',
        ]);

        $min_swap_withdraw = BaseData::where('type','min_swap_'.$request->unit1)->where('extra_field1','1')->first();
        $usdt_amount = GlobalMarketApi::where('type', 'price_currency')->first();
        $usdt_amount= json_decode($usdt_amount->value,true);
        $rls= intval($usdt_amount['stats']['usdt-rls']['bestSell']);
        $usdt_amount= array_merge($usdt_amount['global']['binance'],['rial'=>1/$rls]);

        $asset_unit1 = Asset::where('unit',$request->unit1)->where('user_id',\auth()->user()->id)->first();
        $asset_unit2 = Asset::where('unit',$request->unit2)->where('user_id',\auth()->user()->id)->first();
        $input = $request->input1;

        $fee_swaps = BaseData::where('type','manage_swap_'.$request->unit1)->where(DB::raw('cast(extra_field1 AS UNSIGNED) '),'<=',$input)->where(DB::raw('cast(extra_field2 AS UNSIGNED) '),'>=',$input)->first();

        $max_min_fee = BaseData::select(['extra_field2','extra_field1','id',
            'max'=>BaseData::select([DB::raw('max(cast(extra_field2 AS UNSIGNED))')])->where('type','manage_swap_'.$request->unit1)->limit(1),
            'min'=>BaseData::select([DB::raw('min(cast(extra_field1 AS UNSIGNED))')])->where('type','manage_swap_'.$request->unit1)->limit(1),
            'min_fee'=>BaseData::select([DB::raw('min(cast(extra_field3 AS DECIMAL(10,10)))')])->where('type','manage_swap_'.$request->unit1)->limit(1),
            'max_fee'=>BaseData::select([DB::raw('max(cast(extra_field3 AS DECIMAL(10,10)))')])->where('type','manage_swap_'.$request->unit1)->limit(1)
        ])->groupBy('id')->first();
        if($fee_swaps)
        {
            $fee_swap = $fee_swaps->extra_field3;
        }elseif ($input < $max_min_fee->min ){
            return response()->json(['status'=>500,'msg'=> "حداقل سواپ $min_swap_withdraw->extra_field2 $request->unit1 میباشد"]);
        }elseif ($input > $max_min_fee->max){
            $fee_swap = $max_min_fee->max_fee;
        }


        if($input < $max_min_fee->min){
            return response()->json(['status'=>500,'msg'=> "حداقل سواپ $min_swap_withdraw->extra_field2 $request->unit1 میباشد"]);

        }else{

            if($input <= $asset_unit1->amount){
                if (isset($fee_swap)){
                    $input1 = $input - $fee_swap;
                    $priceCurrency1= $usdt_amount[$request->unit1];
                    $priceCurrency2= $usdt_amount[$request->unit2];
                    $input1 = $input1 * $priceCurrency1;
                    $input2 = $input1 / $priceCurrency2;
                    /*if($request->unit1 == 'trx' ){
                        $priceCurrency1 = $usdt_amount->TrxToUsdt;
                    }else if($request->unit1 == 'usdt' ){
                        $priceCurrency1 = $usdt_amount->UsdtToUsdt;
                    }else if($request->unit1 == 'btt' ){
                        $priceCurrency1 = $usdt_amount->BttToUsdt;
                    }else if($request->unit1 == 'rial'){
                        $priceCurrency1 =  $usdt_amount->usdtToRil;
                    }


                    if($request->unit2 == 'trx' ){
                        $priceCurrency2 = $usdt_amount->TrxToUsdt;
                    }else if($request->unit2 == 'usdt' ){
                        $priceCurrency2 = $usdt_amount->UsdtToUsdt;
                    }else if($request->unit2 == 'btt' ){
                        $priceCurrency2 = $usdt_amount->BttToUsdt;
                    }else if($request->unit2 == 'rial'){
                        $priceCurrency2 =  $usdt_amount->usdtToRil;
                    }


                    if($request->unit1 == 'rial'){
                        $input1 = $input1 / $priceCurrency1;
                    }else{
                        $input1 = $input1 * $priceCurrency1;
                    }

                    if($request->unit2 == 'rial'){
                        $input2 = $input1 * $priceCurrency2;
                    }else{
                        $input2 = $input1 / $priceCurrency2;
                    }*/
                    $type_swap = BaseData::where('type','transactions')->where('name','swap')->value('id');
                    $type_withdraw = BaseData::where('type','transactions')->where('name','withdraw')->value('id');
                    $asset_unit1->amount =  $asset_unit1->amount - $input;
                    $asset_unit1->save();

                    $asset_unit1->transaction()->create([
                        'user_id'=>\auth()->user()->id,
                        'transact_type'=> $type_withdraw,
                        'amount'=>floatval($input),
                        'type'=> 1,
                        'description'=> "$input $request->unit1 بابت سواپ به $request->unit2  از حساب شما کم شد .مبلغ "
                    ]);

                    $asset_unit2->amount = $asset_unit2->amount + $input2;
                    $asset_unit2->save();
                    $input2 = toFixed($input2);
                    $asset_unit1->transaction()->create([
                        'user_id'=>\auth()->user()->id,
                        'transact_type'=> $type_swap,
                        'amount'=>floatval($input2),
                        'type'=> 2,
                        'description'=> "$input2 $request->unit2 از سواپ  $request->unit1  به حساب شما اضافه شد .مبلغ "
                    ]);

                    return response()->json(['status'=>100,'msg'=> 'با موفقیت انجام شد.']);
                }else{
                    return response()->json(['status'=>500,'msg'=>'مقدار وارد شده درست نمیباشد.']);
                }
            }else{
                return response()->json(['status'=>500,'msg'=> "موجودی شما کم میباشد."]);
            }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $unit
     * @return \Illuminate\Http\Response
     */
    public function show($unit)
    {
        $usdt_asset = Asset::where('user_id',auth()->user()->id)->where('unit','usdt')->first();
        $rial_asset = Asset::where('user_id',auth()->user()->id)->where('unit','rial')->first();
        $token = json_decode($usdt_asset->token,true);
        $usdt_amount = GlobalMarketApi::where('type','price_currency')->first();

        $min_swap_withdraw = BaseData::where('type','min_swap_usdt')->where('extra_field1','1')->first();
        $fee_swap_usdt = BaseData::where('type','fee_swap_usdt')->where('extra_field1','1')->first();
        return view('swap.index',compact('unit','token','usdt_asset','rial_asset','usdt_amount','min_swap_withdraw','fee_swap_usdt'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_unit_price(Request $request)
    {
        if(!empty($request->unit1))
        {
            $unit1 = Asset::where('unit',$request->unit1)->where('user_id',\auth()->user()->id)->value('amount');
        }
        if(!empty($request->unit2))
        {
            $unit2 = Asset::where('unit',$request->unit2)->where('user_id',\auth()->user()->id)->value('amount');
        }

        return response()->json(['unit1' => isset($unit1) ? $unit1 : 0,'unit2' => isset($unit2) ? $unit2 : 0]);
    }

    public function get_unit_swap_fee_price(Request $request)
    {

        if(!empty($request->unit1))
        {
            $unit1 = BaseData::where('type','manage_swap_'.$request->unit1)->orderBy('id','desc')->get()->toArray();

        }
        return response()->json( [isset($unit1) ? $unit1 : 0]);
    }


}
