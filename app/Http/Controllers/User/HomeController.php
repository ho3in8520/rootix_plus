<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Finance_transaction;
use App\Models\GlobalMarketApi;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dashboard()
    {
        $price_currency= GlobalMarketApi::query()->Price_currency()->first();
        $price_currency= $price_currency->currency_to_rial(['btc','eth','usdt','doge','xrp','ltc'],['bestSell','dayHigh','dayLow','dayChange']);
        $user = auth()->user();
        $assets= Asset::query()->where('user_id',auth()->user()->id)->get();
        $usdt_asset = Asset::where('user_id',auth()->user()->id)->where('unit','usdt')->first();
        $finance_transactions = Finance_transaction::where('user_id',$user->id)->whereIn('transact_type',[7,2])->orderBy('id','desc')->latest()->take(10)->get();
        return view('user.dashboard', compact('user','usdt_asset','finance_transactions','price_currency','assets'));
    }

    public function get_theter_price(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://gateway.accessban.com/public/web-service/list/crypto?format=json&limit=30&title=theter",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "Accept: application/json",
                'Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZ2F0ZXdheS5hY2Nlc3NiYW4uY29tXC9wdWJsaWMiLCJzdWIiOiJmYjk3YWUxOC02ZjU1LTUyY2ItODg4NS01ODVlNjQ1ZjA3ZTIiLCJpYXQiOjE2MjIyNzMzMDQsImV4cCI6MTc4MDAzOTcwNCwibmFtZSI6InNhamphZCBzdWx0YW5pLTEyMjc5In0.lQSgxebT92IroGbrn_r2HpVrniEDrwytTpkuTxNitWA'
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }

    }


}
