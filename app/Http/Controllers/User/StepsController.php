<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\StepMail;
use App\Models\Admin;
use App\Models\Bank;
use App\Models\City;
use App\Models\Country;
use App\Models\Notification;
use App\Models\User;
use App\Rules\JalaliDataFormat;
use App\Rules\NationalCodeFormat;
use App\Rules\ShebaFormat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use function Symfony\Component\Translation\t;

class StepsController extends Controller
{
    public function mobile()
    {
        $result = [];
        $countries = Country::all();
        $user = auth()->user();
        if ($user->step == 0 && !empty($user->verification) && $user->time_verification > Carbon::now()) {

            $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
            $result['sms']['time'] = $seconds;
            $result['sms']['user'] = $user;
        }

        return view('user.steps.mobile', compact('countries', 'result', 'user'));
    }

    public function sendVerify(Request $request)
    {
        if ($request->mode == 1) {
            $request['country'] = 'IR';
            $request->validate([
                'number' => 'required|numeric',
                'country' => 'required',
            ]);
            $user = auth()->user();
            if ($user->time_verification >= Carbon::now()) {
                $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
                return response()->json(['status' => 300, 'msg' => 'کد یک بار برای شما ارسال شده', 'time' => $seconds]);
            }
            $country = Country::where('code', $request->country)->first();
            $number = '+' . $country->phonecode . $request->number;
            $code = rand(10000, 99999);
            try {
                $user = auth()->user();
                $user->mobile = $request->number;
                $user->verification = $code;
                $user->time_verification = Carbon::now()->addMinutes(1);
                $user->save();
                smsVerify($code, $number, 'verify');

                return response()->json(['status' => 100, 'msg' => 'کد برای شماره موبایل مورد نظر ارسال گردید', 'time' => 60]);
            } catch (\Exception $e) {
                return response()->json(['status' => 500, 'msg' => 'خطا در ارسال']);
            }
        } elseif ($request->mode == 2) {
            $request->validate([
                'code' => 'required|numeric'
            ]);
            $user = auth()->user();
            if ($user->time_verification > Carbon::now()) {
                if ($user->verification == $request->code) {
                    $user->step = 1;
                    $user->step_complate = 1;
                    $user->reject_reason = '';
                    $user->status = 0;
                    $user->save();
                    return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
                } else {
                    return response()->json(['status' => 500, 'msg' => 'کد اشتباه است']);
                }
            } else {
                return response()->json(['status' => 500, 'msg' => 'کد منقضی شده است']);
            }
        }
    }

    public function email()
    {
        $user = auth()->user();
        $countries = Country::all();
        return view('user.steps.email', compact('user', 'countries', 'user'));
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users,email'
        ]);
        $token = Str::random(100);
        $link = route('step.verify-token.email', $token);
        $user = auth()->user();
        $user->verification = $token;
        $user->time_verification = Carbon::now()->addMinutes(15);
        $user->save();
        Mail::to($user->email)->send(new StepMail($link));
        return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
    }

    public function verifyEmail($token)
    {
        $user = User::where('verification', $token)->firstOrFail();
        if ($user->time_verification > Carbon::now()) {
            if ($user->verification == $token) {
                $user->step = 2;
                $user->step_complate = 2;
                $user->reject_reason = '';
                $user->verification = null;
                $user->save();
                return redirect()->route('login.form')->with('flash', ['type' => 'success', 'msg' => 'ایمیل با موفقیت تایید شد']);
            } else {
                return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'لینک اشتباه است']);
            }
        } else {
            return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'لینک منقضی شده است']);
        }
    }

    public function information()
    {
        $user = auth()->user();
        $states = City::where('parent', 0)->get();
        $city = [];
        $images = [
            'national' => $user->files()->where('type', 'national')->orderBy('id', 'desc')->first(),
            'phone_bill' => $user->files()->where('type', 'phone_bill')->orderBy('id', 'desc')->first(),
        ];

        if (!empty($user->city)) {
            $city = City::where('parent', $user->state)->get();
        }
        return view('user.steps.information', compact('states', 'user', 'city', 'images'));
    }

    public function informationStore(Request $request)
    {

        $request->validate([
            'user.*' => 'required',
            'user.birth_day' => [new JalaliDataFormat()],
            'user.national_code' => [new NationalCodeFormat()],
            'user.state' => 'exists:cities,id',
            'user.city' => 'exists:cities,id',
            'user.gender_id' => 'in:1,2',
            'user.phone' => 'numeric',
            'image.phone_bill' => ['mimes:png,jpg', 'max:1024'],
            'image.national_image' => ['mimes:png,jpg', 'max:1024'],
        ]);
        try {
            $model = User::where('id', auth()->user()->id)->update(array_merge($request->user, ['step' => 4, 'reject_reason' => '', 'status' => 0]));
            if ($request->has('image')) {
                $image = [
                    [
                        'user_id' => auth()->user()->id,
                        'path' => upload($request->file('image.national_image'), 'information'),
                        'type' => 'national'
                    ],
                    [
                        'user_id' => auth()->user()->id,
                        'path' => upload($request->file('image.phone_bill'), 'information'),
                        'type' => 'phone_bill'
                    ]
                ];
                User::find(auth()->user()->id)->files()->createMany($image);
            }
            if ($model)
                return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
            return response()->json(['status' => 500, 'msg' => 'خطا در ثبت']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا با پشتیبانی سایت در ارتباط باشید']);
        }

    }

    public function bank()
    {
        $user = auth()->user();
        $bank = Bank::where('user_id', $user->id)->with('files')->first();
        return view('user.steps.bank_information', compact('user', 'bank'));
    }

    public function bankStore(Request $request)
    {
        $request->validate([
            'name_bank' => ['required'],
            'card' => ['required', 'numeric'],
            'sheba' => [new ShebaFormat()],
//            'image' => [Rule::requiredIf(function () {
//                $user = Bank::where('user_id', auth()->user()->id)->first();
//                if (!$user)
//                    return false;
//                $image = $user->files()->orderBy('id', 'desc')->first()->path ?? null;
//                return (!$image) ? true : false;
//            }), 'mimes:png,jpg', 'max:1024'],
        ]);
        try {
            $user = auth()->user();
            $model = Bank::updateOrCreate([
                'user_id' => $user->id,
            ], [
                'user_id' => $user->id,
                'name' => $request->name_bank,
                'account_number' => $request->account_number,
                'sheba_number' => $request->sheba,
                'card_number' => $request->card
            ]);
            $user->step = 3;
            $user->reject_reason = '';
            $user->status = 0;
            $user->save();
            if ($request->has('image')) {
                $model->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image'), 'bank'),
                ]);
            }
            if ($model)
                return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
            return response()->json(['status' => 500, 'msg' => 'خطا در ثبت']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا با پشتیبانی سایت در ارتباط باشید']);
        }

    }

    public function finish()
    {
        $user = auth()->user();
        $admin=Admin::query()->first();
        Notification::query()->create([
            'user_id' => $admin->userGuard->id,
            'title' => 'احراز هویت',
            'description' => 'مراحل احراز هویت پایان یافت.',
            'icon' => 'fas fa-user-alt',
            'color' => 'info',
            'started_at' => Carbon::now(),
            'view' => 0,
        ]);
        return view('user.steps.finish', compact('user'));
    }

}
