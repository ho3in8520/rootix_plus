<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SellBuyController extends Controller
{
    public function sellCurrencyView()
    {
        return view('user.sell_buy.sell_currency');
    }
}
