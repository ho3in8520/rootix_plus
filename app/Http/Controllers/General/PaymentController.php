<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\transactionGateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;

class PaymentController extends Controller
{
    public function payment(Request $request)
    {
        $request->validate([
            'amount' => ['required', function ($attr,$val,$fail){
                $amount=(int)str_replace(',','',$val);
                if (preg_match("/^[0-9]$/", $amount))
                {
                    $fail('مقدار باید از نوع عدد باشد');
                }
                if (amountTransactionDays()->total+ $amount > transactionLimit()){
                    $fail("درخواست بیشتر از حد مجاز سقف تراکنش روزانه شما ".number_format(transactionLimit()));
                }
        }],
            'mode' => 'required|in:asset'
        ]);
        $amount = (int)str_replace(',', '', $request->amount) / 10;

        try {
            $invoice = (new Invoice)->amount($amount);
            return Payment::purchase($invoice, function ($driver, $transactionId) use ($invoice, $request,$amount) {
                $trans = new transactionGateway();
                $trans->user_id = auth()->user()->id;
                $trans->gateway = $invoice->getDriver();
                $trans->amount = $amount;
                $trans->transaction_id = $transactionId;
                $trans->mode = $request->mode;
                $trans->status = 0;
                $trans->save();
            })->pay()->render();
        } catch (\Exception $e) {
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => 'خطا در اتصال به درگاه']);
        }
    }

    public function paymentSuccess(Request $request)
    {
        $trans = transactionGateway::where('transaction_id', $request->token)->firstOrFail();
        $trans_type = BaseData::where('type', 'transactions')->where('extra_field1', 1)->first();

        $receipt = verifyGatewayIrankish([
            "tokenIdentity" => $request->token,
            "retrievalReferenceNumber" => $request->retrievalReferenceNumber,
            "systemTraceAuditNumber" => $request->systemTraceAuditNumber,
        ]);
        $amount = $request->amount;
        if ($receipt->responseCode == 00) {
            $trans->reference_id = $receipt->result->retrievalReferenceNumber;
            $trans->status = 1;
            $trans->save();
            $asset = Asset::where('user_id', $trans->user_id)->where('unit', 'rial')->first();
            $asset->amount = $asset->amount + $amount;
            $asset->save();
            $asset->transaction()->create([
                'user_id' => $trans->user_id,
                'refer_id' => $trans->id,
                'amount' => (int)$amount,
                'type' => 1,
                'transact_type' => $trans_type->id,
                'tracking_code' => $receipt->result->retrievalReferenceNumber,
                'description' => "شارژ کیف پول ریال به مبلغ $amount"
            ]);
            Auth::loginUsingId($trans->user_id);
            return redirect()->route('user.dashboard')->with('flash', ['type' => 'success', 'msg' => $receipt->description]);
        } else {
            $trans->status = 2;
            $trans->save();
            return redirect()->route('user.dashboard')->with('flash', ['type' => 'danger', 'msg' => $receipt->description]);
        }
    }

    public function paymentShow($type)
    {
        return view('user.dashboard');
    }

    public function chargeWallet()
    {
        $asset_rial = Asset::where('user_id',\auth()->user()->id)->where('unit','rial')->value('amount');
        return view('user.wallet.charge',compact('asset_rial'));
    }
}
