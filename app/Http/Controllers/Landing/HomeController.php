<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;
use App\Models\GlobalMarketApi;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function index()
    {
        $currency=GlobalMarketApi::where('type','price_currency')->first();
        return view('landing.main',compact('currency'));
    }

    public function test()
    {
        $user = auth()->user();
        return view('user.test',compact('user'));
    }

    public function about()
    {
        return view('landing.about');
    }

    public function blog(Request $request)
    {
        $posts=Post::query()
            ->where('status',1);
        $request->search && !empty($request->search)?$posts->where('title','like','%'.$request->search.'%'):'';
        $ids=$posts->pluck('category_id')->toArray();
        $categories=PostCategory::withCount(['posts'=>function($q){
            $q->where('status',1);
        }])->where('status',1)->whereIn('id',$ids)->orderBy('posts_count','desc')->limit(5)->get();
        $posts=$posts->paginate(5);
        $new_posts=Post::where('status',1)->latest()->limit(4)->get();
        return view('landing.blog',compact('posts','categories','new_posts'));
    }

    public function post_show($slug)
    {
        $post=Post::where('slug',$slug)->with('post_category')->first();
        return view('landing.desktop.blog_show',compact('post'));
    }

    public function services()
    {
        return view('landing.services');
    }

    public function faq()
    {
        return view('landing.faq');
    }

    public function chart()
    {
        return view('landing.chart');
    }
}
