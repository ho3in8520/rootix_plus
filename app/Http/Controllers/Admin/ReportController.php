<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Finance_transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $start_created_at= \request()->start_created_at?jalali_to_gregorian(request()->start_created_at." 00:00:00"):false;
        $end_created_at= \request()->end_created_at?jalali_to_gregorian(request()->end_created_at." 23:59:59"):false;
        $assets = Asset::query()
            ->distinct()
            ->get(['unit', 'name']);
        $reports = Finance_transaction::query();
        \request()->type && in_array(\request()->type, [1, 2]) ? $reports->where('type', \request()->type) : '';
        \request()->amount ? $reports->where('amount', \request()->amount) : '';
        $start_created_at ? $reports->where('created_at', '>=',$start_created_at) : '';
        $end_created_at ? $reports->where('created_at', '<=', $end_created_at) : '';
        \request()->description ? $reports->where('description', 'like', '%' . \request()->description . '%') : '';
        if (\request()->currency) {
            $reports->whereHasMorph(
                'financeable',
                [Asset::class],
                function (Builder $query, $type) {
                    $query->where('unit', \request()->currency);
                }
            );
        }
        if (\request()->email) {
            $reports->whereHas('user',function ($q) use ($request) {
                return $q->where('email','like',"%{$request->email}%");
            });
        }
        if (\request()->code) {
            $reports->whereHas('user',function ($q) use ($request) {
                return $q->where('code','like',"%{$request->code}%");
            });
        }
        $reports->orderBy('created_at', 'desc');
        $reports = $reports->paginate(20);

        return showData(view('admin.reports.index', compact('reports', 'assets')));
    }
}
