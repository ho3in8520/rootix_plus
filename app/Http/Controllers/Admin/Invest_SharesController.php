<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\BaseData;
use App\Models\Invest_Shares;
use Illuminate\Http\Request;

class Invest_SharesController extends Controller
{
    /**
     * ساختارش در جدول base data
     * -----------------
     * type =     invest | shares
     * name =     usdt-ctr | usdt-btt
     * ext1 =     حداقل مقدار
     * ext2 =     حداکثر مقدار
     * ext3 =     درصد سود
     * -----------------
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!\request()->has('type') || !in_array(\request()->type, ['shares']))
            return redirect(route('admin.invest_shares.index', ['type' => 'shares']));

        $invest_shares = BaseData::query();
        \request()->type == 'invest' ? $invest_shares->invest() : $invest_shares->shares();
        $invest_shares = $invest_shares->orderBy('extra_field1', 'asc')->get();
        $invest_shares = count($invest_shares) > 0 ? $invest_shares : [null];

        $profit_inviter = BaseData::query()
            ->where('type', 'profit_inviter')
            ->where('name', \request()->type)
            ->first();

        $cancel_invest= '';
        if (\request()->type=='invest') {
            $cancel_invest = BaseData::query()
                ->whereIn('type', ['date_cancel_invest', 'percent_cancel_invest'])
                ->get();
            $cancel_invest['date']= $cancel_invest->where('type','date_cancel_invest')->first()->extra_field1;
            $cancel_invest['percent']= $cancel_invest->where('type','percent_cancel_invest')->first()->extra_field1;
        }
        return view('admin.invest_shares.create', compact('invest_shares', 'profit_inviter','cancel_invest'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!\request()->has('type') || !in_array(\request()->type, ['shares']))
            return response()->json(['status' => 500, 'msg' => 'لطفا یکبار صفحه را رفرش کنید']);

        if ($request->ajax()) {
            $request->validate([
                'extra_field1.*' => 'required|numeric',
                'extra_field2.*' => 'required|numeric',
                'extra_field3.*' => 'required|numeric',
            ], [
                'extra_field1.*' => 'حداقل مقدار',
                'extra_field2.*' => 'حداکثر مقدار',
                'extra_field3.*' => 'درصد سود',
            ]);
            try {

                // بررسی عدم تداخل رنج های اعداد
                $conflict_range = $this->range_conflict($request);
                if (count($conflict_range) > 0) {
                    $msg = 'ابتدا مشکل تداخل بازه های زیر را  حل کنید ' . "\n";
                    $msg .= implode(', ', $conflict_range);
                    return response()->json(['status' => 500, 'msg' => $msg]);
                }
                // بررسی عدم تداخل رنج های اعداد


                // بررسی اینکه سهام کاربر خارج از بازه ها نباشه
                $out_of_range = $this->check_out_of_range($request);
                if (count($out_of_range) > 0) {
                    $msg = 'بازه ها از خرید های کاربران با مقادیر زیر پشتیبانی نمیکنند :' . "\n";
                    $msg .= implode(', ', $out_of_range);
                    return response()->json(['status' => 500, 'msg' => $msg]);
                }
                // بررسی اینکه سهام کاربر خارج از بازه ها نباشه

                // اضافه کردن یا ویرایش بازه ها
                $upsert = [];
                for ($i = 0; $i < count($request->extra_field1); $i++) {
                    $item = [
                        'type' => $request->type,
                        'name' => $request->type == 'invest' ? 'usdt-ctr' : 'usdt-btt',
                        'extra_field1' => $request->extra_field1[$i],
                        'extra_field2' => $request->extra_field2[$i],
                        'extra_field3' => $request->extra_field3[$i]
                    ];

                    if (count(array_filter($request->get('id'))) > 0)
                        $item['id'] = isset($request->id[$i]) ? $request->id[$i] : null;
                    $upsert[] = $item;
                }
                BaseData::query()
                    ->upsert($upsert, ['id'], ['extra_field1', 'extra_field2', 'extra_field3']);
                // اضافه کردن یا ویرایش بازه ها

                return response()->json(['status' => 100, 'msg' => 'درصد سود مورد نظر با موفقیت ثبت شد']);


            } catch (\Exception $exception) {
                dd($exception);
                return response()->json(['status' => 500, 'msg' => 'مشکلی بوجود آمده است لطفا با پشتیبانی سایت در ارتباط باشید']);
            }
        }
        return redirect()->back();
    }

    /**
     * تنظیم کردن درصد و نوع سود برای بالاسری
     * @param Request $request
     */
    public function inviter_profit_store(Request $request)
    {
        if (!\request()->has('type') || !in_array(\request()->type, ['shares']))
            return response()->json(['status' => 500, 'msg' => 'لطفا یکبار صفحه را رفرش کنید']);

        $request->validate([
            'unit' => 'required|in:ctr,btt,usdt',
            'percent' => 'required|numeric',
        ]);
        try {
            BaseData::query()
                ->where('type', 'profit_inviter')
                ->where('name', $request->type)
                ->update([
                    'extra_field1' => $request->unit,
                    'extra_field2' => $request->percent,
                ]);
            return response()->json(['status' => 100, 'msg' => 'تنظیمات با موفقیت ذخیره شد']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => "مشکلی بوجود آمده است!"]);
        }
    }

    /*
     * تنظیمات مربوط به کنسل کردن INVEST
     * تاریخ کنسل کردن
     * خسارت در صورت کنسل کردن در تاریخ غیرمجاز
     */
    public function cancel_invest(Request $request)
    {
        $request->validate([
            'percent' => 'required|numeric|min:0',
            'date' => 'date',
        ]);
        try {
            $base_data = BaseData::query()
                ->whereIn('type', ['date_cancel_invest', 'percent_cancel_invest'])
                ->get();
            $date = $base_data->where("type", "date_cancel_invest")->first();
            $percent = $base_data->where("type", "percent_cancel_invest")->first();

            $percent->extra_field1 = $request->percent;
            $date->extra_field1 = $request->date;

            $date->save();
            $percent->save();
            return response()->json(['status' => 100, 'msg' => 'تغییرات با موفقیت اعمال شدند']);
        }catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است با مدیریت تماس بگیرید']);
        }
    }

    // حذف یک مورد از بازه های سود های تعریف شده
    public function destroy(Request $request, $id)
    {
        if (!\request()->has('type') || !in_array(\request()->type, ['invest', 'shares']))
            return response()->json(['status' => 500, 'msg' => 'لطفا یکبار صفحه را رفرش کنید']);
        try {
            $base_data = BaseData::query();
            \request()->type == 'invest' ? $base_data->invest() : $base_data->shares();
            $base_data = $base_data->invest()
                ->where('id', $id)
                ->first();
            if ($base_data) {
                $min = $base_data->extra_field1;
                $max = $base_data->extra_field2;
                $user_exists = Invest_Shares::query()
                    ->whereBetween('initial_value', [$min, $max])
                    ->count();
                if ($user_exists == 0) {
                    $base_data->delete();
                    return response()->json(['status' => 100, 'msg' => 'با موفقیت حذف شد']);
                } else
                    return response()->json(['status' => 500, 'msg' => "تعداد $user_exists نفر در این بازه پلن خریداری کرده اند"]);
            }
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => "مشکلی بوجود آمده است!"]);
        }
    }

    /**
     * بررسی عدم تداخل رنج های اعداد
     * -1 به معنی مثبت بینهایت هست
     */
    private function range_conflict($request)
    {
        $max_numbers = max(array_merge($request->extra_field1, $request->extra_field2));
        $numbers = [];
        $conflict_range = [];
        for ($i = 0; $i < count($request->extra_field1); $i++) {
            $min = $request->extra_field1[$i];
            $max2 = $max = $request->extra_field2[$i];
            $max = $max == -1 ? $max_numbers + 1 : $max;
            $numbers = array_merge($numbers, range($min, $max - 1));
            if ($min > $max) {
                $conflict_range[] = "[$min, $max2)";
            } else if (count(array_unique($numbers)) != count($numbers)) {
                $conflict_range[] = "[$min, $max2)";
            }
        }
        return $conflict_range;
    }

    // بررسی اینکه سهام کاربر خارج از بازه ها نباشه
    private function check_out_of_range(Request $request)
    {
        $initial_values = Invest_Shares::query()->where('type', $request->type)->get(['initial_value'])->pluck('initial_value')->unique()->toArray();
        $initial_values = array_map('floor', $initial_values);
        if (count($initial_values) == 0)
            return $initial_values;
        $max_number = max($initial_values);
        for ($i = 0; $i < count($request->extra_field1); $i++) {
            $min = $request->extra_field1[$i];
            $max = $request->extra_field2[$i];
            $max = $max == -1 ? $max_number + 1 : $max;
            $range = range($min, $max - 1);
            $intersect = array_intersect($initial_values, $range);
            $initial_values = array_diff($initial_values, $intersect);
            if (count($initial_values) == 0)
                break;
        }
        return $initial_values;
    }
}
