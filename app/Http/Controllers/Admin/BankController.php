<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;

class BankController extends Controller
{
    public function index(Request $request)
    {
        $banks= Bank::query();
        \request()->name? $banks->where('name','like',"%{$request->name}%"):'';
        \request()->card_number? $banks->where('card_number','like',"%{$request->card_number}%"):'';
        \request()->account_number? $banks->where('account_number','like',"%{$request->account_number}%"):'';
        \request()->sheba_number? $banks->where('sheba_number','like',"%{$request->sheba_number}%"):'';
        in_array(\request()->status,['0',1,2,3])? $banks->where('status',$request->status):'';
        $banks= $banks->paginate(20);
        return showData(view('admin.banks.index',compact('banks')));
    }

    public function reject(Request $request,$bank_id)
    {
        $request->validate([
            'reject_reason'=>'required'
        ]);
        try {
            Bank::query()
                ->where('id',$bank_id)
                ->update(['status' => 3, 'reject_reason'=>$request->reject_reason]);
            return response()->json(['status'=>100,'msg'=>'حساب مورد نظر رد صلاحیت شد']);
        }catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'مشکلی پیش آمده است!']);
        }
    }

    public function confirm($bank_id)
    {
        try {
            Bank::query()
                ->where('id',$bank_id)
                ->update(['status' => 1, 'reject_reason'=> '']);
            return response()->json(['status'=>100,'msg'=>'حساب مورد نظر تایید شد']);
        }catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'مشکلی پیش آمده است!']);
        }
    }
}
