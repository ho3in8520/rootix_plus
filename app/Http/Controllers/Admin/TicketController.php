<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
use App\Models\TicketRoute;
use App\Models\UploadedFile;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request)){
            $tickets = TicketMaster::query();

            if (isset($request) && !empty($request->ticket_id)) {
                $tickets->where('id',  $request->ticket_id);
            }
            if (isset($request) && !empty($request->email))
            {
                $user = User::where('email', 'like', $request->email)->get();
                $ticketdetial = TicketDetail::whereIn('user_id',$user->pluck('id'))->distinct()->get('master_id');
                $tickets->whereIn('id',$ticketdetial->pluck('master_id'));

            }
            if (isset($request) && !empty($request->code))
            {
                $user = User::where('code', 'like', $request->code)->get();
                $ticketdetial = TicketDetail::whereIn('user_id',$user->pluck('id'))->distinct()->get('master_id');
                $tickets->whereIn('id',$ticketdetial->pluck('master_id'));
            }
            if (isset($request) && !empty($request->title)) {
                $tickets->where('title', 'like', "%{$request->title}%");
            }
            if (isset($request) && !empty($request->status)){
                $tickets->where('status', intval($request->status));
            }
            if (isset($request) && !empty($request->priority)){
                $tickets->where('priority', intval($request->priority));
            }
            if (isset($request) && !empty($request->file) )
            {
                $ticketdetials = TicketDetail::get();
                $upload = UploadedFile::whereIn('uploadable_id',$ticketdetials->pluck('id'))->where('uploadable_type','App\Models\TicketDetail')->first();
                $ticketdetial = TicketDetail::whereIn('id',$upload->pluck('uploadable_id'))->distinct()->get('master_id');
                $tickets = $tickets->whereIn('id',$ticketdetial->pluck('master_id'));
            }
            if (isset($request) && !empty($request->date_from) && !empty($request->date_to)){
                $convert_from = convertEn($request->date_from);
                $convert_from= \Morilog\Jalali\Jalalian::fromFormat('Y-m-d', $convert_from)->toCarbon()->format('Y-m-d');
                $convert_to = convertEn($request->date_to);
                $convert_to= \Morilog\Jalali\Jalalian::fromFormat('Y-m-d', $convert_to)->toCarbon()->format('Y-m-d');
                $tickets->where('created_at','>=', $convert_from)->where('created_at','<=',$convert_to);
            }

            $tickets = $tickets->orderBy('created_at','desc')->paginate(10);

            return showData(view('admin.ticket.index', compact('tickets')));
        }else{
            $tickets = TicketMaster::orderBy('id','desc')->where('accept_id',auth()->guard('admin')->user()->id)->orWhere('accept_id',null)->paginate(10);

        }
        return view('admin.ticket.index',compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = TicketMaster::where('id',$id)->firstOrFail();
        if (empty($ticket->accept_id)){
            $ticket->accept_id = auth()->guard('admin')->user()->id;
            $ticket->save();
        }
        $roles = Role::all();
        $role = Role::where('id',$ticket->role_id)->first();

        return view('admin.ticket.show',compact('ticket','roles','role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticketDetail = TicketDetail::where('id',$id)->first();
        if (!empty($ticketDetail->files[0]->path)){
            $ticketDetail->files[0]->delete();
        }
        $ticketDetail->delete();

        return redirect()->back()->with('flash',['type'=>'200','msg'=>'با موفقیت انجام شد']);
    }

    public function comment(Request $request, $id)
    {
        if($request->has('file')){
            $this->validate($request, [
                'description' => 'required|string',
                'file'=>'required|mimes:jpeg,png,jpg,zip,pdf,rar|max:10240',
            ]);
        }else {
            $this->validate($request, [
                'description' => 'required|string',
            ]);
        }
        TicketMaster::where('id',$id)->update(['status'=>2]);
        $ticket_detail = new TicketDetail();
        $ticket_detail->user_id = auth()->guard('admin')->user()->id;
        $ticket_detail->type = 2;
        $ticket_detail->master_id = $id;
        $ticket_detail->description = $request->input('description');
        $ticket_detail->save();
        if($request->has('file')){
            $ticket_detail->files()->create([
                'user_id' => auth()->guard('admin')->user()->id,
                'path' => upload($request->file('file'), 'ticket'),
            ]);
        }

        return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
    }

    public function route(Request $request,$id)
    {
        $ticket_route = new TicketRoute();
        $ticket_route->master_id = $id;
        $ticket_route->from_id = $request->input('from_id');
        $ticket_route->to_id = $request->input('to_id');
        if ($request->input('to_id') == 0){
            $ticket_route->mode = 1;
        }else{
            $ticket_route->mode = 2;
        }
        $ticket_route->save();

        $ticket_mster = TicketMaster::where('id',$id)->first();
        $ticket_mster->role_id = $request->input('from_id');
        if ($request->input('to_id') == 0){
            $ticket_mster->accept_id = null;
        }else{
            $ticket_mster->accept_id = $request->input('to_id');
        }
        $ticket_mster->save();

        return response()->json(['status'=>'100','msg'=>'با موفقیت ارجاع داده شد.']);
    }

    public function ban($id)
    {
        $ticket_master = TicketMaster::where('id',$id)->first();
        if ($ticket_master->status != 4){
            $ticket_master->status = 4;
            $ticket_master->save();
            return redirect()->back()->with('flash',['type' =>'success','msg'=>'تیکت با موفقیت بسته شد.']);
        }else{
            return redirect()->back()->with('flash',['type' =>'danger','msg'=>'این تیکت بسته شده است']);
        }
    }

}
