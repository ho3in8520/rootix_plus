<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TotalExport;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\User;
use App\Rules\JalaliDataFormat;
use App\Rules\NationalCodeFormat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use niklasravnsborg\LaravelPdf\Pdf;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::query();
        $request->first_name ? $users->where('first_name', 'like', '%' . $request->first_name . '%') : '';
        $request->last_name ? $users->where('last_name', 'like', '%' . $request->last_name . '%') : '';
        $request->mobile ? $users->where('mobile', 'like', '%' . $request->mobile . '%') : '';
        $request->email ? $users->where('email', 'like', '%' . $request->email . '%') : '';
        $request->national_code ? $users->where('national_code', 'like', '%' . $request->national_code . '%') : '';
        $request->birth_day ? $users->where('birth_day', $request->birth_day) : '';
        $request->gender ? $users->where('gender_id', $request->gender) : '';
        $request->status ? $users->where('status', $request->status) : '';

        $users = $users->paginate(10);
        if ($request->has('export') && !empty($request->export)) {
            if ($request->export == 'excel')
                return Excel::download(new TotalExport($this->export($users)),'users.xlsx');
            else{
                $data=$this->export($users);
                view()->share('data',$data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('users.pdf');
            }
        }
        return view('admin.manage.users.index', compact('users'));
    }

    public function authenticate($user)
    {
        $images = [];
        $roles = Role::all();
        $user = User::with(array('bank' => function ($q) {
            $q->orderBy('id', 'desc')->with('files');
        }))->findOrFail($user);
        $images['national'] = $user->files()->where('type', 'national')->first();
        $images['phone_bill'] = $user->files()->where('type', 'phone_bill')->first();
        return view('admin.manage.users.authenticate', compact('user', 'images', 'roles'));
    }

    public function show(User $user)
    {
        $roles = Role::all();
        $states = City::where('parent', 0)->get();
        $cities= !empty($user->city) ? City::where('parent', $user->state)->get() : [];
        $documents= $user->files()->whereIn('type',['national','phone_bill'])->get();
        return view('admin.manage.users.show', compact('user','roles','states','cities','documents'));
    }

    public function change_status()
    {
        $id = $_GET['id'];
        $status = $_GET['status'];
        $user = User::findOrFail($id);

        $user->step_complate = $status;
        if ($status == 3) { // بانک تایید شده
            $user->bank()->first()->update(['status' => 1]);
        }
        if ($status == 4) {
            $user->is_complete_steps = 1;
        }
        if (!empty($user->reject_reason)) {
            $user->reject_reason = null;
        }
        $user->save();

        return redirect()->back()->with('flash', ['status' => '100', 'msg' => 'این مرحله با موفقیت تایید شد', 'type' => 'success']);
    }

    public function disable_status()
    {
        $id = $_GET['id'];
        $status = $_GET['status'];
        $value = $_GET['value'];
        $user = User::findOrFail($id);

        $user->step = $status;
        if ($user->step_complate >= $status) {
            $user->step_complate = $status;
        }
        $user->is_complete_steps = 0;
        $user->reject_reason = $value;
        $user->save();

        return response()->json(['status' => '100', 'msg' => 'لغو صلاحیت شما با موفقیت انجام شد', 'type' => 'success']);
    }

    // ویرایش اطلاعات کاربری
    public function edit(User $user,Request $request)
    {
        $request->validate([
            'birth_day' => [new JalaliDataFormat()],
            'national_code' => [new NationalCodeFormat()],
            'state' => ['nullable','exists:cities,id'],
            'city' => ['nullable','exists:cities,id'],
            'roles' => ['nullable','exists:roles,name'],
            'gender_id' => ['nullable','in:1,2'],
            'stats' => ['nullable','in:1,2'],
        ]);

        try {
            // مربوط به تب تنظیمات کاربر
            if ($request->has('settings-user')){
                if (is_null($request->password) || !$request->password)
                    unset($request['password']);
                else
                    $request['password']= Hash::make($request->password);

                if ($request->has('confirm_type')) {
                    $request['confirm_type'] = 'sms';
                }
            }

            // مربوط به بخش نقش های کاربر
            if ($request->has('roles-user')) {
                $user->syncRoles($request->roles);
            }

            $user->update($request->except(['id','code','google2fa_secret','email_verified_at','step','step_complate','is_complete_steps','panel_type','verification','time_verification','referral_id','reject_reason','remember_token','deleted_at','created_at','updated_at']));
            return response()->json(['status'=>100,'msg'=>'تغییرات با موفقیت اعمال شد']);
        }catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'مشکلی پیش آمده است لطفا مجدد تلاش کنید']);
        }
    }

    // لاگین کردن با کاربر داخل پنل
    public function login_panel(User $user)
    {
        Auth::logout();
        Auth::login($user);
        return redirect('/dashboard');
    }

    public function export($data)
    {
        $result=[];
        foreach ($data as $item){
            array_push($result,[
                'نام'=>$item->first_name ,
                'نام خانوادگی'=>$item->last_name,
                'ایمیل'=>$item->email,
                'کدملی'=>$item->national_code,
                'سطح کاربر'=>$item->step_complate,
            ]);
        }
        return $result;
    }
}
