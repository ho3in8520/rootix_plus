<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function manage_withdraw_swap_page()
    {

        $result = BaseData::where('type', 'manage_swap_rial')->orderBy('id', 'asc')->get();
        $units = Asset::select('unit')->groupBy('unit')->get();
        $min_mithdraw = BaseData::where('type', 'min_withdraw_rial')->value('extra_field1');
        $fee_mithdraw = BaseData::where('type', 'fee_withdraw_rial')->value('extra_field1');

        return view('admin.manage.swap-withdraw.index', compact('result', 'units', 'min_mithdraw', 'fee_mithdraw'));
    }

    public function manage_swap_update(Request $request)
    {
        $request->percent = str_replace(',', '', $request->percent);
        $request->from = str_replace(',', '', $request->from);
        $request->to = str_replace(',', '', $request->to);

        $request->validate([
            'unit' => 'required|string',
            'from.*' => 'required|between:0,999999999.99',
            'to.*' => 'required|between:0,999999999.99',
            'percent.*' => 'required|between:0,999999999.99',
        ]);
        try {
            BaseData::where('type', 'manage_swap_' . $request->unit)->delete();
            foreach ($request->percent as $key => $item)
                $model = BaseData::create([
                    'type' => 'manage_swap_' . $request->unit,
                    'name' => 'مدیریت سواپ ' . $request->unit,
                    'extra_field1' => $request->from[$key],
                    'extra_field2' => $request->to[$key],
                    'extra_field3' => $item
                ]);
            if ($model)
                return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);

        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات.']);
        }

    }

    public function change_unit(Request $request)
    {
        $result = BaseData::where('type', 'manage_swap_' . $request->unit)->get();
        return view('admin.manage.swap-withdraw.form', compact('result'));
    }

    public function change_unit_for_withdraw(Request $request)
    {
        $min_mithdraw = BaseData::where('type', 'min_withdraw_' . $request->unit)->value('extra_field1');
        $fee_mithdraw = BaseData::where('type', 'fee_withdraw_' . $request->unit)->value('extra_field1');

        return view('admin.manage.swap-withdraw.withdraw-form', compact('min_mithdraw', 'fee_mithdraw'));
    }

    public function manage_withdraw_update(Request $request)
    {
        $request->fee_withdraw = str_replace(',', '', $request->fee_withdraw);
        $request->min_withdraw = str_replace(',', '', $request->min_withdraw);

        $this->validate($request, [
            'fee_withdraw' => 'required',
            'min_withdraw' => 'required',
        ]);
        try {
            BaseData::query()->updateOrCreate(
                ['type' => 'min_withdraw_' . $request->unit, 'name' => 'حداقل برداشت ' . $request->unit],
                ['extra_field1' => "$request->min_withdraw"]
            );
            BaseData::query()->updateOrCreate(
                ['type' => "fee_withdraw_$request->unit", 'name' => 'کارمزد برداشت ' . $request->unit],
                ['extra_field1' => "$request->fee_withdraw"]
            );

            return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
        }

    }


}
