<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cats=PostCategory::all();
        $categories = PostCategory::query();
        $request->name && $request->has('name') && !empty($request->name)? $categories->where('name', 'LIKE', '%'.$request->name.'%') : '';
        $request->status && $request->has('status') && !empty($request->status)? $categories->where('status',  $request->status) : '';
        $request->parent_id && $request->has('parent_id') && !empty($request->parent_id)? $categories->where('parent_id',  $request->parent_id) : '';
        $categories->orderBy('id','DESC');
        $categories=$categories->paginate(10);
        return showData(view('blog.category.index', compact('categories','cats')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = PostCategory::all();
        return view('blog.category.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        try {
            PostCategory::create([
                'name' =>$request['name'],
                'description' =>$request['description'],
                'status' =>$request['status'],
                'parent_id' =>$request['parent_id'],
            ]);

            return response()->json(['status' => 100, 'msg' => 'دسته مورد نظر با موفقیت ایجاد شد.']);
        }catch (\Exception $exception){
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است.لطفا بعدا امتحان کنید.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=PostCategory::findOrFail($id);
        $categories=PostCategory::all()->except($category->id);
        return view('blog.category.edit',compact('categories','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'status' => 'required',
        ]);

        try {
            $category=PostCategory::findOrFail($id);
            $category->update([
                'name' =>$request['name'],
                'description' =>$request['description'],
                'status' =>$request['status'],
                'parent_id' =>$request['parent_id'],
            ]);

            return response()->json(['status' => 100, 'msg' => 'دسته مورد نظر با موفقیت ویرایش شد.']);
        }catch (\Exception $exception){
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است.لطفا بعدا امتحان کنید.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        try {
            $category=PostCategory::findOrFail($request->category);
            $posts=Post::where('category_id',$category->id)->get();
            if (count($posts)>0){
                return response()->json(['status' => 500, 'msg' => 'قابل حذف نیست.پست هایی تحت این دسته وجود دارند.']);
            }else{
                $category->delete();
                return response()->json(['status' => 100, 'msg' => 'با موفقیت حذف شد.']);
            }
        }catch (\Exception $exception){
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است.لطفا بعدا امتحان کنید.']);
        }

    }
}
