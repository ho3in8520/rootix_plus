<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LoginMail extends Mailable
{
    use Queueable, SerializesModels;

    private $token, $ip;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $ip)
    {
        $this->token = $token;
        $this->ip = $ip;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.login')->with(['token' => $this->token, 'ip' => $this->ip]);
    }
}
