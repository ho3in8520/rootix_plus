<?php

namespace App\Jobs;

use App\Http\Controllers\User\AssetsController;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InviterProfit implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $amount;
    private $refer_id;
    private $description;
    private $type;

    /**
     * Create a new job instance.
     * $amount => بر اساس usdt
     * @return void
     */
    public function __construct(User $user, $amount, $refer_id, $description, $type)
    {
        $this->user = $user;
        $this->amount = $amount;
        $this->refer_id = $refer_id;
        $this->description = $description;
        $this->type = $type;
    }

    /**
     * این تابع فقط برای خرید usdt و محاسبه سود بر اساس اون هست
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->amount <= 0)
            return;

        $inviter = User::query()->where('code', $this->user->referral_code)->first();

        if (!$inviter)
            return;

        if (in_array($this->type, ['invest', 'shares']) && $inviter->{$this->type}()->active()->count() == 0)
            return false;

        $base_data = BaseData::query()
            ->where(function ($query) {
                $query
                    ->where('type', 'transactions')
                    ->whereIn('extra_field1', [8]);
            })
            ->orWhere(function ($query) {
                $query
                    ->where('type', 'currency_price')
                    ->where('name', 'ctr');
            })
            ->orWhere(function ($query) {
                $query->where('type', 'profit_inviter')
                    ->where('name', $this->type);
            })
            ->get();

        $unit_profit_inviter = $base_data->where('type', 'profit_inviter')->first()->extra_field1;
        $percent_profit_inviter = $base_data->where('type', 'profit_inviter')->first()->extra_field2;
        $transactions8 = $base_data->where('type', 'transactions')->where('extra_field1', 8)->first();
        $ctr_price = $base_data->where('type', 'currency_price')->where('name', 'ctr')->first()->extra_field1;

        if ($percent_profit_inviter <= 0 || empty($unit_profit_inviter) )
            return;
        $price_unit = $this->get_unit_price($unit_profit_inviter, $ctr_price);
        $profit_amount = ($percent_profit_inviter / 100) * $this->amount;
        $profit_amount = $price_unit / $profit_amount;

        $inviter_asset = Asset::query()
            ->where('user_id', $inviter->id)
            ->where('unit',$unit_profit_inviter)
            ->first();
        $inviter_asset->amount = $inviter_asset->amount + $profit_amount;
        $inviter_asset->save();

        $date = date('Y-m-d H:i:s');
        Finance_transaction::query()->insert([
            [
                'financeable_id' => $inviter_asset->id,
                'financeable_type' => 'App\Models\Asset',
                'refer_id' => $this->refer_id,
                'user_id' => $inviter->id, // ایدی کاربر
                'extra_field1' => $this->user->id, // ایدی کاربر
                'type' => 2, // افزایش
                'transact_type' => $transactions8->id, // ایدی از بیس دیتا
                'amount' => $profit_amount, // مقدار اضافه شدن
                'description' => $this->description,
                'created_at' => $date,
                'updated_at' => $date
            ]
        ]);
    }

    // محاسبه قیمت ارز بر اساس usdt
    private function get_unit_price($unit, $ctr_price)
    {
        if ($unit == 'usdt') {
            return 1;
        } else if ($unit == 'ctr') {
            return $ctr_price;
        } else {
            return (new AssetsController())->price_on_usdt($unit);
        }
    }

}
