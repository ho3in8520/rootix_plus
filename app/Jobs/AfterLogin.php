<?php

namespace App\Jobs;

use App\Mail\LoginMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class AfterLogin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email, $token, $mobile, $ip;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $mobile, $token, $ip)
    {
        $this->email = $email;
        $this->token = $token;
        $this->mobile = $mobile;
        $this->ip = $ip;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->send(new LoginMail($this->token, $this->ip));
        if ($this->mobile)
            smsVerify($this->ip, '09372549157', 'notification-user-login-panel');
    }
}
