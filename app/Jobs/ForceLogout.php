<?php

namespace App\Jobs;

use App\Mail\changePassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ForceLogout implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email, $mobile, $password;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email,$mobile,$password)
    {
        $this->email= $email;
        $this->mobile= $mobile;
        $this->password= $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->mobile)
            smsVerify($this->password, $this->mobile, 'new-password');
        Mail::to($this->email)->send(new changePassword($this->password));
    }
}
