<?php

namespace App\Providers;

use App\Models\TicketMaster;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Schema::defaultStringLength(191);
        view()->composer('*', function ($view)
        {
            if (Auth::check()) {
                $number_ticket_user = TicketMaster::where('user_id',Auth::user()->id)->where('status',2)->where('accept_id','!=',null)->count();
            }
            $number_ticket_admin = TicketMaster::where('accept_id',null)->count();

            $view->with(['number_ticket_admin' => isset($number_ticket_admin) ? $number_ticket_admin : '','number_ticket_user'=> isset($number_ticket_user) ? $number_ticket_user : '']);
        });

    }
}
