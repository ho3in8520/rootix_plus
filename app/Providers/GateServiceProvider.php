<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class GateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define('user-check', function (User $user,$model, $model_name='') {
            if (is_numeric($model)) {
                $model_name= "app\Models\\$model_name";
                $model= $model_name::findOrFail($model);
            }
            return isset($model) && $model && $model->user_id ? $user->id == $model->user_id : true;
        });
    }
}
