<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'code',
        'mobile',
        'email',
        'google2fa_secret',
        'login_2fa',
        'login_token',
        'password',
        'national_code',
        'postal_code',
        'sheba',
        'birth_day',
        'language',
        'country_id',
        'gender_id',
        'state',
        'city',
        'address',
        'step',
        'step_complate',
        'is_complete_steps',
        'status',
        'panel_type',
        'confirm_type',
        'verification',
        'time_verification',
        'referral_id',
        'reject_reason',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'google2fa_secret'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Ecrypt the user's google_2fa secret.
     *
     * @param string $value
     * @return string
     */
    public function setGoogle2faSecretAttribute($value)
    {
        $this->attributes['google2fa_secret'] = encrypt($value);
    }

    /**
     * Decrypt the user's google_2fa secret.
     *
     * @param string $value
     * @return string
     */
    public function getGoogle2faSecretAttribute($value)
    {
        return (!empty($value)) ? decrypt($value) : null;
    }

    public function assets()
    {
        return $this->hasMany(Asset::class, 'user_id', 'id');
    }

    // سهام های کاربر
    public function shares()
    {
        return $this->hasMany(Invest_Shares::class)->where('type','shares');
    }

    // invest های کاربر
    public function invest()
    {
        return $this->hasMany(Invest_Shares::class)->where('type','invest');
    }

    // سهام ها و invest های کاربر
    public function invest_shares()
    {
        return $this->hasMany(Invest_Shares::class);
    }

    public function bank()
    {
        return $this->hasMany(Bank::class, 'user_id', 'id');
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function city_name()
    {
        return $this->hasOne(City::class, 'id', 'city');
    }

    public function state_name()
    {
        return $this->hasOne(City::class, 'id', 'state');
    }

    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

    public function getFullNameAttribute()
    {
        if ($this->first_name || $this->last_name)
            return $this->first_name . ' ' . $this->last_name;
        return null;
    }

    public function getAvatarAttribute()
    {
        $avatar = $this->files()->where('type', 'profile_pic')->first(['path']);
        if ($avatar)
            return getImage($avatar->path);
        return asset('general/img/no_avatar.png');
    }

    public function userGuard()
    {
        return $this->morphOne(UserGuard::class, 'guardable');
    }

    public function getUserGuardIdAttribute()
    {
        $user_id = auth("web")->user()->id;
        $user_guard = UserGuard::where("guardable_id", $user_id)->where("guardable_type",
            User::class)->first();
        return $user_guard->id;
    }
}
