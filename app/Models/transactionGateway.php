<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transactionGateway extends Model
{
    use HasFactory;

    public function transaction()
    {
        return $this->morphMany('App\Models\Finance_transaction','financeable');
    }
}
