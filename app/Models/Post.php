<?php

namespace App\Models;

use App\Models\Traits\UserTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    use UserTrait;
    use Sluggable;
    protected $guarded;

    public function user_guard()
    {
        return $this->belongsTo(UserGuard::class,'creator_id');
    }

    public function post_category()
    {
        return $this->belongsTo(PostCategory::class,'category_id');
    }

    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

    public function getBlogImageAttribute()
    {
        $image = $this->files()->where('type', 'blog_pic')->first(['path']);
        if ($image)
            return getImage($image->path);
        return asset('general/img/noimage.png');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
