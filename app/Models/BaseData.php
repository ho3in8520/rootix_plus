<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaseData extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'name',
        'extra_field1',
        'extra_field2',
        'extra_field3',
        'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }

    public function scopeInvest($query)
    {
        return $query->where('type','invest');
    }

    public function scopeShares($query)
    {
        return $query->where('type','shares');
    }

    public function scopeCtr_price($query)
    {
        return $query->where('type','currency_price')->where('name','ctr');
    }

    public function scopeTransactions($query)
    {
        return $query->where('type','transactions');
    }
}
