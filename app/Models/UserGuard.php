<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGuard extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ["guardable_id", "guardable_type"];

    public function guardable()
    {
        return $this->morphTo(__FUNCTION__, 'guardable_type', 'guardable_id');
    }

    public function getFullnameAttribute()
    {
        $user = $this->user;
        if ($user)
            return $user->first_name . ' ' . $user->last_name;
    }

    public function user()
    {
        return $this->hasOne($this->guardable_type, 'id', 'guardable_id');
    }

    public function getPanelTypeAttribute()
    {
        switch ($this->guardable_type) {
            case "Modules\Admin\Entities\Admin":
                return "ادمین";
                break;
            case "Modules\User\Entities\User":
                return "مشتری";
                break;
        }
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
