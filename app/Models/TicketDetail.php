<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketDetail extends Model
{
    use HasFactory;
    protected $table = 'ticket_detail';

    protected $fillable = ['user_id', 'type', 'master_id', 'description'];

    public function user()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }

    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

}
