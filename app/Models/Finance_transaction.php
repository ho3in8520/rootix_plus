<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Finance_transaction extends Model
{
    protected $table = 'finance_transactions';

    protected $fillable = ['financeable_id','financeable_type','tracking_code','refer_id','user_id','transact_type','amount','description','type','extra_field1'];

    public function financeable()
    {
        return $this->morphTo(__FUNCTION__, 'financeable_type', 'financeable_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function base_data()
    {
        return $this->belongsTo(BaseData::class,'transact_type');
    }
}
