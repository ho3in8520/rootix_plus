<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    protected $fillable = ['user_id','parameter','amount','unit','address_wallet','verification','status','time_verification'];

    public function transaction()
    {
        return $this->morphMany(Finance_transaction::class,'financeable');
    }

    use HasFactory;
}
