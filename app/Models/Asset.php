<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'unit', 'name', 'amount', 'logo', 'type_token', 'token'];

    public function transaction()
    {
        return $this->morphMany(Finance_transaction::class, 'financeable');
    }

    public function scopeUsdt($q)
    {
        return $q->where('unit','usdt');
    }
    public function scopeCtr($q)
    {
        return $q->where('unit','ctr');
    }
    public function scopeBtt($q)
    {
        return $q->where('unit','btt');
    }

    public function getAddressHexAttribute()
    {
        $address = $this->attributes['token'];
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_hex;
    }

    public function getAddressAttribute()
    {
        $address = $this->attributes['token'];
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_base58;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
