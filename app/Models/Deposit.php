<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $fillable = ['user_id','hash','amount','unit','mode','status'];

    public function transact()
    {
        return $this->morphMany(Finance_transaction::class,'financeable');
    }

    use HasFactory;
}
