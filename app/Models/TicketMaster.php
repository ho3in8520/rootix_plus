<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketMaster extends Model
{
    protected $table = 'ticket_master';

    protected $fillable = ['user_id', 'title', 'priority', 'role_id', 'status', 'accept_id'];

    public function ticketDetail()
    {
        return $this->hasMany(TicketDetail::class, 'master_id', 'id')->orderBy('id','desc');
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'accept_id');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }

    public function ticketRoute()
    {
        return $this->hasMany(TicketRoute::class, 'master_id', 'id');
    }

    use HasFactory;
}
