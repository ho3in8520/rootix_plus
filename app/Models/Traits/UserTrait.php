<?php

namespace App\Models\Traits;

use App\Models\Admin;
use App\Models\User;

trait UserTrait
{

    public function scopeGuardFilters($query)
    {
        if ((request()->has('user') && request('user') != "") || (request()->has('email') && request('email') != "") ||(request()->has('creator') && request('creator') != "")) {
            return $query->whereHas("user_guard", function ($query) {
                return $query->whereHasMorph('guardable', [Admin::class, User::class],
                    function ($query) {
                        if (request()->has('user') && request('user') != "") {
                            return $query->where("first_name", "like", '%' . request('user') . '%')->orWhere("last_name",
                                "like",
                                '%' . request('user') . '%');
                        }
                        if (request()->has('email') && request('email') != "") {
                            return $query->where("email", request('email'));
                        }
                        if (request()->has('creator') && request('creator') != "") {
                            return $query->where('first_name', 'like', '%' . request('creator') . '%')
                                ->orWhere('last_name', 'like', '%' . request('creator') . '%')
                                ->orWhere('email', request('creator'));
                        }
                    });
            });
        }
    }
}
