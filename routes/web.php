<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use PragmaRX\Google2FALaravel\Support\Authenticator;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;
use Tzsk\Sms\Facades\Sms;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    (new Authenticator(request()))->logout();
    return redirect()->route('login.form');
})->name('logout');
Route::get('/image/{type}/{id}/{file}',
    function ($type, $id, $file) {
        $path = storage_path('app/uploaded/' . $type . '/' . $id . '/' . $file);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    })->name('image.view');
Route::get('get-cities', function () {
    $cities = \App\Models\City::select(['id', 'name'])->where('parent', request('id'))->get();
    $html = '<option value="">انتخاب کنید...</option>';
    foreach ($cities as $row) {
        $html .= '<option value="' . $row->id . '">' . $row->name . '</option>';
    }
    return json_encode($html);
})->name('get-cities');
Route::namespace("App\Http\Controllers\Auth")->middleware('guest')->group(function () {
    Route::get('auth/login', 'LoginRegisterController@loginForm')->name('login.form');
    Route::post('auth/login/verify', 'LoginRegisterController@login')->name('login.login');
    Route::get('auth/register', 'LoginRegisterController@register')->name('register.form');
    Route::post('auth/register/store', 'LoginRegisterController@registerStore')->name('register.store');
    Route::get('forget-password', 'LoginRegisterController@forgetPasswordForm')->name('login.forget_password.form');
    Route::post('forget-password', 'LoginRegisterController@forgetPasswordSendEmail')->name('login.forget_password.send');
    Route::post('forget-change-password', 'LoginRegisterController@forgetChangePassword')->name('login.forget_password.change-password');
});
Route::get('verify-email/token/{token}', "App\Http\Controllers\User\StepsController@verifyEmail")->name('step.verify-token.email');
/// Route Landing
Route::namespace("App\Http\Controllers\Landing")->middleware('guest')->group(function () {
    Route::get('/', 'HomeController@index')->name('landing.home');
    Route::get('/join', 'HomeController@test');
    Route::get('/about','HomeController@about')->name('landing.about');
    Route::get('/blog-page','HomeController@blog')->name('landing.blog');
    Route::get('/services','HomeController@services')->name('landing.services');
    Route::get('/faq','HomeController@faq')->name('landing.faq');
    Route::get('/chart','HomeController@chart')->name('landing.chart');
    Route::get('/post/{slug}','HomeController@post_show')->name('landing.post');
    Route::get('/search','HomeController@post_search')->name('landing.search');

});

Route::get('withdraw/token/{token}', "App\Http\Controllers\User\WithdrawController@verifyEmail")->name('verify-withdraw.email');
Route::get('reject/token/{token}', "App\Http\Controllers\User\WithdrawController@rejectEmail")->name('reject-withdraw.email');



///// Route User
Route::middleware(['auth', 'steps', '2fa'])->namespace("App\Http\Controllers\User")->group(function () {
    ///// Route Step
    Route::prefix('step')->group(function () {
        Route::get('verify-mobile', "StepsController@mobile")->name('step.verify-mobile');
        Route::get('verify-email', "StepsController@email")->name('step.verify-email');
        Route::post('verify-mobile/send-verify', "StepsController@sendVerify")->name('step.send-verify');
        Route::post('verify-email/send-link', "StepsController@sendEmail")->name('step.send-email');
        Route::get('information', "StepsController@information")->name('step.information');
        Route::post('information/store', "StepsController@informationStore")->name('step.information.store');
        Route::get('bank', "StepsController@bank")->name('step.bank');
        Route::post('bank/store', "StepsController@bankStore")->name('step.bank.store');
        Route::get('finish', "StepsController@finish")->name('step.finish');
    });

    Route::post('google2fa/authenticate',function (){
        return redirect(\route('user.dashboard'));
    });
    Route::get('dashboard', "HomeController@dashboard")->name('user.dashboard');
    Route::get('profile', "ProfileController@formProfile")->name('user.profile.form');
    Route::get('swap/{unit}', 'SwapController@show')->name('swap.show');
    Route::post('swap', 'SwapController@store')->name('swap.store');
    Route::post('get-unit-price', 'SwapController@get_unit_price')->name('get-unit-price');
    Route::post('get-unit-swap-fee-price', 'SwapController@get_unit_swap_fee_price')->name('get-unit-swap-fee-price');
    Route::post('get_theter_price', 'HomeController@get_theter_price')->name('get_theter_price');
    Route::post('exchange', 'SwapController@exchange')->name('exchange');
    Route::post('createAccount/{unit}', 'ProfileController@wallet')->name('createAccount');
//    Route::post('check-withdraw-amount', 'WithdrawController@check_withdraw_amount')->name('check-withdraw-amount');
//    Route::post('withdraw-from-wallet', 'WithdrawController@withdraw_from_wallet')->name('withdraw-from-wallet');
//    Route::post('send-message-admin', 'WithdrawController@send_message_admin')->name('send-message-admin');
    Route::post('get-total-usdt', 'WithdrawController@get_total_usdt')->name('get-total-usdt');
//    Route::get('withdraw/{unit}','WithdrawController@index')->name('withdraw.index');
//    Route::post('withdraw','WithdrawController@store')->name('withdraw.store');
    Route::get('withdraw/token/{token}', "WithdrawController@verifyEmail")->name('verify-withdraw.email');
    Route::get('reject/token/{token}', "WithdrawController@rejectEmail")->name('reject-withdraw.email');
    Route::get('change-pass', 'ProfileController@change_pass_view')->name('change_pass.index');
    Route::post('change-pass', 'ProfileController@change_pass')->name('change_pass.store');
    Route::get('/charge-wallet', '\App\Http\Controllers\General\PaymentController@chargeWallet')->name('charge_wallet.view');
    Route::get('tickets', 'TicketController@index')->name('ticket.index');
    Route::get('ticket/new', 'TicketController@create')->name('ticket.create');
    Route::post('ticket', 'TicketController@store')->name('ticket.store');
    Route::get('ticket/{id}', 'TicketController@show')->name('ticket.show');
    Route::post('comment/{id}', 'TicketController@comment')->name('comment.store');
    Route::post('/setting/change_confirmation_type', 'SettingController@change_confirmation_type')->name('setting.change_confirmation_type');
    Route::post('/setting/verify_confirmation_type', 'SettingController@verify_confirmation_type')->name('setting.verify_confirmation_type');
    Route::get('setting', 'SettingController@form')->name('setting.form');
    Route::post('/setting/status-google-authenticator', 'SettingController@status_google_authenticator')->name('setting.status_google_authenticator');
    Route::post('/setting/login_2fa', 'SettingController@login_2fa')->name('setting.login_2fa');

    Route::get('/sell-currency', 'SellBuyController@sellCurrencyView')->name('sell_currency.view');
    Route::post('get-user-assets', 'AssetController@get_user_assets');
    Route::resource('my-wallet', 'AssetController');
    Route::resource('banks', 'BankController')->middleware("can:user-check,bank");
    Route::post('update-profile-picture/{user}', 'ProfileController@update_avatar')->name('profile.update.picture');
    Route::post('/complete-registration', 'ProfileController@completeRegistration')->name('complete-registration');
    Route::post('get-address-wallet', 'AssetController@getAddressWallet')->name('wallet.get-address-wallet');
    Route::post('withdrawal-fee', 'AssetController@WithdrawalFee')->name('wallet.withdrawal-fee');
    Route::post('withdraw', 'AssetController@withdraw')->name('withdraw.store')->middleware(['auth','2fa']);
    Route::post('security-withdraw', 'AssetController@security_withdraw')->name('withdraw.security');
    Route::post('security-send-sms', 'AssetController@security_send_sms')->name('withdraw.send-sms');
    Route::get('transactions','TransactionController@index')->name('transactions.index');
    Route::resource('notification','NotificationController')->names('notification');

    // Shares & Invest Route
//    Route::get('invest/cancel/{invest_shares}', 'Invest_SharesController@cancel')->name('user.invest.cancel');
//    Route::get('invest/add-to-share/{invest_shares}', 'Invest_SharesController@add_to_share')->name('user.invest.add_to_share');
//    Route::get('invest/{invest_shares}/logs', 'Invest_SharesController@logs')->name('user.invest.logs');
    Route::get('shares/{invest_shares}/logs', 'Invest_SharesController@logs')->name('user.shares.logs');
//    Route::resource('invest', 'Invest_SharesController', ['as' => 'user']);
    Route::resource('shares', 'Invest_SharesController', ['as' => 'user']);
    //== Shares & Invest Route

    Route::get('buy-ctr', 'AssetsController@buy_ctr_view')->name('user.buy-ctr');
    Route::post('buy-ctr', 'AssetsController@buy_ctr_store')->name('user.buy-ctr.store');
    Route::post('assets/{asset_unit}/price-on-usdt', 'AssetsController@price_on_usdt')->name('assets.price_on_usdt');
    Route::post('assets/{asset_unit}/convert-to-usdt', 'AssetsController@convert_to_usdt')->name('assets.convert_to_usdt');

});

///// Route Admin
Route::middleware('guest.admin')->prefix('admin')->namespace("App\Http\Controllers\Admin\Auth")->group(function () {
    Route::get('login', 'LoginController@loginForm')->name('admin.login.form');
    Route::post('login', 'LoginController@login')->name('admin.login');
});
Route::middleware('auth.admin')->prefix('admin')->namespace("App\Http\Controllers\Admin")->group(function () {
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('dashboard', "AdminController@dashboard")->name('admin.dashboard');
    Route::get('manage-withdraw-swap-page', 'AdminController@manage_withdraw_swap_page')->name('manage-withdraw-swap-page');
    Route::post('manage_withdraw_update', 'AdminController@manage_withdraw_update')->name('manage-withdraw-update');
    Route::post('manage-swap-currency', 'AdminController@manage_swap_update')->name('manage-swap-update');
    Route::post('change-unit', 'AdminController@change_unit')->name('change-unit');
    Route::post('change-unit-for-withdraw', 'AdminController@change_unit_for_withdraw')->name('change-unit-for-withdraw');
    Route::get('users', 'UserController@index')->name('user.index');
    Route::get('users/authenticate/{user}', 'UserController@authenticate')->name('user.authenticate');
    Route::get('users/{user}', 'UserController@show')->name('user.show');
    Route::get('user/changeStatus', 'UserController@change_status');
    Route::post('user/{user}/edit', 'UserController@edit')->name('admin.user.edit');
    Route::get('user/{user}/login-panel', 'UserController@login_panel')->name('admin.user.login-panel');
    Route::get('user/disableStatus', 'UserController@disable_status');
    Route::get('tickets', 'TicketController@index')->name('ticket.admin.index');
    Route::get('ticket/{id}', 'TicketController@show')->name('ticket.admin.show');
    Route::get('ticket/ban/{id}', 'TicketController@ban')->name('ticket.admin.ban');
    Route::delete('ticket/destroy/{id}', 'TicketController@destroy')->name('ticket.destroy');
    Route::post('ticket/route/{id}', 'TicketController@route')->name('ticket.route');
    Route::post('comment/{id}', 'TicketController@comment')->name('comment.admin.store');
    Route::resource('currencies', 'CurrencyController');
    Route::get('roles', "RolesController@index")->name('roles.index');
    Route::get('roles/create', "RolesController@create")->name('roles.create');
    Route::post('roles/store', "RolesController@store")->name('roles.store');
    Route::get('roles/{id}/edit', "RolesController@edit")->name('roles.edit');
    Route::post('roles/{id}/update', "RolesController@update")->name('roles.update');
    Route::delete('roles/delete', "RolesController@destroy")->name('roles.destroy');
    Route::get('banks','BankController@index')->name('admin.bank.index');
    Route::post('banks/{bank}/confirm','BankController@confirm')->name('admin.bank.confirm');
    Route::post('banks/{bank}/reject','BankController@reject')->name('admin.bank.reject');
    Route::resource('notifications','NotificationController');
    Route::get('get-user','NotificationController@users')->name('notifications.get_user');
    Route::get('reports','ReportController@index')->name('admin.report.index');
    Route::resource('posts','PostController');
    Route::post('posts/reject','PostController@reject')->name('posts.reject');
    Route::post('posts/accept','PostController@accept')->name('posts.accept');
    Route::resource('categories','CategoryController');
    Route::post('categories/delete','CategoryController@delete')->name('categories.delete');

    // Shares & Invest Route
    Route::resource('invest_shares','Invest_SharesController',['as'=>'admin']);
    Route::post('invest_shares/profits','Invest_SharesController@inviter_profit_store')->name('admin.invest_shares.inviter_profit.store');
    Route::post('invest_shares/cancel-invest','Invest_SharesController@cancel_invest')->name('admin.invest_shares.cancel_invest');
    //== Shares & Invest Route
});

///// Route General
Route::middleware('auth')->namespace("App\Http\Controllers\General")->group(function () {
    Route::post('payment', "PaymentController@payment")->name('payment');
});
Route::get('test', function () {

    $result = \App\Models\TransactionTron::select(['user_id', 'status', 'unit', 'id', 'amount_commission', 'commission_hash', 'transfer_hash', 'ethers_data', 'amount',
        'address' => \App\Models\Asset::select('token')->whereColumn('assets.user_id', 'transaction_trons.user_id')->limit(1),
    ])->whereIn('status', [0, 1, 2, 3, 4, 5, 6])->get();

    foreach ($result as $item) {
        $transfer = Tron()->sendTransaction(json_decode($item->address)->address_hex, $item->amount_commission, '', 'TCpFQMd7stauWRgX9jVBHKDLasFugePC3v');
        dd($transfer);
        if ($item->status == 0) {

        } elseif ($item->status == 1) {
            \Illuminate\Support\Facades\DB::beginTransaction();
            try {
                $transfer = Tron()->sendTransaction(json_decode($item->address)->address_hex, $item->amount_commission, '', 'TCpFQMd7stauWRgX9jVBHKDLasFugePC3v');
                $item->commission_hash = $transfer['txID'];
                $item->status = 2;
                $item->save();
                \Illuminate\Support\Facades\DB::commit();
            } catch (Exception $e) {
                \Illuminate\Support\Facades\DB::rollBack();
            }
        } elseif ($item->status == 2) {
            try {
                $result = \Tron()->getTransaction($item->commission_hash);
                if ($result['ret'][0]['contractRet'] == 'SUCCESS') {
                    $item->status = 4;
                } else {
                    $item->status = 3;
                }
            } catch (Exception $e) {
                $item->status = 3;
            }
            $item->save();
        } elseif ($item->status == 3) {
            /// fail fail withdraw admin
            $item->status = 3;
            $item->save();
        } elseif ($item->status == 4) {
            \Illuminate\Support\Facades\DB::beginTransaction();
            try {
                $transfer = Tron(json_decode($item->address)->private_key)->triggerSmartContract($item->ethers_data, '41a614f803b6fd780986a42c78ec9c7f77e6ded13c', json_decode($item->address)->address_hex);
                $item->transfer_hash = $transfer['txID'];
                $item->status = 5;
                $item->save();
                \Illuminate\Support\Facades\DB::commit();
            } catch (Exception $e) {
                \Illuminate\Support\Facades\DB::rollBack();
            }
        } elseif ($item->status == 5) {
            try {
                $result = \Tron()->getTransaction($item->transfer_hash);
                if ($result['ret'][0]['contractRet'] == 'SUCCESS') {
                    $item->status = 7;
                } else {
                    $item->status = 6;
                }
            } catch (Exception $e) {
                $item->status = 6;
            }
            $item->save();
        }
    }
    return 1;
});
Route::match(['post', 'get'], 'payment/verify', "App\Http\Controllers\General\PaymentController@paymentSuccess")->name('payment.success');
Route::get('payment/{type}', "App\Http\Controllers\General\PaymentController@paymentShow")->name('payment.show');
Route::get('force-logout/{token}','App\Http\Controllers\Auth\LoginRegisterController@force_logout')->name('force-logout');
