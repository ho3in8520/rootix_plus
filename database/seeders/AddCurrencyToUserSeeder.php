<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\User;
use Illuminate\Database\Seeder;

class AddCurrencyToUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users= User::all(['id']);
        foreach ($users as $user) {
            $array = [
                ['user_id' => $user->id, 'unit' => 'btt','name' => 'BitTorrent', 'amount' => 0, 'type_token' => 10, 'logo' => 'Bittorrent'],
                ['user_id' => $user->id, 'unit' => 'jst','name' => 'JUST', 'amount' => 0, 'type_token' => 20, 'logo' => 'jst'],
                ['user_id' => $user->id, 'unit' => 'win','name' => 'WINkLink', 'amount' => 0, 'type_token' => 20, 'logo' => 'win'],
            ];
            Asset::insert($array);
        }
    }
}
