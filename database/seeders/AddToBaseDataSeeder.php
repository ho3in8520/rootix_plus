<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class   AddToBaseDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $array = [
            ['type' => 'transactions', 'name' => 'برداشت از ولت کاربر برای ارسال به ولت دیگر', 'extra_field1' => '5', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
        ];
        \App\Models\BaseData::insert($array);
    }
}
