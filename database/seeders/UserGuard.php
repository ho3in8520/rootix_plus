<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserGuard extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = Admin::all();
        foreach ($admins as $admin) {
            $admin->userGuard()->updateOrCreate([
                'guardable_type' => 'App\Models\Admin',
                'guardable_id' => $admin->id,
            ]);
        }
        $users = User::all();
        foreach ($users as $user) {
            $user->userGuard()->updateOrCreate([
                'guardable_type' => 'App\Models\User',
                'guardable_id' => $user->id,
            ]);
        }
    }
}
