<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(array(
            AdminSeeder::class,
            UserSeeder::class,
            WithdrawFee::class,
            BaseDataSeeder::class,
        ));
        // \App\Models\User::factory(10)->create();
    }
}
