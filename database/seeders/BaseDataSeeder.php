<?php

namespace Database\Seeders;

use App\Models\BaseData;
use Illuminate\Database\Seeder;

class BaseDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // این فعلا تستیه بعدا باید اینو پیشرفته کنیم
        \App\Models\BaseData::updateOrCreate(
            ['type' => 'date_cancel_invest', 'name' => 'تاریخ کنسل کردن invest'],
            ['extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '']
        );
        \App\Models\BaseData::updateOrCreate(
            ['type' => 'percent_cancel_invest', 'name' => 'درصد خسارت کنسل کردن invest'],
            ['extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '']
        );

        \App\Models\BaseData::updateOrCreate(
            ['type' => 'currency_price', 'name' => 'ctr', 'extra_field1' => '11'],
            ['extra_field2' => '', 'extra_field3' => '']
        );
        \App\Models\BaseData::updateOrCreate(
            ['type' => 'profit_inviter', 'name' => 'invest'],
            [ 'extra_field1' => 'usdt', 'extra_field2' => '1', 'extra_field3' => '']
        );
        \App\Models\BaseData::updateOrCreate(
            ['type' => 'profit_inviter', 'name' => 'shares'],
            [ 'extra_field1' => 'usdt', 'extra_field2' => '1', 'extra_field3' => '']
        );
        \App\Models\BaseData::updateOrCreate(
            ['type' => 'profit_inviter', 'name' => 'buy-ctr'],
            [ 'extra_field1' => 'usdt', 'extra_field2' => '1', 'extra_field3' => '']
        );
    }
}
