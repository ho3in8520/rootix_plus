<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableWithdrawsChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdraws', function (Blueprint $table) {
            $table->dropColumn('parameter1');
            $table->dropColumn('parameter2');
            $table->string('token');
            $table->float("amount", 20, 8)->change();
            $table->float('fee');
            $table->boolean('status')->default(0)->comment('0 => Not approved,1 => accept')->change();
            $table->dropColumn('verification');
            $table->dropColumn('time_verification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdraws', function (Blueprint $table) {
            //
        });
    }
}
