<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('code')->unique();
            $table->string('mobile', 11)->nullable();
            $table->string('phone', 11)->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('national_code', 10)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->string('sheba', 30)->nullable();
            $table->string('birth_day', 20)->nullable();
            $table->tinyInteger('language')->comment('1=>fa 2=>en')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->tinyInteger('gender_id')->comment('1=>mard 2=>khanom')->nullable();
            $table->unsignedBigInteger('state')->nullable();
            $table->unsignedBigInteger('city')->nullable();
            $table->text('address')->nullable();
            $table->tinyInteger('step')->default(0)->comment("0=>new register 1=>verification mobile 2=>verification email 3=>bank 4=>Complete information 5=>panel ready");
            $table->boolean('is_complete_steps')->default(0);
            $table->boolean('status')->default(0);
            $table->tinyInteger('panel_type')->nullable();
            $table->enum('confirm_type',['sms','google-authenticator'])->default('sms');
            $table->string('verification', 300)->nullable();
            $table->string('time_verification',20)->nullable();
            $table->unsignedBigInteger('referral_id')->nullable();
            $table->string('reject_reason', 255)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
