<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->text('tags');
            $table->tinyInteger('status')->default(0)->comment('0 => new,1 => accepted,2 => rejected, 3 => edited ');
            $table->text('reject_reason');
            $table->unsignedBigInteger('creator_id');
            $table->foreign('creator_id')->references('id')->on('user_guards');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('posts_category');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
