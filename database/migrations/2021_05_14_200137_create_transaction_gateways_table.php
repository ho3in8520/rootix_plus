<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionGatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_gateways', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('gateway')->nullable();
            $table->float('amount');
            $table->string('transaction_id',500);
            $table->unsignedBigInteger('reference_id')->nullable();
            $table->string('mode')->nullable();
            $table->tinyInteger('status')->comment('0=> unpaid 1=> successful payment 2=>Unsuccessful payment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_gateways');
    }
}
