<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta
        name="google-signin-client_id"
        content="134236322338-l2j42tsl6pgfj9fefhgjse5trjulu3jb.apps.googleusercontent.com"
    />
    <title>@yield('title_browser')</title>
    @include('templates.landing_page.layout.header')
    {!! htmlScriptTagJsApi() !!}
    <style>
        .help-block{
            color: #a94442;
        }
        .g-recaptcha-response{
            display: none;
        }
    </style>
    @yield('style')
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="0d388766-bc01-4ba9-bb6b-b79f274e0180";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>

<body class="main-form-body en-body">
@include('general.flash_message_desktop')
<main class="main-register">
    <section class="register-info register login-info">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="register-form-container">
                        <div class="register-form__title">
                            <h1>Welcome</h1>
                            <p>For Login to Panel Please Register</p>
                        </div>

                        <div class="register-mark-btns">
                            <div
                                class="g-signin2 register-mark-btn"
                                data-onsuccess="onSignIn"
                            ></div>
                            <button class="register-mark-btn register-facebook-btn">
                                <div class="facebook-container">
                                    <i class="fa fa-facebook-f"></i>
                                </div>
                                <div class="text-center w-100 sign-in-with-facebook">
                                    <span>Login With Facebook</span>
                                </div>
                            </button>
                        </div>
                        @yield('form')
                        <h5 class="development-rootix">Develope In Rootix</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="register-img register">
        <div>
            <div>
                <div>
                    <div class="main-register-img">
                        <div class="register-logo">
                            <img
                                class="register-logo"
                                src="{{asset('theme/landing/images/logo.png')}}"
                                alt="Rootix Logo"
                            />
                        </div>
                        <div class="register-img__container">
                            <img
                                src="{{asset('theme/landing/images/register/Group 1497.png')}}"
                                alt="Register in rootix"
                            />
                        </div>

                        <div class="register-img__bottom">
                            <hr class="register-hr" />
                            <div>
                                <h3>
                                    Easily register in the Ronix panel and experience buying and
                                    selling
                                    <br class="br-register" />
                                    in the world of digital currency in one of the best exchange
                                    offices.
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="{{asset('theme/landing/scripts/register-validation.js')}}"></script>
<script src="{{asset('theme/landing/scripts/app.js')}}"></script>
@yield('script')
</body>
</html>
