<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta
        name="google-signin-client_id"
        content="134236322338-l2j42tsl6pgfj9fefhgjse5trjulu3jb.apps.googleusercontent.com"
    />
    <title>@yield('title_browser')</title>
    @include('templates.landing_page.layout.header')
    {!! htmlScriptTagJsApi() !!}
    <style>
        .help-block{
            color: #a94442;
        }
        .g-recaptcha-response{
            display: none;
        }
    </style>
    @yield('style')
    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="0d388766-bc01-4ba9-bb6b-b79f274e0180";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>

<body class="en-body">
<!-- Start Header -->
<header class="header">
    <div class="container">
        <div class="res-menu">
            <div class="res-menu__top res-menu__top-form">
                <button class="res-menu__bar-icon">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="30"
                        height="21"
                        class="res-menu__bars"
                        viewBox="0 0 30 21"
                    >
                        <g
                            id="Icon_feather-menu"
                            data-name="Icon feather-menu"
                            transform="translate(-3 -7.5)"
                        >
                            <path
                                id="Path_8025"
                                data-name="Path 8025"
                                d="M4.5,18h27"
                                fill="none"
                                stroke="#fff"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="3"
                            />
                            <path
                                class="path-menu-icon"
                                id="Path_8026"
                                data-name="Path 8026"
                                d="M4.5,9h27"
                                fill="none"
                                stroke="#fff"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="3"
                            />
                            <path
                                id="Path_8027"
                                data-name="Path 8027"
                                d="M4.5,27h27"
                                fill="none"
                                stroke="#fff"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="3"
                            />
                        </g>
                    </svg>
                </button>

                <a href="#">
                    <img src="{{asset('theme/landing/images/logo.png')}}" alt="logo" class="res-menu__logo" />
                </a>
            </div>

            <div class="container-menu">
                <ul class="ul-res-menu">
                    <h2>Rootix</h2>
                    <li>
                        <a href="{{route('landing.home')}}">
                            <div class="home-index">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15.455"
                                    height="17.061"
                                    viewBox="0 0 15.455 17.061"
                                >
                                    <g
                                        id="Icon_feather-home"
                                        data-name="Icon feather-home"
                                        transform="translate(0.5 0.5)"
                                    >
                                        <path
                                            id="Path_434"
                                            data-name="Path 434"
                                            d="M4.5,8.621,11.727,3l7.227,5.621v8.834a1.606,1.606,0,0,1-1.606,1.606H6.106A1.606,1.606,0,0,1,4.5,17.455Z"
                                            transform="translate(-4.5 -3)"
                                            fill="none"
                                            stroke="#fff"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="1"
                                        />
                                        <path
                                            id="Path_435"
                                            data-name="Path 435"
                                            d="M13.5,26.03V18h4.818v8.03"
                                            transform="translate(-8.682 -9.97)"
                                            fill="none"
                                            stroke="#fff"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="1"
                                        />
                                    </g>
                                </svg>
                            </div>

                            Home Page
                        </a>
                    </li>
                    <li>
                        <a href="{{route('landing.about')}}">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="22.744"
                                height="23.941"
                                viewBox="0 0 22.744 23.941"
                            >
                                <g id="about" transform="translate(-3.75 -4.5)">
                                    <path
                                        id="Path_8037"
                                        data-name="Path 8037"
                                        d="M22.9,24.85H7.341L3.75,28.441V8.091A3.6,3.6,0,0,1,7.341,4.5H22.9a3.6,3.6,0,0,1,3.591,3.591V21.259A3.6,3.6,0,0,1,22.9,24.85Z"
                                        transform="translate(0)"
                                        fill="#1b274c"
                                    />
                                    <g
                                        id="Group_1404"
                                        data-name="Group 1404"
                                        transform="translate(13.925 8.69)"
                                    >
                                        <path
                                            id="Path_8038"
                                            data-name="Path 8038"
                                            d="M16.5,15h2.394v6.584H16.5Z"
                                            transform="translate(-16.5 -10.81)"
                                            fill="#fff"
                                        />
                                        <path
                                            id="Path_8039"
                                            data-name="Path 8039"
                                            d="M18.894,10.947a1.2,1.2,0,1,1-1.2-1.2A1.2,1.2,0,0,1,18.894,10.947Z"
                                            transform="translate(-16.5 -9.75)"
                                            fill="#fff"
                                        />
                                    </g>
                                </g>
                            </svg>

                            About Us
                        </a>
                    </li>
                    <li>
                        <button class="main-top-menu-1 main-top-menu">
                            <div
                                class="
                      d-flex
                      justify-between
                      items-center
                      w-100
                      position-re
                    "
                            >
                                <div class="d-flex items-center">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="21.488"
                                        height="18.623"
                                        viewBox="0 0 21.488 18.623"
                                    >
                                        <g id="serverless" transform="translate(-7.5 -9)">
                                            <path
                                                id="Path_8040"
                                                data-name="Path 8040"
                                                d="M17.223,9H7.5v4.3h8.159Z"
                                                fill="#1b274c"
                                            />
                                            <path
                                                id="Path_8041"
                                                data-name="Path 8041"
                                                d="M14.616,16.5H7.5v4.3h5.552Z"
                                                transform="translate(0 -0.337)"
                                                fill="#1b274c"
                                            />
                                            <path
                                                id="Path_8042"
                                                data-name="Path 8042"
                                                d="M16.506,20.8l1.564-4.3H29.393v4.3Z"
                                                transform="translate(-0.405 -0.337)"
                                                fill="#1b274c"
                                            />
                                            <path
                                                id="Path_8043"
                                                data-name="Path 8043"
                                                d="M12.01,24H7.5v4.3h2.945Z"
                                                transform="translate(0 -0.674)"
                                                fill="#1b274c"
                                            />
                                            <path
                                                id="Path_8044"
                                                data-name="Path 8044"
                                                d="M13.776,28.3,15.34,24h13.93v4.3Z"
                                                transform="translate(-0.282 -0.674)"
                                                fill="#1b274c"
                                            />
                                            <path
                                                id="Path_8045"
                                                data-name="Path 8045"
                                                d="M19.236,13.3,20.8,9h8.716v4.3Z"
                                                transform="translate(-0.528)"
                                                fill="#1b274c"
                                            />
                                        </g>
                                    </svg>

                                    Services
                                </div>

                                <!-- <svg xmlns="http://www.w3.org/2000/svg" class="down-arrow" width="16.9" height="10.436" viewBox="0 0 16.9 10.436">
                                    <path id="Icon_material-expand-more" data-name="Icon material-expand-more" d="M23.914,12.885l-6.464,6.45-6.464-6.45L9,14.871l8.45,8.45,8.45-8.45Z" transform="translate(-9 -12.885)" fill="#1b274c"/>
                                    </svg> -->

                                <i class="fas fa-angle-down down-arrow"></i>

                                <ul class="under-menu under-menu-2">
                                    <li><a href="#"> Test 1</a></li>
                                    <li><a href="#"> Test 2 </a></li>
                                    <li><a href="#"> Test 3</a></li>
                                </ul>
                            </div>
                        </button>
                    </li>
                    <li>
                        <button class="main-top-menu main-top-menu-2">
                            <div
                                class="
                      d-flex
                      justify-between
                      items-center
                      w-100
                      position-re
                    "
                            >
                                <div class="d-flex items-center">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="22.595"
                                        height="21.263"
                                        viewBox="0 0 22.595 21.263"
                                    >
                                        <g
                                            id="pricetags-outline"
                                            transform="translate(-0.519 -1.75)"
                                        >
                                            <path
                                                id="Path_8046"
                                                data-name="Path 8046"
                                                d="M18.463,2.25h-5.5a.648.648,0,0,0-.457.188l-11,11a1.292,1.292,0,0,0,0,1.822L6.74,20.5a1.292,1.292,0,0,0,1.822,0l11-11a.648.648,0,0,0,.188-.457V3.54a1.283,1.283,0,0,0-1.285-1.29Z"
                                                fill="none"
                                                stroke="#1b274c"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                stroke-width="1"
                                            />
                                            <path
                                                id="Path_8047"
                                                data-name="Path 8047"
                                                d="M23.933,8.49a1.433,1.433,0,1,1,1.433-1.433A1.433,1.433,0,0,1,23.933,8.49Z"
                                                transform="translate(-7.765 -1.226)"
                                                fill="#1b274c"
                                            />
                                            <path
                                                id="Path_8048"
                                                data-name="Path 8048"
                                                d="M16.172,23.532,27.9,11.8a.618.618,0,0,0,.179-.448V5.625"
                                                transform="translate(-5.466 -1.226)"
                                                fill="none"
                                                stroke="#1b274c"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                stroke-width="1"
                                            />
                                        </g>
                                    </svg>

                                    Prices
                                </div>

                                <i class="fas fa-angle-down down-arrow"></i>

                                <ul class="under-menu under-menu-2">
                                    <li><a href="#"> Test 1</a></li>
                                    <li><a href="#"> Test 2 </a></li>
                                    <li><a href="#">Test 3 </a></li>
                                </ul>
                            </div>
                        </button>
                    </li>
                    <li>
                        <a href="#">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="14.839"
                                height="23.886"
                                viewBox="0 0 14.839 23.886"
                            >
                                <path
                                    id="Icon_open-question-mark"
                                    data-name="Icon open-question-mark"
                                    d="M7.375,0A7.716,7.716,0,0,0,1.762,1.971,6.515,6.515,0,0,0,0,5.792l2.986.388a3.618,3.618,0,0,1,.926-2.06A4.434,4.434,0,0,1,7.375,2.986,5.268,5.268,0,0,1,11.017,4a2.5,2.5,0,0,1,.836,1.971c0,2.478-1.015,3.165-2.508,4.479s-3.463,3.225-3.463,6.718v.746H8.868v-.746c0-2.478.926-3.165,2.418-4.479s3.553-3.225,3.553-6.718a5.736,5.736,0,0,0-1.762-4.21A8.252,8.252,0,0,0,7.375,0ZM5.882,20.9v2.986H8.868V20.9Z"
                                    fill="#1b274c"
                                />
                            </svg>

                            Faqs
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="29.54"
                                height="26.886"
                                viewBox="0 0 29.54 26.886"
                            >
                                <g
                                    id="Icon_feather-book-open"
                                    data-name="Icon feather-book-open"
                                    transform="translate(-1.5 -3)"
                                >
                                    <path
                                        id="Path_8054"
                                        data-name="Path 8054"
                                        d="M3,4.5h7.962A5.308,5.308,0,0,1,16.27,9.808V28.386a3.981,3.981,0,0,0-3.981-3.981H3Z"
                                        fill="none"
                                        stroke="#1b274c"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="3"
                                    />
                                    <path
                                        id="Path_8055"
                                        data-name="Path 8055"
                                        d="M31.27,4.5H23.308A5.308,5.308,0,0,0,18,9.808V28.386a3.981,3.981,0,0,1,3.981-3.981H31.27Z"
                                        transform="translate(-1.73)"
                                        fill="none"
                                        stroke="#1b274c"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="3"
                                    />
                                </g>
                            </svg>

                            Education
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="18.317"
                                height="22.646"
                                viewBox="0 0 18.317 22.646"
                            >
                                <g
                                    id="Icon_feather-file-text"
                                    data-name="Icon feather-file-text"
                                    transform="translate(-5.5 -2.5)"
                                >
                                    <path
                                        id="Path_8049"
                                        data-name="Path 8049"
                                        d="M16.823,3H8.165A2.165,2.165,0,0,0,6,5.165V22.482a2.165,2.165,0,0,0,2.165,2.165H21.152a2.165,2.165,0,0,0,2.165-2.165V9.494Z"
                                        fill="none"
                                        stroke="#1b274c"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1"
                                    />
                                    <path
                                        id="Path_8050"
                                        data-name="Path 8050"
                                        d="M21,3V9.494h6.494"
                                        transform="translate(-4.177)"
                                        fill="none"
                                        stroke="#1b274c"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1"
                                    />
                                    <path
                                        id="Path_8051"
                                        data-name="Path 8051"
                                        d="M20.658,19.5H12"
                                        transform="translate(-1.671 -4.595)"
                                        fill="none"
                                        stroke="#1b274c"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1"
                                    />
                                    <path
                                        id="Path_8052"
                                        data-name="Path 8052"
                                        d="M20.658,25.5H12"
                                        transform="translate(-1.671 -6.265)"
                                        fill="none"
                                        stroke="#1b274c"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1"
                                    />
                                    <path
                                        id="Path_8053"
                                        data-name="Path 8053"
                                        d="M14.165,13.5H12"
                                        transform="translate(-1.671 -2.924)"
                                        fill="none"
                                        stroke="#1b274c"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1"
                                    />
                                </g>
                            </svg>

                            Blog
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="18.318"
                                height="18.353"
                                viewBox="0 0 18.318 18.353"
                            >
                                <path
                                    id="Icon_feather-phone"
                                    data-name="Icon feather-phone"
                                    d="M20.484,15.991V18.6a1.741,1.741,0,0,1-1.9,1.741,17.231,17.231,0,0,1-7.514-2.673,16.978,16.978,0,0,1-5.224-5.224A17.231,17.231,0,0,1,3.175,4.9,1.741,1.741,0,0,1,4.908,3H7.52A1.741,1.741,0,0,1,9.261,4.5,11.18,11.18,0,0,0,9.87,6.944a1.741,1.741,0,0,1-.392,1.837L8.373,9.887A13.931,13.931,0,0,0,13.6,15.111L14.7,14.005a1.741,1.741,0,0,1,1.837-.392,11.18,11.18,0,0,0,2.447.609,1.741,1.741,0,0,1,1.5,1.767Z"
                                    transform="translate(-2.667 -2.5)"
                                    fill="none"
                                    stroke="#1b274c"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="1"
                                />
                            </svg>
                            Contact Us
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row header-row">
            <div class="col-12">
                <div>
                    <div class="header-info header-info-form register-header-info">
                        <div class="header-info__desc">
                            <h1>
                                Welcome

                                <h2>For Login To panel</h2>
                                <h2>Please Register</h2>
                            </h1>
                        </div>
{{--                        <p>--}}
{{--                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و--}}
{{--                            با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و--}}
{{--                            مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی--}}
{{--                            تکنولوژی مورد نیاز--}}
{{--                        </p>--}}

                        <div class="header-currency-image">
                            <img
                                src="{{asset('theme/landing/images/register/Group 1497.png')}}"
                                alt="Digital currency"
                                class="header-currency"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header -->

<!-- Start main -->
<main>
    <!-- Start Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="register-form-container">
                        <div class="register-mark-btns register-mark-btns-responsive">
                            <div
                                class="g-signin2 register-mark-btn"
                                data-onsuccess="onSignIn"
                            ></div>
                            <button class="register-mark-btn register-facebook-btn">
                                <div class="facebook-container">
                                    <i class="fa fa-facebook-f"></i>
                                </div>
                                <div class="text-center w-100 sign-in-with-facebook">
                                    <span>Login With Facebook</span>
                                </div>
                            </button>
                        </div>
                        @include('general.flash_message')
                        @yield('form')

                        <h5 class="development-rootix">Develope In Rootix</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- End main -->

<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="{{asset('theme/landing/scripts/login-validation.js')}}"></script>
<script src="{{asset('theme/landing/scripts/app.js')}}"></script>
@yield('script')
</body>
</html>
