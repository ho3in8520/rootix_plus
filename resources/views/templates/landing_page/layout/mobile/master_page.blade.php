<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>@yield('title_browser')</title>
    @include('templates.landing_page.layout.header')
    @yield('style')
    <script type="text/javascript">
        CRISP_RUNTIME_CONFIG = {
            locale: "{{ \Illuminate\Support\Facades\App::currentLocale() }}"
        };
        window.$crisp = [];
        window.CRISP_WEBSITE_ID = "0d388766-bc01-4ba9-bb6b-b79f274e0180";
        (function () {
            d = document;
            s = d.createElement("script");
            s.src = "https://client.crisp.chat/l.js";
            s.async = 1;
            d.getElementsByTagName("head")[0].appendChild(s);
        })();
    </script>
</head>
<body class="en-body">

@include('templates.landing_page.layout.mobile.top_menu')

<main>
    @yield('content')
</main>
<!-- Footer Starts -->
<footer>
    <div class="container-fluid">

        <div class="footer-logo">

            <h2>
                R
            </h2>
            <h2>
                OOTIX</h2>
        </div>

        <div class="row footer-row">
            <div class="col-6 col-lg-3">
                <div class="footer-contact footer-item">
                    <h3 class="footer-title">Rootix</h3>
                    <div>
                        <h5>rootix@company.com</h5>
{{--                        <h5>648-145-8360</h5>--}}
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-sign-in footer-item">
                    <h3 class="footer-title">Subscribe</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> Register </a>
                        </li>
                        <li>
                            <a href="{{route('login.form')}}"> Login </a>
                        </li>
                        <li>
                            <a href="#"> Faqs </a>
                        </li>

                        <li>
                            <a href="#"> Contact Us</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-about-us footer-item">
                    <h3 class="footer-title">About Us</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#">About Rootix </a>
                        </li>
                        <li>
                            <a href="#"> Our Team </a>
                        </li>
                        <li>
                            <a href="#"> Career Opportunities </a>
                        </li>
                        <li>
                            <a href="#"> Users Comments </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-quick-access footer-item">
                    <h3 class="footer-title">Quick Acces</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#">  How Is Rootix Work</a>
                        </li>
                        <li class="footer-rootix-menu-item">
                            <a href="#">Rootix Features </a>
                        </li>
                        <li>
                            <a href="#"> Rootix News</a>
                        </li>
                        <li>
                            <a href="#"> Prices </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr />

        <div class="footer-bottom">
            <div class="social-networks">
                <a href="#" class="social-network instagram">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="18"
                        viewBox="0 0 18 18"
                    >
                        <image
                            id="instagram_copy"
                            data-name="instagram copy"
                            width="18"
                            height="18"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABHNCSVQICAgIfAhkiAAAAaNJREFUOE+dlD0ohVEYx12fyVcYFDZiM2Bj8rExKZOPKHV9LAYp3A9yFxkpJEooMZFFschoMEgRE2IgRWLQ9fvXOfW6vfd6eerX85xznvffeZ/znOOLRqNJxlrxo1BnJ37xZ6zPwrryfEZohHgGvmAXniBZ6y5iysmDJsiHeRiSUAPBISxDn8fd2LQQQRj8EjonKIcMjyLaaRVcwCdcQ4mEVKRtaI8jlM58Kryb9WL8HXTABkxBwAotMOiPESoyiY1mXjvohFOoNztSLccgYoVWGfQ6hHKIbyEX5uADhiHFiJw4csPEoXhCByw2QxncmI/S8C8mzvIqpLqtQXfM7/YwXgEdjoosS7gjCbm1gw5kCyrh0ovQsalFKV4nZO2BoADUKvZKJNyROvbZfB3Bv8EEqDY6xSOH+A+hRRb8jkWFFbAJNWb+Ht8FugVOG2cwbU9NjaUGc7NCJtWQj3HWg8xPSkg1yAT9+39MDVotoTaCHalC+I9KuuRLELTPiJ6CAbiCPVDjqYvdTM9INujVqIV9aLFC+mAQAqA75sVeSdIbNq3kb1Ymla7Wdx0PAAAAAElFTkSuQmCC"
                        />
                    </svg>
                </a>
                <a href="#" class="social-network twitter">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="14"
                        viewBox="0 0 18 14"
                    >
                        <image
                            id="twitter_copy"
                            data-name="twitter copy"
                            width="18"
                            height="14"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAOCAYAAAAi2ky3AAAABHNCSVQICAgIfAhkiAAAAUBJREFUKFNj/P//PxMDBPyD0rioEKBEHRDLAfFTIG4F4rVAHATEDxiBBmkCGauBOAqIL+EwJRoovgSL3HOg2B8gTgUZZAdkHATiv0AcAcRr0DQIAPmvgJgVhyUguWyQQbJAxiMkRRuA7HYgPgUVEwTS7/B4ewJQrgRkEEhNBhBPBmIWJA0gb54EYlDYpQIxLCzRzQQFzQ2QQcYgpwGxNxCL4bEZlxRI/zmQQTJAxmMyDABp+QjEUkD8Dea1SCBnGRmGLQDqSQTpgxkEYncBcSwQS5BgoANQLSjGUQwCJbAqEgw5DVRrBlOP7CJhoOA2ZEkChoKSzRNsBsHEHIGM+UAsj8cgZ6DcPmR5kIskgQKgaGcHYnsgDgBiKxyGgBJmGBDvRZeH5bUyoEQoEHNDFXwF0siZ+BOQvwiIe4AYayoHAAP+W7zpyrnuAAAAAElFTkSuQmCC"
                        />
                    </svg>
                </a>
                <a href="#" class="social-network face-book">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="8"
                        height="18"
                        viewBox="0 0 8 18"
                    >
                        <image
                            id="facebook-logo_copy"
                            data-name="facebook-logo copy"
                            width="8"
                            height="18"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAASCAYAAABmQp92AAAABHNCSVQICAgIfAhkiAAAALFJREFUKFNj/P//PwMSSAeyg4HYEIjZgfgLI5KCEqBAN7JqEBtZwTsgXxBNwS+YAkWgxD0kyfNAdgqyFUpAzl0kBW1AdjXMCh4gwwCIDyMpmA9k54OcALLiIZAhAsRcSAp+A9k/gPgrSAGKP7H5AmSCMBBzYzHhM8gEkBv0gPgokoIFQHYecjjIAzkPkBRMRlegDxS4gKRgNpCdhmzCYFBgDHTQGSRHLgeyo5AdqYsrHACDnVzHH+anLgAAAABJRU5ErkJggg=="
                        />
                    </svg>
                </a>
            </div>

            <h5>© 2018 Rootix All Right Reserved</h5>
            <h5 class="inf">info@rootix.io</h5>
{{--            <h5 class="inf">648-145-8360</h5>--}}
        </div>
    </div>
</footer>
<!-- Footer Ends -->
@include("templates.landing_page.layout.footer")
@yield('script')
</body>
</html>
