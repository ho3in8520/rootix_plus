<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>@yield('title_browser')</title>
    @include('templates.landing_page.layout.header')
    @yield('style')
    <script type="text/javascript">
        CRISP_RUNTIME_CONFIG = {
            locale: "{{ \Illuminate\Support\Facades\App::currentLocale() }}"
        };
        window.$crisp = [];
        window.CRISP_WEBSITE_ID = "0d388766-bc01-4ba9-bb6b-b79f274e0180";
        (function () {
            d = document;
            s = d.createElement("script");
            s.src = "https://client.crisp.chat/l.js";
            s.async = 1;
            d.getElementsByTagName("head")[0].appendChild(s);
        })();
    </script>
</head>
<body class="en-body">
{{--@include('templates.landing_page.layout.desktop.top_menu')--}}
<main>
    @yield('content')
</main>
<!-- Footer Starts -->
<footer>
    <div class="container-fluid">
        <div class="footer-logos">
            <svg
                class="footer-logo footer-logo-1"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="85"
                height="53"
                viewBox="0 0 85 53"
            >
                <image
                    id="Layer_1216"
                    data-name="Layer 1216"
                    width="85"
                    height="53"
                    opacity="0.5"
                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFUAAAA1CAYAAAAwAacAAAAABHNCSVQICAgIfAhkiAAABlhJREFUeF7tnAesLUMYx+/T8gghRG8heoguRHsRvUaUIMFVoxMiOlcvESRqnvZ0nhKeGiV6eaL39pAgES0I0cvvxw7rOOfc2XNmrruyX/LP2XvON7Mz/53Zmf32/90xAwMD94NFgTYG/FYcV/mw3CdgW/BulYKjxHcl2nFTiYNemhW4m+TB+2DBXmppU2ZVvpscUddYfLYAD4OPI/xzu4zjBA8mOslkSX0bLAL2Ai8Wo7WX+r+i0GuRBY/C7yRwBRiMLJPTba3iAsvFzn2cyFk+pUzqynzxTB8VxhadDseXwWLgS7DkKBitgdTnacsKsR3p5FcmdU2cHuu3wojyq7ec5wD+PjeiXE6XQKozdXnQy7ryV/v+C1Iv5uy7gw/BfOBxsEZOxiLqrjWps9FB71uzgM3ARWAusCyIvR9HcFTZpdakbk93rwXh3nUpx7uCM8GhlalIV6DWpN4OD5uCfcEFYGNwJ3gPLAW+T8dTpZpqS+oSdNNV/wewOPgITA9eAm7p1gUPVKIinXNtST0SDk4GdwDvp8GO42AIXAL2SMdTpZpqSepUdPFZsBzwSWpSqcvLcPwC+BpU3bO6DfRW4j25nz12LUldm04/BD4ATv3vSqS6rXNbtRrYDVxWYYz5RLYTOAMcVqFcq2stSR1fTO3z+dyvTed3Kci8l88NKpBzOb6D4HRweIVytSd1JnrwJpgTuMl/sk3n5+A7968GWrxFvB5JkKPaC3IaOCKyTDu32o3UzenFbcDolVGsTnYDPxg6dDE7OpKgUU+qMUUXk9QmoRIb9qad6l+PH+4BbwCfsH6OaEhqUpMFVN6i8QapDVYbaHbhKJt/fwpOAJ9FdLTsYpxWkpzW3i8t31q//gYwjF65M5gWuLA9EnGu1KQaNbu7aE+7drqgXgh+7dY2CwZSh+vD1jjcPJxTy+8uSr1EoK6m3I4R50pN6nCn/AUH14bPhyM1BKmvwtEtT7sr9AXfu4K7l4y1qXF8AqwCJgBfs7SrO9TnaJ0VHFQ02nir5+1mqUl1phqP6GTeHm8BXUOD5dCfcUQ34anMRcmV3lvH/MDH0xh7FCd3CTuA60aY1Oc434oxjezmkzOe6lPOIaDqKxNf63jf8p3ROiNM6qgOUhsoeRUsVIw6b/CxZuDa+7wL19LAha6TpZ7+yUidQosXBm6AnXopLMRNX6EyN/Pe4KvYjTi7MBpscdeRm9RxnMCZYRTN7Vzfr1N8ejEsl/LF3znUdyA4HgxVYbPw3ZBPtzZ3gU1GgFQXUx9Oku1TDWS4T3VR+KkHAtoVmYcvHflu5t37VbVpKLAVMNba7ZE11fR3b+zC6OP0U1Ub2+rfbYvTb90jUT4VqUnb2pCalM4/K2tIbUj9FwPN9M8wKBpSG1IzMJChymakZiS114eMDE36/6z+iod90Oi2m/E3H25UTKuIyWZ131L5FLhdRXZUHe5ZsUwl97qT6ptU5UJdX28UjNhXY7q+Jjdums3qTmo2YvqpuCG1H/Y6lG1IbUjNwECGKpuR2pCagYEMVTYjtSE1AwMZqmxGakNqBgYyVNmM1IbUDAxkqDJ2pPoefwbwDlCKY45+0KouwLEIycLWGcRu63Os8iOI0xQtqANQ1qMpDzIjxQDH3GBGoAqxbCattaZYmulsnUqLupnSeJMzvgFGtL4tnGfmM4icTUA2QS7YRhw8DcpySfMQFNzpN2wWTCypW1LZ2UDxmIIz/1YmqRklEuVEBkUQtwKJMj0yCCrMpfLCTCzK+ru+qqwPBmYEKk8PphxJMbLCDBOEgyl8U2ZpIkUns29GpNRieQHM3zIhTrNeJZunAPWz25QqMc3T/0UQLqQKRNs2AZhVc1+Xc/7xUyypKpwl50egcEwCw8hUmSeCTl9d6vXApN55gaM2jFTTctRuhX+tocRSga/BY9XWaj/PKjX6GI7VUpkkYbJEMCVF/tMGL3An3ZPtkMi9gbpa2xBGqjPvGqB01FmmGibYlRycCoIyRk2X0vn9gQkf5YtbKvb3YSypTgml5hKwD1A6HuQxTmlxXlGtkp0hYOMGi2Mvhmb8U2Gxo09T4ecoPRGonzLL2iCyZj3KMYPWy4sa8gDsqPlTtieM+qLYPz682GYRmvN6bHFuHWYHjlJHo21w5Adzpij8VaSseduzrH6+XfCcXe134tWMtaCK5pkAAAAASUVORK5CYII="
                />
            </svg>

            <svg
                class="footer-logo footer-logo-2"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="246"
                height="57"
                viewBox="0 0 246 57"
            >
                <image
                    id="Layer_1215"
                    data-name="Layer 1215"
                    width="246"
                    height="57"
                    opacity="0.5"
                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAAA5CAYAAAAFrNapAAAABHNCSVQICAgIfAhkiAAAIABJREFUeF7t3QvcddlcB/B5J5dEF4pSaYbu95Jy1xui3NIFCRmVYVKkRCl5c2mopLvLoInuKZRSEu9UhEQXuZR4JneRa4rG6Pfd715n1lln7X3O87zPM/O+8zn/z2d9zjl7r7322muv3/++1jl09tlnn3IJ0mfn3l+XcoOUz035pJSPpPxXyr+kPG8s/73bPn70ox895dChQ6f4RL6j8ru0V47vtv291K/v3btv6fNe2j7Ia+pxPMj7lLaf8IQnLG7T3vvi6Mtu5sgm/Vk3x9o52hvjtk/r3sOhSwDYH59OnZHyjSlfm/Jxazr5bzn/2ymPSPm/dQ9Uzm+BvelIra+3yeRd38rmNbbAXh2rEx3YN0+XH5PyZVXXP5zvr015Q8o7Uy6X8gUp12ke7xn5fc+Ud20yRbbA3mSUNquzBfYxba9Hm4zNpV1if0cG5jerwfnHfP+NlD9KeV3KheVcQHlqBuN78/uBKdeorvm1fP+uTabjFtibjNJmdTaZvJu1tFmtrcReHacTVWLfLF390xTSmNF7v5RfmnrNla15tdT59ZRvGOsC/41S/nbdFNkCe90IbX5+C+ytxO7Nlsvk4ItTvmoE9W0DuhdmsnxCfv9H74LaiZTv10jdV6Xelca6T8vnd66blg2wL5c2Ps39duMYWXeP3Z7fOs82G7GtxD45JDYQkrroF1LY0j+ewtP9hSn/0z5GA2we7ZemzleP9Xy/7sQU+ebx3IVp48W5jpqPnpJyx5RfyvGH5POCcv06+2ezqbhRrZvl3i9LzfeqvfWKT4/ZFtirY3OiqeKXTRcB8StGQB/N5z3Gbv9sPtnQx+JR8/T8nBYWQ6T/9ZvqH5vfnHLs8pp+MT/un/JXKTccT7j/uRO3E367a8otUgCQD+C31nVuzXkay0+kYCg/kIK5dWkb7jo2LFtgr06PEw3Y10wXX51y+REonzh2+bx8Hq67PzOpgZYqri0EaHdJIYH/NeUfUtjr3zeBF9L9K1MeP54/P5+88u9r6t9tbKf0sZzW7q9MtL3J4Z9KpR8dK/5YPv3u0hbYx4ZlC+zV6XGiARugXt50k+pN+r6kPt6b1OOxL0+9v0/5mLE+55lw2JNS/jJFCO0VKbSCv0khpdnTD08B0t9NAU4JL5xx6N7mT3V/oOZxdw9qujaE226SwnvvOWgWpP6RFNrGn1fXT3397pw4J4X35e0pmAzGgiTjiOl/MGUI4W2BfWxgtsBenU4nGrCBkkStCeCopks0AewrxBb9/VS89Vi5ANmxb0v5u5SvSXn2WOfn80n1Rs9M+aaUN6UImZGaRVqyvZ1D7HzMh2bwthThtOekPC4FA2AG8OrLjDua8ukpP5Tyc+P19cdn5ce1U0QAaCmSaz41RTbdLVN48++Q8i0pmAZw8zWwvZ+dMXh2nvc1nXYv0UNbr/jWK95OwKvmACn6eeMJUu/2KaQX1fqKKe9IeXMm9U4mkFTSQoBBBSb10JtS51ap88/5/ssp90l5VY59cT4fmuNH8smWls32ySnACmivTPnSFAADNFKyttNd99DxHmz+n0khWTEL7egvRvBnKcCJmAGYS003zg/awdVT2PEYBeaAzk15cMqzUooTkBnxnymfkvL5Yz2/aQdvbtq+RH9ugX0SAvu0007rThov8173utd+TCjgAKTPSQHs/025Qqdhk5qzChgwBE6mkn1Gkt40IH716E1+QH4DYJHGd8r330l5S8qXjNdSr9EjU3jhqdn/lPJFKRjATVMwDl770h/xdaYCVRwwpbJKrOFMK6r3Tr4D33vG9n0APPCL0yP9A25MRx8xNv0o2oRsO9pMkc6cgZ7nOnlGjGkwUy5Gj331KKtfR2B7NuUDs5X34eRWFV8dxF2r4nPALs3vA8CLDdz2+E9y4EUpwHjbFLFqwAIqbFJCCilHkr6uUtep0VTt96d8ZgqwUnPZx4B0+ngjtqv4+fnj5CTFgZJ9/ZMpGEFNGE9hz0J0VG5tFLXeeSE1fSpEuv9hChUd/UUKdZ5ExjB46jErz4g5lHq0B33mP6AdvD6FVkGD2SRSUHXhQL+elrF7UMYeI9Q/UY5HpzBRDoS2wF4d1gMBttto+N73ZnLuiXjB2ZQt8Xab7KQ1lZe9XBPbk33berB5tTELIPRdaOrfU4TXCklTPSNFMoxjV8gzABtG8K35/qwc54BTp6ZX5Nyv5VzJjANOuezs9DaGbgELYHLmoQ/l2puMcXfq+rtTAL0Q04AjjwMQQzi1OvcH+Q4w/AaXJNEkaA3GHFP76er5mCN8DbQw9boJRsfb+S2wV0fwQIF9HOr5U9PVohp/KN95qE2M4ukuT8JDDJAksPgvic1G/9YUC0QKXSVfgI19yon2xymkrwnnk0T+1eaau+f3uSmYCZX3f/I8gEVSskeYASbxv+TchZUaTI3WX30F1qdX/RCbflj1+6xc+/hGhdZHqrxPzIhZYpWamDmAC935xEA4GmkUHHCepdjcGAq7m1+C5Ndfmg2PvXF4Y9WH8tWzqY+RKcZPchBNoWgEmJV7UbHd29jxYZTQorZoQBji4RRRBuaFZ2DGcILWfpFON3Z/aAvs1TE7cGC75R5U82IDuxxYOasAhP1KAvAa/0M6/8yA4vUpd8r3OjGE5Ka2m8yPzrlUOURSAoHJxdPOXuWEIiFvUTl8MAhqvlRU4AFO0rEQqc1BxyQoSSzlHFtfGIxXnvQFEMwHuRcwMBsQZnLvxrvPkUZlLRJdPfYzm7pWt8XYOTQAn8cc86n9EPwStBKqcEtsXmAk6YFQu/r59enLFzVMxtjrs7GkbXBAlv67Ts4Bpvp7KT+cYtyNlbafnPLtKXdOKREFDO96Kftqd2+BvfqSLxZgu+0uwU3aUePEnBFJZHKQPKSvifTOAooRlLfLMc6rdr32D6fez6ZOCXHxRJtwHGI80/+Yc2Lal009mV4kNSmDMAYOLI42E/IzUoChOMbYyhxtjmE6PNnUZ8ThhUE8N4U0xSisKUdPzT3Z1RhUTWenDz8yHmCnYxIAyjQpRBvBFLT5xJQfTKFFiLnzGajvHPJdKA2gaAD6Q/L3aCcHhQeZKKQ3SU9zMN6FSGLaAImNSWKyCJA5L2Xz1cwEYxCe01+a12vzfIBfmN1EVzY//MQnGoKLqPXIXxwe+hZEc07MTfqzzgla7jdX72IDdhn6XQCcE4uk6EkdqvTtGmC7BVBK8KhtZ6onCci5xaNNqgghASXHFklekktqANXzZSc/OOqumMG0KIWZ4F6Imky9LP0U1rqVuikmMDWUek4qonNT7pN2FpO7egli3cUTbj05dZ+HH9Ak0WB0JfTAXuWgAkRmCp8EgO2kUNfZu7SROhRGqh9OwdAwQKDjZHxF+gP87cYUHHfATdWmFegThoUul+sO5zraC2ZTzCTOvOfn3BNyTpjS2PgcaLcTrlzXfp5zzjndtrbA3ts4H9rEK15e4BRH2RDckjFsdQTgNZFOPNTs8IGal3lmDtVZYiYrYN03BWhIfyovdfZ7Uth+HFTq8EY7zxFEwpFupHdt29MkSF7hKFIaYEkmtihgWkBCZZXgQsLXRHXlvLugjE0z0U/POeChQcwRkAEUpnHc1JgDG7dXXUcd955oCEyHt0+1uR/ABmrUa2sL7GOvb7fjfHEBm2QhlXm4C5nEMsU4y4StpFpS73rEhqYiFzLpgIXdB+hAa832/TMAwPmGCSZEUgIiO5vqTCoDPcCyE6+Y66+ea9m0b02pVesr5zdVFUMhQUk6EvaFOjUBbKdIesyJuUBS0j6YBKQevwDgU9OL5HTNcdE+AHvl/gcF7AJqN9wCezoR5hIBtpeyRmoDElsYyQTj8AJIWx3xCJPmpJbvpESPxJSLgwl4SwIJEP9gHnwA4Sb2ytg4ZmATRcReJ6VX8rXrAc19qM/6CeRLu7nMALt+liunvY9JXSDGzA4kXn0iA7vY0LXZVQZoC+wTENhnnkkoTdIXZjLL5tpJofqS3ILi9ZPwkrNz5xwx0kxXpHqtru0C2N3OtqCoJttlch9bO2FS1HtMgbaxK9or6HZzk73eY+6645XYrVNsC+yL3ugmc/YSk9ilm1MAHyUajyybul4aydkF9AsbezeTWN2DBHbpS+5xxwxu0TqE6+r49cZd3ivoNr5BKpZ71GEj1/e0KnXUl3x0EMCecoptgX2SARvI7nlPGvYyjcCuc645nqSK8pQfl0raArtIhw2deksdnZjcVG/e92ul0CgsKsGgdk0XB7Af//hjS8/XhVlK56f6VI/fbiV2AfRUOGgL7JMQ2F7ahOT++LxosWXxU4Del8SGMnkAulVZyuReYyqsA6jgauFWPPhH1l0wdf4ggV0AXe59vMCun8H49drrqYjeQ3knW2CvZ7AnhSpev8gemDadbCZV7WjpAaUMyFlnnXXK4x73uFNOPfXU2X/62CO4JW9wmAmVvSP3tH57No1y7hkPAtgtoA8C2KXNVgtqgd1K6i2wL4XA7qnlmwK7drZMOQ7a4+uAXSbnLgHOe76TImz1qNyzbHM0KbT1fcoMmAN2sYk3MSGmwFx3atOx3iuz0c/6HdTx6K3EvuhNrHsP+ySxmYl8V7Im/3Nf4tjtZKo72qrl6x6yltSl3f0GtnZ3AW5htRek8Mjb0mkqHDd0t4CzTOyehCtjAJyY0dxzttdvAuiDlNhT3Kx+r4VR1J/tdVsb+6IR2QdgW4shp0P2IWDfcTfAZh/zatsPnHrK4SXRwuII6Y5D+mKtevXU8nXAbsMi2twlsC36oC7rz2vWqcU1CGbAXhZKSFyZpN7ihXoMioSrTYx1wC43KwDfBbDFyzn5jMecc/LUjO+/pi6GheOX3W6E9Hbmnrc+d5IAW4IQDWzIWRhJyFUqLmfuQM18u1KeTQpzbwxz6pCtrq2ya9cJXHSD8Q8hJ8by6rnell2XTVvCp0d69aYwkLqEjRBy2eTD5S+aA7YJIYdazjIxz8aUSGKSl5RM4OEllr+sU69K556eTghhtamhg5RsgdaGZNY4aNz7QSm2N/rf3MeuIwsJGpBcP8eklMouM9DPTXtSNeU7r1A9WPV9dyHNlzbeq2+wThVV1/03BXav/2uOWeQiK65e873yTnLeQhPjZWLLLSjbOS0l4Mzdq31nJ6jENnftoFP/ZVR5LLvhWCTT25LKIp65v6SVQSlX3zjuhaxrKGvw5ffrxwpNCaiMdb1lV7luZwrYpLM0RymTuyUPKMf6kWnjfTOcptvuDLBtrXQkRTpnISuv3jje4yoByd/n++lNw7+QYzzxS9Tep5U46+xcDKmWxr3251RR9Q8Y2OsmZOmysJ3ccIk/tff/3Py2vdNaOkmAbSGPSMwUWVv/iHaa5LflvJjkFM1uKb128I4tuClr/Lt/hjGjdX5F5pBr6kVSbnn+odNPP713b+uPpVkeD/1tOvTNufFiJdAmjXUe4rojMC1R5MAqJCWTym0jRMdsdNh7cf7Wx9rpJTV6HbA1OJXQUTpwggN74ORzfRyfw+o12XQmB4lW1o5vJLGnGPE6jeVitrFpLdbF2xlmiqy6s0CojniYX9TsFjh1G0A/7FG3F8o4PTljYd0CmvuXm17zZYVjK2ReNQXsI2lFdtWuqZ5I+f5X6fThNHI8CSi1FKn78/60PQB7nFzUIWuyWxKqsgxyVlVqJXbvwVuQnMDA5ujbSZHbvo5sqGCfdCmydqspab6XJmBvJKjyPq1B+O1Ky7Ra8NjSsz7J+afad029dQOf8xYhUb/LevvdAJuvwBLmen19ueV5PWCzYy3UWFrA30zismmgjvV2HK2f6Tb5MacCrXt+e4/1/uVjIbHHBkxmudxUm5q8HB7DWTqZgL2BefOxeR7J+9Z703R6ZBJZWWaTC+r4o3KNTQtL3UsTsM1B48DvwskEwL15W+83bxxunfG4dcbFpiBlE0rHSXEq+k6K7bT2+mcPZ+VSW3gV2g2wr59+vWhiLjyiB2x7YP1KLvjLXEh1seKqJZLRGmbroG1uYLUWh1aPbC5Q9jubqLJ8uGEiZdug9tr3p16tinNEXfHCCy+0swmHBG5KzVwbd9bwyQTsMpHmBnN8HjYjG7BHdmp57HiCjW2i1jn8lyZgL57fnBm92D31msCyaUf7Jxd+0/oKMV3MrQUV02LunTTneOjtcFPvL0eln7Pn6yakY9v8siWc+QY9YNcrqKZ2F20XQYij2cZoZd1ZBvJojnPJT4WteNiFHxaiogE2L/hdU9jJ9ctoJfbgYQ6w3aq7CqwzCDQOO6NafSYKMGwJnJckFrhCB6CKn5Y+e5G0ow/nvswGnuzZWHmvbxPHSKCexDZImJ891jBnGpWtkRaUvpwzSv3Z250kNvbwDGNfj+TZ5szMp+W8nXlKfZLauvkiuIydKAIVuh6vjXPzx/dtzMsfRZR2eMfth1cIJhahuUb4TGGTNvFVUza2hm0dJKfbi1+i0W4+z43Gm1lnbKOEntS2ZVGrHvNwW1tt80D30XlxU3uRPS1tDpsOVGqGRRgmfW0ztjZ23UdtGnxgfUNKu4unl2VFGY2ErVKT0IdBt6FCcaRQ5a6XfqVLF7kLRqBjZsIkwkT+OICtb7fFop65yDjWq9doRexa4GrHDFOhpYhtHo9vgqNRCKsX3hl2ak0R3vLnBrWaWRwxOzlONfR8nE9HUphoSzQD7E/Kc9tU8kb5NIesCzDpzIfhf8qr+aNNe9TZyFG/ayYvzm6zxmFvtlxzl/y2M618CrYtjZBJsSnxQLfzsb72bWkfo/3gGIq8c37XG2vayZWDkWApdPnUuVn6ZjmyHXjMX2OMsdYMwFjSkixPFn5rSV5IWQqsrmcWrm2Zxmfmft6ted7SsKlmF9jjyxIT5kxZioPm91tGp9Ww1/dYl1QwuO12wqqwj8WWh+op1Ad2L7D2iDfdhOOhLeA2+YFjCdj5PajiTSO0hwenADY6kmLhRiE7reiP2PwcUU2/LX2xk8qT8szDAE+QvuG+mMX3d+r8dY7JB0DsO4Pfeyn1pWtV4TW29o3T954z0T1sUkhNl1y0KVmZV++RPimh0i/mj/HqbbSIWfIE/0T6V0cqmAEYcE84mDPGDIjL5hh1v4Uzbb64jow5oJUNKqfq3yN9O3ccX/esQ6w0U++wkJDg/VJqVb2c83yW+jKHzFPzn/QfkoBaDbDTGX8oMTxvw0Bvnr4tvYvq2uE9zUlsdnMNiHKtHT2Bo0gkWxQ9a2KwgN92QjgrZ4WXs4m9/do8CBW5xMG9dAMyBWxckt3DyTEAqAyaCZSfDx8HRl9M9k28xZo5N8WLmwJsGZOygaIJ+MDOC3tOjt0qfeGXEEZsmWVpp/2s7eCpOlPHqZtHJk7qB3sOkOoQ4uQ9RmkkZDRHHFOcSfdbw3S0QXJ6Z0UL+oTcwzte0RBzzLyRRdcDtbYwC2CRAbmgTh9IVfvuraOXm3+53vPoU80IqOn8OIjWeWyN7Dz5B5rDKaQ882fdHniltaP5MpixDcFQb/8CS4uZdh+ZAzaJVev7pW0PQn37sjz4vTIAJGRJuWw7UHNSquWKI2uGaz017d99BGQX2DnHEYITAp9NB3tUJx6ULYrreiQyFdFgt4DnRXX8jBRbM00RVZGGQBICY0t/nr7eJn0lLUrKZqlD2jMXTNpWWkkaUd8OprslIOxNCs+EwckYFLvdlFYkdufCTZNiyqX8J4NXOUS1ngK2DMfJWPI4RzB0mtGCOsCmqX3v3ANX89EY6dOQRTnSB8Y5x1yyPTaBtikxQWClNwe6beRebG4CrvVPGbdHtRflee+Yawan3hSwqVDsqR5gJZxQMU6beSKcyc6ehbPxrJto9cvxskgVNgXV3MRZonTU3+V4WeskNi4qWL9Cuf4haYfaaYCoNrW03MlvdjbgXC3Fi2q9kl6I60yI2oNZ7uU5XHeHFKCmIdTeZbalfwd5Q/pRTJJyrcUlzBi5yiQSbaiVWCXO3Hu8qWP1UtO2jknJx0EtfUr6ZQNHz9xKbrakOVBsbMy89RbXbRvfoyltGOnlad9e74c748cWZetjjGVl0joTZeqZvccp08M1Fkh4nt477LV5JAfZycKBhdi1Uj7hAuBLMk85bx6Z55g8P0pLfAOflbH4/IwJO7/179Bs7dOOzFNx6hK5qNsixNqsQFqL9zqYV1PA5sQoN+j079ihCWlrIB6QcwuHVR6i/NtG3da5VedwRqphO1C4Eim/TmJTTQB7xSufY0Vic26x3WuSay5sUAgwSd2a/BUQW/5deQ5q3HXb8/ltUps0Xga/RK3peJmATysQVlpQxXTKsZ75w9dg8rd7hM95YT87fZ1izK2N6N4m7Bc071NixuxGds04cDhK26zpD9PmnROp+HAcUddIn2TDLY1Bfg/vJ/Xsa35GvrNfexliHGpFM2puM/zzKR/HIlFklOJ1PWAiLQc/UM7bf52GwDHaI5qgMeQQLcR8wMDNSRpoTdo6nELwYU7G79ubOs/JbxmSJDC1ud2Ke2FTT/Rp6HqK/PRBkle0lBI7Bexex2futXQKp39MOi/wTrJfJg9BpWjtCgNUcmQ10LMJgQHnw22FolZs7FEVv3Lud3rO/2o+r9eoYBxpkviHyds8BPVaXnshE+po+6DazLGXpF1ht6KF1NVIMwzBM2JQZaUNCaf/pLA/Amg1IA6m2oSQUNI6RS7IfTlmSI8l6kzecn7ONPEMEnkK0aIApg29rHXeVW2QPEyK2iFKbdXv1wtBjh7mb8iz8DFYeVbUyyHEk98fGd9bz2RjPnBgYZDGtzhGSxc4r/hBFtQZm3rPOvWYeg9LPYzT/NqEeLPZtm1c27V22CHAChkTYC9ZZY5jztJWzeWerb1JgopMM+1etboXhsZkW4RJp4Bt8KXhtYRTUCdNVBzDIpFeSpvrgAmoqOxAtaSiZUBd+4LxZeJCwkv2Ga8JY6BecIqwQ7vOM22ML5JEqDmsttgjXrqHb21YNiGHl0FiCgCEkEVLpC4Nxv7jTIcWoE/P/e+QfrROtgG4OSdZnwNlSaPIMT4KKbM89KQdadXzaziH2y/RFLDTrvDMnTv12dW0jyHcNBImZHIfD7BJPfvG13TeqH7X9qH8ApO6DsF5x/q0M17c81MU56QqVGlOXX/v+9bxWYWV1hFmVnuzz8y1YvU9tbbXllAUE5VDmMpdz3vnjF8bVqW9CenWVJj5XoHNFMBY6ggUP4V5vqAesE1yun0vHCWU88vV9SY6V77UuJbEpm+YgfsgG6tznmPsNTmHE3N8GLReqMPA4NJsw3XAfm7qfH1zL//15b+1vAwTqyYMB9MgyVt7p66H6UhtRQDOVKnp/LR/49zHiywe1A/lGAeMl83D2lso4P4FbHNSg9OltvWGe08Am808Fb/2l8JtJiEV+HiB/fB0p/5DB91j+rSOKowNSNtkmMM5hhG4rgdsDtuVOVZJ/eZ1rPwEwlen/tVGhsYpSerTMnpaUq+9oznIGUmgEXA1k6aNkcTaralngj5yHKu9AltE40+rmwC53Am+nAUduuY1j/kSKg4uRZTDaIUyMEDW5qcCIy4si6slsUCOIwPREkDVakrvllQLktQgAOZUuKtca/21f5ms2+J8oj67vqjIvXu1xwCSTa3vnFrnjxWMASfN4ISrJhfTwyqdIs3Zl/6HWzWToQfsuX5grhgVW8z92W6bEIkve63nb5Azz1FZ035I7DbWq30OS3ZfS8az9VPwMBeJ3wO2ZB7vcYl6wJ5gdrfMe6DNFfKPriIqnHdMEe+55BlMjfEQNk1hQrZMljbW2svaIUnbfpdn2SuwW3OQhrkShuwBm9HfvnydpDKRbGWC1wPQUzmc59Em0ee8lXU7bBAcSBFuk0v7xryET8xLYFesBXbqtBKbikLaCu1w0k0RBwwbhy1roPTdsR5NhQJLXXaxiSLZxjG294oq3jSMkajDYWMSsj1bCbC4pGFetfRe2NediV9rHqWt/QA22/d29f3yfQHspq/GWD52/SzyIoQEHTsuYE+8L74UgCpkHYQkj/KbqbNuoVIxxzDvdlHRFLAlzdy3eQ9MPybgAOzm3CY2tnl1XspHcu335xloRivUAzZV6dqdSUFtEZIZ0j0rousDHVu4JdxZEF+b0jt7JNQDyOwktv2Sk2h82bPArvraU8WL84wEbGPI+oN5sL84raj7m9DDUmlJGjXj9cIM+KDyjv2nFnmu1jY3lqTdM6nxqbtRJlgL6uo+vookLNlb4wOVmHybFtoFdvoyOM969+oMUC9NU7Yeh1LNdGg53rV5VBNpBxxoBdjpy4rE1q91qnjpe+qZW8KK5Ro2+pGqA7zY5jDTskcY7JekHf6VuzHtmko7+c1PQPjV9Dv5wYyqqczHFYmd9gdgrxlzY8hO99dWz5uqW4B9+1Rg/FNJgKtN1NCxns3kONVE2tzAbtsJnkMkqEFr0/jYBGJ5QhBDWmj7oopalb4Z+J5XHFDfVl1HZW3j4TQGnlYT9YzlMR5+keYkWU0muzGYSgxhO/MbLByCjbQa1LZq0Nn2ANUyPxoKT6sQ1EDjhOWcLPnxSx2bfJEX7avVU3W1MSVVPCuveMv0emp7Z/iGQ73MvFemr+zYIW9+pBLdqHPTmRjmXvHozkrsVtWeA8F4zp8smn8LbS2/B5OyjPfYN/6jJc969bAvyjVy3ol4qm+bvWZVGL9ByzSFr5byItKEcBdHaE8V9+56ztOlcS9zbe7Zi/MMBxILxtVxhFy7CEeURjlHSDZ1AE08kiHP+zrl+ClxaPZTGy/kXGOXih0PMdrc80rpLFWSWvrM8SUKl7HFAam24yXBGMzzU8+GciYoptR6tUlX2zQZ4KOp19qe+sEhRr0pmV4cKryfh+uJ1AxkGbMyPuXzPblGmGdgCtU1Uym6PKo8s57Ps5IMUmMlr6yk3068zFNzjQQbmgEzopdYxJFDyzAJ31l1msbl3pw/A43vHiNcLouhAAAF/ElEQVTgRbZYYZ1/gAOU5lETEEgZrUOaxpWZUXt02at3quZbL9xVHE5dh+EacN81bS9ClKkrXXkIe1aCw09zmDaBabf04NQ9e7wPqY5RtIk01O4fqJ4DYyaMaj8Sc5BkZ+INmnFzI0zucAomR/q3Ghwz4Mtzj/emLzaE4CTtEmADBO7RJg60F5gQVEdAIIEmU/zGC0lS3mAg5R1ts65K+1RUE8dk5FQR+nl+rr3ZOJAWC9wj34VI6gmhH1RnDgoDLZ+YpGuZjIwcKs5t087fpJ3WcTM1NibmzXPNwjHRTCDaRj1pSzv+IH6RCFNd4wUb55XVVh0mqi2+DIxrE/UcIzk6jmEP1NrjfceUgYSdVxMmcrgzEBid+eFZnzE1UDlOu2GqtVENIUrMnxkk6oGBtosl7pbnxxSskGK2YQZtiIgPh5R7e+pigkve0Qlg8xSzR0UwagCZK5jyf6ct3nbLZcujGRuqcksLCZ8T5iAGIOxUE7DyM/x12uOFB3SOrppoBGLuaMovBdQ6RAuuE6ponvwWhd6Z+zAveo7pU2xmeCOdWX2W4z5yZto9Zxw0jICntuc57N3ofblW7NfE6GWM1deQyAAzN/F2cp40k8HFkdemPU497PPSD3FxTKTN18WRccw2GmCwaQ494pTknNyU6okwd4303ToMOVk3z0PVGyZDNaFJtHYS1m0wl9jFS4CqK6TdZ6e9Re5DxaxMVI5AajmtYqDxPO/0tXOdOrILmWRzi1KElUjUyX5UfaKBDZ7uCcZJbcZkhnc7kjnCLKn7gCEyFRb+l7T3wPS5ZY6auGAUHsxOAqomJonoyMvGcSeI5jL7+J4wJAyThuEdDJGYiqTrElSE7hKR2FO2Z1t3N79xPplkJEQh0odkqPOoJ9vMIDw6neY9ZANb+jlFVFyqJM/sFJEYJtaH0y5/gom8LieZiizdlLlQv/z6Hpww7l+I6uU5vZQp4lCSlLKgiYm3kwokGIfgUt224TzPlMRtq1JDh3FwogL2lPZRX3///GgTiOrzQIFpWmm3Cck0u2Ey0mT0qT+3CKS0VzL51gEb+AG0zVuo+7WS1DGebJ2PmBIzrp4DxexrIzBzz33/jP0wfuPzYrCDnT9DbHn34Cyrs9rKJcxFDEToeIkAWxolm+72KceC2nukdFxGk4wzKk6PSOxfT1nYc51K7G1pnvK4dXgq/FYuxUTYg9S8KcJtqYKAbWBxQoD03C3h0DK3cFQx5DnCmUmR4vFvE3imrhVecf+FWVCBm52FmRinldCieg19cZ6nddpM3fcZuX6RXFMBW6PuWat+dRvMJZoR9XCOrFCzumjhHJtgWlT8++T+T67+lmk/gS3F1/yZI9rFkOjRqPKflz7TxIp0bD3opU0ZlfwDQ872xHOWugAN2MPvcj9Az/eimvf6ilHeNIWDeuFgre43lRhzSp15xj7SCPXFhGUL6jyVc4rzkWrUJ9zxpemohBQOgjlia1rOScVlp1CLtaOTVET2T20anDHW7XFpI2VwOap+aGZwpR4KAV3Q/L8XO4yzjU2EoQhnCH9RfzYlzh4M0TNwBE7FvpfaS185ysRv2W/GHkNh/1Hjpb9uShizrKwpraK0Y6yEfZYmSHWTy6RP4qLGAzDLePAxAPRSZtNM50hLzFbSSW/eGF9q7F+Y4BWjoslhZF2Nbny3NC/Op3USu7cgpe6yZzs95S3lYA3u3GsRHst5zJePpkeYEQkvpDWkmDZzkIaBYQ7mV2d+2nTyobm3pDCSl/0OP3DAyQgLmKD34Vjrn+G76sexJ/YVLw9hkNlF0i1NPiDkgeVwoAYYGMAe4ndruFZvYK6Va2yh856U/8j3FVuhd9HUsbn7lxfX++O+3dxjv+ruYaz269aT7exzn3j3ZQGSgLaH4i85mrLwsDfA5ozF6MyxJeCO/dLMu3ONfIMl6mgx7ksgdYVBzmFSgLNghk0bEoqE+946ajiL3I1Gug/9SJ3TcpzHn82u/8BJzaZKDxGfsd7Kvn/js2GAhAMVn5DqCQcm3mNTgFx/xMhrZ9rSmPw/L+c7nIqISsUAAAAASUVORK5CYII="
                />
            </svg>

            <svg
                class="footer-logo footer-logo-3"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="148"
                height="42"
                viewBox="0 0 148 42"
            >
                <image
                    id="Layer_1214"
                    data-name="Layer 1214"
                    width="148"
                    height="42"
                    opacity="0.5"
                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJQAAAAqCAYAAABV/HLRAAAABHNCSVQICAgIfAhkiAAADblJREFUeF7t3AWwJUcVBuAN7u4QeEBwd4dCgwQPDmGB4BLcJbi7Q4DFgwUPhT+cwnWxABs0SPAECEH+b2t6a97UzB25d/N2H7er/nfvu9PT0/L36dPnnJ49Nm3adMrg1cHJggOCnwaLTDdLYU8OXhi8JvhvT+F75fqWYFtwz+CoRv7T5P93B5cPjmkp69/57V/V78fL54la8vj9L8F9gvcPaOxtkufFwYmD/7TkV4/yu+cpv5n8/olg3+CfA55ZshiX8wXXC64QXCA4Y3DC4M/BT4JvVGV/MZ9/GFF2W9aT58e9g1sFlwtOV7Wnbdy0Uz30zWOCY/fIn5Xga8Fpg48H+wfb5qxUuf3m+fKy4CzBluBeQV9naoxB/m1w6eqzXp0z5J/3Beeufqw31KCeOjhFdc3/v2lpi3arx6ODtwxo612S50nBCYL683w/fnCmQJkSov41qJOqXPtefr9pdX3AYzddNZlMKoOLzJKy/xZo20kCE0wdpC8F+vuQEc+o12PP/POs4LbVj54DXUKgtOsdyfPI4OhCqM/nn7NWhXwmn5sDzJ8nmYmvCE5fFXJQPu8X9BHq2smDMEcEVwx+11KJc+Q3hNKppbE+SadLBc8MEOulwZur+0vj/WuwSb5vVff0tRORLhGctHpmye95JI923Tr4WfCo4LDAPSV5Nvhdu/qSuj8oeGDg+x+DTwbG6UeBPjm2unbOfJIk1wguVBX8znw+PkDgock4IcY1A1LuTcFqYGJ3EUo/ura1quP2Rq4ERKVZbbCJuC8EZuUPhtamke8O+f9FVVm/yufZAsvdfatnzCp2CKFm3a9jdP7FgqsFn53YhjG36avXBu8JTCREm5pIO+pBkRKvy/ct1ZiUpbyt7AvmR9LvwYEyDPI9gs8NrMhzku8hgUlBFfjgwPvWZKsveUQ1VivMWv3V4M7Bd0cWfKfkf0lwqoCEUEES47gilBmrMy4a0Ds+OrL+U7Jbyl8evDfYL9CXUxJ91pJ1x+BPwSOCNwT/GFHYlZIXOa4cWGVuGdCxZiV6mYlHGt4/eOWI563JWicU6XSdgCi1Bl84+HJw1+A7Ax+AgEhEsbPEqZzOofQfV4Q6V56FUBcJrh98eGDd58l279yMCHQ/E4qyPCXR6Z4aIBOpR+JNSSu56Y0BHWw1oMsqsyvdLReM0bere+hpk1KdUMTpPgES0UOsoUhFYUeUPlLpAGSiZ6jcAwIzy87xBcGSULOHyK6V7mgHZ5WYLCWqx9D5PhTQjR8bIGpXemIuWJ0ODm43u5qzr3YRyl12WMStmY5UJNU3O4q7e0UaW1yK+EODst3fnQilPyj7tsJj9Md5JZRdmq23ct5e9XXTXDJlnOms1I/vB6T14R2FHJjfnxBQym0uJqdZhFIolr81sHuwIyLOfdYT/eH5gS0ssY9Mf69l2J0IxbxhVpPWtupdA9Ds8HkJdf4USHkm3S1Pi9L7mILoRsZPHbuk3uZcs6kwiehedpWTUh+hFHrxwPJn12QbSun8SvU0ZHlaQDLZmdABjm7UZHcilCXeJoS+Ycf06YG9Oi+hDKjdHMMnJXqWvjOwSjuyMTg+JSAYrDJtCv5Kfkc8y6Olb9byOPP5QwilAKRiz7Fz0uHEo52BhzO4PS9Q8bbK7k6E0iYS2Ay9RcDuMyTNQ6j6cvfsPOzhQx44Ig+7EqJSV0ySLqlb9CgKud2lXevoNJRQCr5k8PoAuazvxDPD1jMCBGtzg7hvSajZw8IwypV0w4BV/FWjR3H2DSaJiWF8mFHs5NoSk4XxteSyo32swq8bmXGGMZM5yEZtzfI4hlDKpVNR1JGKlfrpAWbPMrgtCTV7wEl4OhMjLBNLsezPvmv41ZVkXQ3oU8xCdvFdSR7LI10ZwfoSaW4C0L+2681jCeUepOLX4jciorskU6nM/wOhym6K/asYJfsGo1xHqEODawVML1uG3jgwn10r/chzSCg79r6kLpZHTmmbrbpDnHSyOnF/sflJbF5cRUdOIVRfZZrXj2tCnScVsFOzc2JXG+NCmKpDMeByNZE0dodjDJt8fqSS7bpNDam/yERV4Ur7RdUfY8wh6mZJbjrEmVX0M5sVmxlpxlx0wEYk1JnTsA8E7GhXD4b6sgziVEJdNveS2gyTPAS8DUOTMbChEeIj8oGU6pP6Q8uWz/JFTRFJIgznyDE3D8hLOotQQLJ9NiKhbBT4s9iULCV1m1hf/0wllHI5pfnwZumTXc8vO7FtyXCDgCFyEYmEQVJS0+aJBOyLRxv7XP1NwnJmH7QRCTW2Q+r55yHUPM9FRtKNUVFcEWf6IhLJKRzJpLKk2rntjERv5FrbuiTU2u5dL0KpBfeVHZPtOBPC2CiPJlFIJ3FRlGtkJaUWuZTWn2f3+DbEXRJq1yGUGCbWbDssGwm6z2QXSO7lEKaXsbqzvjNu7qykfDu9w5eE2nUIpSZ0PyEryMXzL2KzLYS5jxjuozMxFYgg9X1nJo5tUasHLwm1tptFPXJRjHW9LHKwzHZLn8jZ1YCh0Q5tSFpJJvYgW3lLnkgDUZg7a6lTJ8uzXSQ9cN8lodYOk2jPHwZiqm8SFCf4kMFcZB72M/5RhkXx48wgwlrYk5o2LsTh1KYrUbz5W7lOOHgp901n/aLqybjJDGFZZeQUlLlfIdTX8w8W3zhgAV9kIn6FtzDPmzl9hxQoeOWQglDktkMKpX4MauK1GN/mieNWHmswyy9dwPbfUsGqXE6bzNsnnMAcr3xpQ+xUpCU96PaBcUIMjl336xPjJcxaPoTiNpFc57QXPTq0T9wviNK5grZjYvW2MzuwnvOYsPUh9GrAfratEEpYCvsJ07wDC4tMRK4YZ2JR0Hwfoa6bPB8JOCVZeZ266Epmh07fXRKnOgfwUH+dwWKj2lx9WgZNHuNWEnIql6NWuSTZWOPlw3IPC305jjWkPxHPc4wrF9x2XU/FBKaLxWHpFCz3yyGljciDIM8N6AXW9L7E9yRQD6Eoel0iW92FGbPU6ohFGexKOfVB66vzkOvKc8aNVdwSNjbpFxKBy8NZPOOlPC4VUskqM0TytT2XZHZSyZnHvjK0g1DAk9VgzVGt0mmWDhWc99RpW2U9Q5w0RXeoFZkoJ9L7Tnuw0upos7ZPVI8dwEXnV1cE+PmiC96Vylv0LNyV2rasyzr0wJJQ69DpG/mRS0Jt5NFdh7YtCbUOnb6RH7kk1EYe3XVo25JQ69DpG/mRS0Jt5NFdh7ZNJRSbyq5u91lEd+qfRRlM6/UZUi5j7VDXySLaOk8ZO9rjixdVcToyJPIYCzLzKp82FwkD6N6B9z25zk3T9b4DeR0UUJbEystF0BYk7y0h3gvF+uptKWONf57FH+VZTUsvZ6mIxR+39Jj7tN/rblicm4mxV0iJNnLKys+HxTo8xr1hAgqR1bcSp7Pw5CZhjIcjanyY/GXqtBpMMTiruz7nm+2aFGfPNUZnbptmv7nG+cu/xxLv7GAz8SN6H5Z6699DfBHJp8JCJHznwORLa0og1muhngihg5n/kctgOEjYrLRjzV7QwK0jMe2zluvIZnJNpCJS61CuieYBw5bbdvzE58VPKEKR76+8utCAcVqqXxuRuTEeF3AIdwWgcZjrJw5rxF8JxGmPldClP7iSTBoErfdZmdzeQ2AsBMbpi72qdv1+Vge0XPNKJa8N8D6DrrpeJteQ4l1B0ythIol2EEKsrk1Sqy8/I3fNatWW7ceoBHMJ1cBSBRjwttnn5asGvE4IhSGDAWk+0CEBUqc4QrEdoZC1mbDcewSQwXdvvRt6DLyU5ej44QHp4VkONPru3UfezdD0CZIapJOQFYNnELlGmomk2ByY6Wb8FAlayuTXJKXb2kbq62PRmtpREoc9cniZ2RgSe9+ESAUTumvp5Hy3ipgsTUKRksbQmT7Xmm+D0X8mmOdoj7odVXQop1aJZGxuvl2lNGz/fEEGkqSe/G52e1taPbURysC1vQDMs3mrLU2WLnnGOqkF5COH5USgv1lnyUAC8c7NwRDP4wy/d14aTMTvOrOmTmK+hYRMDaXV1yQ20pr1zWRpNsDqWvd5Cs8hSUQR9Pk262XOSyj9KXjOOz2pIIjVTASGgxU4s71dhVC8zW7WmK712nJoGaoXbPbqaOGqzbglIl5sE+klkTyW0zZCkXLnDUQ+bAm6zt+3tGnHT+4VYIaIRf/jOCap2vQ8A0hkrwY6z32kVNts1k/iupBuVjjNrPr1EWrP3IxwQoDrfWmym5wC2MYo6QhloopD60qzJJQXoFmx9J/4sObLZrVH3ehgJrFJcESdUA5FErddhLI0qiBCYSQdin6hg53UbUoACiyieIGqJdQytBq0neYgmi25KwHlsE3P6uqU+u/eTSVmyMs7bhRYyr3NpKl/aDed61OBM3DaJuLRstJGGAoufYSEmodQIjHNZH3STHZ1lkQTUX+qszcJWnosSc2Voa8/EMpyb0IjoiWrOUYkH52N7tmUfpYzQXva3JaKDqVvHM+itx5TCEUXItZFKLbpEaVASwoxZ7mQiEOD0hWzTOl1cJEksyOEtllGAaTvqI/vdp1TTnzoBIQyAHZUBsT3pv6k/trh6HiRZqSVJY/+1kwG24SzizRbpyRtIxE8b2tHAQbnKoFlTlJvy+NhEx5Ix7F6aGuZ9M0+Xck1uzn6YTO0iG5F6uPELB2MVLXykPCH/g8XBdbo+fuPOgAAAABJRU5ErkJggg=="
                />
            </svg>

            <svg
                class="footer-logo footer-logo-4"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                width="248"
                height="46"
                viewBox="0 0 248 46"
            >
                <image
                    id="Layer_1218"
                    data-name="Layer 1218"
                    width="248"
                    height="46"
                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPgAAAAuCAYAAAAFttQ5AAAABHNCSVQICAgIfAhkiAAAGx1JREFUeF7t3QWULEcVBmCCu7sursFdN0CCBXd/uLv74u4QPDw0aAhOCJCHBHcILou7u/N/j76cPn16dmZ3Z3Ym2a5z7pl5M93VVbfu/1+pmn17HH744UcZ0/bI96eO3DJys4h/vyjyssjfxt08fD9oYNDA/DSwxwQAP06Gd6PIfSIXaIb6jrzePvKz+Q19ePKggUED4zQwCcCPnU7uEHlU5GRNh9/L690i7xu8+DgVD98PGpifBiYBuNFdLLJ/5HzNUH+X11dFHhH5beRYkRNHThhBCMeIHK259t95/XvkT5FfRdw7tEEDgwa2QAOTAvzkGcuTIzeNCNm1L0WE7T+JCN3PHTlr5BSRE0SOGZGv/6MB9Q/z+vnIxyI/ivyi+W4Lpjk8YtDA9tTApACnnctHnhbhzTXA/WbzXugO+Dz50SPlvUur/2qu/0tefxn5dORtkQ9Efr49VT/MetDA7DWwHoCfN8N5bmSvKQwL4L8beXvk+ZFvT6HPoYtBA4MGOhqYBOBC7ms0ctm82jKbVvtjOnptZL/IF6bV6dDPoIFBA//TwDiAXyTX2Pu+ceR0M1LaX9Pv6yMPigzbbjNS8tDt9tTAKICfKOq4XOTekctEVMZn2X6fzp8YcYDmN7N80ND3oIHtpIE+gJ8kCrhjI2fm5bdIIars9tYV39bbVOzPGVHo+0pE6D+0QQPbXgNdgNvHvksDtDNssXb+2XhxlXoefT3ttrmYCPdfEjkwoso/tEED21oDbYDb6rpV5F6RM81BKwB+UOQxEXvskzQe+6oRdQI1AtV5W3APi/x5kg6GawYNHJk1UACXYzvE4jjqPMBNx/9pwOl03METKt2W3cMj74mo9p8+ckjkNRGn54Y2aGA7asCpUvJXAD9q3jjE8ujmdV4KcaT1I5GVyKETDMKpOTn7hSIPjSCo40fuEXEs9ojU1BBEMHQwSTteLnKgyDzdN42mP21a/U1jTEfUPmBKLes8zRp9Oa9b6XCcKr1K5NgAbiCOofp12FYV1PoWjgLshxvLTydY2TvlmutGXhdxVPZsESG+n7H2NUoXJZBRjZHTAaAJ9ydp+q2Te8CxVv/d/twLrAxBYfD7kT+s8VDPOWnkkhEp1Rcj0pluvcFvAczDHMbNw3XYXkFVczrRWNYzj0n0NO6ath6NeVKyG9dv+3tzLSKb1TM8z5reJLISOSxy54jfbGxVO00e9ODIvgAuh+W9L75VTx/xHAUy4H5qZJwHZpB3j1wwsqt5/+bm3rYiAcKCnjbihzJfizg11zUe15w9cu0I5Xwn8q5IHcUdpRppAf05vuuHNG9tnlHAWstIee2rRa4XASapkZ/hIqg+Y7B1ec3IDSJ+sOO8v9TKQgK6BiTmeteICMeOhDMGo7wyIpDmXKu5z78dODog0veHAuipjiRbI2u2FhEYj3u8GsOocZjHFRtB9J+KvD+yVrEVWOtoNIJz31pjOW6+R4w8G92LFqVz6y3o5paxzXyWI9b3sxE/zJoFYY0aCN08JPJAAAcqRSr56zybxX9D5HGRr44ZyL75/uYRlX5GJgSyh173mSBAAJ7DOk7gnTLSl9+7FnAUGBmMBUcG74wo+Om/29xjW86PbQDpxxFGbGG/HgGOb0Q+F+nzoIhHBCKtQDp+dmuMP4jQwWrrgZ51joif7CJhP+7h6a8Q4c15CoVFTRRjF4QhXyrypua+vl/wGYM/4mHeCM2PgJAb0nLfh1tj8Naz9o441YhgPxixW+HIcV8DbLq/aETqhCz99qDIy7wA0rogJHNTGPW5dV2rFkPXdHLD5pWu3xKh9z6QG6907uoR27Hm7rnPi9D3LBoCtivFnrpRmTlWxIIwkY+GpEoqiqz5rCeiukT6kbaeFcDfnTeMyyLMs5kA74p5GFi3VeEA+z4wgokZEYBIL+o8OwOVl5sTb3bhCE//0Qiv7xdt7eYgj6o7QnhBxK/cXhqxMNdv+u+OBYDuGwFykYMw7NYRgOFNeSBjU+wDlDZJWFiGpjgoFeGBzQGA/Grv1xE/yKlm7Iydp3cI6MURY7NrgEw8Q/Sg0cnOiN/rm4/iI322n8+g6Kj69axHRmrMPqcDfVRDYsiIl+UtkbEjy6INuurml6fKZ5eOXCciKvA9g/9khOf0oyP6YdiIip6tzxsjSPkJEQXfJzXPaw1lNzj3idAHkLIHxMYbm0dbdwjbGrle5LMaeWFEtOZ3FaIbNtEFIKJGdojbXy2iS+Kk5bi/YgRH1gYR90UH+qVjr8bPrgt7+qZb64UI2YJX/Vh7InJChO1dIjrWD3sXCTrHov/XAjjWk5TPM//O43c3g3dk1Q9QyhANXHRxrubVvy0YJTLsV0R2RfwqDUiAW8jpfvk5JQpD9dutzlMsgDI2gOPFeD4hFfZ1ks9Z+WoU6Xug4Zl48Fc3z2QsZ4xgTmTD2FYjyAOAq5mLcQGRZ671N7P2zPcMXRrAu+sXKTA8c0Es1RAH7y3FYRTG5UzAtyIWm97oAhgQFOLTp/67pNfqdneEYk68N+I1L8bl+auR+0faEQLw3CIiwvpM5JkRIHxshGfh+c0DuM2Pzp4deXoE8EVNCJxHNjbeuZrxIxn6B/TbRYwdMSBqUY6oQhMFGLM6jTXjrUWHAHTPyFMi6hfWuK1HQBZa0w/Chwv6V6MwdtFWX1TXPHY3Yd0mwq52RdrkAXwiN2TD5vTD5j2DmBO9tOsEdG09EQYBerbD3vVNt+ox9AjHCNI9HMH+AI7x6i+11CDn9co7AKxwW8gmxBOKMU6DNiGLbDIUYbK8gkYpGFvjfV4eET4+IOKXcEI0Hq8almYgDtYwMEbM+B2ZBULfAxJDEBIDN2C4B5FIbYR3Ff75s1Y8sJD3QxHehxdjTBXuWkCe0DOf1bz25aUWjfEDK3JCOMYFSHRULJ63u4lMoZQR20E4f0R4J8J4bwQRMTqfGx89niVi3Io/CGxUGkEfQKY28ZwIgHi2/qyROgWw8Tz6A2DzLU/PyyI3Bi+6WYr4BSFvba0AB1EDoRDe9YhYGA0MQCJCMT5jl0rSrzGoG+2MiA78MRLPUAOxzqILZAf0Cl7A49/Gay7IxlpYU7k+ILMt4JaCiAg1kcRyhL0ZL70iNFFIX07NJtkFDwrI7KM8LRtE6CJL9mCs7NFzK/xmw+zOmop4yiPTkfciL88wn3p+OWbkSG9qKNJLev8zgGMHE1uEZqJA6w9CMBreTV7N0wiPfC8UpgQKsEjAVDmoyaoA83K8BAbnDYTNAIf5qlE047F4FhgzY1bAEhJ7vnuB2X0VBumLd3Q9w2OUwjxhFJbn1Q+N8NwMEugYkWaReVU5PoO3yO1m/KIK43bgyKLyzCILobwF1HxmPAwZiQj59f2VCG/kx0EiEECgJ4UexkSP+rAtuiMCFN1cW/+M6HIRc0EwPDddWBdEAuyIA5jpjuEhPpETUjY+OkOa5kqHAAxcyFTqYK5IEREBEhukLxHH4yPmoNZiDkAAqMjNjgl9ALoIzg4EkkEWq5FdEc6BDRh35fjARgcIxNg8Q1/WAckiTzoDZGSCsFzHjnhDtna/iPVHtuoW3Ya8RSJsWNrhee7zHPeILkWnnJi16CPWdp/GYkyVz3v1GbH+HAGgiwTYfWGnUrbdvyYzAYy+KM1gMeQrI9hIiFYhEeMXagn9fHelCMOwwBis3ZCABWFUjNACV3Ve2MmoKFzuyQsIYy00o9jZdKR/IsKhQIsq12uHjRhdlCB8svj6ZaSMW6iKkBiksQtvXet5OyIHNM+pF8ADEuTDqI0b0SAerA6cFhGALLxcF7iAhVfk8Rg2ArpyhHFV2MtIq8AlEhFFyJ93dsYAHAgDYIBTFGMeIiXgBnRhONAiE2EufRK2ZFxI0n2IZTlivnTAE61EFEU1docIKxdFUABAj9YDwOWrgIBcFC3p03cIAAB5TCT98ab/2+aVbQj1hdTIyjOBjINg/PJv11sTHpunp1P6dY11EIXQJUJgOwDF87MnpI3g2030Yr3MRWRkR0SjOx7d5/J/0UZfbt7pbjr/BHDexcIxoEVogGLRMH23CTN5wP0iCIDXkENRYjePtLCAr0K+Ejmo6Yxx8Oa8JE9t/ryAYgzi4NG6FUvMiWR4kG4DaM9hgLyR5yAKY+MlGBPj5ukZjvc8M7Lh6UUmSMrzAQMxiUgQDfLRGBvgMxKA1T/mRgKM0vUADtBFKu4FGGurf8RYueZS3jNSXt98eRoeEnABQT/SG5/J66QTiKbybYYqjUEU7AZoAEa6J3dFnOX9eBk6B9JPRISPSEoTkhoDgNGvKvtq8129ABaPhWA1nlvkAOzmLgoALGnMSoQ9SG2kFnSF/MxLWmFeCLoKsvUMY/QM+qRr6YdoTBQFkFUUtAbsT3GRDZkH0qq8G6nSOVIwbvNajiiO6bOveFxjmMkrgDNCoZpFWITG8CmDp+iGMJQv1KFcRRhhkPfyLgvXBqZFw/bCZ8bPAChdGCjslAvK47Ap9iae1wX3OJ0oJjEyUYfFLy/JcHlxoR5wLEUAmr55LmSgcAgcQM3AGAdvwdC7hRxA0hfiQjQMnuF5X+PWH+PmXUUK5m0rSZ/yP8+uxjuvRBiyMQMb/QgdhZBSH96Tvq2JMWru8R39ytHdo+bBiM3JPf5tDDypuchDjd/nfSTp2fQ+LmT1fCR3k4hw2H2ITDQiXUH8yM9zvK/UyH3ADex9NQ/fV1NnoDv2gaTbBUSFO87HvNU3rBcCZJeKXsZvfaydqE90tSuCgEQt67Wt9rg29B7AsYxBe12ERgmqopRbrG1cFGdReSigPjiCMVVpGS7AA1I1i4ldsbCwyqILyfTDgIWxFmWzjXcn2L9dMWVQCkX0arGFnYSXMzZGwtO1cz1gN6Y+EEwyzqVcJG9ejrwugnSEskJuXoVeqxmD8fHYQn3eSJ7sVVQhrObBeUxemTG/u7lmFBB35nugE8VYG/0Zh+hmmo2+RRCeZayiH0RuTs+ISIWkUzx9XzFsrbFIGZCeyAtBtRsngJw5DqkMICMA0aPIxHufq4vQY0VRxjiXBuAMXq5KFqXYpghDiW0AyrNUQqtgIbRUCFLUsBgHRirHbiuTEavSCu95J7mT3LDv2lksAs/Hy7XBX89BAjwhME3ivcaNz3PkwgpydOPfwCUikp/3jQEojHHUGEYRWHcsjJ/XW428PCIPVauwfYhcZ9nYwHUiAA1oioAiFI6LI1hPQ3aczFrkLzpxnfUTnbi2nJH1RNrAv2W59qgJ1q/JGEXlbthr3k1xBMDbhiFPRkIMzsLJl5cjFpcXB/DakuiO34II4bCqwsmRvSEzoTJdyTfl/e1Qc9rzRxIqzyIExKt4J7RXmANwdZVZtMqv1QQQpIq49RWxqBcgN3a0Xi8+i7HOpc/278HlNgpS8lrhFfafV6ttENXoasJZeTlPI8+U06iyyr/kl1XcmteYF+25vIvGuGed+3mWyEokJVJAttIghVD77N0djmnpCsClHg6mvKZ5pjRNJIr4hcq+m3UEMa35TL2f7l90AWrgtlC8+rwagGPjdrXT/qrF5IkAXPiHnVcit4+0CyrzGvd2fa4Q37baXSPALV9XfVbRru2iWehGVAbYqulIXgqmAb4UwZbZUyLvimxVSjaLeW64z76/yUZpAM6TU9Q8miotgH+/9XAA58HlOsI/4FfttAOgMm7bZFQzj/JoVXH2mcq13FFlVWFrPZ7O/e7t01G7Ku95Gw0R9U2699fn9Xzjdk33dBN9dOdUulCg0sx9VGW55jZOL4qX1oBzQM68qGKfomgdQlpjeTb0lbWzbWW/W7QgTasiq3EbC4Dz3rZcZ5UmbGjwW3VTH8B5ceC2qa/SO4+mMKJ62y502HaSVzmW6RXQVyI+V3FvHz7pjtk9clJGweBs6ai8OkCyFEEOTpXxAOZfxq8fwCfAxFMBrDTBlokqvTxXoVLFmZdwnX1uBz4cF1RJtSfsHkBRhCFVKS+QFskUSI1DPUT/cklbWWXAqvKKPNIqRcQ6VKIKb4615Skf9Vzj8+p+9Qj7tvQBCMhUtb3mWOMyfmRvTuMq++Zuq1DKZI7Whs6BvO/EVz7edKN/25DqDSrn1q9NVIps7MJ1io7tdG/TDz+idDDqzybbv7PVIMxpG/tWzIuB21px+qcdVgEXjyBMl3czVHvQQkKnn2w/9TVGeoeIrTKGbj9cFf1Kkb0igEQUgxRrPJdxmLd0QIgpv3e6zH470DoV5uQSr2XbBGAZl+0QIGPc9n0dIAFG/dgLXW36ByxAWGquda9DIG+LOGCiAbYQ10kr1W99yWkVF43R9hfQOmACpPa8rdudIheL2DZ0vS0un7lG//qzdYgYNAUxczdOaZlwVnSkqMkDIgn76O1oqrn1/y+iI0DjRVXveXFRlrSpu5/fvXej/7ZG1p2H9izr0o40ECCA8/L0ZYty27W1/uMD+3mq1ctbrBXGDGjYuRs6YmMHEYAdiIRd9sTtt44KM0Uj+lPl1VR2eRWnm7QvRRg9A5EC2B9n6D53mMG2GvDI+xkKb/mICMD5dx24QARACuBLEWPkeaURgOfkmN0B4aQ+GKDz3EjBczzDPnMVhMwPYNQZqtmGYszSEqSFAJGdwpbxIDEEpqjFex7QvLpnV3OdQipyoy/eHrj16zN6qANAClUOi9h5eHRkrRTI+JCcZyIXAHf6bRpbf/rua84X2P7jDOipm2OLQOhJqkdX48Y/6jlH6M/H/c8m9p0BiLK2qgmlGK1TYX25K8/iJBMDdfRPQWdUAzKRCK8PALyiEJS353XsUwKlY6QMWaEGGQixEQlQu14a8LIIr60BRR12sOcrMqArYSLPLb0Aat5Wn6q5qrpOdTnaKiSWIgAV7yiPlGK0ScpYHbpAEHXKS1QhHDUv5ALICIkcHkE2jB7AeF+kBZyIiwfjmZEKvTqcYV7G/9WIgyK2lzSezxgVqKQQ0jWpxrgmVDdGJL3RusO4Z9T39r3pEnkhtW5DULZ+Ebz98Tr/Pmn/R4rrxgFcDs5bCUW3IlRnyPJvgORB+4o7wlvewnfjCmPCZwZwlQggAWId5nESCnHxoAz90MZgGAsP7rCN89tAyfAZk5yYHpxwcnjGv4XeCAGQhIzCfh5FxICAbB/xNkJm+f+OCCIAcDkr4CEM0s5XAVwUI380Rt8JSxGSHNec6MHes6KoUN2RSc+UcgC1LSrhOZJj4PpQfJIyAK9zA4CslkBPxgmYwnbhNaLS10rTf14WohkTAkOyCNQadZuUA+HRk5oNgt92bRzAKUSozhguH+mrGE9TafI1ns6iTaPxuKIBwOAJhcBCOVs6KxFAZtDIBIBFA3JQp+YQgiOwtmB4AMYu/OaZVY2FfgACGPboeXvRzo4IYuDFhI9yXUYGXEJzfQMkMPEuPCNiEKa3K728IZAiWFGNEN64RRqKdtaFx0eGnuNzYb8w2XjlzsjH/FYiIgThPxIQuXimEB1RAoPP6UlToxB9nCOCRJCMAyyL0hC1rVF1iFFjo2eOydogyvaW66LMY+bjmATgQjS5nZyK0mbVGJqQUljK2222ISMg4k2NW2GJ52XcCk/CTt5VY+wKWq5TjJL/Ihlz5p3VIhCcsHYpArS+RwyIg9dGTMJ6obpnIgZABhLVekYJkEAGlIyTAfKyD4kAd/sMulBf2PyoCECrHah+q2j796UiimyeR19VzEJaioLAijxECFIa5IVgliM8+M6ICAcJArqUxVzpzS6D5/jMuMxR5LAozRgRLOAiwPYZ+xojYAvReXvkW3vkizKHLRnHJAA3EAaggswQq1g1iwHyOCuNcW22f0bAeIHUmPWrwssDW2zglhszAOeXhXG8rlBXGC+0VaQCZh4ZuNyn0IeMeGtGz1s6LSU3FqbbeQBw1XC59VJEOOk+DVAV3ABzR0QhC/l0i4TybN8zYGeb1Q3sCAjvAVD4CYC8vMKdLTBzEVUI921bAadCnxSERwba5YjcXYUdkdFJramdCSkIr0+M6bAI3SDHRWp7ZzD7R9RGEHD3tJy0Si1BYVONQoSz7dqkAKcYHkX4xzgZ2rSbSjgvKkSeVpMjA54tLossxD1XhGfjxRAAIGmIgLHIPe0vIzNeec8IL8szClMBfimiGOda4FWZBhhAVbl1sMK2Ek+tkiyMFDVo7gFu3mdHxDafgpnxVDHNdfTNQI2DZ9WkCE6L0b/0ARg9w48rEJBUQE6taqwIBdCKb0DsVQ2A51db2RVBBvJvpGfOnikq8b7WmK6MF0EsUhMZ0bNUpi/CoHNREsKVcmz0F3qLNOd1j2U9ANe5ohSj47WW1v200TcwXCGziu40f4EDAMYKDDwcYxCmqjIrYgnhLb5TcryzEBtghLf2jHlPwObBFbqEy6rQXhXheBDh7w0jgOs9z6FIBTw8n+LdSkS4yMiQCIPk7REm4MnjhdKrEQU0DfEgCwUyYPO55yEGz7AXz7MjH6kAj2auPLaDLsAqMpF768P4PQfpISIhvRBfWoRc7E7w6OZm/sar8eCul54sUlMbQGIiKSmKMFzdQUp5yYjIBrk6KzGrvfhF0kfvWNYLcJ0AiZyc0WDRzRbenNBipAxr1GGVzSiy/mjBPumE8SsY8krYXZFLzs/z8XI8ggIa4AtXVfQZNi/uXqGw63h2ng6wgFv64lqeWHVcP/J5IJZ2uIa+5M4M0Z65rTneBagU4BipQluFksJvKQRD9SqyQYIA698rEXUBwFdFtl9vZ4C3FVrTqa3GpcjOiPRD1RnRqTmIXgBbhIPwkJwxK0q6R/4vNTskIlVBEIvW1DWM15rIw83ZZ4gauXtVnNy2bSMApyzegVIxqFxPFXYjTS7JE9m7lbPOojFSYbmCEvDIJRm7z3hnHophe28/GOEAHe8IPFXZ5n3d73MgVo1fjSxFeD/vkQHdCMcRBK8s9XAPMuQxVXM93zOExwgFSSKQbqENmQqnhcz6d6/cWPShvzpFJyoxdh4LoQE4ItCn5zh8YvyIjTcThZivyMkYvYrOgENxkM7UDIAEKZn3qINE+WquDakpVppjeWp6UGh0aMf7bds2CnAKY5SMhGfkAXkE2zSTNl7LnjOvOCtwjxuLOch7vdb7vr33SftpX1f9jbt3M99X9DStMW9mLPO8l90hLbUKRGTPH3HV2f15jm2uz94MwGvgciHeBMhVpoGe9+IN5JFCTQbIG7pWfsgwVZgVtnjxoQ0a2KwGRBvybw3IRTrbvk0D4KVEWzTCPOGrIo1qs9BdyErhqspCS3vGPlMN3rXtV2BQwKCBGWrgv8Fo/Ac62febAAAAAElFTkSuQmCC"
                />
            </svg>
        </div>

        <div class="row footer-row">
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-contact">
                    <h3 class="footer-title">Rootix</h3>
                    <h5>info@rootix.io</h5>
                    {{--                    <h5>648-145-8360</h5>--}}
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-sign-in">
                    <h3 class="footer-title">Subscribe</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> Register </a>
                        </li>
                        <li>
                            <a href="{{route('login.form')}}"> Login </a>
                        </li>
                        <li>
                            <a href="#"> Faq </a>
                        </li>

                        <li>
                            <a href="#"> Contact Us </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-about-us">
                    <h3 class="footer-title">About Us</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> About Rootix </a>
                        </li>
                        <li>
                            <a href="#"> Our Team </a>
                        </li>
                        <li>
                            <a href="#"> Career Opportunities </a>
                        </li>
                        <li>
                            <a href="#"> User Comments </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
                <div class="footer-quick-access">
                    <h3 class="footer-title">Fast Access</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> How Rootix Work </a>
                        </li>
                        <li>
                            <a href="#"> Rootix Feature </a>
                        </li>
                        <li>
                            <a href="#"> Rootix News</a>
                        </li>
                        <li>
                            <a href="#"> Prices </a>
                        </li>
                        <li>
                            <a href="#"> Report Bug </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr/>

        <div class="footer-bottom">
            <div class="social-networks">
                <a href="#" class="social-network instagram">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="18"
                        viewBox="0 0 18 18"
                    >
                        <image
                            id="instagram_copy"
                            data-name="instagram copy"
                            width="18"
                            height="18"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABHNCSVQICAgIfAhkiAAAAaNJREFUOE+dlD0ohVEYx12fyVcYFDZiM2Bj8rExKZOPKHV9LAYp3A9yFxkpJEooMZFFschoMEgRE2IgRWLQ9fvXOfW6vfd6eerX85xznvffeZ/znOOLRqNJxlrxo1BnJ37xZ6zPwrryfEZohHgGvmAXniBZ6y5iysmDJsiHeRiSUAPBISxDn8fd2LQQQRj8EjonKIcMjyLaaRVcwCdcQ4mEVKRtaI8jlM58Kryb9WL8HXTABkxBwAotMOiPESoyiY1mXjvohFOoNztSLccgYoVWGfQ6hHKIbyEX5uADhiHFiJw4csPEoXhCByw2QxncmI/S8C8mzvIqpLqtQXfM7/YwXgEdjoosS7gjCbm1gw5kCyrh0ovQsalFKV4nZO2BoADUKvZKJNyROvbZfB3Bv8EEqDY6xSOH+A+hRRb8jkWFFbAJNWb+Ht8FugVOG2cwbU9NjaUGc7NCJtWQj3HWg8xPSkg1yAT9+39MDVotoTaCHalC+I9KuuRLELTPiJ6CAbiCPVDjqYvdTM9INujVqIV9aLFC+mAQAqA75sVeSdIbNq3kb1Ymla7Wdx0PAAAAAElFTkSuQmCC"
                        />
                    </svg>
                </a>
                <a href="#" class="social-network twitter">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="14"
                        viewBox="0 0 18 14"
                    >
                        <image
                            id="twitter_copy"
                            data-name="twitter copy"
                            width="18"
                            height="14"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAOCAYAAAAi2ky3AAAABHNCSVQICAgIfAhkiAAAAUBJREFUKFNj/P//PxMDBPyD0rioEKBEHRDLAfFTIG4F4rVAHATEDxiBBmkCGauBOAqIL+EwJRoovgSL3HOg2B8gTgUZZAdkHATiv0AcAcRr0DQIAPmvgJgVhyUguWyQQbJAxiMkRRuA7HYgPgUVEwTS7/B4ewJQrgRkEEhNBhBPBmIWJA0gb54EYlDYpQIxLCzRzQQFzQ2QQcYgpwGxNxCL4bEZlxRI/zmQQTJAxmMyDABp+QjEUkD8Dea1SCBnGRmGLQDqSQTpgxkEYncBcSwQS5BgoANQLSjGUQwCJbAqEgw5DVRrBlOP7CJhoOA2ZEkChoKSzRNsBsHEHIGM+UAsj8cgZ6DcPmR5kIskgQKgaGcHYnsgDgBiKxyGgBJmGBDvRZeH5bUyoEQoEHNDFXwF0siZ+BOQvwiIe4AYayoHAAP+W7zpyrnuAAAAAElFTkSuQmCC"
                        />
                    </svg>
                </a>
                <a href="#" class="social-network face-book">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="8"
                        height="18"
                        viewBox="0 0 8 18"
                    >
                        <image
                            id="facebook-logo_copy"
                            data-name="facebook-logo copy"
                            width="8"
                            height="18"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAASCAYAAABmQp92AAAABHNCSVQICAgIfAhkiAAAALFJREFUKFNj/P//PwMSSAeyg4HYEIjZgfgLI5KCEqBAN7JqEBtZwTsgXxBNwS+YAkWgxD0kyfNAdgqyFUpAzl0kBW1AdjXMCh4gwwCIDyMpmA9k54OcALLiIZAhAsRcSAp+A9k/gPgrSAGKP7H5AmSCMBBzYzHhM8gEkBv0gPgokoIFQHYecjjIAzkPkBRMRlegDxS4gKRgNpCdhmzCYFBgDHTQGSRHLgeyo5AdqYsrHACDnVzHH+anLgAAAABJRU5ErkJggg=="
                        />
                    </svg>
                </a>
            </div>

            <h5>© 2018 Rootix All Right Reserved</h5>
        </div>
    </div>
</footer>
<!-- Footer Ends -->
@include("templates.landing_page.layout.footer")
@yield('script')
</body>
</html>
