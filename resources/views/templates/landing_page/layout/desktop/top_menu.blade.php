{{--<header class="header about-header">--}}
{{--    <div class="container">--}}
{{--        <nav class="nav">--}}
            <div>
                <a href="#" class="logo">
                    <img src="{{asset('theme/landing/images/logo.png')}}" alt="لوگو"/>
                </a>
                <ul>
                    <li><a href="{{route('landing.home')}}"> Home </a></li>
                    <li><a href="{{route('landing.about')}}"> About Us </a></li>
                    <li><a href="{{route('landing.services')}}"> Services </a></li>
                    <li><a href="#"> Prices </a></li>
                    <li><a href="{{route('landing.faq')}}">Faq</a></li>
                    <li><a href="#"> Educations </a></li>
                    <li><a href="{{route('landing.blog')}}"> News </a></li>
                    <li><a href="#contact-us"> Contact Us </a></li>
                </ul>
            </div>

            <div class="login-container">
                <a href="#" class="language">
                    <div>
                        <img src="{{asset('theme/landing/images/flag.png')}}" alt="flag" />
                    </div>
                    <p>EN</p>
                    <i class="fas fa-angle-down"></i>
                </a>
                <a href="{{route('login.form')}}" class="login-btn">
                    Login
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="9"
                        viewBox="0 0 18 9"
                    >
                        <image
                            id="icons8-long_arrow_up_filled"
                            width="18"
                            height="9"
                            xlink:href="{{asset('theme/landing/images/login-right-arrow.png')}}"
                        />
                    </svg>
                </a>
            </div>
{{--        </nav>--}}
{{--        @yield('header')--}}
{{--    </div>--}}
{{--</header>--}}
