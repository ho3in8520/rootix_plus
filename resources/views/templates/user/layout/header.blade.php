<link rel="apple-touch-icon" href="{{ asset('theme/user/app-assets/images/ico/apple-icon-120.png') }}">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('theme/user/app-assets/images/ico/favicon.ico') }}">
<link href="{{ asset('theme/user/app-assets/images/fonts.googleapis.css') }}" rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/vendors/css/charts/apexcharts.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/vendors/css/extensions/tether-theme-arrows.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/vendors/css/extensions/tether.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/vendors/css/extensions/shepherd-theme-default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/bootstrap-extended.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/colors.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/components.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/themes/dark-layout.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/themes/semi-dark-layout.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/font/font-awesome/css/all.css') }}">

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/core/colors/palette-gradient.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/pages/dashboard-analytics.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/pages/card-analytics.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/plugins/tour/tour.min.css') }}">
<link rel="stylesheet" href="{{ asset("general/datepicker/bootstrap-datepicker.min.css") }}">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/app-assets/css/custom.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('theme/user/assets/css/style.css') }}">
<!-- END: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('theme/user/app-assets/css/pages/data-list-view.min.css')}}">

<link rel="stylesheet" href="{{ asset("general/select2/select2.min.css") }}">
<script>
    var address_user = '';
    var route_get_total_usdt = '';
    if ("{{isset($token) && !empty($token)}}") {
        address_user = "{{ (!empty($token))?$token['address']['hex']:''}}";

        var route_get_total_usdt = "{{route('get-total-usdt')}}";
        var csrf = "{{csrf_token()}}";
    }
</script>
<script type="module" src="{{asset('theme/user/assets/js/TronWeb.js')}}" ></script>
<script type="module" src="{{asset('theme/user/assets/js/getAmountUsdt.js')}}" ></script>
