
<!-- BEGIN: Vendor JS-->

<script src="{{ asset('theme/user/app-assets/vendors/js/vendors.min.js') }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('theme/user/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
<script src="{{ asset('theme/user/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
<script src="{{ asset('theme/user/app-assets/vendors/js/extensions/tether.min.js') }}"></script>
<script src="{{ asset('theme/user/app-assets/vendors/js/extensions/shepherd.min.js') }}"></script>
<script src="{{ asset('general/sweetalert/sweetalert.min.js')}}"></script>

<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('theme/user/app-assets/js/core/app-menu.min.js') }}"></script>
<script src="{{ asset('theme/user/app-assets/js/core/app.min.js') }}"></script>
<script src="{{ asset('theme/user/app-assets/js/scripts/components.min.js') }}"></script>
<script src="{{ asset('theme/user/app-assets/js/scripts/customizer.min.js') }}"></script>
<script src="{{ asset('theme/user/app-assets/js/scripts/footer.min.js') }}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{ asset('theme/user/app-assets/js/scripts/pages/dashboard-analytics.min.js') }}"></script>
<!-- END: Page JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('theme/user/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('theme/user/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('theme/user/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{asset('theme/user/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('theme/user/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}"></script>
<script src="{{asset('theme/user/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<!-- END: Page Vendor JS-->


<script src="{{  asset("general/datepicker/bootstrap-datepicker.min.js") }}"></script>
<script src="{{  asset("general/datepicker/bootstrap-datepicker.fa.min.js") }}"></script>

<script src="{{asset('theme/user/app-assets/js/scripts/ui/data-list-view.min.js')}}"></script>
<!-- END: Page JS-->
<script src="{{ asset("theme/landing/js/select2.min.js") }}"></script>
<script src="{{ asset('general/js/func.js') }}"></script>
<script src="{{ asset('general/js/project.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/scripts.js') }}"></script>
