<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed navbar-shadow navbar-brand-center">
    <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav flex-row">
        </ul>
    </div>
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                                class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                    class="ficon feather icon-menu"></i></a></li>
                    </ul>
                </div>
                <ul class="nav navbar-nav float-right">
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i
                                class="ficon feather icon-maximize"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i
                                class="ficon feather icon-search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i class="feather icon-search primary"></i></div>
                            <input class="input" type="text"
                                   placeholder="Search ..." tabindex="-1"
                                   data-search="template-list">
                            <div class="search-input-close"><i class="feather icon-x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#"
                                                                           data-toggle="dropdown"><i
                                class="ficon feather icon-bell"></i><span
                                class="badge badge-pill badge-primary badge-up">{{ countNotificationsNotView() }}</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header m-0 p-2">
                                    <h3 class="white">{{ countNotificationsNotView() }} notification </h3><span
                                        class="notification-title">new app</span>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                @if(countNotificationsNotView() >= 1)
                                    @foreach(getNotificationsNotView() as $item)
                                        <a class="d-flex justify-content-between"
                                           href="{{ route('notification.show',base64_encode($item->id)) }}">
                                            <div class="media d-flex align-items-start">
                                                <div class="media-left"><i
                                                        class="{{ $item->icon }} {{ $item->color }}"
                                                        style="font-size: 20px"></i></div>
                                                <div class="media-body">
                                                    <h6 class="{{ $item->color }} media-heading">
                                                        {{ $item->title }}
                                                    </h6>
                                                    <small
                                                        class="notification-text">{!! \Illuminate\Support\Str::limit($item->description,30) !!}
                                                    </small>
                                                </div>
                                                <small>
                                                    <time class="media-meta"
                                                          datetime="2015-06-11T18:29:20+08:00">{{ jdate_from_gregorian($item->created_at,'Y/m/d') }}
                                                    </time>
                                                </small>
                                            </div>
                                        </a>
                                    @endforeach
                                @else
                                    <div class="d-flex justify-content-between">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-body">
                                                <h6 class="media-heading text-center primary">
                                                    Nothing found!
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </li>
                            <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center"
                                                                href="{{ route('notification.index') }}">See all
                                    notifications</a></li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link"
                                                                   href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none"><span
                                    class="user-name text-bold-600">{{ auth()->user()->name }}</span><span
                                    class="user-status">Access</span></div>
                            <span><img class="round"
                                       src="{{ auth()->user()->avatar }}"
                                       alt="avatar" height="40" width="40"></span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('user.profile.form') }}"><i
                                    class="feather icon-user"></i>Profile</a>
                            <a class="dropdown-item" href="{{ route('change_pass.index') }}"><i
                                    class="feather icon-user"></i>Change pass</a>


                            <a class="dropdown-item" href="{{route('setting.form')}}"><i
                                    class="feather icon-settings"></i>Settings</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"><i
                                    class="feather icon-power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="horizontal-menu-wrapper">
    <div
        class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-without-dd-arrow navbar-shadow menu-border"
        role="navigation" data-menu="menu-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                            class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                            class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                            data-ticon="icon-disc"></i></a></li>
            </ul>
        </div>
        <!-- Horizontal menu content-->
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <!-- include ../../../includes/mixins-->
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('user.dashboard') }}">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('banks.index') }}">Banks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('my-wallet.index') }}">My wallet</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('setting.form') }}">Settings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ticket.index') }}"> Tickets <span
                            class="count badge badge-danger">{{$number_ticket_user}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('transactions.index') }}">Transactions List</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('user.shares.index')}}" class="nav-link">Shares</a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- END: Main Menu-->
