<header class="light-bb">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="exchange-light.html"><img src="{{ asset('theme/user/assets/img/logo-dark.png') }}" alt="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#headerMenu"
                aria-controls="headerMenu" aria-expanded="false" aria-label="Toggle navigation">
            <i class="icon ion-md-menu"></i>
        </button>

        <div class="collapse navbar-collapse" id="headerMenu">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">{{__('attributes.userMenu.minePage')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('user.dashboard') }}">{{__('attributes.userMenu.dashboard')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('swap.show',['unit'=>'usdt']) }}">{{__('attributes.userMenu.exchange')}}</a>
                </li>
            </ul>
            <ul class="navbar-nav mr-auto">
                <li class="nav-item header-custom-icon">
                    <a class="nav-link" href="#" id="clickFullscreen">
                        <i class="icon ion-md-expand"></i>
                    </a>
                </li>
                <li class="nav-item dropdown header-custom-icon">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="icon ion-md-notifications"></i>
                        <span class="circle-pulse"></span>
                    </a>
                    <div class="dropdown-menu">
                        <div class="dropdown-header d-flex align-items-center justify-content-between">
                            <p class="mb-0 font-weight-medium">6 New Notifications</p>
                            <a href="#!" class="text-muted">{{__('attributes.userMenu.clear')}}</a>
                        </div>
                        <div class="dropdown-body">
                            <a href="#!" class="dropdown-item">
                                <div class="icon">
                                    <i class="icon ion-md-lock"></i>
                                </div>
                                <div class="content">
                                    <p>{{__('attributes.userMenu.changePassword')}}</p>
                                    <p class="sub-text text-muted">5 sec ago</p>
                                </div>
                            </a>
                            <a href="#!" class="dropdown-item">
                                <div class="icon">
                                    <i class="icon ion-md-alert"></i>
                                </div>
                                <div class="content">
                                    <p>{{__('attributes.userMenu.securityIssue')}}</p>
                                    <p class="sub-text text-muted">10 min ago</p>
                                </div>
                            </a>
                            <a href="#!" class="dropdown-item">
                                <div class="icon">
                                    <i class="icon ion-logo-android"></i>
                                </div>
                                <div class="content">
                                    <p>{{__('attributes.userMenu.downloadApp')}}</p>
                                    <p class="sub-text text-muted">1 hrs ago</p>
                                </div>
                            </a>
                            <a href="#!" class="dropdown-item">
                                <div class="icon">
                                    <i class="icon ion-logo-bitcoin"></i>
                                </div>
                                <div class="content">
                                    <p>Bitcoin price is high now</p>
                                    <p class="sub-text text-muted">2 hrs ago</p>
                                </div>
                            </a>
                            <a href="#!" class="dropdown-item">
                                <div class="icon">
                                    <i class="icon ion-logo-usd"></i>
                                </div>
                                <div class="content">
                                    <p>Payment completed</p>
                                    <p class="sub-text text-muted">4 hrs ago</p>
                                </div>
                            </a>
                        </div>
                        <div class="dropdown-footer d-flex align-items-center justify-content-center">
                            <a href="#!">{{__('attributes.userMenu.viewAll')}}</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown header-img-icon">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <img src="{{ asset("theme/user/assets/img/avatar.svg") }}" alt="avatar">
                    </a>
                    <div class="dropdown-menu">
                        <div class="dropdown-header d-flex flex-column align-items-center">
                            <div class="figure mb-3">
                                <img src="{{ asset("theme/user/assets/img/avatar.svg") }}" alt="">
                            </div>
                            <div class="info text-center">
                                <p class="name font-weight-bold mb-0">{{ auth()->user()->first_name . ' ' . auth()->user()->last_name }}</p>
                                <p class="email text-muted mb-3">{{ auth()->user()->email }}</p>
                            </div>
                        </div>
                        <div class="dropdown-body">
                            <ul class="profile-nav">
                                <li class="nav-item">
                                    <a href="{{ route('user.profile.form') }}" class="nav-link">
                                        <i class="icon ion-md-person"></i>
                                        <span>{{__('attributes.userMenu.profile')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('change_pass.index') }}" class="nav-link">
                                        <i class="icon ion-md-person"></i>
                                        <span>{{__('attributes.userMenu.changePass')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="settings-light.html" class="nav-link">
                                        <i class="icon ion-md-settings"></i>
                                        <span>{{__('attributes.userMenu.settings')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item" id="changeThemeLight">
                                    <a href="#!" class="nav-link">
                                        <i class="icon ion-md-sunny"></i>
                                        <span>{{__('attributes.userMenu.theme')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('logout') }}" class="nav-link red">
                                        <i class="icon ion-md-power"></i>
                                        <span>{{__('attributes.userMenu.logOut')}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
