<script src="{{ asset('theme/user/assets/js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/popper.min.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('general/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/amcharts-core.min.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/TronWeb.js') }}"></script>
<script type="module" src="{{ asset('theme/user/assets/js/withdrawController.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/amcharts.min.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/jquery.mCustomScrollbar.js') }}"></script>
<script src="{{ asset('theme/user/assets/js/custom.js') }}"></script>
<script src="{{  asset("general/select2/select2.min.js") }}"></script>
<script src="{{  asset("general/datepicker/bootstrap-datepicker.min.js") }}"></script>
<script src="{{  asset("general/datepicker/bootstrap-datepicker.fa.min.js") }}"></script>

<script src="{{ asset('general/js/func.js') }}"></script>
<script src="{{ asset('general/js/project.js') }}"></script>
<script>
    var address_user = '';
    var route_get_total_usdt = '';
    if ("{{isset($token) && !empty($token)}}") {
        address_user = "{{ (!empty($token))?$token['address']['hex']:''}}";

        var route_get_total_usdt = "{{route('get_total_usdt')}}";
        var csrf = "{{csrf_token()}}";
    }
</script>
<script type="module" src="{{ asset('theme/user/assets/js/getAmountUsdt.js') }}"></script>
<livewire:scripts/>
<script src="{{ asset('general/js/project-wire.js') }}"></script>
