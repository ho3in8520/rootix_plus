<link rel="stylesheet" href="{{ asset('theme/user/assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset("theme/landing/css/font-awesome.min.css") }}">
<link rel="stylesheet" href="{{ asset("general/select2/select2.min.css") }}">
<link rel="stylesheet" href="{{ asset("general/datepicker/bootstrap-datepicker.min.css") }}">
<livewire:styles />
