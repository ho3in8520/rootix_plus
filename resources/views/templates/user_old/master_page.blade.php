<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title_browser')</title>
    <link rel="icon" href="assets/img/favicon.png" type="image/x-icon">
    @include('templates.user.layout.header')
    @yield('style')

    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="0d388766-bc01-4ba9-bb6b-b79f274e0180";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>

<body>
@include("templates.user.layout.top_menu")
<div class="container-fluid mtb15 no-fluid">
    @yield('content')
</div>

@include('templates.user.layout.footer')
<script>
    $('tbody, .market-news ul').mCustomScrollbar({
        theme: 'minimal',
    });
    $(document).on("change", "select.state", function () {
        var id = $(this).val();

        $.get("{{route('get-cities')}}", {id: id}, function (data) {
            $("select.city").html(JSON.parse(data));
        })
    });
</script>
@yield('script')
</body>

</html>
