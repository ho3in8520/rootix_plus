<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>@yield('title_browser')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    @include('templates.admin.layout.header')
    @yield('style')
</head>

<body id="app-container" class="menu-default show-spinner" >
@include('templates.admin.layout.topbar')
<div class="menu">
    @include('templates.admin.layout.leftmenu')
</div>

<main>
    <div class="container-fluid">
        <div class="col-12">
            <h1>@yield('title_browser')</h1>
            @yield('after-title')
            <div class="separator"></div>
        </div>
        @include('general.flash_message')
        @yield('content')
    </div>
</main>

@include('templates.admin.layout.footer')
<script>
    $(document).on("change", "select.state", function () {
        var id = $(this).val();

        $.get("{{route('get-cities')}}", {id: id}, function (data) {
            $("select.city").html(JSON.parse(data));
        })
    });
</script>
@yield('script')

</body>

</html>
