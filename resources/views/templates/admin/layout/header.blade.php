<link rel="stylesheet" href="{{asset('theme/admin/font/iconsmind-s/css/iconsminds.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/font/simple-line-icons/css/simple-line-icons.css')}}" />

<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/bootstrap.rtl.only.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/\fontawesome/all.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/fullcalendar.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/dataTables.bootstrap4.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/datatables.responsive.bootstrap4.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/select2.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/select2-bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/dropzone.min.css')}}" />
{{--<link rel="stylesheet" href="{{ asset("theme/admin/css/vendor/select2-bootstrap.min.css") }}" />--}}
<link rel="stylesheet" href="{{asset('theme/admin/css/persian-datepicker.min.css')}}" />
<link href="{{ asset('theme/plugins/bootstrap-iconpicker/css/bootstrap-iconpicker.css') }}">
<link rel="stylesheet" href="{{ asset('theme/admin/css/vendor/bootstrap-tagsinput.css') }}">
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/perfect-scrollbar.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/glide.core.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/bootstrap-stars.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/nouislider.min.css')}}" />
{{--<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/bootstrap-datepicker3.min.css')}}" />--}}
<link rel="stylesheet" href="{{asset('theme/admin/css/vendor/component-custom-switch.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/multi-select.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/main.css')}}" />
<link rel="stylesheet" href="{{asset('theme/admin/css/style.css')}}" />
<style>
    .swal-overlay,.swal-modal ,.swal-title , .rtl {
        text-align: center;
    }
    /*.datepicker-container{*/
    /*    top: 292px !important;*/
    /*}*/
</style>

