<div class="main-menu">
    <div class="scroll">
        <ul class="list-unstyled">
            <li>
                <a href="#dashboard">
                    <i class="iconsminds-shop-4"></i>
                    <span>پیشخوان</span>
                </a>
            </li>
            <li>
                <a href="#users">
                    <i class="iconsminds-digital-drawing"></i>
                    <span>کاربران</span>
                </a>
            </li>
            <li>
                <a href="#invest_shares">
                    <i class="iconsminds-coins"></i>
                    <span>Invest & Shares</span>
                </a>
            </li>
            <li>
                <a href="#swap_withdraw">
                    <i class="iconsminds-digital-drawing"></i>
                    <span>سواپ و برداشت ها</span>
                </a>
            </li>
            <li>
                <a href="#reports">
                    <i class="iconsminds-digital-drawing"></i>
                    <span>گزارشات سامانه</span>
                </a>
            </li>
            <li>
                <a href="{{route('admin.bank.index')}}">
                    <i class="glyph-icon iconsminds-bank"></i>
                    <span>بانک</span>
                </a>
            </li>
            <li>
                <a href="{{route('notifications.index')}}">
                    <i class="simple-icon-note"></i>پیغام ها
                </a>
            </li>
            <li>
                <a href="{{route('ticket.admin.index')}}">
                    <span class="btn btn-danger count ml-1">{{$number_ticket_admin}}</span>
                    <i class="simple-icon-credit-card"></i>
                    <span>تیکت</span>
                </a>
            </li>
            <li>
                <a href="#posts">
                    <i class="iconsminds-digital-drawing"></i>
                    <span>مقالات</span>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="sub-menu">
    <div class="scroll">
        <ul class="list-unstyled" data-link="dashboard">
            <li class="active">
                <a href="{{ route('currencies.index') }}">
                    <i class="iconsminds-dollar-sign-2"></i> <span class="d-inline-block">ارزها</span>
                </a>
            </li>
        </ul>
        <ul class="list-unstyled" data-link="users" id="users">
            <li>
                <a href="#" data-toggle="collapse" data-target="#collapseAuthorization" aria-expanded="true"
                   aria-controls="collapseAuthorization" class="rotate-arrow-icon opacity-50">
                    <i claکss="simple-icon-arrow-down"></i> <span class="d-inline-block">احراز هویت</span>
                </a>
                <div id="collapseAuthorization" class="collapse show">
                    <ul class="list-unstyled inner-level-menu">
                        <li>
                            <a href="{{route('user.index')}}">
                                <i class="simple-icon-user-following"></i> <span
                                    class="d-inline-block">کاربران</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="list-unstyled" data-link="invest_shares" id="invest_shares">
            <li>
                <a href="#" data-toggle="collapse" data-target="#collapseAuthorization" aria-expanded="true"
                   aria-controls="collapseAuthorization" class="rotate-arrow-icon opacity-50">
                    <i claکss="simple-icon-arrow-down"></i> <span class="d-inline-block">Invest & Shares</span>
                </a>
                <div id="collapseAuthorization" class="collapse show">
                    <ul class="list-unstyled inner-level-menu">
                        {{--<li>
                            <a href="{{ route('admin.invest_shares.index',['type'=>'invest']) }}" class="waves-effect">
                                <i class="iconsminds-coins"></i>
                                <span> Invest </span>
                            </a>
                        </li>--}}
                        <li>
                            <a href="{{ route('admin.invest_shares.index',['type'=>'shares']) }}" class="waves-effect">
                                <i class="iconsminds-coins"></i>
                                <span> Shares </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="list-unstyled" data-link="swap_withdraw" id="swap_withdraw">
            <li>
                <a href="#" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true"
                   aria-controls="collapseProduct" class="rotate-arrow-icon opacity-50">
                    <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">برداشت</span>
                </a>
                <div id="collapseProduct" class="collapse show">
                    <ul class="list-unstyled inner-level-menu">
                        <li>
                            <a href="{{route('manage-withdraw-swap-page')}}">
                                <i class="simple-icon-credit-card"></i> <span class="d-inline-block">سواپ و برداشت ها</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('manage-withdraw-swap-page')}}">
                                <i class="simple-icon-credit-card"></i> <span class="d-inline-block">لیست تراکنش ها</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="list-unstyled" data-link="reports" id="reports">
            <li>
                <a href="#" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true"
                   aria-controls="collapseProduct" class="rotate-arrow-icon opacity-50">
                    <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">واریز و برداشت</span>
                </a>
                <div id="collapseProduct" class="collapse show">
                    <ul class="list-unstyled inner-level-menu">
                        <li>
                            <a href="{{route('admin.report.index')}}">
                                <i class="simple-icon-credit-card"></i> <span class="d-inline-block">لیست تراکنش ها</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="list-unstyled" data-link="posts" id="posts">
            <li>
                <a href="#" data-toggle="collapse" data-target="#collapseAuthorization" aria-expanded="true"
                   aria-controls="collapseAuthorization" class="rotate-arrow-icon opacity-50">
                    <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">مقالات</span>
                </a>
                <div id="collapseAuthorization" class="collapse show">
                    <ul class="list-unstyled inner-level-menu">
                        <li>
                            <a href="{{route('categories.index')}}">
                                <i class="simple-icon-docs"></i> <span
                                    class="d-inline-block">دسته بندی</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('posts.index')}}">
                                <i class="simple-icon-list"></i> <span
                                    class="d-inline-block">لیست مقالات</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
