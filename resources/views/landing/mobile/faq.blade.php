@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    سوالات متداول
@endsection
@section('header')
    <div>

        <div class="about-header-info-res">
            <h5>سوالی دارید؟</h5>
            <h1>از ما بپرسید</h1>
        </div>

        <form action="#" class="faqs-search__search-box">
            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 17.302 17.302">
                <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(-4 -4)">
                    <path id="Path_8028" data-name="Path 8028" d="M18.807,11.653A7.153,7.153,0,1,1,11.653,4.5,7.153,7.153,0,0,1,18.807,11.653Z" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                    <path id="Path_8029" data-name="Path 8029" d="M28.865,28.865l-3.89-3.89" transform="translate(-8.27 -8.27)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                </g>
            </svg>

            <input type="search" placeholder="چه سوالی دارید؟" />
        </form>
    </div>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container faqs-container">
            <div class="row">
                <div class="col-12">
                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">کریپتوکارنسی چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">بلاک چین و دفتر کل توزیع شده چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">وضعیت قانونی بیت کوین و ارز های دیجیتال</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">ریسک های سرمایه گذاری</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">توصیه های قبل از سرمایه گذاری</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">کیف پول و انواع آن</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">خرید و فروش ارز های دیجیتال</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">تحلیل بازار و ابزار های آن</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای
                            کاربردی می باشد. کتابهای زیادی در شصت و سه درصد
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="res-form-container">
                        <div>
                            <h2>جواب سوالت رو پیدا نکردی؟</h2>
                            <p class="top-form-desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله
                                در ستون و

                            </p>
                        </div>

                        <div class="form-img">
                            <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="فرم" />
                        </div>

                        <form action="#" class="res-form">
                            <input type="text" placeholder="نام و نام خانوادگی...">
                            <input type="email" placeholder="آدرس ایمیل...">
                            <input type="text" placeholder="آدرس سایت...">
                            <textarea placeholder="متن پیام..."></textarea>
                            <button type="submit" class="res-form__btn btn light-blue-btn">
                                ارسال
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/faq.js')}}"></script>
@endsection
