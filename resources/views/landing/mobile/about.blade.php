@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    درباره ما
@endsection
@section('header')
    <div>

        <div class="about-header-info-res">
            <h5>درباره روتیکس</h5>
            <h1>شرکتی خلاق و کاربردی</h1>
        </div>

        <div class="about-header-image-res">
            <img src="{{asset('theme/landing/images/about-us/about-header-image.png')}}" alt="درباره ما">
        </div>

    </div>
    </div>
    <div class="go-down-container">
        <a href="#currency-table" class="go-down about-go-down-container">
            <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="به پایین رفتن"/>
        </a>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section class="history">
        <div class="container">
            <h3 class="history-title about-title-res">تاریخچه شرکت</h3>
            <div class="row items-center">
                <div class="col-12 col-lg-6">
                    <div class="company-history-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-1.png')}}" alt="تاریخچه شرکت">
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="history-descriptions">
                        <p class="about-top-desc">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                            از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و
                            برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                            کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا
                            با نرم افزارها شناخت بیشتری را برای طراحان</p>
                    </div>
                </div>
            </div>

            <hr class="rootix-company-line">

        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="about-currency">
        <div class="container">
            <div class="about-currency-title-res">
                <h5>درباره ارز دیجیتال</h5>
                <h2>
                    ارز دیجیتال
                    <br>
                    همیشه و همه جا به صرفه
                </h2>
            </div>
            <div class="row items-center">
                <div class="col-12 col-lg-7">
                    <div class="about-currency-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-2.png')}}" alt="تاریخچه شرکت">
                    </div>
                </div>

                <div class="col-12 col-lg-5">
                    <div class="about-currency-descriptions">
                        <h3>ارز دیجیتال چطوری به کسب و کارت کمک می کنه؟</h3>
                        <p class="about-top-desc">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده
                            از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و
                            برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                            کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا
                            با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان</p>
                        <div class="about-currency-btn-container">
                            <a href="#" class="about-btn about-currency-btn">
                                شروع خرید و فروش
                            </a>
                        </div>
                    </div>
                </div>


            </div>

            <hr class="rootix-company-line">

        </div>

        <img src="{{asset('theme/landing/images/about-us/left-background-sec-3.png')}}" alt="بک گراند راست"
             class="back-sec-3 left-back">
        <img src="{{asset('theme/landing/images/about-us/right-background-sec-3.png')}}" alt="بک گراند چپ"
             class="back-sec-3 right-back">
    </section>
    <!-- End Section Two -->

    <!-- Start Section Three -->
    <section class="rootix-team">
        <div class="container">
            <div class="team-info">
                <h2 class="about-title-res">اعضا تیم روتیکس</h2>
                <p class="about-top-desc">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از
                    طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و</p>
            </div>
            <div class="row">
                <div class="col-6 col-sm-4">
                    <div class="team-users-res__user-res team-users-res__user-res-2">
                        <div class="team-users-res__user-image">
                            <img src="{{asset('theme/landing/images/about-us/user-res-6.png')}}" alt="عضو روتیکس">
                        </div>
                        <div class="team-users-res__user-title">
                            <h4>حامد</h4>
                            <h5>توسعه دهنده</h5>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="team-users-res__user-res team-users-res__user-res-2">
                        <div class="team-users-res__user-image">
                            <img src="{{asset('theme/landing/images/about-us/user-res-2.png')}}" alt="عضو روتیکس">
                        </div>
                        <div class="team-users-res__user-title">
                            <h4>حسین</h4>
                            <h5>توسعه دهنده</h5>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="team-users-res__user-res team-users-res__user-res-2">
                        <div class="team-users-res__user-image">
                            <img src="{{asset('theme/landing/images/about-us/user-res-3.png')}}" alt="عضو روتیکس">
                        </div>
                        <div class="team-users-res__user-title">
                            <h4>محمد رضا</h4>
                            <h5>توسعه دهنده</h5>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="team-users-res__user-res">
                        <div class="team-users-res__user-image">
                            <img src="{{asset('theme/landing/images/about-us/user-res-4.png')}}" alt="عضو روتیکس">
                        </div>
                        <div class="team-users-res__user-title">
                            <h4>سجاد</h4>
                            <h5>توسعه دهنده</h5>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="team-users-res__user-res">
                        <div class="team-users-res__user-image">
                            <img src="{{asset('theme/landing/images/about-us/user-res-1.png')}}" alt="عضو روتیکس">
                        </div>
                        <div class="team-users-res__user-title">
                            <h4>حسین</h4>
                            <h5>توسعه دهنده</h5>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-4">
                    <div class="team-users-res__user-res">
                        <div class="team-users-res__user-image">
                            <img src="{{asset('theme/landing/images/about-us/user-res-5.png')}}" alt="عضو روتیکس">
                        </div>
                        <div class="team-users-res__user-title">
                            <h4>زهرا</h4>
                            <h5>توسعه دهنده</h5>
                        </div>
                    </div>
                </div>
            </div>

            <hr class="rootix-company-line">

        </div>
    </section>
    <!-- End Section Three -->


    <!-- Start Section Four -->
    <section class="rootix-company counter-row-2">
        <div class="container">
            <div class="company-title">
                <h2 class="about-title-res">یک شرکت قوی در زمینه ارز دیجیتال</h2>
                <p class="about-top-desc">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از
                    طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط
                    فعلی تکنولوژی مورد نیاز و کاربردهای</p>
            </div>
            <div class="row row-sec-5-four">
                <div class="col-12">
                    <div>
                        <button class="about-video">
                            <img src="{{asset('theme/landing/images/about-us/res-video.png')}}" alt="فیلم">
                            <video src="https://www.w3schools.com/tags/movie.mp4" controls class="self-video"></video>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row counter-row-1">
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>مشتری های پنل</h3>
                        <h5 id="about-customers">۲۵۶,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>تراکنش های روزانه</h3>
                        <h5 id="daily-transaction">۲۵۶,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-4">
                    <div class="about-counter">
                        <h3>تراکنش های ماهیانه</h3>
                        <h5 id="monthly-transaction">۲۵۶,000</h5>
                    </div>
                </div>
            </div>

            <hr class="rootix-company-line">
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="res-form-container">
                        <div>
                            <h2 class="about-title-res">با ما در ارتباط باشید</h2>
                            <p class="top-form-desc top-form-desc-2 about-top-desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله
                                در ستون و

                            </p>
                        </div>

                        <div class="form-img">
                            <img src="{{asset('theme/landing/images/about-us/Ai.png')}}" alt="فرم"/>
                        </div>

                        <form action="#" class="res-form">
                            <input type="text" placeholder="نام و نام خانوادگی...">
                            <input type="email" placeholder="آدرس ایمیل...">
                            <input type="text" placeholder="آدرس سایت...">
                            <textarea placeholder="متن پیام..."></textarea>
                            <button type="submit" class="res-form__btn btn light-blue-btn">
                                ارسال
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Five -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/about-counter.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/about-video-playr.js')}}"></script>
@endsection
