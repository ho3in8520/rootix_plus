@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
     مجله خبری
@endsection

@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container">
            <div
                class="
              owl-carousel owl-theme
              blog-page-top-slide blog-page-top-slide-res
            "
            >
                <div class="blog-image-slide-container">
                    <div class="blog-page-top-slide__img">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="ارز دیجیتال" />
                    </div>

                    <div class="blog-page-top-slide__info">
                        <h1>
                            7 اشتباه رایج در صرافی بایننس
                        </h1>
                        <div class="blog-page-top-slide__title">
                            <span>ادمین</span>
                            <span>۵ نظر</span>
                        </div>
                    </div>
                </div>
                <div class="blog-image-slide-container">
                    <div class="blog-page-top-slide__img">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="ارز دیجیتال" />
                    </div>

                    <div class="blog-page-top-slide__info">
                        <h1>
                            آموزش صرافی بایننس
                        </h1>
                        <div class="blog-page-top-slide__title">
                            <span>ادمین</span>
                            <span>۵ نظر</span>
                        </div>
                    </div>
                </div>
                <div class="blog-image-slide-container">
                    <div class="blog-page-top-slide__img">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="ارز دیجیتال" />
                    </div>

                    <div class="blog-page-top-slide__info">
                        <h1>
                            تریدر حرفه ای
                        </h1>
                        <div class="blog-page-top-slide__title">
                            <span>ادمین</span>
                            <span>۵ نظر</span>
                        </div>
                    </div>
                </div>
                <div class="blog-image-slide-container">
                    <div class="blog-page-top-slide__img">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="ارز دیجیتال" />
                    </div>

                    <div class="blog-page-top-slide__info">
                        <h1>
                            بلاکچین
                        </h1>
                        <div class="blog-page-top-slide__title">
                            <span>ادمین</span>
                            <span>۵ نظر</span>
                        </div>
                    </div>
                </div>
            </div>

            <hr class="blog-page-line" />
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section>
        <div class="container">
            <div class="search-blog-res">
                <form action="">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="18.461"
                        height="18.461"
                        viewBox="0 0 18.461 18.461"
                    >
                        <g
                            id="Icon_feather-search"
                            data-name="Icon feather-search"
                            transform="translate(0.5 0.5)"
                            opacity="1"
                        >
                            <path
                                id="Path_8461"
                                data-name="Path 8461"
                                d="M19.837,12.168A7.668,7.668,0,1,1,12.168,4.5,7.668,7.668,0,0,1,19.837,12.168Z"
                                transform="translate(-4.5 -4.5)"
                                fill="none"
                                stroke="#000"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="1"
                            />
                            <path
                                id="Path_8462"
                                data-name="Path 8462"
                                d="M29.145,29.145l-4.17-4.17"
                                transform="translate(-11.891 -11.891)"
                                fill="none"
                                stroke="#000"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="1"
                            />
                        </g>
                    </svg>
                    <input type="search" placeholder="جست و جو..." />
                </form>
            </div>

            <div class="blog-res-filters-title-btn">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18.881"
                    height="18.881"
                    viewBox="0 0 18.881 18.881"
                >
                    <g
                        id="Group_1518"
                        data-name="Group 1518"
                        transform="translate(-354.846 -594.737)"
                    >
                        <path
                            id="Path_3041"
                            data-name="Path 3041"
                            d="M13.185,7.875a.59.59,0,0,1,.59.59v4.72a.59.59,0,0,1-.59.59H8.465a.59.59,0,1,1,0-1.18H12.6V8.465A.59.59,0,0,1,13.185,7.875Z"
                            transform="translate(351.101 590.992)"
                            fill="#404040"
                            fill-rule="evenodd"
                        />
                        <path
                            id="Path_3042"
                            data-name="Path 3042"
                            d="M16.875,17.465a.59.59,0,0,1,.59-.59h4.72a.59.59,0,0,1,0,1.18h-4.13v4.13a.59.59,0,1,1-1.18,0Z"
                            transform="translate(346.821 586.713)"
                            fill="#404040"
                            fill-rule="evenodd"
                        />
                        <path
                            id="Path_3043"
                            data-name="Path 3043"
                            d="M9.441,17.7A8.261,8.261,0,1,0,1.18,9.441,8.261,8.261,0,0,0,9.441,17.7Zm0,1.18A9.441,9.441,0,1,0,0,9.441,9.441,9.441,0,0,0,9.441,18.881Z"
                            transform="translate(354.846 594.737)"
                            fill="#404040"
                            fill-rule="evenodd"
                        />
                    </g>
                </svg>

                <div class="blog-res-filters__title">
                    <span>دسته بندی ها</span>
                </div>
            </div>

            <div class="blog-res-filter">
                <div class="blog-res-filter__title">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="19.143"
                        height="16.408"
                        viewBox="0 0 19.143 16.408"
                    >
                        <g id="settings-adjust" transform="translate(-2.25 -4.5)">
                            <path
                                id="Path_3029"
                                data-name="Path 3029"
                                d="M21.393,7.235h-2.8a3.42,3.42,0,0,0-6.7,0H2.25V8.6h9.639a3.42,3.42,0,0,0,6.7,0h2.8ZM15.24,9.969a2.051,2.051,0,1,1,2.051-2.051A2.015,2.015,0,0,1,15.24,9.969Z"
                                fill="#262c3f"
                            />
                            <path
                                id="Path_3030"
                                data-name="Path 3030"
                                d="M2.25,24.352h2.8a3.42,3.42,0,0,0,6.7,0h9.639V22.985h-9.64a3.42,3.42,0,0,0-6.7,0H2.25ZM8.4,21.617a2.051,2.051,0,1,1-2.051,2.051A2.015,2.015,0,0,1,8.4,21.617Z"
                                transform="translate(0 -6.179)"
                                fill="#262c3f"
                            />
                        </g>
                    </svg>
                    <span>فیلتر ها :</span>
                </div>

                <div class="blog-res-filter__btns">
                    <div class="blog-res-filter__btns-inner">
                        <button type="button">بیت کوین</button>
                        <button type="button">رمز ارز</button>
                        <button type="button">ترید</button>
                        <button type="button">بیت کوین</button>
                        <button type="button">رمز ارز</button>
                        <button type="button">ترید</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Start Section Three -->

    <!-- Start Section Four -->
    <section>
        <div class="container">
            <div>
                <div>
                    <div class="main-news">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>

                        <div class="main-news__info">
                            <h2 class="main-news__title">
                                بیت کوین
                            </h2>
                            <div class="main-news__writer">
                                <span>آیناز رضایی</span>
                                <span>۲۵ نظر</span>
                            </div>
                        </div>
                    </div>

                    <hr class="main-news__res-line" />
                </div>

                <div>
                    <div class="main-news">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>

                        <div class="main-news__info">
                            <h2 class="main-news__title">
                                اتریوم
                            </h2>
                            <div class="main-news__writer">
                                <span>آیناز رضایی</span>
                                <span>۲۵ نظر</span>
                            </div>
                        </div>
                    </div>

                    <hr class="main-news__res-line" />
                </div>

                <div>
                    <div class="main-news">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>

                        <div class="main-news__info">
                            <h2 class="main-news__title">
                                بلاکچین
                            </h2>
                            <div class="main-news__writer">
                                <span>آیناز رضایی</span>
                                <span>۲۵ نظر</span>
                            </div>
                        </div>
                    </div>

                    <hr class="main-news__res-line" />
                </div>

                <div>
                    <div class="main-news">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>

                        <div class="main-news__info">
                            <h2 class="main-news__title">
                                7 اشتباه رایج در صرافی بایننس
                            </h2>
                            <div class="main-news__writer">
                                <span>آیناز رضایی</span>
                                <span>۲۵ نظر</span>
                            </div>
                        </div>
                    </div>

                    <hr class="main-news__res-line" />
                </div>
                <div>
                    <div class="main-news">
                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>

                        <div class="main-news__info">
                            <h2 class="main-news__title">
                                آموزش کامل صرافی بایننس
                            </h2>
                            <div class="main-news__writer">
                                <span>فاطمه نوروزی</span>
                                <span>۲۵ نظر</span>
                            </div>
                        </div>
                    </div>

                    <hr class="main-news__res-line" />
                </div>

{{--                <div>--}}
{{--                    <div class="main-news">--}}
{{--                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>--}}

{{--                        <div class="main-news__info">--}}
{{--                            <h2 class="main-news__title">--}}
{{--                                تایتل بلاگ در اینجا قرار میگیرد تایتل بلاگ در اینجا قرار--}}
{{--                                میگیرد--}}
{{--                            </h2>--}}
{{--                            <div class="main-news__writer">--}}
{{--                                <span>محمد حسین تاجدین</span>--}}
{{--                                <span>۲۵ نظر</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <hr class="main-news__res-line" />--}}
{{--                </div>--}}

{{--                <div>--}}
{{--                    <div class="main-news">--}}
{{--                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>--}}

{{--                        <div class="main-news__info">--}}
{{--                            <h2 class="main-news__title">--}}
{{--                                تایتل بلاگ در اینجا قرار میگیرد تایتل بلاگ در اینجا قرار--}}
{{--                                میگیرد--}}
{{--                            </h2>--}}
{{--                            <div class="main-news__writer">--}}
{{--                                <span>محمد حسین تاجدین</span>--}}
{{--                                <span>۲۵ نظر</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <hr class="main-news__res-line" />--}}
{{--                </div>--}}

{{--                <div>--}}
{{--                    <div class="main-news">--}}
{{--                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>--}}

{{--                        <div class="main-news__info">--}}
{{--                            <h2 class="main-news__title">--}}
{{--                                تایتل بلاگ در اینجا قرار میگیرد تایتل بلاگ در اینجا قرار--}}
{{--                                میگیرد--}}
{{--                            </h2>--}}
{{--                            <div class="main-news__writer">--}}
{{--                                <span>محمد حسین تاجدین</span>--}}
{{--                                <span>۲۵ نظر</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <hr class="main-news__res-line" />--}}
{{--                </div>--}}

{{--                <div>--}}
{{--                    <div class="main-news">--}}
{{--                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>--}}

{{--                        <div class="main-news__info">--}}
{{--                            <h2 class="main-news__title">--}}
{{--                                تایتل بلاگ در اینجا قرار میگیرد تایتل بلاگ در اینجا قرار--}}
{{--                                میگیرد--}}
{{--                            </h2>--}}
{{--                            <div class="main-news__writer">--}}
{{--                                <span>محمد حسین تاجدین</span>--}}
{{--                                <span>۲۵ نظر</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <hr class="main-news__res-line" />--}}
{{--                </div>--}}

{{--                <div>--}}
{{--                    <div class="main-news">--}}
{{--                        <img src="{{asset('theme/landing/images/blog/res-sec-1.png')}}" alt="تایتل خبر"  class="main-news-res-image"/>--}}

{{--                        <div class="main-news__info">--}}
{{--                            <h2 class="main-news__title">--}}
{{--                                تایتل بلاگ در اینجا قرار میگیرد تایتل بلاگ در اینجا قرار--}}
{{--                                میگیرد--}}
{{--                            </h2>--}}
{{--                            <div class="main-news__writer">--}}
{{--                                <span>محمد حسین تاجدین</span>--}}
{{--                                <span>۲۵ نظر</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <hr class="main-news__res-line" />--}}
{{--                </div>--}}
                <div class="pagination">
                    <button class="pagination-page">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="7.645"
                            height="12.38"
                            viewBox="0 0 7.645 12.38"
                        >
                            <path
                                id="Icon_material-expand-more"
                                data-name="Icon material-expand-more"
                                d="M19.926,12.885,15.19,17.61l-4.735-4.725L9,14.34l6.19,6.19,6.19-6.19Z"
                                transform="translate(-12.885 21.38) rotate(-90)"
                                opacity="0.6"
                            />
                        </svg>
                    </button class="pagination-page">
                    <button class="pagination-page">
                        <span>3</span>
                    </button>
                    <button class="pagination-page active">2</button>
                    <button class="pagination-page">1</button>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->


    <!-- Start Section Five -->
    <section>
        <div class="container">
            <div>
                <h3 class="new-news-title">اخبار جدید</h3>
                <div class="main-news main-news-2">
                    <img src="{{asset('theme/landing/images/blog/side-news.png')}}" alt="تایتل خبر" class="main-news-res-image-2">
                    <div class="main-news__info main-news__info-side-res">
                        <h2 class="main-news__title">
                            7 اشتباه رایج در صرافی بایننس
                        </h2>
                        <div class="main-news__writer main-news__writer-side">
                            <span>۱۴ مرداد ۱۳۹۹</span>
                        </div>
                    </div>
                </div>
                <div class="main-news main-news-2">
                    <img src="{{asset('theme/landing/images/blog/side-news.png')}}" alt="تایتل خبر"  class="main-news-res-image-2">
                    <div class="main-news__info main-news__info-side-res">
                        <h2 class="main-news__title">
                            آموزش صرافی بایننس
                        </h2>
                        <div class="main-news__writer main-news__writer-side">
                            <span>۱۴ مرداد ۱۳۹۹</span>
                        </div>
                    </div>
                </div>
                <div class="main-news main-news-2">
                    <img src="{{asset('theme/landing/images/blog/side-news.png')}}" alt="تایتل خبر"  class="main-news-res-image-2">
                    <div class="main-news__info main-news__info-side-res">
                        <h2 class="main-news__title">
                            تریدر حرفه ای
                        </h2>
                        <div class="main-news__writer main-news__writer-side">
                            <span>۱۴ مرداد ۱۳۹۹</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Five -->
@endsection
@section('script')

@endsection
