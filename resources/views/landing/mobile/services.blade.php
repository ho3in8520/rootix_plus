@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    خدمات ما
@endsection
@section('header')
    <div>

        <div class="about-header-info-res">
            <h5>خدمات ما</h5>
            <h1>خدمات شرکت روتیکس</h1>
        </div>

        <div class="about-header-image-res">
            <img src="{{asset('theme/landing/images/services/responsive-header-image.png')}}" alt="سرویس های روتیکس">
        </div>

    </div>
    </div>
    <div class="go-down-container">
        <a href="#currency-table" class="go-down about-go-down-container">
            <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="به پایین رفتن" />
        </a>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section class="services-carts">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="d-block">
                        <div class="services-cart">
                            <div class="services-cart__image">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="145"
                                    height="145"
                                    viewBox="0 0 145 145"
                                >
                                    <g transform="translate(-1281 -984)">
                                        <g
                                            transform="translate(1281 984)"
                                            fill="rgba(247,242,255,0)"
                                            stroke="#fff0f1"
                                            stroke-linejoin="round"
                                            stroke-width="1"
                                        >
                                            <circle cx="72.5" cy="72.5" r="72.5" stroke="none" />
                                            <circle cx="72.5" cy="72.5" r="72" fill="none" />
                                        </g>
                                        <image
                                            width="63"
                                            height="63"
                                            transform="translate(1325 1029)"
                                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAA/CAYAAABXXxDfAAAABHNCSVQICAgIfAhkiAAACHVJREFUaIHdm22MXUUZx3+73ZaXrcDGaQUbrGNpJRJBKhBMFLbWlgirHXwJ2EogpkUoVrEtBqOw9YO2QJUYCmuAJpQI1EhkulapSGxrNWr6AYRSl1A6EFuLdALWwpZuhfXDM6d37t1z9p5z7927dP/Jzc6ZMzPn+c/r8zzzbMvg4CDGu4nAUuCzwGkcWzgM9AFrrNK/L1KxZd7+3ROBrcDMkZCsybjBKn1P3sIt8/bvvhX4QXjuB3YBgyMh2QihHTgjpN8CplqlX81TsQ2Z6iDEZ1il9zZevpGF8W4F0A0cD8wCfpGnXiulNb7rWCQesDVKn5q3UluUrnmqG+9OBK4GzgVeB35llf5bDe20AF3AJSFrM/CYVfqdKlVj2Vvyfq+1mHhDYbybCjwD3AMsAr4D/NV4112wnfHAY0AvcEP4PQpsMt4dV6+caaibPHA/MC0lf4Xx7qIC7SwF5qXkzwFuqUWwaqiLvPFuMvCZ8Pg08CHg2qjIVws0Nz/87QcuAi4A/lvxrqGod+QnRektVmkHPBLlvbeGtpxVeptVejvwXMhTdciYibbqRYbFbmAAmAAsNt4dAC6O3r9QoC2HnDxnGe/uDe1+IrzbUaecqah35L8AjA/pCchZ2xm9n2u8O7laI8a7M4AZUdYiZMNL8KM65UxFzeSNdwuABykdLW9HrwfC33OBJ4brgEB8C6WpfSR6/R/gaqv0xlrlHA41kY+IJ/W/i6zZTwJnI+rmi+HdBWR0QER8SsjaDEwOdS4ETrVKP1iLjHlQeM2nEbdKrwrpP0flOhFi0yh1wC3AOYglthN4gHLiXVbpfmB7cSrFUYh8FeJlsErvSemA32U0HRNvGopO+2vIQTyBVXoPsgG+PkyxZxkF4lCc/DxklKoSTxA6IEF/aOMbUd6e0SAOBckHIefmJQ5gvDsB6AiPj1ule63SdwP7Qt77i8jQSBTe8KzS/ytY/pDxbh+iwFxqvPs6cDolU/rFzMojjHo1vLz4KbAKOAH4WcW7u5okwxA0wqrLgzuANZTb3YeAhVbpLU2SYQhSR954903g0+FxgVX6zXo+EpwRS4x3dyL6+mHEEPL1tFsvsqb9TEq29fiMMoVhld6NGENDYLwbB0xHdIKTEDX3NeD5kXKvNWvNp8J414p08lXAbIR0WrmXEQ/PWqv03xv1/VTyVulrEIVmxGC8M8Bq0r1AlZgKLEGWzm+BpVbp5+uVoekjb7x7D3AfcEXFqx2IArUT8IhspwEfR1xZk0O5S4HZxrvlVuk19ciSteHNBT4SHnus0ofr+UjU7iRgE+W3Q+uBlVbpZ4apNw74HOLLmwkcB9xlvJsBfAt4BVgXiu/MK0/WyM9HXNEgllfd5MOIx8T3IifJ1ozyHwT+ZZUesEq/DVjjXS+wDHFutCFL4bBV+iZqWKbNOudBpnpCfAdwfhbxAAccMt5tN94tM96dbJV+xyp9B3AZoicALDfeVS6hXMgiH4/0pIwyuRE2t0TAvYh9sG+YKglagfOQjfEF490XAazSTwBficr1GO8KOzmzyMfOhF7j3Urj3Qrj3c1FPxCOs9VR1oKcxHuAJym5tSYBjxrvbgKwSm8A7gzvOoDvFZUti/xDwD9C+kzgZsQ5WZg8co4nx9nDVab6UVilF1ul5yCentgeuN149+WQ7kZOBoBFeZylMVLJW6UPIeqtBardk1XDVVH6tqKVrdL7rdLXAwuj7B7j3SlW6YOUDKN20m98MpG54VmlX7FKXw6cAujwO7tI4+GImh0edwx3nFWDVXotYhyBXIYkru2Ho2KXUABtyIU+wMSMjx4EDhZpNMJ0Sirr5hrbiNGNzIDjkQ3vh1bpXca7fyI+gvOLNNaGrO0PA9OMdz8GtjVAyN9YpY9Qrro+l1U4L6zSrxnvnkSusc8Kx98BRLE5HZmdudGG7JifR5bA0vCrFx3IhUNsqOxvQLsgRLtCegpwIGq7zXjXntcEb7VK/xH4GrVP7eEQ3740yjTuRjq3A0iMm7jtI0NqZKANwCq9znhngU9R7Eb0A5SCmTYgpwOIlxbKXdYNcVRapd+itE8lSPyBb1ilB8iJo7p9WDuF7sSMdx+jRP5pq/QDFUX6ovSIhLqFE+Wc8FjIzB1R3T54YF4Oj3OCoI3GhUCi3PylSMVmGDa/Dn/fR2mjaiTiSJANRSo2g/zaKH1r0PUbAuPddEoGzksU1CVq8uQY765D4t3imLfOEAwYo88qvd549zgS7DgTuBH4SS3frZBhHGImJzv9ymD350atbqzrKG0yCS6mPCQFZBquB76NqLkTgFXGu2eLBgmnYHX0vacon2G50BRnRnA2LguP44ENxrua1r/xblzw/98YsgaA+UVHHWof+SuQq6dqOJAkrNJrgs9tSajbG9TpFXk1srDG76U87mcCEm/bl1ppGLQMDjYvwDqEl94OLI+y/w3cDfw8hLJV1mlFbnkWAgsorfEkCizBYqt0j/HuS8BcYFW4JMlEU8knMN5diYSrdlS8egkZwVcRklOAj6aUewpxss4K7SRYD1wZ0nuBTqv0riw5RoU8QPC5fR8Z0fac1Rxy27s2WePGu+sp74AYw3bAqJFPYLw7CbgccUSch4SwJprgG4jK+idE9f5DWgS28e4RSiMOMnvODOnMDhh18mkw3rUDR/IYKWGN/zLKWoxcYGxElgVkdEAz/fa5YZV+s4B1NidK9wHrQvhMFyWNbwqwJcT9HcW7knxB3IaMLMhU32i8OzFPBxzz5MNx1kmpA2aRswPelWu+FmSEsnZZpfvDv8EM2QPGDHnI1QGbEG8VwNYxRR6yOwBxo8X5D4058pDaAdsQ/aGsQ8YkeUjtgARHl8Ixv9tnISg08SkAFdHdY3bkE4QZcD+wB7g2DnL+P5kP6ZKg4E0eAAAAAElFTkSuQmCC"
                                        />
                                    </g>
                                </svg>
                            </div>

                            <div class="services-cart__title">
                                <h3>تحلیل و بررسی بازار</h3>
                            </div>

                            <p class="services-cart__desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                                با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="d-block">
                        <div class="services-cart">
                            <div class="services-cart__image">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="145"
                                    height="145"
                                    viewBox="0 0 145 145"
                                >
                                    <g
                                        id="Group_1441"
                                        data-name="Group 1441"
                                        transform="translate(-885 -984)"
                                    >
                                        <g
                                            id="Ellipse_14_copy_2"
                                            data-name="Ellipse 14 copy 2"
                                            transform="translate(885 984)"
                                            fill="rgba(247,242,255,0)"
                                            stroke="#f2edff"
                                            stroke-linejoin="round"
                                            stroke-width="1"
                                        >
                                            <circle cx="72.5" cy="72.5" r="72.5" stroke="none" />
                                            <circle cx="72.5" cy="72.5" r="72" fill="none" />
                                        </g>
                                        <image
                                            id="startup_1_"
                                            data-name="startup (1)"
                                            width="60"
                                            height="60"
                                            transform="translate(927 1026)"
                                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAABHNCSVQICAgIfAhkiAAACbBJREFUaIHdmnuwVVUdxz/AxSUYICipwfgAM9/5ADTynQ8YMAdfGSjqgEvFycgaXCWJWeCaioePIJdShg+kHGjS0kSiIULxlZhWElcHsgZCRRTFJY/bH7/fZp+72eecfc69wLXvzJl99nr91nevtX7r9/ut1a6pqYn/B1gXOwGfB44CDgH2B/YB9gI+Bh4CpjTssh62ENbFdsDxwLnAl4ABQMcKVY4DaPdJG2HrYm9gNDASOKhMsXXAKv2tBk4GDgVe/MSMsHXxWGAccBHQoSRrM/AnYAnwZ+CZ4M26TN27EMKmzRO2LvYFbkOIJmgCFgAPA/OCN+9UaWYvfb7bZglbF3cHHPBtYDdN/gCYCUwL3rxRQ3P76HNNmyRsXTwGeAA4QpM2ATOA7wdv3qqjyWStr2pzhK2LVwN3kI7qH4AxwZvX6myvM7JFATS2GcLWxQbgJ4DVpA+BbwE/Dd60ZCs5Emiv/5e1CcI6Co8AgzVpOXBB8OaVVmh+oD630hYIK9nHgNM16SngwuDN+lYScZo+lwVv3mtfsegOhnVxN2AeKdk5wJDWImtdNIgVBrKNsatH+B7gbP3/EDAyeLOltIDOgBHACUh/lwH3F9TWZwKf0v+PQbqYdzqsiw4xDwEeB67IIdsPeA0IwCjgcmAK8Lp18csFxFyqzzXAYthFtrR1cTDyxdsDLwNfDN5syJTZF/grsLcmLQc2Ih4RyN58YvDmxTIy9gbeBAxwe/BmLOyCEbYuHgLMVtlvAedlySrGIWS3ApcEbz4XvDkGcQTeRzyjSZVEIWRBZgjQCiNsXTxUO3E4cCDQHeiqnWoC3tNOb0AM/ZOBPsAW4KzgzcIy7f4NOAyYE7y5JJP3PeBmZJQ7B282Z/I7ASuBnsCC4M2ZSV5dSksN+tHAcFIrplZ8oxxZRQ99Ls/Je1WfHRGl9G4m/2sIWYCJpRk1EVaiExHPJbsc/g38E3gbGc2OwB6IK9cF6KfvAPcFb+6sIm4VYvSfbl1sl7G2Es2+PnjTjKyu3e/o6+LsRy1E2LrYAbhJG0rWxWZEu85Bps3qCvWHAXP19VngmgJiZwP9gZOAGdbFW4GPgGsRjQ3wq5x6Pwa66f8bsplVCVsX90P8zlM0aRMwHZgcvPlXgfpHAvfr62pgWPAmVquHeEcjkDDO1forxX+A72ZkDUK2LoCZwZvnso1WVFrWxYOBJ0ndq8XAVcGbfxToMNbF7sBzQF/kQ50avHm6SN2S+vcAF2SylgCXlvrE1sWeyBa3L7LvHp4XGChL2Lp4FOKaJfugB8ZnjYMKne2ATPmzNGl08GZmkbo5bR1MGqRbFrx5KUfWfFITdUjw5nd5beVOaRXwe1Ky1wVvptfYT09Kdnq9ZAGCNyuAFRWK3ElKdko5spBDWMn+EdhPk8YEb2bU0kHr4nDElwVYBIytpX6Nsm5CFBnAQuDGSuWbTekSsr00qR6yxyNRxE7I1jIgeLOmljZqkOWQAB/A34GB2W0qi20j3EpkP41sP52QLeT8HUFWg/CTkCAfQCNwdjWyoIRbiWxHZF9MLK9RwZsXKpQ/AvgCMCt483ENcjoDPwcu1qRG4LTgzZtF6jfkkA3AAt1/NwRv3i/Yl2mke/Xk4M1DVcrPRc6ADBLLqgq19OYCR2vSi8Cg4M3agn2kAflavUrSLGkgDesiwHrEGdigz+SXpO8BXKFV5lNFcSge13pLi3TUujgUMWD21KRfAlcGbz4sUj9Bu6tu/CgAV9VSqQIaESWVexJgXexR4JQgW6cDMIHUqtqCrN3J9UQz2zU1NWFd7Iq4dV0Q76OL/rrqr0smr5um9wY+q219gDjkuZFG6+Ik5BThR8GbcUU6Z13sgdjUibPwX8Q3ruRlVUQDQPDmPcRvrQnWxadJCV9Wgawnnea9C7bdDwndHlCSPLslZKEFEQ+NCB6rr88Hb+aVKVdK9hXg6wXaHoXY7QnZrfX2M4u6CavHc7u+9rMufjVbJofsGZU0qnVxd+vivcC9iPbeCFwGVPXKiqKlMa09S/7fYV38TPJSB9kDkFFNfN1GRCc80MI+NkPdcWnr4vWUbF+IozHPungqcAu1kT0HiUsnYZ1HkRh1VcupVtQb0xoMTNXX1xDP6nrEhVuGGBRQhayaiOORD9QeWasTgIktPEAri5oJWxcPQyIg7YF3gKHAG4i2Hkxxst2BWVofbeuS4M38CuILm6DlUM8afhDZgzchJ3wrNChwIfBMSbnlyN68HayLRwPPk5J9HjiuAtkk+JfbXi2oh3Biyq1HQioAqIk3CAm/AJwPLNJbN9tgXRyJfJg+mnQvcFLwZmWeMHVKuutrPaf/zVDPGr4FsZf3RhyG5HyI4M166+JZiJ07BAnAvWJdHIsopanAGC0eEa/sZ1XkHUR6a+f1OvrbDDWPcPDmKaTzAJcpmdL8D5HLYlM0qRvioLxJShbgmwXIgijCBK+WLVUQ9e7DY0hjTJOzpIGDSddngp6Z90LBQNLz3ZXBm1XFu5iPooH4TsAw4NXgzTKduoOR9doTmKo3b8YiV/weIV13dyNG/wjgF0isK3FGqsk1Khdk62sxKhJWolcjRsS+iNLoCRJJtC4O1I70QQLgQxDrqwHZU8eWHKncrG1eQ+p5VcNFpKcIswuzqoCyU9q6eC4ybaciZEGCc9ug4dN+iJICUWQNiAYfXOb8KImgdMvJK5XfHnEnQQJ0iyqVL4pcwtbFK4HfAIltvBQhcH62bPBmXfDmK4jRsRQJu5wYvHmyjMzkLLhrlb5Z5AgW4IfBm1bxmPLi0n2Qcx2AtYAN3vw6U+YuZPoOS04BgjdPAE8UkJmMcNkpbV3cnzT8+jLp2VSLkbeGb0Bcs83AOcGbv2Q60w+4Tl/7Ay9RGxKHIJew3ux5GNEFW4Frix7vFEHelB6kz7lZsorx+lxP/nFlNSRTejstrfGrB5HwLcCtwZsl2XItQR7hA/W53SmfdXEAcJ6+TqvTfcslrGRnITY5yP2tH9TRfkU0I6zBvMSMW5vJ66UdAol/3VWnzO20dAnZ4Zq0EBjRmlM5QXYNlwoYaF1cjWw1/ZE7HUknXZ3XeCEzwuoT301zskODNxvrbL8itjsfti4uAM6oUOfW4M2EegWqGToVcR46IbcJkisQzwJn1nDaUTPy1vDliH9aigj8FjilJWQVSTjYICNbSvbsHUkWKt8A6KudWgesaa2N37p4MXIRphQJ2da6QVsWDdbFExDPZlrw5u0kI3jTuINkZkdwp5EFUVozkID6FsS539EoJbxTyYIQvg+5IvjoTpL5AqIPPkZO/3YaWYD/AZw7cEO04RyEAAAAAElFTkSuQmCC"
                                        />
                                    </g>
                                </svg>
                            </div>

                            <div class="services-cart__title">
                                <h3>سریع ترین در</h3>

                                <h3>خرید و فروش</h3>
                            </div>

                            <p class="services-cart__desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                                با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="d-block">
                        <div class="services-cart">
                            <div class="services-cart__image">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="145"
                                    height="145"
                                    viewBox="0 0 145 145"
                                >
                                    <g transform="translate(-484 -984)">
                                        <g
                                            transform="translate(484 984)"
                                            fill="rgba(247,242,255,0)"
                                            stroke="#ffeff0"
                                            stroke-linejoin="round"
                                            stroke-width="1"
                                        >
                                            <circle cx="72.5" cy="72.5" r="72.5" stroke="none" />
                                            <circle cx="72.5" cy="72.5" r="72" fill="none" />
                                        </g>
                                        <image
                                            width="61"
                                            height="61"
                                            transform="translate(530 1025)"
                                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAAABHNCSVQICAgIfAhkiAAACk5JREFUaIHd2320Z1VZB/DPvQwgwxBOimJGjohFOChihNFZOBgygjSGIFk2C4Vy1VmRFkolCoWsNF9SZ8nRWkZaJAgsBVrUhIkDbhMZkyZfQGmQlwkwtQFnFHAcpj+efTi/372/83LvHWYt/K511z5nn2e/fH9772c/z7P3ndqxY4cfGxTlT+PD2CBVf9wmNvVjQ7ooD8Bn8Kycs0Sqvj9JdNEu69RjiaI8COvw9JHc3dvEpx/zDj3WmEwY9m0r8vgmPZvwx0e+tnJ7/JKeTfhP8Y4RidaRfnwqskmEU/WOnH9bzvsN3Ijt2IJteEiqtu860kW5B7ZJ1cIabCMc35bhmz01rNm52rsofxJHocAhYvv4KTxxRGYL/k+MyC24CeukatOA+tsJB+7Nf0/rqOWghY90US7Fr+OVWGH+euKr+Bgulqq7JrTTR7iW2xfPxE/kvqzG6fnrRqyYP+mifCbOwmuw94yvW/Bf+AbuxmZszXJPwDNwIA7HfjPK7sAncYFU3ZzbOgCf10d4dh9X4QqxZ2/ECqnaNHfS8Uu+Ba83bgB8TYzUv+Jmqdo+sL5n4Xi8QsyUqZGvV+CPcBLen/OGEj4l92eMMHPV3kV5PC7C/jlnBy7Du6TqP4ZX1Fr/gfhdlJrZsxX/KfQE7CZVj/TU82r8vZjeY4QZSrooF+HdYnRrXIM3SNV/91cwRxTlk3EOzsRuM74ular7O8p2EmYI6aLcB5djZc75Nn5Hqq4aTGK+KMrn4aN43kjuflL1nRb5XsL0adpYvzdoCN+Aw3YJYUjVBrxQEKmxZKLsQMJ0jXRRLsZ1ODLn/AN+W6p+OPfeLxBF+WtCoxPKbgN2SNUD+ftgwrS5lkU5JRRUTfhDKBdsTc0fo7vEukefihIe0czYXsK0+9Nvxsvy82XmQ7gol+OX8HM4AHviQdyHW/EFEeHorrcojxTRkDbMiTCTpndRHoXP5srWoxg8pYvy6WLLWS0MkD7cK5bNhS1W2JG4VlhX2/AmPIBfFfs6JOFSXi5V3xrSzXHSsTXdjOW4H4dLVZ8BT1Euwblii3nCjK/34y5hle0lrKqZDv82/DXOkarv5TpnEj5Fqq7O3/YWA/Lz+D4OmfijtWAm6bPEfgxnSNVFvTUU5S/gEhw0knuTUCxrpWrjhDJLcYyw2U/SrNl7ct42bYSbOo4QS2RKjPKpvX3NaEjHaN2BJwk/9KgB620VLhUjCF/Em6RqXXuhWXX8DP4Mr8052/Cw2JomE27KXiisN3iBVH1pSJOj+/TvCcJyx/sInyhs472EBj0PL5wTYUjVXVJ1Ol6C74hR7yccOE9MbyJyMgjTmcA0fj/nXS9VqbNUaOZLcwe34VVSdf5gJ2MytmCPGXndiikssyq/nZRnTS/qkX4R6gJrOksU5e6CcO0QvFaqLh/SWEedo0rrQc2IX5qVVhc+JByf3fCbQ5qrSb86p5uFI9GFP8Rz8vMaqfrHIQ21YjbhE/Gq/HWZcDzakarbRZCfiIv1oib9kpx+Uqoe7ujgEpyd3+7GnwxppKO+2YRTdZ1UfVpjkLxeUT6lp6ZP5PS5irIrVAQW5TBMPbWv65H/LY2y+3OperCvgXy+tFJYZVvxObE7/KJJhBuci9OwGGfg7R2tXIMP5OdjRPCgFdM4YuR9CGliGVzcKVmUeyrKNWIb/LDQtO/Cv4v9tYswqbpXuLT0rdVU3SHMW3hBDwfTwjaG+3NDbST2FW4eXNmzDHbDVcaDAN9Cbc4eoYtwg0tyujybuF1Yn9NDe+TGSN/aI/t8DYFP98ieqfHBr8aBUrV/zhsN9Ww06jXNxroR+SM75OD2nPZuW9NYmp/v6xLEwSPPX+6RrcNKN+FkqfpmVlpXGTeIlovtcjJStRV35ref7Wnz7pw+tUfOtObM54Ee2SePPLe7bxGuXZbf1kjVjxTlocZt6VOFMQK/3NNu3VYfmTpu9sROKUG69or6NPGo791leY02ek9OVxp3Hi7H/+ZvS3Wjnt5TnVLdfRrDIs0IL+6RHfWp9+yQ2yQspCkRzP+M0N6LxfHNDYrySRp/++6JtTSoTdN2xRmoB6+X/CI8lJ9bjzYzRju3TDNS40jVZkV5o4ianK0oP56jGeejDkW9WzNz/rmn3Xqp3NMp1QQM28PDGYs0ZA7okb1l5PlwoaTacC4+hadgvaJ8W5Z/mnAFX5rlLpaqb7TWEtZVbWHd0ioXqLV2+7abMY06WN+nHTdo3LiVXYJS9W94Y37bHxeKffRqDeHPC3e2C3U7jwgrrgv17nJ7p5Qg/dX8vCQfq0xGqrZhbX47Ia/LdqTqPeKMav2ML98WQYNj8pbUhdU5XfdoGKkdz83phh45i8QvXuPFun+pj+JkoVzOFJ1vR6rWYq2i3F8sn+/htt6zKCjK5+f+1O12yR6s2dK6lh2Ylqrv4iv5/Vd65K/RWG5vzHtyP1J1n1Stl6qvDyQ8hb/Kb5uMX6CZhONyuh3X91VfW0dX5nRVdh8nIzpcu5N74yPZzt7ZOFOcZMBbOu38QO1HXy9VWzolNaQvy+linNJZIs6xrshvL9aMyM5BUR6nichea/wca5L8szWO0CVdojWCdKq+LCKZhNPeZ/28TtwygD9QlO/NcbaFIYKNV4pQ0T04bcDJylk53aoZvE6MdvQ9OT0ML+8slarN4tjnf3LOG/AvQ6IWE1GUuyvK84VDspeIkb1UqrqdoGivDh1fNEDDY5z0FZo9+y/yFah2xGH8Co1iOw5fV5TnKMpeoz93elpRvlJsM2/N/bkTL8qzrw9/KXaSh/DOQW2afcIxeiT6ZqnqCtHUZfbBBzXBRfgB/kncP/ki7pCqLYpyT3HF6lDxg51q/IjnSnHgP/nQfbzdFZqA4Du7rjrPxKQDvE/hWOFgHDX4LklRHivCQYcNbXwEXxMXaLoC+6NtLcWXhF1+F54zwNB5FJOUz+nCaN8Dlw2eqmF6Hi5G8O/0ByW+KzTzcVg+B8JTuf5lwpt73VwI03YTIdZZrQk/i5WDIp+zO/cMcXPwqcIL2iK8s9uk6raO0l31vk8TmblAqt461yq6rl+cL5QLsT5PlaqHJgvvIhTleRrT91qcMJ+jpC7S9TQ6LefcgFWP3vPYlQgb4H3CUiP8hWOl6gfzqa7doAij4Az8Tc45WvjG81FU80ecbqzVEL4Rx8+XMMPukU3hAnEPhQjbnIP3S9WP5tvwIBTly8V2WBs9n8DqhRBmLtckowMf0QT+viLOsde2F5onivIQYXicmHO2i7X89gUeB2Pud0OXiTOjl43k3oT3ilOP+Su6WLdHC5N2lSb6eSteI1VfmHfdMzC/q89F+Qrx/xLPHsl9QEy/a0Wko2+frk9BjxZ+/MnGbyRtFqNb7exltJD73tPCjDxbHPnMxH3CE7tTREweFpGafcW+fbAgOdOju1No6r8d4hvPBzvnfzjiOsZqnCCOauaKTSIq8zGkQdGVBWDn/+NKUe4n/o/jYHHPaz9xurGPGPEfCl95o5gJn5vLHbCdgf8HcTmAmvoyvGsAAAAASUVORK5CYII="
                                        />
                                    </g>
                                </svg>
                            </div>

                            <div class="services-cart__title">
                                <h3>هدف گذاری بازار</h3>
                            </div>

                            <p class="services-cart__desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                                با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="d-block">
                        <div class="services-cart">
                            <div class="services-cart__image">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145" height="145" viewBox="0 0 145 145"><g transform="translate(-1281 -1484)"><g transform="translate(1281 1484)" fill="rgba(247,242,255,0)" stroke="#fff0f1" stroke-linejoin="round" stroke-width="1"><circle cx="72.5" cy="72.5" r="72.5" stroke="none"/><circle cx="72.5" cy="72.5" r="72" fill="none"/></g><image width="57" height="61" transform="translate(1333 1528)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADkAAAA9CAYAAAAXicGTAAAABHNCSVQICAgIfAhkiAAABwJJREFUaIHdm2mMFEUUx3+7IGq8dXEtT9RSs4mgJkSIBwKaeHK4Go2Kxq3E+0YMHkT4BJioqIkxmljeIrAK4gGCRpeoUSGuJniF8gCyW2JAYDlW2IXxw6t212Fmdqanexn9J53qft2vXv2nqqvqvX5TlclkAPDG1QBTgauBfeg9fA1MUlYvSMtAVSaTwRtXC3wGHA+0AV8CW9MyGlANaKAuXN+qrH4mDUMRyUbgMqARMMrqTWkYywVv3GhgFrAHUKesXpG0jarWhhX9gTVAK3C8snpbCQ0cA4zIEm8BpimrN5dQz0RgOjBdWf1AsXrFoi8wEKgCFpZCMGASMDiHfBHQVEI9byMkTy3RflHoC+wfzov+5bthLHBClmwb8EWJ9fwVyr1jtKFH9C1HWVndArQk1JbUsAtJb9wlwMvAQSnb3gnMRya8VJGrJxsQgi1AZ5AdHcpVCdo+FBnuJ9I1XFNBLpJ9QnmWsvo3AG/cBgBl9YCkDHvj5gFjgL3YDSQrBt64i4EngP7ADmAOcIeyuqOUegqRfN8btz2c7xeMfhOjrflwbKGb3riDEVJ7A6uBQ4CbgB8R4kWjEMm6HLJTSqm8TJyNEJyjrL7CGzcMWXtLXksLkTyFZCeabLwGXJQt9MZpZJNxbRBFe+i2uIYKkWxTVm+IW3FP8Mbleq8GI8OxD9CBbOLLRqITjzfuAGT52a6sbi1S7WjgznC+D7AOeBRYDryTRLsS+aUAvHH3A2uBX4EWb1yTN27/HtRA9q0NwEZgMjBAWT2deNvMnEikJ71xAxCHeyuwADgJGAbcBkzLerYGeAg4P4g2AY8DTyTxenjjLkRGwn3K6vchueE6APFk3lJWX+eNGwl8hJCNjNcAE4DbkWG5BXgEeFRZvTaJRnjj7gYeQ0bovpG8LJLeuLOBK+ma1oeHnUxNuB4ZrquAkcHwX8AMYGpS5EJbxiME24EGZfXs6F5skt64ycCULPFR4ch3/STwiLLax7ULHOONq1VWr8luErAUCaMs634jFklv3BCE4AZgPPANkCmgMgs4UVl9dxx7WRiOTGyLgVeAecrqrcrqmcDMXApxe/KKUD6grH6hp4e9ce0x7eTCb8CewAXhaPPGjVdWP59PIe4SckgolxV8Kh00Ia/A+cCriBfzrDfumHwK5c6unT0/kjyU1TuQONIib1wbcCuyDV3pjduDrrhxp7J6c0W7WkUi2h5WB4KrgMOim964hv8Dye7oBJbQtT53AD//p0l646qBI8PlNmV1Blm3/4W4JKPloje/mfwDb9wgYBxwDXA4sB74PN/zcWfXX0I5KqZ+ObgK+Ba4D3n3FgIjlNUb8ynE7ckXgYnARG/cEUAzEmLMh/4gw0tZXei5YtAP2Xy8DMxUVv8e1Q0cqKz+M1shFkll9WpvXD3i3Y8LRzFY7o17GHgzvD9x8Lqy+poc8inAg964Ccrqf8WAYvuTyupFwHFAPfBUEC8F7gGezroejyzidUhw6mtv3FhvXFUM0/kidZ+FezO8cc954/pFN8pympXVm5TVc4G5QfR9+BUbu18rq2coq4cjnsiXiNcyF/jKG3dBOW3o1pYPEB/WAzcAd0T3EosMFNmQj5XVQ4ELkUV7MLDAG/eFN+68BOpfGup8Cngvku+WdVJZvdAb14zEd1YDQ4DF3rgmYIqy+pMy6m4F7uou69WezIMxwPXh/BzgY2/ch8DpSRmohB1Phq4PtquRANa54UgEldCT3fELMAiJ3v2cVKWVRhJldaey+kUk1QZAe+NORT4bQAz3rhKGaz58i0QBzkR2VBHmlVpRxfVkhJCkMRx4BglAzwYuV1a/W2pdldyTKKtXIl5/WUiK5JZQDvLGXUrX+1Ns0tMaYAUpxYySItmMeAanAW8FWQcSMuwRyup2JHcgFSRCUlnd6Y0bARhkF7MFaFRWNxfW7B0k9k6GjzWPx9H1xvVB9ptLlNWzkmpThEqZXY9AJpib06i8UkhG7YjjX/aIXMM18tjf8MalmV9zcihTD1DnIjkf8QyGpG0cyVr+CRmuqWEXksrqF7xx8wm5OyliJ9CirN7hjUvVUM7ZVVm9DklQ6BHeuBuB+5GI3GLglhzfDncryv3SXA88i/TKZuBSoBbZVFcMyp1dI3eoHiH3HXCGN65gSllvo1ySUaB4L2RU9M2SVwSqgT/CeW0M/ZdC+QaSw3MS0BS8h1JwcChTyQCrRjbW7cAob9zhpSgrq99DcuCWIw18lXiZyDeFMu9Hm3IQ/S9kGjJD/oAEZj8vI4xfNEK654PAvchsfoKyen3SdiKS/ZDw/egg30bx2cQOGBo8kRuRvz4U+64fEMoNwChl9adF6pWEagBl9XZk+r8OyaQqJYmok66tYCfFTzo7kNn4SWBgWgQB/gZnlC6nTOYAGgAAAABJRU5ErkJggg=="/></g></svg>
                            </div>

                            <div class="services-cart__title">
                                <h3>مشاوره آنلاین</h3>

                                <h3>در خرید و فروش</h3>
                            </div>

                            <p class="services-cart__desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                                با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="d-block">
                        <div class="services-cart">
                            <div class="services-cart__image">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145" height="145" viewBox="0 0 145 145"><g transform="translate(-885 -1484)"><g transform="translate(885 1484)" fill="rgba(247,242,255,0)" stroke="#fff0f1" stroke-linejoin="round" stroke-width="1"><circle cx="72.5" cy="72.5" r="72.5" stroke="none"/><circle cx="72.5" cy="72.5" r="72" fill="none"/></g><image width="58" height="58" transform="translate(930 1527)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAYAAADhu0ooAAAABHNCSVQICAgIfAhkiAAABW1JREFUaIHdm39oVWUYxz/32kjRrAwiKFPClbrA9WNUrMFmFJm6VwpW7yxrRkF/NRmLosD6L7LZWGg/RLZ+7S1C4zWDIoj1y4ooyoIiM2YFgpg4ZVqZW3887/Xe3Xvuveece97t6hfGy3nP8559v/d57nOec97npsbHx/EOrS4BOoFWYL6bHQZ2AL0Y+6dvCinvQrW6D3gBmF7E4m/gIYwd8Ekj7fPiTmQ/InIIWAlc5P5WurnpQL+z9QZ/HpVw3YMI6QG6MXY8zyYFbAC6EM/W+gpjnx7tJOvJQpGAm+sm69lOX2R8Cm11Y0+gyAzkXE/emsThR6hWabLZ9esQKzI280taVYCzEr2aVtcDa4BVQE2i164QlQsV77UBjwJLAiwagJ1lrtLgxuGK+RRBZaGrVQuwGzCIyOPAK8Ay4Hln1eWya7FrpJCsC1JABNnUotW1lVCNd3vR6hygD8jc+0aAZ4HNGHvI2VwK/AzMoNLbi1a/A3ORD/FhjB2JSjm6UK2WANuBy4BxpOp5HGMPB9iuBba6oyFEcCbxNCACm91xR9HqSKv1wHogBewF7sDY76PQjiZUq+XAW8BMYB/QjrG7yqzpADYhng1CuBJQqybgVSQzHwPuxNhy3/1TCC9Uq9VI6EwDPgTaAr0YvHYesA5YjkTCSeIU9VrNAd4EbgbGgDUY+0aYpeGEarUCsEjyGgTuxdj/QpErvJYkplJFROn1NcAA0I6IXYWx75ZbVl6oVlcDnyGh1w88gLEnY5FMClpNA7YAHUimvxFjvy21pLRQrc4DvkHC7QNgRWxPJg0RuxO4FckX9aW+SuXuo32IyGFAV41IwEVVO5KF5yEJryiKe1SrWxAvjgGNGPtlokSTglbXAZ8jSXIZxr4fZBbsUQmLje6or2pFAhj7FRJ5AH1oFVjWFgvd1UAdcAh4Knl2ieNJ4C+gFrg7yKBQqKT/bnf0TOh75VTC2CPA0+7okaDaOsijTcCVwCjwoj92ieMl4CiwCGjJPxkkNOP6wTjF85TB2KNApkq6J//0RKHybHm7Oxr0SswPMpxbXUI9hXyPXgVcgDx2fToJxJLGLiQpzSH7MA8UCm1y49CUl3lxIJw/cUeNuafyhda7McwLrWpF5p5fnzuZL3ShG3/wTscffnLjotzJfKFz3fiHdzr+8JsbL86dzBf6D/LEv3cyGHnCATdemDuZL7QRWOwqjdMVJ9yYzq2QJhbAxu6fTEaTCb/bhlODc914JPd1zZko9Hw3Tihfz0ShtW7clzt5Jgqtc+OPuZMTk1EVNFVEQjDfTOn6a65p9p1RlTRVhEZ5vseRfZotkAndKmqqCIVwfGcAL6PV/QCp8btaq6qpoiziNYFckabKmipCIA7frjRV1lQRAnH43ha/qaLULrZfxOG74PS6j8b/cE+kyTZINJSyzLMZjr3tVwnkf8bimybbIBGlqaLsfqRHxOG7Iw30Iim4GdgQuDibrpud7XOJ0Y6OOHx7pTLK3oChfFPFWoztZyoRjW8Hxg5EKQEz2IY0afybHPMYiFiyTtwfLSySa5Di+D3kzeAm4GzgY6RZ4wBTiQgPIaU2ggubKrRaCrwDzAb2I54dSpq/D8RpqFqIhO9ipKFqK/AYxh5MnF2CiNsiNxNJAA8i3VwjyK5zX7UKrqzVXKsbgM1kX/8fQ9rnXgM+itzcITtgLciO+03AOozdFp9gFpX31Mt3WQFPANfknBlFUv8XSPPjL8BhZLMWYBbyImsBshXSiGxyzc65RhfGbiQBJPvjAa0akMbkNvLelEfAQeBt4PWyfYYR4OdXEuLlOmApEtaXI56bhTRMgoT5KPIQvQf4DomA3Rg7ljSl/wHvwQlVqih1TQAAAABJRU5ErkJggg=="/></g></svg>
                            </div>

                            <div class="services-cart__title">
                                <h3>تبدیل ارز های</h3>

                                <h3>ریالی</h3>
                            </div>

                            <p class="services-cart__desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                                با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه
                            </p>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <a href="#" class="d-block">
                        <div class="services-cart">
                            <div class="services-cart__image">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145" height="145" viewBox="0 0 145 145"><g transform="translate(-484 -1484)"><g transform="translate(484 1484)" fill="rgba(247,242,255,0)" stroke="#ffeff0" stroke-linejoin="round" stroke-width="1"><circle cx="72.5" cy="72.5" r="72.5" stroke="none"/><circle cx="72.5" cy="72.5" r="72" fill="none"/></g><image width="53" height="56" transform="translate(531 1525)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADUAAAA4CAYAAABdeLCuAAAABHNCSVQICAgIfAhkiAAAA2xJREFUaIHtmk+ITVEcxz+PaYbRyJHyL02RvydhhPzJv4WjyELWMrJgrNhYkLKzVDSyEAvJn4RYOLMZNQvlz4qTCCX5k5gjpCiexT2TO2/uee/d++577p3muznvnt/3/O75dM495/TuLRSLRbIsa9Ry4DzQ6rH0AzuE1K8GKpoa0bEatQqYXybeDqwFcgXVDbzj30gVgMPALF+DzEMJqX8BVwCsUQUCSC8QwKgG9CsVhYD2uqqLwJMoby6gPEA7gd9R/sxD+YCE1JFAkAMo4CR+oD8lJfCfFgpr1ExACKkfVfAdB/a7y6gROgZsAW6H2xUavflaoza6TowFuoTUpz2+48Ahd1lxyoXV0OlXAgTQbY3aF+FLDAQNhCoBegv0udAgsFqBoEHTLwJoPcEp4Tawwdm6CI48NQFBA6CigITUL1yslcFgA0oMBHWefuWAAITUP4CtQG+oWU1AUMeRqgRU4m0l2I8+AkdqAYI6QcUBqpCni2Cfmh4R/gU8BA4IqZ+FA6lDpQi0GzhbhfU9MFdI/W2gItUTRVpATp2u7AGiNujxwBlgKrAZuDoQSA0qZSCANldeElLf8NzzILAIGBeuT2X1qwNQWLGfj5qh6gyUSDVBZREIaoCyRq0kg0CQEMoatQK4QwaBIAGUA+ohWFIzBwQxofIABDGg8gIEVUJZozrICRBUP1LbyAkQVH9MOgF8Am4Kqd/UsT+pqCooIfUX4FScxNao0cBCYEJE+DvwWEj9M07OahUJZY2aANwDXgupN8dNao2aTbAxzylj+2CN2i6kvhc3fyX5nqk2YDawJGHec5QHApgCXHYjmqoiR0pI/cYatQD4FhUvJ2vUGGC1u9whpL4W4ekAHgEzCOCfxr1POXmfKSH184Q5x4R+v/R4XoV+tyS8j1d5eEEQWyNQedGwhPIuFNaoyfx7OxHW5/DfUVnUECi33F7A/+1C0Rp1HegUUn+tZ+eSahCUNaoFuAVMK9OmAGwHLLCnfl1LrtKRWkoAVATWAVGH107gKMHJPZMqhWp25VchdV+pGcAa1UsA1RwVz4KG5eo3ApUXjUDlRSNQedGwhGqyRrUTfKxRAOa5+mZr1C5Pm0qe8Ae826xRixN6JrpyjTXK05UhniJwt9D/ZNN9YJmvVQ71oIngADuJ4TEV/wC3/gLAL2QwGaKkzgAAAABJRU5ErkJggg=="/></g></svg>
                            </div>

                            <div class="services-cart__title">
                                <h3>بررسی دقیق</h3>

                                <h3>نوسانات</h3>
                            </div>

                            <p class="services-cart__desc">
                                لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و
                                با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->
    <!-- Start Section Two -->
    <section class="mobile-app section-background services-mobile-app">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6" style="order: 1;">
                    <div class="what-currency-info">
                        <h5>اپلیکیشن موبایل رو فراموش نکنید</h5>
                        <div class="what-crypto-title what-crypto-title-3">
                            <h2>همیشه و همه جا بازار رو چک کن</h2>
                            <span>با اپلیکیشن موبایل روتیکس</span>
                        </div>
                        <p class="what-crypto-desc">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                            استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و
                            مجله در ستون و سطرآنچنان که لازم است و
                        </p>

                        <div class="download-btns">
                            <button class="btn">
                                Google Play
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="23"
                                    height="24"
                                    viewBox="0 0 23 24"
                                >
                                    <image
                                        id="Google_Play_logo"
                                        width="23"
                                        height="24"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAYCAYAAAARfGZ1AAAABHNCSVQICAgIfAhkiAAABURJREFUSEullGtsU3UYxp///5zTc9pu7bobG4OxMbnjBHSKREQSLkYlwgcMUUwYYBQ/+KGJCUbiZYOFyM0QohGCGITED+OD0URUmBO5GAzgsiGRTXDM0XXt2rVbL6fn5ntOwRgNhMuHrs3a83vf93me92Wvf3Fh7JCU2FIzGyGWNrZVy4XxyZ4i9BlxBAQRfWYY5S4vBEuHy8XggYlyVoqY8ic8ig+XInGsbOuH26MCbg54RaDADTAGtuyjn14JJTN7Jz1qYvwEq63GKmuu95a0d2uD9w+ftfWbTQxCs0UdzZibw7RaEV698DB1u7VIELr+MPox5l47b9jzfVDi2JHLAabFMKdhFJXj0/AYxYZfkPZIorWrSJR7mZW7e1nmfnIsyBnbwbkJVeMwLGBhQwq141KIpzl8kidTrHh2eiy+W5b4oALtzjWfdyAPBwME8iOjc2gGwzMNCcwcl8bQKBkjMhSI7niBy9USsJTtFShD2H0ZXsV/e0OfONQWZCQLJzjoD+cWMjRBzuRYMSeGh8ePYiglwmSGU9wvFnaXCIEmU0oekhUPfovEbp2W+a3tQQHYwZgFy4ZThLg9gcGRpQlWPRTF3HFJxDIUMZLM5DpEiYrwgg7JXdx8Pj16ZM3XFEt35v9RfOrLH4OcMwfOCE5syihJRBXTuoAsTbB6RhgLquKIqi7ne24ZMGQOUZRR2R5qn/iXuVkIWMchU3UvPXgz54uOnggy21CC21BbGrsIqHsuMCpAE1CBtVNDWFI5hKgmQRdFWEzApG97UHI2BFQVAAFvKwoLm1Do6oTXRSBqdvEPJ4PEcAy92bn9bhGciuY9MAWkLREb6q5haekwQvBiwnc98J8bACppG10mIBqARwaKfPvhL2sBl66wp0+fctLiyHFDFjLYAdvvoCk5vbJkckTxYs3kCDacPwV+PAxUKIBCD0oEJx+cAiJ9Dvjp//4l7LlzZxxZqNd/JHHgdiGC2iJzy4LqUxAuKkDjpla8luoEVk4AkloeZoPtFzUOL+2KqSIxqi5mz3edJUNtWUgjx618WpzDY3dNNTN+GemAG+uavsLSj48hViaisrEKnoWloBhRAfq9m8GULKQ0fX9iaKRF19kVtqLnF9KclsjmOR3nJXGK0DTZIgVZr4yX3j2KRZ+dQrKW2sulIaUSqHh1PJTl1UAkBVXXW1VVbxpJ5zoNNUdpk8BWXjuXl8U21DHV1plOGU2S8cnIuWS88E4b5n96FsmJMgQxS9OnIWdG4VKTMF8e024+Xr7Zo/PjnB7O5DRoWZVYItiq8IX8+tNtcXS2I0hFsgUyNC5j+aYTeGx/B0bqRHACB5hGp1hDzIp1qH3h5iviwJHJHy5Ehc9HG0a7of4LvjrxK61/3lBG+y2QyKpMHcOFZ986g9n7ujAyRYIi5lDIVQzqke6+7MD7DW7v4VRkEEOlCdS8Nw9lXsr6f+GNaqdzW27IjCx30d0TsXjjeTy493ekpkpwS3SPrZG4m2daOtK929sSl/BB1SyEB/oxWDJ8a/g662KQxKDO6eQSVKNgLwhexMwD/VCn23FJZkwM79T1xG6fqA32a1H8PNKNN8unIBq+fnt4I8EFgpMFoBUgcD/qP49joC5qmDy+x8OsXSpivbKZQTEtS29u8M7h663Lb+dgbjZI83lvRDHm4FVEamOHBSG6VTPDXVM902j1Q7SEaQQk4+7gL6ZPrlessn31G+OoOTjU1lV9sflS/HT70opH6KZcxwPuOoIP3Bt8WfTI2GknqrfUr02E9OqRbf3oiV8dvoAny6ffN/xvgcG0BE4DDGUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </button>
                            <button class="btn">
                                Apple Store
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="18"
                                    height="21"
                                    viewBox="0 0 18 21"
                                >
                                    <image
                                        id="Apple-Logo-black"
                                        width="18"
                                        height="21"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABHNCSVQICAgIfAhkiAAAAWFJREFUOE+llM8rRFEUx40VVmI3ZEEjNCkLGykbZKMUqbe1UNamKGk2ivwHytoGCyJpsqDGShZIsZiNmcKKyY9E8Tm6p26Pabzj1qd77pnz/c599513Y0EQVEQY09ROwRKs+rpYBKM9hENO3Md8ZDGaQ7TghFfMbeGn+MuOqhC9OuE7cwvcWIyqEZ3DLqRAzH6M8I7GqRiFRsjDDmzBCyRhGEagEs5gDfbF1TfadoXhf7slcQ+dv+2E3CZMqNE6i7ESheXSTxQkxKiHIFuuusTvOfId8CZGKwSTRqNBdBk9o2vZmsHoEU2t6mRHnwYTkUgvNflGBRZxg9kHmnoo6qNtEEjvWMYMomU1miVYtLigkWPphWM5o3aCS6ORytLakKdkuv5hdqBG/Zh894NxNPjfmn9xRfGTe2reN6ohIb1RF8HlhNpufWu+rpWF3ALaVw/Eh3AHze4NyUUn4wIG4FkWX2MdTncYDySxAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="mobile-images">
                        <img src="{{asset('theme/landing/images/mobile-1.png')}}" alt="برنامه موبایل" />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->

    <section class="rootix-company counter-row-2 services-rootix-company">
        <div class="container">
            <div class="company-title">
                <h2 class="about-title-res service-move-title-res">پنل کاربری روتیکس</h2>
                <h2 class="about-title-res service-move-title-res">تحول خرید و فروش  رمز ارز ها</h2>
            </div>
            <div class="row row-sec-5-four">
                <div class="col-12">
                    <div>
                        <button class="about-video services-a-video">
                            <img src="{{asset('theme/landing/images/about-us/res-video.png')}}" alt="فیلم">
                            <video src="https://www.w3schools.com/tags/movie.mp4" controls class="self-video"></video>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Start Section Three -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/about-video-playr.js')}}"></script>
@endsection
