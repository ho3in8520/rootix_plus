@extends('templates.landing_page.auth.desktop.master_page')
@section('title_browser')
   Login Rootix
@endsection
@section('form')
    <form class="register-form" action="{{route('login.login')}}" method="post">
        @csrf
        <div class="login-form-container__form">
            <div
                class="register-form-container__form login-form-container__form">
                <div class="register-form-container__input-container">
                    <div class="register-form-container__input-img">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="31.393"
                            height="25"
                            viewBox="0 0 31.393 25">
                            <g transform="translate(-2.304 -5.5)">
                                <path
                                    d="M6,6H30a3.009,3.009,0,0,1,3,3V27a3.009,3.009,0,0,1-3,3H6a3.009,3.009,0,0,1-3-3V9A3.009,3.009,0,0,1,6,6Z"
                                    fill="none"
                                    stroke="#000"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="1"/>
                                <path
                                    d="M33,9,18,19.5,3,9"
                                    fill="none"
                                    stroke="#000"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="1"/>
                            </g>
                        </svg>
                    </div>

                    <div class="register__input-container">
                        <div class="register__input">
                            <label for="email">Email</label>
                            <input type="text"
                                   id="email" name="email"/>
                            @error('email') <p class="email-text-error active">{{ $message }}</p>@enderror
                        </div>
                    </div>
                    <div class="valid-logo" id="check-email">
                        <svg
                            id="checklist"
                            xmlns="http://www.w3.org/2000/svg"
                            width="27.754"
                            height="27.754"
                            viewBox="0 0 27.754 27.754">
                            <g id="Group_8" data-name="Group 8">
                                <circle
                                    id="Ellipse_1"
                                    data-name="Ellipse 1"
                                    cx="13.877"
                                    cy="13.877"
                                    r="13.877"
                                    transform="translate(0 0)"
                                    fill="#00af80"/>
                                <circle
                                    id="Ellipse_2"
                                    data-name="Ellipse 2"
                                    cx="11.432"
                                    cy="11.432"
                                    r="11.432"
                                    transform="translate(2.445 2.445)"
                                    fill="#00af80"/>
                                <path
                                    id="Path_12"
                                    data-name="Path 12"
                                    d="M46.254,57.11a1.353,1.353,0,0,1-.972-.411l-4.4-4.533a1.354,1.354,0,1,1,1.943-1.886l3.395,3.5L52.361,47a1.354,1.354,0,1,1,2.007,1.818L47.26,56.666a1.355,1.355,0,0,1-.982.444Z"
                                    transform="translate(-33.731 -37.955)"
                                    fill="#fff"/>
                            </g>
                        </svg>
                    </div>
                </div>

                <div class="register-form-container__input-container-2">
                    <div class="register-form-container__input-container">
                        <div class="register-form-container__input-img">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="26"
                                height="36.356"
                                viewBox="0 0 26 36.356">
                                <g transform="translate(0)">
                                    <path
                                        d="M72.295,13.925H68.064V7.712a8.072,8.072,0,0,0-16.129,0v6.258H47.705a.7.7,0,0,0-.705.705V31.6a4.758,4.758,0,0,0,4.715,4.759H68.241A4.768,4.768,0,0,0,73,31.6V14.631A.7.7,0,0,0,72.295,13.925ZM53.346,7.712a6.664,6.664,0,0,1,13.308,0v6.258H53.346ZM71.59,31.6A3.3,3.3,0,0,1,68.285,34.9H51.715A3.3,3.3,0,0,1,48.41,31.6V15.38H71.59Z"
                                        transform="translate(-47)"/>
                                    <path
                                        d="M132.617,178.2v3.79a.705.705,0,0,0,1.41,0V178.2a3.922,3.922,0,1,0-1.41,0Zm.705-6.39a2.511,2.511,0,0,1,2.512,2.512,2.512,2.512,0,1,1-5.024,0A2.511,2.511,0,0,1,133.322,171.81Z"
                                        transform="translate(-120.322 -151.627)"/>
                                </g>
                            </svg>
                        </div>

                        <div class="register__input-container">
                            <div class="register__input">
                                <label for="password">Password</label>
                                <input type="password" id="password"
                                       name="password"/>
                            </div>
                            @error('password') <p class="password-text-error active">{{ $message }}</p>@enderror
                        </div>

                        <div class="valid-logo" id="check-password">
                            <svg
                                id="checklist"
                                xmlns="http://www.w3.org/2000/svg"
                                width="27.754"
                                height="27.754"
                                viewBox="0 0 27.754 27.754">
                                <g id="Group_8" data-name="Group 8">
                                    <circle
                                        id="Ellipse_1"
                                        data-name="Ellipse 1"
                                        cx="13.877"
                                        cy="13.877"
                                        r="13.877"
                                        transform="translate(0 0)"
                                        fill="#00af80"/>
                                    <circle
                                        id="Ellipse_2"
                                        data-name="Ellipse 2"
                                        cx="11.432"
                                        cy="11.432"
                                        r="11.432"
                                        transform="translate(2.445 2.445)"
                                        fill="#00af80"/>
                                    <path
                                        id="Path_12"
                                        data-name="Path 12"
                                        d="M46.254,57.11a1.353,1.353,0,0,1-.972-.411l-4.4-4.533a1.354,1.354,0,1,1,1.943-1.886l3.395,3.5L52.361,47a1.354,1.354,0,1,1,2.007,1.818L47.26,56.666a1.355,1.355,0,0,1-.982.444Z"
                                        transform="translate(-33.731 -37.955)"
                                        fill="#fff"/>
                                </g>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="forgot-password">
                <a href="#"> Forgot Your Password? </a>
            </div>
        </div>
        <div class="register__input-container">
            {!! htmlFormSnippet() !!}
            @error('g-recaptcha-response') <p class="password-text-error active">{{ $message }}</p>@enderror
        </div>
        <div class="register-submit-btns">
            <button type="submit">Login</button>
            <a href="{{route('register.form')}}" class="register-submit-btns__link">
                <span > Register </span>
            </a>
        </div>
    </form>
@endsection
