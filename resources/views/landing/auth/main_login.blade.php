@mobile
@include('landing.auth.mobile.login')
@endmobile

@desktop
@include('landing.auth.desktop.login')
@enddesktop
