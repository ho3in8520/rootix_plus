@extends('templates.landing_page.auth.desktop.master_page')
@section('style')

@endsection
@section('content')
    <div>
    @if(isset($message['type']) && $message['type'] == 'change-pass')
        <!-- Section Title Starts -->
            <div class="row text-center mx-auto">
                <h2 class="title-head hidden-xs">تغییر <span>رمز عبور</span></h2>
                <p class="info-form">رمز عبور خود را وارد کنید</p>
            </div>
            <!-- Section Title Ends -->
            <form method="post" action="{{ route('login.forget_password.change-password') }}" onsubmit="return false;">
            @csrf
            <!-- Input Field Starts -->
                <div class="form-group">
                    <input class="form-control @error('password') input-error-border @enderror"
                           id="password" placeholder="پسورد جدید" type="password" name="password"
                           required>
                    <span class="text-secondary d-block" style="font-size: 11px;">رمز عبور باید شامل حروف کوچیک،بزرگ،عدد و کارکترهای خاص باشد</span>
                    <br><span class="text-secondary" style="font-size: 11px;">کارکتر های خاص: !@#$%^&*()</span>
                    @error('password') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <input class="form-control @error('password_confirmation') input-error-border @enderror"
                           id="password_confirmation" placeholder="تکرار پسورد" type="password" name="password_confirmation"
                           required>
                    @error('password_confirmation') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
                <input type="hidden" value="{{ $message['token'] }}" name="token">
                <!-- Submit Form Button Starts -->
                <div class="form-group">
                    <button type="button" class="btn btn-primary ajaxStore">تغییر رمز عبور</button>
                </div>
                <!-- Submit Form Button Ends -->
            </form>
            <!-- Form Ends -->
    @else
        @if (isset($message['type']) && $message['type'] == 'error')
            <div style="display: flex; justify-content: center; text-align: center">
                <span class="alert alert-danger col-md-8 mx-auto">{{ $message['text'] }}</span>
            </div>
        @endif
        <!-- Section Title Starts -->
            <div class="row text-center mx-auto">
                <h2 class="title-head hidden-xs">فراموشی <span>رمز عبور</span></h2>
                <p class="info-form">رمز عبور خود را بازیابی کنید</p>
            </div>
            <!-- Section Title Ends -->
            <form method="post" action="{{ route('login.forget_password.send') }}" onsubmit="return false;">
            @csrf
            <!-- Input Field Starts -->
                <div class="form-group">
                    <input class="form-control @error('email') input-error-border @enderror"
                           id="email" placeholder="ایمیل" type="email" name="email"
                           required>
                    @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
                <!-- Submit Form Button Starts -->
                <div class="form-group">
                    <button type="button" class="btn btn-primary ajaxStore">بازیابی رمز عبور</button>
                </div>
                <!-- Submit Form Button Ends -->
            </form>
            <!-- Form Ends -->
        @endif
    </div>
@endsection
