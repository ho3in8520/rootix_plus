@extends('templates.landing_page.auth.mobile.master_page')
@section('title_browser')
    Register Rootix
@endsection
@section('form')
    <form class="register-form" action="{{route('register.store')}}" method="POST">
        @csrf
        <div class="register-form-container__form">
            <div class="register-form-container__input-container">
                <div class="register-form-container__input-img">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="31.393"
                        height="25"
                        viewBox="0 0 31.393 25"
                    >
                        <g transform="translate(-2.304 -5.5)">
                            <path
                                d="M6,6H30a3.009,3.009,0,0,1,3,3V27a3.009,3.009,0,0,1-3,3H6a3.009,3.009,0,0,1-3-3V9A3.009,3.009,0,0,1,6,6Z"
                                fill="none"
                                stroke="#000"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="1"
                            />
                            <path
                                d="M33,9,18,19.5,3,9"
                                fill="none"
                                stroke="#000"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="1"
                            />
                        </g>
                    </svg>
                </div>

                <div class="register__input-container">
                    <div class="register__input">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email"/>
                    </div>
                    @error('email') <p class="email-text-error active">{{ $message }}</p>@enderror
                </div>
                <div class="valid-logo" id="check-email">
                    <svg
                        id="checklist"
                        xmlns="http://www.w3.org/2000/svg"
                        width="27.754"
                        height="27.754"
                        viewBox="0 0 27.754 27.754"
                    >
                        <g id="Group_8" data-name="Group 8">
                            <circle
                                id="Ellipse_1"
                                data-name="Ellipse 1"
                                cx="13.877"
                                cy="13.877"
                                r="13.877"
                                transform="translate(0 0)"
                                fill="#00af80"
                            />
                            <circle
                                id="Ellipse_2"
                                data-name="Ellipse 2"
                                cx="11.432"
                                cy="11.432"
                                r="11.432"
                                transform="translate(2.445 2.445)"
                                fill="#00af80"
                            />
                            <path
                                id="Path_12"
                                data-name="Path 12"
                                d="M46.254,57.11a1.353,1.353,0,0,1-.972-.411l-4.4-4.533a1.354,1.354,0,1,1,1.943-1.886l3.395,3.5L52.361,47a1.354,1.354,0,1,1,2.007,1.818L47.26,56.666a1.355,1.355,0,0,1-.982.444Z"
                                transform="translate(-33.731 -37.955)"
                                fill="#fff"
                            />
                        </g>
                    </svg>
                </div>
            </div>

            <div class="register-form-container__input-container-2">
                <div class="register-form-container__input-container">
                    <div class="register-form-container__input-img">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="26"
                            height="36.356"
                            viewBox="0 0 26 36.356"
                        >
                            <g transform="translate(0)">
                                <path
                                    d="M72.295,13.925H68.064V7.712a8.072,8.072,0,0,0-16.129,0v6.258H47.705a.7.7,0,0,0-.705.705V31.6a4.758,4.758,0,0,0,4.715,4.759H68.241A4.768,4.768,0,0,0,73,31.6V14.631A.7.7,0,0,0,72.295,13.925ZM53.346,7.712a6.664,6.664,0,0,1,13.308,0v6.258H53.346ZM71.59,31.6A3.3,3.3,0,0,1,68.285,34.9H51.715A3.3,3.3,0,0,1,48.41,31.6V15.38H71.59Z"
                                    transform="translate(-47)"
                                />
                                <path
                                    d="M132.617,178.2v3.79a.705.705,0,0,0,1.41,0V178.2a3.922,3.922,0,1,0-1.41,0Zm.705-6.39a2.511,2.511,0,0,1,2.512,2.512,2.512,2.512,0,1,1-5.024,0A2.511,2.511,0,0,1,133.322,171.81Z"
                                    transform="translate(-120.322 -151.627)"
                                />
                            </g>
                        </svg>
                    </div>

                    <div class="register__input-container">
                        <div class="register__input">
                            <label for="password">Password</label>
                            <input type="password" id="password"
                                   name="password"/>
                        </div>
                        @error('password') <p class="password-text-error active">{{ $message }}</p>@enderror
                    </div>
                    <div class="valid-logo" id="check-password">
                        <svg
                            id="checklist"
                            xmlns="http://www.w3.org/2000/svg"
                            width="27.754"
                            height="27.754"
                            viewBox="0 0 27.754 27.754"
                        >
                            <g id="Group_8" data-name="Group 8">
                                <circle
                                    id="Ellipse_1"
                                    data-name="Ellipse 1"
                                    cx="13.877"
                                    cy="13.877"
                                    r="13.877"
                                    transform="translate(0 0)"
                                    fill="#00af80"
                                />
                                <circle
                                    id="Ellipse_2"
                                    data-name="Ellipse 2"
                                    cx="11.432"
                                    cy="11.432"
                                    r="11.432"
                                    transform="translate(2.445 2.445)"
                                    fill="#00af80"
                                />
                                <path
                                    id="Path_12"
                                    data-name="Path 12"
                                    d="M46.254,57.11a1.353,1.353,0,0,1-.972-.411l-4.4-4.533a1.354,1.354,0,1,1,1.943-1.886l3.395,3.5L52.361,47a1.354,1.354,0,1,1,2.007,1.818L47.26,56.666a1.355,1.355,0,0,1-.982.444Z"
                                    transform="translate(-33.731 -37.955)"
                                    fill="#fff"
                                />
                            </g>
                        </svg>
                    </div>
                </div>

                <div class="password-progress">
                    <div id="password-progress"></div>
                </div>
            </div>

            <div class="register-form-container__input-container">
                <div class="register-form-container__input-img">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="26"
                        height="36.356"
                        viewBox="0 0 26 36.356"
                    >
                        <g transform="translate(-1155.334 -413.822)">
                            <g transform="translate(1155.334 413.822)">
                                <g transform="translate(0)">
                                    <path
                                        d="M72.295,13.925H68.064V7.712a8.072,8.072,0,0,0-16.129,0v6.258H47.705a.7.7,0,0,0-.705.705V31.6a4.758,4.758,0,0,0,4.715,4.759H68.241A4.768,4.768,0,0,0,73,31.6V14.631A.7.7,0,0,0,72.295,13.925ZM53.346,7.712a6.664,6.664,0,0,1,13.308,0v6.258H53.346ZM71.59,31.6A3.3,3.3,0,0,1,68.285,34.9H51.715A3.3,3.3,0,0,1,48.41,31.6V15.38H71.59Z"
                                        transform="translate(-47)"
                                    />
                                    <path
                                        d="M132.617,178.2v3.79a.705.705,0,0,0,1.41,0V178.2a3.922,3.922,0,1,0-1.41,0Zm.705-6.39a2.511,2.511,0,0,1,2.512,2.512,2.512,2.512,0,1,1-5.024,0A2.511,2.511,0,0,1,133.322,171.81Z"
                                        transform="translate(-120.322 -151.627)"
                                    />
                                </g>
                            </g>
                            <path
                                d="M7.777,13.468,5.769,11.46l-.669.669,2.677,2.677,5.737-5.737L12.845,8.4Z"
                                transform="translate(1159.027 410.447)"
                            />
                        </g>
                    </svg>
                </div>

                <div class="register__input-container">
                    <div class="register__input">
                        <label for="password_confirmation">Repeat Password</label>
                        <input type="password"
                               id="password-again"
                               name="password_confirmation"/>
                    </div>
                </div>
                <div class="valid-logo" id="check-password-again">
                    <svg
                        id="checklist"
                        xmlns="http://www.w3.org/2000/svg"
                        width="27.754"
                        height="27.754"
                        viewBox="0 0 27.754 27.754"
                    >
                        <g id="Group_8" data-name="Group 8">
                            <circle
                                id="Ellipse_1"
                                data-name="Ellipse 1"
                                cx="13.877"
                                cy="13.877"
                                r="13.877"
                                transform="translate(0 0)"
                                fill="#00af80"
                            />
                            <circle
                                id="Ellipse_2"
                                data-name="Ellipse 2"
                                cx="11.432"
                                cy="11.432"
                                r="11.432"
                                transform="translate(2.445 2.445)"
                                fill="#00af80"
                            />
                            <path
                                id="Path_12"
                                data-name="Path 12"
                                d="M46.254,57.11a1.353,1.353,0,0,1-.972-.411l-4.4-4.533a1.354,1.354,0,1,1,1.943-1.886l3.395,3.5L52.361,47a1.354,1.354,0,1,1,2.007,1.818L47.26,56.666a1.355,1.355,0,0,1-.982.444Z"
                                transform="translate(-33.731 -37.955)"
                                fill="#fff"
                            />
                        </g>
                    </svg>
                </div>
            </div>

            <div class="register-form-container__input-container">
                <div class="register-form-container__input-img">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="25"
                        height="25"
                        viewBox="0 0 25 25"
                    >
                        <path
                            d="M18,18a6,6,0,1,0-6-6A6,6,0,0,0,18,18Zm0,3c-4.005,0-12,2.01-12,6v3H30V27C30,23.01,22.005,21,18,21Z"
                            transform="translate(-5.5 -5.5)"
                            fill="none"
                            stroke="#000"
                            stroke-width="1"
                        />
                    </svg>
                </div>

                <div class="register__input-container">
                    <div class="register__input">
                        @php($referral_code= isset($_GET['referral_code']) && $_GET['referral_code']?$_GET['referral_code']:'')
                        <label for="identifier-code">Referral Code</label>
                        @if (empty($referral_code))
                            <input type="text" id="referral-code" name="referral_code"/>
                        @else
                            <input type="text"
                                   disabled
                                   value="{{ $referral_code }}">
                            <input type="hidden" name="referral_code" value="{{ $referral_code }}">
                        @endif
                        @error('referral_code') <p class="email-text-error active">{{ $message }}</p>@enderror
                    </div>
                </div>
            </div>
        </div>

        {!! htmlFormSnippet() !!}
        <div class="register__input-container">
            @error('g-recaptcha-response') <p class="email-text-error active">{{ $message }}</p>@enderror
        </div>
        <div class="register-submit-btns">
            <button type="submit">Register</button>
            <a href="{{route('login.form')}}" class="register-submit-btns__link">
                Already Have An Account?

                <span href="#"> Login </span>
            </a>
        </div>
    </form>
@endsection

