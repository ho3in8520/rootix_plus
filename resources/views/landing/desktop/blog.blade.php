@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    مجله خبری
@endsection
<header class="blog-page-header">
    <div class="container">
        <nav class="nav blog-nav">
            @include('templates.landing_page.layout.desktop.top_menu')
        </nav>
    </div>
</header>
@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container-secondary container-secondary-blog">
            <div class="blog-page-top-slide-container">
                <div class="owl-carousel owl-theme blog-page-top-slide">
                    @if(count($new_posts)>0)
                        @foreach($new_posts as $item)
                            <div class="blog-image-slide-container">
                                <div class="blog-page-top-slide__img">
                                    <img style="width: 920px; height: 460px;"
                                         src="{{$item->blogImage}}"
                                         alt="ارز دیجیتال"
                                    />
                                </div>

                                <div class="blog-page-top-slide__info">
                                    <div class="blog-page-top-slide__title">
                                        <span>{{$item->user_guard->fullname}}</span>
                                        <span>۵ نظر</span>
                                    </div>

                                    <h1>
                                        {{$item->title}}
                                    </h1>

                                    <a href="{{route('landing.post',$item->slug)}}"> بیشتر بخوانید </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section>
        <div class="container-secondary container-secondary-blog">
            <div class="row blog-row-section-2">
                <div class="col-12 col-md-8">
                    <div class="right-blog-page">
                        @if(count($posts)>0)
                            @foreach($posts as $post)
                                <div class="main-news">
                                    <div class="main-news__image">
                                        <img src="{{$post->blogImage}}" alt="تایتل خبر"/>
                                    </div>

                                    <div class="main-news__info">
                                        <div class="main-news__writer">
                                            <span>{{$post->user_guard->fullname}}</span>
                                            <span>۲۵ نظر</span>
                                        </div>

                                        <h2 class="main-news__title">
                                            {{$post->title}}
                                        </h2>

                                        <p class="main-news__desc">
                                            {!! \Illuminate\Support\Str::limit($post->description,50) !!}
                                        </p>

                                        <a href="{{route('landing.post',$post->slug)}}" class="main-news__btn"> بیشتر
                                            بخوانید </a>
                                    </div>
                                </div>
                            @endforeach
                                <div class="pagination">
                                    {!! $posts->links() !!}
                                    {{--                                <button class="pagination-page">--}}
                                    {{--                                    <svg--}}
                                    {{--                                        xmlns="http://www.w3.org/2000/svg"--}}
                                    {{--                                        width="7.645"--}}
                                    {{--                                        height="12.38"--}}
                                    {{--                                        viewBox="0 0 7.645 12.38"--}}
                                    {{--                                    >--}}
                                    {{--                                        <path--}}
                                    {{--                                            id="Icon_material-expand-more"--}}
                                    {{--                                            data-name="Icon material-expand-more"--}}
                                    {{--                                            d="M19.926,12.885,15.19,17.61l-4.735-4.725L9,14.34l6.19,6.19,6.19-6.19Z"--}}
                                    {{--                                            transform="translate(-12.885 21.38) rotate(-90)"--}}
                                    {{--                                            opacity="0.6"--}}
                                    {{--                                        />--}}
                                    {{--                                    </svg>--}}
                                    {{--                                </button class="pagination-page">--}}
                                    {{--                                <button class="pagination-page">--}}
                                    {{--                                    <span>3</span>--}}
                                    {{--                                </button>--}}
                                    {{--                                <button class="pagination-page active"></button>--}}
                                    {{--                                <button class="pagination-page">1</button>--}}
                                </div>
                        @endif

                    </div>
                </div>

                <div class="col-12 col-md-4" dir="ltr">
                    <div class="left-blog-page">
                        <div class="blog-search">
                            <h3>جست و جو</h3>
                            <form action="{{route('landing.blog')}}" class="left-blog-page__search-form">
                                <input name="search" type="search" placeholder="جست و جو..."/>
                                <button>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="18.461"
                                        height="18.461"
                                        viewBox="0 0 18.461 18.461"
                                    >
                                        <g
                                            id="Icon_feather-search"
                                            data-name="Icon feather-search"
                                            transform="translate(0.5 0.5)"
                                            opacity="0.6"
                                        >
                                            <path
                                                id="Path_8461"
                                                data-name="Path 8461"
                                                d="M19.837,12.168A7.668,7.668,0,1,1,12.168,4.5,7.668,7.668,0,0,1,19.837,12.168Z"
                                                transform="translate(-4.5 -4.5)"
                                                fill="none"
                                                stroke="#000"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                stroke-width="1"
                                            />
                                            <path
                                                id="Path_8462"
                                                data-name="Path 8462"
                                                d="M29.145,29.145l-4.17-4.17"
                                                transform="translate(-11.891 -11.891)"
                                                fill="none"
                                                stroke="#000"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                stroke-width="1"
                                            />
                                        </g>
                                    </svg>
                                </button>
                            </form>
                        </div>

                        <div class="side-news-container">
                            <h3 class="left-blog-page__title">اخبار جدید</h3>
                            @if(count($new_posts)>0)
                                @foreach($new_posts as $item)
                                    <div class="side-news-container__side-news">
                                        <div class="side-news-container__image">
                                            <img src="{{$item->blogImage}}" alt="تایتل اخبار"/>
                                        </div>
                                        <div class="side-news">
                                            <h4>{{$item->title}}</h4>
                                            <span>{{jdate_from_gregorian($item->created_at)}}</span>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>

                        <div class="categories-blog">
                            <h3 class="left-blog-page__title">دسته بندی</h3>
                            <ul class="categories-blog__menu">
                                @if(count($categories)>0)
                                    @foreach($categories as $category)
                                        <li class="categories-blog__item">
                                            <div class="circle-blog-page"></div>
                                            <a href="#" class="categories-blog__link">
                                                {{$category->name}}
                                                <span>{{$category->posts_count}}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif

                            </ul>
                        </div>

                        {{--                        <div class="blog-tags">--}}
                        {{--                            <h3 class="left-blog-page__title">تگ ها</h3>--}}
                        {{--                            <div class="blog-tags__items">--}}
                        {{--                                <a href="#" class="blog-tags__item">رمز ارز,</a>--}}
                        {{--                                <a href="#" class="blog-tags__item">بیت کوین,</a>--}}
                        {{--                                <a href="#" class="blog-tags__item">ترید,</a>--}}
                        {{--                                <a href="#" class="blog-tags__item">بیت مارکت,</a>--}}
                        {{--                                <a href="#" class="blog-tags__item">اتریوم,</a>--}}
                        {{--                                <a href="#" class="blog-tags__item">صرافی,</a>--}}
                        {{--                                <a href="#" class="blog-tags__item">تحلیل</a>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                        <div class="newsletters-container">
                            <h3 class="left-blog-page__title">خبرنامه آنلاین</h3>

                            <div class="newsletters">
                                <div class="newsletters__desc">
                                    <p>برای دریافت خبرنامه های روتیکس</p>
                                    <p>ایمیل خود را وارد کنید و به روز باشید</p>
                                </div>

                                <form action="#" class="newsletters__form">
                                    <input type="email" placeholder="email address..."/>
                                    <button type="submit" class="newsletters__btn" disabled>
                                        ثبت ایمیل
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->
@endsection
@section('script')

@endsection
