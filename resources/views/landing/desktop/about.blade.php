@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    درباره ما
@endsection
<header class="about-header">
    <div class="container">
        <nav>
            @include('templates.landing_page.layout.desktop.top_menu')
        </nav>

    </div>
    <div class="d-flex items-center header-row">

        <div class="about-header-image">
            <img src="{{asset('theme/landing/images/about-us/about-header-image.png')}}" alt="درباره ما">
        </div>

        <div class="about-header-info">
            <h5>درباره روتیکس</h5>
            <h1>شرکتی خلاق و کاربردی</h1>
        </div>
    </div>

    <div class="go-down-container">
        <a href="#currency-table" class="go-down about-go-down-container">
            <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="به پایین رفتن" />
        </a>
    </div>
</header>
{{--@section('header')--}}
{{--    <div class="d-flex items-center header-row">--}}

{{--        <div class="about-header-image">--}}
{{--            <img src="{{asset('theme/landing/images/about-us/about-header-image.png')}}" alt="درباره ما">--}}
{{--        </div>--}}

{{--        <div class="about-header-info">--}}
{{--            <h5>درباره روتیکس</h5>--}}
{{--            <h1>شرکتی خلاق و کاربردی</h1>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="go-down-container">--}}
{{--        <a href="#currency-table" class="go-down about-go-down-container">--}}
{{--            <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="به پایین رفتن" />--}}
{{--        </a>--}}
{{--    </div>--}}
{{--@endsection--}}
@section('content')
    <!-- Start Section One -->
    <section class="history">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-6">
                    <div class="company-history-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-1.png')}}" alt="تاریخچه شرکت">
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="history-descriptions">
                        <h3>تاریخچه شرکت</h3>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="about-currency">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-5">
                    <div class="about-currency-descriptions">
                        <h5>درباره ارز دیجیتال</h5>
                        <h2>
                            ارز دیجیتال
                            <br>
                            همیشه و همه جا به صرفه
                        </h2>
                        <h3>ارز دیجیتال چطوری به کسب و کارت کمک می کنه؟</h3>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. </p>
                        <a href="#" class="about-btn about-currency-btn">
                            شروع خرید و فروش
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-7">
                    <div class="about-currency-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-2.png')}}" alt="تاریخچه شرکت">
                    </div>
                </div>

            </div>
        </div>

        <img src="{{asset('theme/landing/images/about-us/left-background-sec-3.png')}}" alt="بک گراند راست" class="back-sec-3 left-back">
        <img src="{{asset('theme/landing/images/about-us/right-background-sec-3.png')}}" alt="بک گراند چپ" class="back-sec-3 right-back">
    </section>
    <!-- End Section Two -->

    <!-- Start Section Three -->
    <section class="rootix-team">
        <div class="container-secondary">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="team-container">
                        <div class="three-team-container">
                            <div class="team">
                                <img src="{{asset('theme/landing/images/about-us/user-team-1.png')}}" alt="عضو اول">
                                <h5>name name</h5>
                            </div>
                            <div class="team">
                                <img src="{{asset('theme/landing/images/about-us/user-team-2.png')}}" alt="عضو دوم">
                                <h5>name name</h5>
                            </div>
                            <div class="team">
                                <img src="{{asset('theme/landing/images/about-us/user-team-3.png')}}" alt="عضو سوم">
                                <h5>name name</h5>
                            </div>
                        </div>
                        <div class="three-team-container">
                            <div class="team">
                                <img src="{{asset('theme/landing/images/about-us/user-team-4.png')}}" alt="عضو چهارم">
                                <h5>name name</h5>
                            </div>
                            <div class="team">
                                <img src="{{asset('theme/landing/images/about-us/user-team-5.png')}}" alt="عضو پنجم">
                                <h5>name name</h5>
                            </div>
                            <div class="team">
                                <img src="{{asset('theme/landing/images/about-us/user-team-6.png')}}" alt="عضو ششم">
                                <h5>name name</h5>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="team-info">
                        <h2>اعضا تیم روتیکس</h2>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت</p>

                        <a href="#" class="about-btn">
                            به ما بپیوندید!
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Three -->


    <!-- Start Section Four -->
    <section class="rootix-company counter-row-2">
        <div class="container-secondary">
            <div class="company-title">
                <h2>یک شرکت قوی در زمینه ارز دیجیتال</h2>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای</p>
            </div>
            <div class="row row-sec-5-four">
                <div class="col-12">
                    <div>
                        <button class="about-video">
                            <img src="{{asset('theme/landing/images/about-us/sec-four.png')}}" alt="فیلم">
                            <video src="https://www.w3schools.com/tags/movie.mp4" controls class="self-video"></video>
                        </button>
                        <hr class="rootix-company-line">
                    </div>
                </div>
            </div>
            <div class="row counter-row-1">
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>مشتری های پنل</h3>
                        <h5 id="about-customers">۲۵۶,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>تراکنش های روزانه</h3>
                        <h5 id="daily-transaction">۲۵۶,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>تراکنش های ماهیانه</h3>
                        <h5 id="monthly-transaction">۲۵۶,000</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section class="team-contact">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-6">
                    <div class="team-contact__form-container">
                        <div>
                            <h2 class="team-contact__title">با تیم ما در ارتباط باشید</h2>
                            <p class="team-contact__desc">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و</p>
                        </div>

                        <form action="#" class="team-contact__form">
                            <div class="team-contact__inputs">
                                <input type="email" placeholder="نام خود را وارد کنید..." class="team-contact__input ">
                                <input type="text" placeholder="ایمیل خود را وارد کنید..." class="team-contact__input ">
                            </div>

                            <textarea placeholder="پیام خود را بنویسید..." class="team-contact__textarea"></textarea>

                            <button type="submit" class="team-contact__btn about-btn ">ارسال</button>
                        </form>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="about-form__image">
                        <img src="{{asset('theme/landing/images/about-us/Ai.png')}}" class="form-image" alt="ارتباط با ما">
                    </div>
                </div>
            </div>
        </div>
        <img src="{{asset('theme/landing/images/about-us/back-sec-5.png')}}" alt="بک گراند سکشن آخر" class="back-sec-five">
    </section>
    <!-- End Section Five -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/about-counter.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/about-video-playr.js')}}"></script>
@endsection
