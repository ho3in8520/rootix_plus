@extends('templates.admin.master_page')
@section('title_browser')
    ایجاد دسته بندی
@endsection
@section('style')
    <style>
        #ckeditor {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('categories.store')}}" method="post">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="name" class="">عنوان</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="description" class="">توضیحات</label>
                                <textarea id="ckeditor" name="description" rows="10" cols="125">
            </textarea>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-2 col-md-4 col-sm-6">
                                <label for="parent_id">زیرمجموعه</label>
                                <select name="parent_id" class="form-control select2-single">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-4 col-sm-6">
                                <label for="status">وضعیت</label>
                                <select name="status" class="form-control">
                                    <option value="">انتخاب کنید</option>
                                    <option value="1">فعال</option>
                                    <option value="0">غیرفعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-info btn-lg ajaxStore">ایجاد</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        CKEDITOR.replace('ckeditor',
            {
                customConfig: 'config.js',
                toolbar: 'simple'
            });

    </script>
@endsection
