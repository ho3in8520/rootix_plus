@extends('templates.user.master_page')
@section('title_browser') Dashboard @endsection
@section('style')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css/pages/dashboard-ecommerce.min.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/vendors/css/charts/apexcharts.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css/core/menu/menu-types/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css/core/colors/palette-gradient.min.css') }}">
    <!-- END: Page CSS-->

@endsection
@section('content')
    <div class="row settings">
        <div class="col-md-12 col-lg-12">
        @if(!auth()->user()->is_complete_steps)
            <!-- Level User Alert -->
                <div class="col-md-12">
                    @include('general.flash_message')
                    <div class="alert alert-warning py-2 text-center">
                        <span>Your level {{ convertStepToPersian(auth()->user()->step_complate) }}</span>
                        <a class="btn btn-warning text-white"
                           href="{{ route('step.verify-mobile') }}">Continue the authentication process</a>
                    </div>
                </div>
                <!--## Level User Alert -->
        @endif
        <!-- Top Widget -->
            <div class="row">
                @foreach($price_currency as $key=>$item)
                    <div class="col-xl-2 col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-header py-0" style="background-color: {{ currency_color($key) }}">
                                    <img class="card-img img-fluid mx-auto p-2" style="width: 85%;"
                                         src="{{ asset('/theme/user/assets/img/'.strtoupper($key).'.png') }}"
                                         alt="Card image cap">
                                </div>
                                <div class="card-body py-1 text-center" style="font-size: small">
                                    <h5 class="">{{ number_format($item['bestSell']/10) }}
                                        <span class="text-black-50" style="font-size: 13px;">Toman</span>
                                    </h5>
                                    @if ($item['dayChange'] >0 )
                                        <p class="badge badge-success mb-0">{{ $item['dayChange'] }}%</p>
                                    @else
                                        <p class="badge badge-danger mb-0">{{ $item['dayChange'] }}%</p>
                                    @endif
                                    <hr style="margin: 5px auto;">
                                    <h5 class="">{{ number_format($item['dayLow']/10) }}
                                        <span class="text-black-50" style="font-size: 13px;">Toman</span>
                                    </h5>
                                    <p class="text-black-50">Miniumum Toman</p>
                                    <hr style="margin: 5px auto;">
                                    <h5 class="">{{ number_format($item['dayHigh']/10) }}
                                        <span class="text-black-50" style="font-size: 13px;">Toman</span>
                                    </h5>
                                    <p class="text-black-50">Maximum Toman</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!--## Top Widget -->

            <div class="row">
                <!-- Wallet Chart -->
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Wallets</h4>
                        </div>
                        <div class="card-body row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        @foreach($assets as $item)
                                            <tr>
                                                <td><img
                                                        src="{{ asset('theme/user/assets/img/'.strtoupper($item['logo']).'.png') }}"
                                                        width="45"></td>
                                                <td class="font-weight-bold">{{ $item['unit'] }}</td>
                                                <td>{{ number_format($item['amount']) }} {{ $item['unit'] }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--## Wallet Chart -->

                <!-- profile Status -->
                <div class=" col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Account status</h4>
                        </div>
                        <div class="card-body row">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td class="font-weight-bold">Level:</td>

                                        <td>Confirmed level {{ convertStepToPersian(auth()->user()->step_complate) }} <a
                                                href="{{ route('step.verify-mobile') }}"
                                                class="px-1 text-info">Upgrade</a></td>
                                        <td><a class="fa fa-question-circle text-info" style="font-size: 20px;"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">Daily withdrawal of Rials:</td>
                                        <td>0 From {{ number_format(transactionLimit()) }} Toman</td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--## profile Status -->
            </div>

        </div>
    </div>
@endsection
@section('script')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/user/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script src="{{ asset('theme/user/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/user/app-assets/js/scripts/charts/chart-apex.min.js') }}"></script>
    <!-- END: Page JS-->

    <script>
        var walletChartOptions = {
            chart: {
                type: 'donut',
                height: 350
            },
            colors: ['#01c19c', '#fe9e56'],
            series: [44, 55],
            labels: ['Team A', 'Team B'],
            legend: {
                show: false
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 350
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                },
                {
                    breakpoint: 380,
                    options: {
                        chart: {
                            width: 200,
                            height: 260
                        },
                    }
                },
            ]
        }
        var WalletChart = new ApexCharts(
            document.querySelector("#wallet-chart"),
            walletChartOptions
        );
        WalletChart.render();
    </script>


    <script>
        $(document).ready(async function () {
            var usdt = document.getElementsByClassName('usdt_balance');
            if (usdt && "{{!empty($token)}}") {
                var contract = await tronWeb.contract()
                    .at("TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");

                var result = await contract.balanceOf("{{!empty($token['address']['base58']) ? $token['address']['base58'] : ' '}}").call();
                console.log(result / 1000000);
                $('.usdt_balance').html(result / 1000000);
            }
        });

        function separate(Number) {
            Number += '';
            Number = Number.replace(',', '');
            x = Number.split('.');
            y = x[0];
            z = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(y))
                y = y.replace(rgx, '$1' + ',' + '$2');
            return y + z;
        }

        $('#tag').on('click', function () {
            if ("{{isset($token['address']['base58'])}}") {
                $(this).attr('href', "{{route('swap.show',['unit'=>'usdt'])}}");
            } else {
                swal('Unsuccess', 'please first create wallet!! after you can swap currency.', 'error')
            }
        });


        function create_account(unit) {
            const fullNode = 'https://api.trongrid.io';
            const solidityNode = 'https://api.trongrid.io';
            const eventServer = 'https://api.trongrid.io';

            const tronWeb = new TronWeb(fullNode, solidityNode, eventServer);
            var account = tronWeb.createAccount();
            var res = Promise.resolve(account);
            res.then(function (v) {
                var account = v;

                $.ajax({
                    url: '{{ route('createAccount', '') }}' + "/" + unit,
                    type: 'post',
                    data: {account: account, "_token": "{{ csrf_token() }}"},
                    success: function (data) {
                        if (data.status == '100') {
                            swal('success', data.msg, 'success');
                            setTimeout(function () {
                                window.location.reload()
                            }, 2000)
                        } else {
                            swal('Unsuccess', data.msg, 'danger');
                        }

                    }
                });
            }, function (e) {
                console.error(e); // TypeError: Throwing
            });
        }

        function copy(input) {
            var copyText = document.getElementById(input);
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
            swal('success', 'Your address was successfully copied!', 'success');
        }


        $(document).ready(function () {
            $(document).on('keyup', '.amount-rial', function () {
                var val = $(this).val();
                $(this).val(numberFormat(val))
            })
        })
    </script>
@endsection
