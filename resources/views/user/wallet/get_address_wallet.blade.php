<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="text-center">
                {!!  QrCode::size(150)->generate($address); !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="address wallet" id="address"
                   aria-label="Recipient's username" aria-describedby="button-addon2" value="{{ $address }}" readonly>
            <div class="input-group-append">
                <a title="copy" class="btn btn-info text-white copy-address" type="button" id="button-addon2"><i
                        class="fa fa-copy"></i>
                </a>
            </div>
        </div>
    </div>
</div>
