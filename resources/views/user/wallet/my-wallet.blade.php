@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-MyWallet')}}
@endsection
@section('style')
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <style>
        :root {
            --animate-duration: 5s;
        }
        @-moz-keyframes spin {
            100% {
                -moz-transform: rotateY(360deg);
            }
        }
        @-webkit-keyframes spin {
            100% {
                -webkit-transform: rotateY(360deg);
            }
        }
        @keyframes spin {
            100% {
                -webkit-transform: rotateY(360deg);
                transform: rotateY(360deg);
            }
        }
        .animate__flipOutY {
            -webkit-animation: spin 6s linear infinite;
            -moz-animation: spin 6s linear infinite;
            animation: spin 6s linear infinite;
        }
        .table thead th,
        .table tbody td {
            text-align: center !important;
        }
        .table tbody td:first-child div {
            display: inline-grid;
        }
        .table tbody td p {
            line-height: 0rem;
        }
        .table tbody td small {
            display: block;
        }
        .body-withdraw a {
            padding: .9rem 1rem;
        }
        @media only screen and (min-width: 600px) {
            .mt-5 {
                margin-top: 4.3rem !important;
            }

        }
    </style>
@endsection
@section('content')
    <!-- BEGIN: Content-->

    <div class="card mt-3">
        <div class="card-header">
            <h2 class="content-header-title float-left mb-0">Fast currency Swap</h2>
        </div>
        <div class="card-body">
            <form>
                <div class="row col-md-12 mx-auto">
                    <div class="col-md-5">
                        <div class="form-group  mt-3">
                            <div class="input-group ">
                                <label class="col-12">Currency</label>
                                <select class="form-control" id="unit1" onchange="send_ajax(this,'amount1')">
                                    <option value="">Origin currency</option>
                                    @foreach($assets as $asset)
                                        <option>{{$asset->unit}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <label class="col-12">Inventory: <span style="color: green" id="amount1">0</span></label>

                            <input aria-label="Example text with button addon" aria-describedby="button-addon1"
                                   type="text" class=" form-control  " id="amount_swap1"
                                   onkeyup="($(this).val(numberFormat($(this).val())),getPrice())" disabled
                                   placeholder="To Active select origin and destination currency ">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon1">total</button>
                            </div>
                            <label style="color: red" class="fee col-12">Your commission is 0</label>
                        </div>
                    </div>
                    <div class="col-md-2 m-auto text-center" style="">
                        <i class="fa fa-exchange" style="font-size: 35px;color: #000;"></i>
                    </div>
                    <div class="col-md-5 ">
                        <div class="form-group mt-3">
                            <div class="input-group ">
                                <div class="input-group ">
                                    <label class="col-12">Currency</label>
                                    <select class="form-control" id="unit2" onchange="send_ajax(this,'amount2')">
                                        <option value="">Destination currency</option>
                                        @foreach($assets as $asset)
                                            <option>{{$asset->unit}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-12">Inventory<span style="color: green" id="amount2">0</span></label>
                            <input type="text" class=" form-control " id="amount_swap2" disabled>
                        </div>
                    </div>
                </div>
                <div class="row col-md-12 mx-auto">
                    <div class="col-md-2 ml-auto">
                        <button type="button" class="btn btn-outline-warning waves-effect waves-light float-right"
                                data-toggle="modal" id="button_click">
                            Swap
                        </button>
                    </div>
                </div>
            </form>

        </div>
        <div class="modal fade text-left" id="warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel140"
             aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-warning white">
                        <h5 class="modal-title" id="myModalLabel140">Swap confirm</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th scope="row">Origin currency</th>
                                    <td class="currency1"></td>

                                </tr>
                                <tr>
                                    <th scope="row">Destination Currency</th>
                                    <td class="currency2"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Amount entered</th>
                                    <td class="amount"></td>
                                </tr>
                                <tr>
                                    <th scope="row">commission</th>
                                    <td class="fee_swap"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Your deposit :</th>
                                    <td class="result"></td>
                                </tr>
                                </tbody>
                            </table>
                            <button class="text-center btn btn-success">Are you confirm?</button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success waves-effect waves-light" onclick="swap()">Confirm</button>
                        <button type="button" class="btn btn-warning waves-effect waves-light" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2 class="content-header-title float-left mb-0">
                        assets
                        <span class="btn btn-info btn-sm  refresh-wallet">
                            Update
                            <i class="fa fa-refresh animate__animated animate__infinite	animate__faster" style="font-size: 15px; cursor: pointer"></i>
                        </span>
                    </h2>
                </div>
                <div class="card-body">
                    <div class="content">
                        <section id="data-list-view" class="data-list-view-header">
                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="wallet-table">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Currency</th>
                                        <th>Inventory</th>
                                        <th>Operation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="5">Loading please wait...</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->

                        </section>
                        <!-- Data list view end -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END: Content-->
    <div class="modal fade text-left" id="backdrop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Deactive</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left" id="backdrop1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel4">Deactive</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left receive-modal" id="backdrop" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel4"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel4">Wallet address</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body body-receive">

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left modal-withdraw" id="staticBackdrop" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Withdrawal of assets</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    @csrf
                    <div class="modal-body body-withdraw">
                        <div class="text-center loading-page" style="display: block">
                            <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                                <span class="sr-only">please wait...</span>
                            </div>
                            <h4>please wait...</h4>
                        </div>
                        <div class="row justify-content-center withdraw-content" style="display: none">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="card border-secondary">
                                                    <div class="card-body modal-body-logo-unit text-center">
                                                        <img src="" width="100" class="animate__flipOutY">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="card border-secondary">
                                                    <div class="card-body">
                                                        <p class="text-info">Currency : <span
                                                                class="font-weight-bold modal-body-info-unit"></span>
                                                        </p>
                                                        <p class="text-success">Assets : <span
                                                                class="font-weight-bold modal-body-info-amount">0</span>
                                                        </p>
                                                        <p class="text-danger">Commission : <span
                                                                class="font-weight-bold modal-body-info-fee">0</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>Address</label>
                                <input class="form-control" name="address">
                            </div>
                            <div class="col-md-6">
                                <label>Amount</label>
                                <div class="input-group">
                                    <input class="form-control" name="amount">
                                    <div class="input-group-append">
                                        <a class="btn btn-outline-secondary all-amount" title="Total assets"
                                           id="button-addon2"><i
                                                class="fa fa-dollar"></i> </a>
                                    </div>
                                </div>
                                <p class="text-primary">Final amount : <span
                                        class="font-weight-bold modal-body-result-fee">0</span></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary withdraw-submit">OK</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade text-left security-modal" id="staticBackdrop" data-backdrop="static" tabindex="-1"
         role="dialog"
         aria-labelledby="myModalLabel4"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel4">Confirm the transaction</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body body-security">

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('theme/user/app-assets/js/scripts/modal/components-modal.min.js')}}"></script>
    <script>
        let feeUnit = 0;
        $(document).ready(function () {
            let button_click = $('#button_click');
            $(button_click).attr("disabled", true)

            $(document).on('click', '.receive', function () {
                $(".body-receive").html('<div class="text-center">\n' +
                    '                        <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">\n' +
                    '                            <span class="sr-only">Loading...</span>\n' +
                    '                        </div>\n' +
                    '                        <h4>Please Wait</h4>\n' +
                    '                    </div>');
                $(".receive-modal").modal("show");
                var type = $(this).data('type');
                $.post(`{{ route('wallet.get-address-wallet') }}`, {
                    type: type,
                    _token: `{{ csrf_token() }}`
                }).done(response => {
                    $(".body-receive").html(response)
                }).fail(error => {
                    swal('error', 'Invalid request', 'error')
                });
            })
            $(document).on('click', '.copy-address', function () {
                var val = $(this).closest(".input-group").find("input").val();
                copy()
            })

            $(document).on('click', '.withdraw', function () {
                var elem = $(this);
                var trc = $(this).data('trc');
                var unit = $(this).data('type');
                var amount = $(this).data('amount');
                var security = $(this).data('security');
                var fee = 0;
                $.post(`{{ route('wallet.withdrawal-fee') }}`, {
                    '_token': `{{ csrf_token() }}`,
                    'unit': unit
                }, function (response) {
                    if (response.status == 100) {
                        fee = response.data
                        feeUnit = response.data
                        $(".body-withdraw").append("<input type='hidden' value='" + trc + "' name='trc'><input type='hidden' value='" + unit + "' name='unit'>");
                        $(".modal-body-logo-unit").find("img").attr('src', logo_unit(unit));
                        $(".modal-body-info-unit").html(unit.toUpperCase());
                        $(".modal-body-info-amount").html(amount);
                        $(".modal-body-info-fee").html(fee);
                        $(".loading-page").hide();
                        $(".withdraw-content").show();
                    }
                })

                $(".modal-withdraw").modal("show");
                $(document).on('click', '.all-amount', function () {
                    if (amount > fee) {
                        $(this).closest('.input-group').find("input").val(amount)
                        $(".modal-body-result-fee").html(numberFormat(amount - feeUnit));
                    } else {
                        $(this).closest('.input-group').find("input").val(0)
                        $(".modal-body-result-fee").html(0);
                    }
                });
            });
            $(document).on('click', '.withdraw-submit', function () {
                var elem = this;
                var unit = $(this).closest('.modal-content').find("input[name='unit']").val();
                var amount = $(this).closest('.modal-content').find("input[name='amount']").val();
                var address = $(this).closest('.modal-content').find("input[name='address']").val();
                withdraw(unit, amount, address, elem)
            });

            $(document).on('click', '.withdraw-store', function () {
                var data = new FormData($(this).closest('form')[0]);

                $.ajax({
                    url: `{{ route('withdraw.store') }}`,
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == 100) {
                            swalResponse(response.status, response.msg, 'Success')
                            setTimeout(function () {
                                window.location.reload()
                            }, 3000)
                        } else {
                            swalResponse(response.status, response.msg, 'Error')
                            return false;
                        }
                    }, error: function (xhr) {
                        swalResponse(500, 'Invalid request', 'Error')
                    },
                })
            });

            $(document).on('keyup', "input[name='amount']", function () {
                if (($(this).val() - feeUnit) > 0) {
                    $(".modal-body-result-fee").html(numberFormat($(this).val() - feeUnit));
                } else {
                    $(".modal-body-result-fee").html(0);
                }
            });
        });

        function copy() {
            /* Get the text field */
            var copyText = document.getElementById("address");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            document.execCommand("copy");

            /* Alert the copied text */
            swal('success', 'Copied successfully', 'success')
        }

        function my_wallet(data) {
            var tr = '';
            if (data === false) {
                tr = '<tr><td colspan="3" class="text-center">Nothing found!</td></tr>';
            } else if (data.hasOwnProperty('assets')) {
                var assets = data.assets;
                var to_rial = data.to_rial;
                var to_usdt = data.to_usdt;
                assets.map(function (item, index) {
                    item.unit = item.unit === 'rial' ? 'rls' : item.unit;
                    let title = item.unit === 'rls' ? 'Rial' : 'Unit';

                    let key = item.unit;
                    let R_unit = '';
                    let R_price = '';
                    if (to_rial.hasOwnProperty(item.unit)) {
                        R_unit = `<span class="d-block" style="font-size: 13px">${numberFormat(to_rial[item.unit]['bestSell'])} Rial</span>`;
                        R_price = `<span class="d-block" style="font-size: 13px">${numberFormat(to_rial[item.unit]['bestSell'] * item.amount)} Rial</span>`;
                    }

                    key = item.unit;
                    let T_unit = '';
                    let T_price = '';
                    if (to_usdt.hasOwnProperty(key) && key != 'usdt') {
                        T_unit = to_usdt.hasOwnProperty(key) ? `<span class="d-block" style="font-size: 13px">${numberFormat(to_usdt[key])} Tether</span>` : '';
                        T_price = to_usdt.hasOwnProperty(key) ? `<span class="d-block" style="font-size: 13px">${numberFormat(to_usdt[key] * item.amount)} Tether</span>` : '';
                    }

                    tr += `<tr>
                            <td>
                            <img src="${logo_unit(item.unit)}" width="50">
                            </td>
                            <td>
                            <span class="mb-1 font-weight-bold">${item.unit.toUpperCase()}</span>
                            ${R_unit}
                            ${T_unit}
                            </td>
                            <td>
                                <span class="mb-1" style="font-size: 15px;">${numberFormat(item.amount)} ${title}</span>
                                ${R_price}
                                ${T_price}
                            </td>`
                    if (item.unit == 'rls') {
                        tr += `<td>
                                <a href="{{ route('charge_wallet.view') }}" class="btn btn-success">Deposit</a>
                            </td>`
                    } else {
                        tr += ` <td>
                                <a href="#" class="btn btn-success receive" data-type="usdt">Deposit</a>
                                <a class="btn btn-danger withdraw text-white" data-trc="${item.type_token}" data-amount="${item.amount}" data-type="${item.unit}">Withdraw</a>
                            </td>`
                    }
                    tr += `</tr>`;
                });
            }
            $('.refresh-wallet').removeClass('animate__rotateIn');
            $("#wallet-table tbody").html(tr);
        }

        function logo_unit(unit) {
            return `/theme/user/assets/img/${unit}.png`;
        }

        function withdraw(unit, amount, address, elem) {
            swal({
                title: "Is it from withdrawing assets?",
                icon: "warning",
                buttons: ['Cancel', 'Confirm'],
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $(elem).text('Processing');
                        var data = new FormData($(elem).closest('form')[0]);

                        $.ajax({
                            url: `{{ route('withdraw.security') }}`,
                            type: 'POST',
                            data: data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if (response.status) {
                                    swalResponse(response.status, response.msg, 'Error')
                                    $(elem).text('OK');
                                    return false;
                                }
                                var form = response
                                form = $(form).append("<input type='hidden' value='" + amount + "' name='amount'><input type='hidden' value='" + address + "' name='address'><input type='hidden' value='" + unit + "' name='unit'>")
                                $(".body-security").html(form)
                                $(".security-modal").modal('show');
                                $(".modal-withdraw").modal('hide');
                                $(elem).text('OK');
                            }, error: function (xhr) {
                                $(elem).text('OK');
                                errorForms(xhr);
                            },
                        })
                    }
                });
        }

        function send_ajax() {
            var unit1 = $('#unit1').val();
            var amount1 = $('#amount1');
            var unit2 = $('#unit2').val();
            var amount2 = $('#amount2');
            $('#amount_swap1').val(' ');
            $('#amount_swap2').val(' ');
            $.ajax({
                url: '{{route('get-unit-price')}}',
                type: 'post',
                data: {unit1: unit1, unit2: unit2, "_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    data1 = numberFormat(data.unit1);
                    data2 = numberFormat(data.unit2);

                    amount1.text(data1);
                    amount2.text(data2);
                }
            });

            $.ajax({
                url: '{{route('get-unit-swap-fee-price')}}',
                type: 'post',
                data: {unit1: unit1, "_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    $.data(document, "fee_swap", data);
                }
            });


            if (unit1 != unit2 && unit1 !== '' && unit2 !== '') {
                $(button_click).attr("disabled", false);
                $('#amount_swap1').attr("disabled", false);
                $('#button-addon1').attr("disabled", false);

                $('#button-addon1').on('click', function () {
                    $('#amount_swap1').val(data1);
                    getPrice();
                });
            } else {
                $(button_click).attr("disabled", true);
                $('#amount_swap1').attr("disabled", true);
                $('#button-addon1').attr("disabled", true);
            }
        }

        var usdt_amount= '{{ $usdt_amount }}';
        usdt_amount= JSON.parse(usdt_amount.replaceAll('&quot;','"'));

        function getPrice() {
            var dollar = '';
            let unit1 = $('#unit1').val();
            let unit2 = $('#unit2').val();

            var input1 = $('#amount_swap1').val();

            var input1 = input1.replace(/,/g, '');
            // var input1 = parseInt(input1);

            var math1 = [];
            var length = $.data(document, "fee_swap")[0].length;

            $.each($.data(document, "fee_swap"), function (key, value) {
                for (let i = 0; i < length; i++) {
                    var extra_field = value[i]['extra_field2'];
                    math1.push(extra_field);
                    Math.max(math1);

                    var math2 = Math.max.apply(Math, math1);
                    if (parseFloat(input1) < parseFloat(value[length - 1]['extra_field1'])) {
                        $('.fee').text('Commission is:' + 0);
                        return true;
                    } else if (parseFloat(input1) >= parseFloat(value[i]['extra_field1']) && parseFloat(input1) <= parseFloat(value[i]['extra_field2'])) {
                        $.data(document, "fee", value[i]['extra_field3']);
                        $('.fee').text('Commission is: ' + numberFormat(value[i]['extra_field3']) + ' ' + unit1);
                        return true;
                    } else if (parseFloat(input1) >= parseFloat(math2)) {
                        if (parseFloat(value[i]['extra_field2']) == parseFloat(math2)) {
                            $.data(document, "fee", value[i]['extra_field3']);
                            $('.fee').text('Commission is: ' + numberFormat(value[i]['extra_field3']) + ' ' + unit1);
                        }
                        return true;
                    } else {
                        $('.fee').text('Commission is: ' + 0);
                    }

                }
            });

            var input1 = $('#amount_swap1').val();
            var input1 = input1.replace(/,/g, '');

            var input1 = input1 - $.data(document, 'fee');

            let priceCurrency1= usdt_amount[unit1];
            let priceCurrency2= usdt_amount[unit2];
            $.each($.data(document, "fee_swap"), function (key, value) {
                for (let i = 0; i < length; i++) {
                    var extra_field = value[i]['extra_field1'];
                    math1.push(extra_field);

                    $.data(document, "math5", Math.min.apply(Math, math1));
                }
            });
            input1 = input1 * priceCurrency1;
            $main_input = $('#amount_swap1').val();
            var $main_input = $main_input.replace(/,/g, '');

            if (parseFloat($main_input) >= parseFloat($.data(document, "math5"))) {
                if (parseFloat(input1) && parseFloat(input1) > 0) {
                    var input2 = parseFloat(input1) / parseFloat(priceCurrency2);
                    var input2 = parseFloat((parseInt(input2 * 1000) / 1000).toFixed(3));
                    $('#amount_swap2').css('color', 'green').val(numberFormat(input2));
                } else {
                    $('#amount_swap2').css('color', 'green').val(0);
                }
            } else {
                if (parseFloat($.data(document, "math5"))) {
                    $('#amount_swap2').css('color', 'red').val('Minimum swap: ' + numberFormat($.data(document, "math5")) + ' ' + unit1);
                } else {
                    $('#amount_swap2').css('color', 'red').val('Problem! Pleas contanct with admin');
                }
            }

            $('.currency1').text($('#unit1').val());
            $('.currency2').text($('#unit2').val());
            $('.amount').text($('#amount_swap1').val());
            if ($.data(document, 'fee')) {
                $('.fee_swap').text(numberFormat($.data(document, 'fee')));
            } else {
                $('.fee_swap').text(0);
            }
            if (input2) {
                $('.result').text(numberFormat(input2));
            } else {
                $('.result').text(0);
            }

        }

        function swap() {

            let unit1 = $('#unit1').val();
            let unit2 = $('#unit2').val();
            var input1 = $('#amount_swap1').val();
            var input1 = input1.replace(/,/g, '');
            if (input1 == '') {
                swal('Unsuccess', 'Please fill in the fields.', 'error');
                setTimeout(function () {
                    location.reload();
                }, 2000)
            }
            var math1 = [];
            var length = $.data(document, "fee_swap")[0].length;
            $.each($.data(document, "fee_swap"), function (key, value) {
                for (let i = 0; i < length; i++) {
                    var extra_field = parseInt(value[i]['extra_field1']);
                    math1.push(extra_field);

                    $.data(document, "math3", Math.min.apply(Math, math1));

                }
            });

            if (input1 >= $.data(document, "math3")) {
                $.ajax({
                    url: '{{route('swap.store')}}',
                    type: 'post',
                    data: {unit1: unit1, unit2: unit2, input1: input1, "_token": "{{ csrf_token() }}"},
                    headers:
                        {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    success: function (data) {
                        if (data.status != 500) {
                            swal('Success', data.msg, 'success');
                            setTimeout(function () {
                                location.reload();
                            }, 2000)
                        } else {
                            swal('Unsuccess', data.msg, 'error');
                            setTimeout(function () {
                                location.reload();
                            }, 2000)
                        }
                    }
                });
            } else {

                swal('Unsuccess', 'Minimum Swap:' + $.data(document, "math3") + unit1 + ' ' , 'error');
                setTimeout(function () {
                    location.reload();
                }, 2000)
            }
        }

        $('#button_click').click(function () {
            var input1 = $('#amount_swap1').val();
            var input1 = input1.replace(/,/g, '');
            var math1 = [];
            var length = $.data(document, "fee_swap")[0].length;
            $.each($.data(document, "fee_swap"), function (key, value) {
                for (let i = 0; i < length; i++) {
                    var extra_field = parseInt(value[i]['extra_field1']);
                    math1.push(extra_field);

                    $.data(document, "math4", Math.min.apply(Math, math1));

                }
            });

            if (input1 >= $.data(document, "math4")) {
                $('#button_click').attr("data-target", "#warning");
            } else {
                let unit1 = $('#unit1').val();
                if ($.data(document, "math4")) {
                    swal('Unsuccess', 'Minimum Swap: ' + numberFormat($.data(document, "math4")) + ' ' + unit1 + ' ', 'error');
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                    $('#button_click').removeAttr("data-target", "#warning");
                } else {
                    swal('Unsuccess', 'An error has occurred', 'error');
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                    $('#button_click').removeAttr("data-target", "#warning");
                }

            }
        });

        get_user_assets(my_wallet);

        $(".refresh-wallet").click(function () {
            $(this).addClass('animate__rotateIn');
            $("#wallet-table tbody").html('<tr><td colspan="5">Pleas waiting...<td></tr>');
            setTimeout(function (){
                get_user_assets(my_wallet);
            },300)
        })

    </script>
    <script>
        function formatState(state) {
            if (!state.id) {
                return state.text;
            }
            var baseUrl = `{{asset('theme/user/assets/img/countries')}}`;
            var $state = $(
                '<span><img style="width: 22px" src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.svg" class="img-flag" /> ' + state.text + '</span>'
            );
            return $state;
        };


        $("input[name='number']").change(function () {
            $(".help-block").html("");
            $('span.error-mobile').remove();
            $(".sendCode").removeAttr('disabled');
            $("input[name='number']").css({'border-color': '#A6A9AE'});
            var inputVal = $(this).val();
            var characterReg = /^(0)?9\d{9}$/;
            if (!characterReg.test(inputVal)) {
                $(this).after('<span class="danger error-mobile" style="margin-left: 90px">Mobile number format is incorrect</span>');
                $("input[name='number']").css({'border-color': '#bc0005'});
                $(".sendCode").attr('disabled', true)
            }
        });

        $(document).on("click", '.sendCode', function () {
            var elem = $(this);
            var text_btn = elem.text();
            elem.text('Please waiting...');
            var number = $("input[name='number']").val();
            var country = $("select[name='country']").val();
            $.post(`{{ route('withdraw.send-sms') }}`, {
                _token: `{{ csrf_token() }}`,
                number: number,
                country: country,
            }).done(function (response) {
                if (response.status == 100) {
                    swal(response.msg, '', 'success');
                    $("input[name='number']").attr('disabled', true);
                    $(".code-verify").html("<input type='text' class='form-control text-center' placeholder='Enter Confirm Code' maxlength='5' name='code' style='letter-spacing: 5px'>")
                    $("select[name='country']").prop("disabled", true);
                    $(".label-code").removeClass("d-none");
                    $(elem).removeClass("sendCode");
                    $(elem).addClass("button-verify");
                    $(elem).html("OK");
                    time(response.time)

                } else if (response.status == 300) {
                    time(response.time)
                    elem.text(text_btn);
                } else {
                    swal(response.msg, '', 'error')
                    elem.text(text_btn);
                }
            })
        })

        $(document).on("click", '.button-verify', function () {
            var elem = $(this);
            var text_btn = elem.text();
            $(".body-security .alert-danger").remove()
            elem.text('Processing...');
            var data = new FormData($(this).closest('form')[0]);

            $.ajax({
                url: `{{ route('withdraw.store') }}`,
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == 100) {
                        swalResponse(response.status, response.msg, 'Success')
                        setTimeout(function () {
                            window.location.reload()
                        }, 3000)
                    } else {
                        swalResponse(response.status, response.msg, 'Error')
                        elem.text(text_btn);
                        return false;
                    }
                }, error: function (xhr) {
                    var error = '<div class="alert alert-danger"><ul>';
                    $.map(xhr.responseJSON, function (v, i) {
                        $.map(v, function (val, key) {
                            error += '<li>' + val + '</li>'
                        })
                    })
                    error += '</ul></div>'
                    $(".body-security").prepend(error)
                    elem.text(text_btn);
                    swalResponse(500, 'Resolved problem', 'Error')
                },
            })
        })

        function time(time) {
            $(".time-expire").show(100);
            var minutes = 1;
            var seconds = minutes * time;

            function convertIntToTime(num) {
                var mins = Math.floor(num / 60);
                var secs = num % 60;
                var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
                return (timerOutput);
            }

            var countdown = setInterval(function () {
                var current = convertIntToTime(seconds);
                $('.time').html(current);

                if (seconds == 0) {
                    clearInterval(countdown);
                }
                seconds--;
                if (seconds >= 0) {
                } else {
                    $("input[name='number']").attr('disabled', false);
                    $('h5').hide();
                    $('.time').html('<img src="{{ asset('theme/user/assets/img/icon/reload.svg') }}" class="sendCode" style="width:80%;cursor:pointer" title="Resend"></img>');
                }
            }, 1000);
        }


    </script>
@endsection
