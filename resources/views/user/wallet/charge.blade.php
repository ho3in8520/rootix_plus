@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-buySell')}}
@endsection

@section('content')
    <div class="settings mtb15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 m-auto col-12 row">
                    <div class="row col-12 bg-dark-2 text-light"
                         style="min-height: 200px; margin-bottom: -50px; padding-bottom: 40px; border-radius: 3px;">
                        <div class="col-12 row align-items-end m-auto">
                            <div class="col row  m-auto">
                                <div class="">
                                    <img src="{{asset('theme/user/assets/img/wallet-icon.png')}}" alt="logo" width="110"
                                         height="80">
                                </div>
                                <h3 class="col my-3 d-flex flex-row align-items-center text-white">{{__('attributes.charge.header')}}</h3>
                                {{--                                <h4 class="col my-3 d-flex flex-row align-items-center">{{__('attributes.charge.wallet')}}</h4>--}}

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8 text-dark ">
                        <div class="card">
                            <div class="card-body">
                                <p class="col-10 mx-auto my-2">
                                    <span>{{__('attributes.charge.totalAsset')}}</span>
                                    <span>{{ number_format($asset_rial) }} {{__('attributes.swap.Rile')}}</span>
                                </p>
                                <p class="col-10 mx-auto my-2 text-primary">
                                    <span>{{__('attributes.charge.asset')}}</span>
                                    <span>{{ number_format($asset_rial) }} {{__('attributes.swap.Rile')}}</span>
                                </p>
                                <hr>
                                <div class="card overflow-hidden">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" id="profile-tab-fill" data-toggle="tab"
                                                       href="#profile-fill" role="tab"
                                                       aria-controls="profile-fill"
                                                       aria-selected="true">{{__('attributes.charge.deposit')}}</a>
                                                </li>
                                                <li class="nav-item disabled">
                                                    <a class="nav-link disabled" id="home-tab-fill" data-toggle="tab"
                                                       href="#home-fill" role="tab"
                                                       aria-controls="home-fill"
                                                       aria-selected="false">{{__('attributes.charge.withdraw')}} <span
                                                            class="text-danger" style="font-size: 13px">(به زودی)</span>
                                                    </a>
                                                </li>
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content pt-1">
                                                <div class="tab-pane active" id="profile-fill" role="tabpanel"
                                                     aria-labelledby="profile-tab-fill">
                                                    @include('general.errors_alert')
                                                    <form class="col-10 mx-auto" id="deposit" method="post"
                                                          action="{{ route('payment') }}">
                                                        @csrf
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <label class="py-1">{{__('attributes.charge.amount')}}
                                                                    <sub
                                                                        class="text-danger">({{__('attributes.swap.Rile')}}
                                                                        )</sub></label>
                                                                <input type="hidden" name="mode" value="asset">
                                                                <input
                                                                    onkeyup="$(this).val(numberFormat($(this).val()))"
                                                                    type="text" name="amount" class="form-control"
                                                                    placeholder="{{__('attributes.charge.example')}}: 100,000">
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-success d-block mx-auto px-4">{{__('attributes.charge.deposit')}}</button>
                                                            </div>
                                                        </fieldset>
                                                    </form>
                                                </div>
                                                <div class="tab-pane" id="home-fill" role="tabpanel"
                                                     aria-labelledby="home-tab-fill">
                                                    <div class="alert alert-warning">این امکان به زودی اضافه می گردد
                                                    </div>
                                                    {{--<form class="col-10 mx-auto" id="withdraw" method="post"
                                                          action="{{ route('payment') }}">
                                                        @csrf
                                                        <fieldset>
                                                            <div class="form-group">
                                                                <label
                                                                    class="py-1">{{__('attributes.Purification.amount')}}
                                                                    <sub
                                                                        class="text-danger">({{__('attributes.swap.Rile')}}
                                                                        )</sub>
                                                                </label>
                                                                <input
                                                                    onkeyup="final_price(this);"
                                                                    data-max="124000000"
                                                                    data-min="5000"
                                                                    type="text" name="amount" class="form-control"
                                                                    placeholder="{{__('attributes.charge.example')}}: 100,000">
                                                                <p class="col-12 mx-auto mt-1 text-primary d-block"
                                                                   style="font-size: 12px; margin-bottom: 5px;">
                                                                    <span>{{__('attributes.charge.asset')}}</span>
                                                                    <span>124,000,000 {{__('attributes.swap.Rile')}}</span>
                                                                    <span class="btn btn-info btn-sm px-1 select-price"
                                                                          data-price="124000000">{{__('attributes.charge.select1')}}</span>
                                                                </p>
                                                                <p class="col-12 mx-auto text-primary d-block"
                                                                   style="font-size: 12px; margin-bottom: 5px;">
                                                                    <span>{{__('attributes.charge.fee')}} (5%) : </span>
                                                                    <span class="wage" data-wage="5">0 {{__('attributes.swap.Rile')}}</span>
                                                                </p>
                                                                <p class="col-12 mx-auto text-primary d-block"
                                                                   style="font-size: 12px; margin-bottom: 5px;">
                                                                    <span>{{__('attributes.charge.final')}}: </span>
                                                                    <span
                                                                        class="text-danger font-weight-bold final-price">0 {{__('attributes.swap.Rile')}}</span>
                                                                </p>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="py-1">{{__('attributes.charge.code')}} :</label>
                                                                <input type="number" name="authenticator" class="form-control"
                                                                    placeholder="{{__('attributes.charge.example')}}: 569856">
                                                                <p class="col-12 mx-auto mt-1 text-primary d-block"
                                                                   style="font-size: 12px; margin-bottom: 5px;">
                                                                    <span>{{__('attributes.charge.codeGA')}}</span>
                                                                </p>
                                                            </div>

                                                            <div class="form-group">
                                                                <label
                                                                    class="py-1">{{__('attributes.charge.address')}}</label>
                                                                <select class="form-control custom-select">
                                                                    <option disabled
                                                                            selected>{{__('attributes.charge.select')}}</option>
                                                                    <option
                                                                        value="1">{{__('attributes.charge.account1')}}</option>
                                                                    <option
                                                                        value="2">{{__('attributes.charge.account2')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-danger d-block mx-auto px-4">{{__('attributes.charge.withdraw')}}</button>
                                                            </div>
                                                        </fieldset>
                                                    </form>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 card ">
                        <div class="card-body">
                            {{--<p class="alert alert-info">
                                در صورت تمایل اینجا میشه برای افزایش موجودی متنی اگه لازم بود گذاشت
                            </p>--}}
                            <div class="video mx-auto">
                                <video style="width: 100%;" height="240" controls>
                                    <source src="movie.mp4" type="video/mp4">
                                    <source src="movie.ogg" type="video/ogg">
                                    مرورگر شما از این قابلیت پشتیبانی نمی کند
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".select-price").click(function () {
            $("#withdraw input[name='amount']").val($(this).data('price'));
            $("#withdraw input[name='amount']").keyup();
        });

        function final_price(elm) {
            let max = $(elm).data('max');
            let min = $(elm).data('min');
            let price = $(elm).val();
            price = price.replaceAll(',', '');

            if (price > max) price = max;
            if (price < min) price = min;

            $(elm).val(numberFormat(price));
            let wage = $(".wage").data('wage');
            let wage_price = price * (wage / 100);
            let final_price = price - wage_price <= 0 ? 0 : price - wage_price;
            $(".wage").text(numberFormat(isNaN(wage_price) ? 0 : wage_price) + ' {{__("attributes.swap.Rile")}} ');
            $(".final-price").text(numberFormat(isNaN(final_price) ? 0 : final_price) + ' {{__("attributes.swap.Rile")}} ');
        }
    </script>
@endsection
