@extends('templates.user.master_page')
@section('title_browser')
    Profile
@endsection
@section('style')
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('theme/user/app-assets/css/pages/app-user.min.css')}}">

    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css/core/menu/menu-types/horizontal-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css/plugins/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/vendors/css/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('theme/user/app-assets/vendors/css/vendors-rtl.min.css')}}">
    <style>
        .avatar-img {
            cursor: pointer;
        }
        .hover-avatar {
            height: 25px;
            line-height: 25px;
            position: absolute;
            font-size: 11px;
            bottom: 0px;
            left: 0;
            right: 0;
            background: #00000070;
            color: #fff;
            text-align: center;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }
        .avatar-img label {
            cursor: pointer;
            display: inline;
            position: relative;
            overflow: hidden
        }
    </style>
@endsection
@section('content')

    <!-- Nav Filled Starts -->
    <section id="nav-filled">
        <div class="row">
            <div class="col-sm-12">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h4 class="card-title">Profile</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab-fill" data-toggle="tab" href="#home-fill"
                                       role="tab"
                                       aria-controls="home-fill" aria-selected="true">Profile</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab-fill" data-toggle="tab" href="#profile-fill"
                                       role="tab"
                                       aria-controls="profile-fill" aria-selected="false">Information</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="messages-tab-fill" data-toggle="tab" href="#messages-fill"
                                       role="tab"
                                       aria-controls="messages-fill" aria-selected="false">Banks</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content pt-1">
                                <div class="tab-pane active" id="home-fill" role="tabpanel"
                                     aria-labelledby="home-tab-fill">
                                    <div class="tab-pane active" id="account" aria-labelledby="account-tab"
                                         role="tabpanel">

                                        <!-- users edit media object start -->
                                        <div class="col-12 mb-2 row align-items-center border p-1">
                                            <div class="avatar-img mx-auto mx-md-0">
                                                <form action="{{route('profile.update.picture',auth()->user()->id)}}"
                                                      method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <label class="mr-2 my-25 p-0 d-flex" href="#" for="upload_file">
                                                        <img class="users-avatar-shadow rounded" height="90" width="90"
                                                             src="{{ auth()->user()->avatar }}"
                                                             alt="users avatar">
                                                        <span class="hover-avatar">Edit Avatar</span>
                                                    </label>
                                                    <input type="file" id="upload_file" name="file" class="d-none"
                                                           onchange="form.submit()">
                                                </form>
                                            </div>
                                            <div class="col-md col-sm-12 text-center text-md-left mt-2 mt-md-0">
                                                <h4 class="mb-0">
                                                    {{ auth()->user()->name ?? 'No name'}}
                                                </h4>
                                                <p style="margin: 5px 0px; font-size: 14px">
                                                    User code: {{auth()->user()->code}}
                                                </p>
                                                <p class="mb-0">
                                                    Your level : {{convertStepToPersian(auth()->user()->step_complate)}}
                                                    <a class="btn btn-sm btn-primary"
                                                       href="{{ route('step.verify-mobile') }}">Continue authentication</a>
                                                </p>
                                            </div>
                                        </div>

                                        <!--   Profile -->
                                        <div class="col-12 row border p-1">
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <label>Name</label>
                                                <input type="text" class="form-control"
                                                       placeholder="Name"
                                                       value="{{ auth()->user()->name ?? 'No name'}}"
                                                       required
                                                       readonly>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>National code</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="National code"
                                                               value="{{auth()->user()->national_code}}"
                                                               readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Email</label>
                                                        <input type="email" class="form-control"
                                                               placeholder="Email"
                                                               value="{{auth()->user()->email}}"
                                                               readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Identifier code</label>
                                                        <input type="email" class="form-control"
                                                               placeholder="Identifier code"
                                                               value="{{auth()->user()->referral_id}}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-4 col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Introduce link to friends</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="referral-link"
                                                                   placeholder="Introduce link to friends"
                                                                   value=" {{ route('register.form',['referral_code' => auth()->user()->code]) }}"
                                                                   readonly>
                                                            <div class="input-group-append">
                                                                <span class="btn btn-info copy-btn"
                                                                      data-input="referral-link"
                                                                      style="padding-right: 10px; padding-left: 10px;"><i
                                                                        class="fa fa-copy mx-0"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--  Profile -->

                                        <!-- users edit account form ends -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile-fill" role="tabpanel"
                                     aria-labelledby="profile-tab-fill">
                                    <div class="tab-pane" id="information" aria-labelledby="information-tab"
                                         role="tabpanel">
                                        <!-- users edit Info form start -->
                                        <form novalidate>
                                            <div class="row mt-1">
                                                <div class="col-12 col-sm-6">
                                                    <h5 class="mb-1"><i class="feather icon-user mr-25"></i>Personal Information
                                                    </h5>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>Birthday</label>
                                                                    <input type="text"
                                                                           value="{{auth()->user()->birth_day}}"
                                                                           class="form-control"
                                                                           placeholder="Birthday"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Mobile</label>
                                                            <input type="text" class="form-control"
                                                                   value="{{auth()->user()->mobile}}"
                                                                   placeholder="Mobile"
                                                                   readonly>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Gender</label>
                                                        <ul class="list-unstyled mb-0">
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                        <input type="radio" name="vueradio"
                                                                               @if(auth()->user()->gender_id == 1 ) checked
                                                                               @else disabled @endif
                                                                               value="false">
                                                                        <span class="vs-radio">
                                                                          <span class="vs-radio--border"></span>
                                                                          <span class="vs-radio--circle"></span>
                                                                        </span> Male
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                        <input type="radio" name="vueradio"
                                                                               @if(auth()->user()->gender_id == 2 ) checked
                                                                               @else disabled @endif
                                                                               value="false">
                                                                        <span class="vs-radio">
                                                                          <span class="vs-radio--border"></span>
                                                                          <span class="vs-radio--circle"></span>
                                                                        </span> Female
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                        <input type="radio" name="vueradio"
                                                                               @if(empty(auth()->user()->gender_id)  ) checked
                                                                               @else disabled @endif
                                                                               value="false">
                                                                        <span class="vs-radio">
                                                                          <span class="vs-radio--border"></span>
                                                                          <span class="vs-radio--circle"></span>
                                                                        </span> Unknown
                                                                    </div>
                                                                </fieldset>
                                                            </li>

                                                        </ul>
                                                    </div>

                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <h5 class="mb-1 mt-2 mt-sm-0"><i
                                                            class="feather icon-map-pin mr-25"></i>Address</h5>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Landline phone</label>
                                                            <input type="text" class="form-control" required
                                                                   value="{{auth()->user()->phone}}"
                                                                   placeholder="Landline phone"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Zip code</label>
                                                            <input type="text" class="form-control" required
                                                                   placeholder="Zip code"
                                                                   value="{{auth()->user()->postal_code}}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>City</label>
                                                            <input type="text" class="form-control" required
                                                                   value="{{isset(auth()->user()->city_name->name) ? auth()->user()->city_name->name : ''}}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Country</label>
                                                            <input type="text" class="form-control"
                                                                   value="{{isset(auth()->user()->country->name) ? auth()->user()->country->name : ''}}"
                                                                   readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit Info form ends -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="messages-fill" role="tabpanel"
                                     aria-labelledby="messages-tab-fill">
                                    <div class="tab-pane" id="social" aria-labelledby="social-tab" role="tabpanel">
                                        <!-- users edit socail form start -->
                                        <form novalidate>
                                            <div class="row">
                                                <div class="col-12 col-sm-12">
                                                    <div class="table table-hover table-striped">
                                                        <table style="width: 100%">
                                                            <thead>
                                                            <tr>
                                                                <td>Bank</td>
                                                                <td>Card number</td>
                                                                <td>Sheba number</td>
                                                                <td>Account number</td>
                                                                <td>Operations</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(auth()->user()->bank->count() > 0)
                                                            @foreach(auth()->user()->bank as $bank)
                                                                <tr>
                                                                    <td>{{$bank->name}}</td>
                                                                    <td>{{$bank->card_number}}</td>
                                                                    <td>{{$bank->sheba_number}}</td>
                                                                    <td>{{$bank->account_number}}</td>
                                                                    <td><a href="#"><i class="feather icon-trash-2"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            @else
                                                                <tr><td colspan="6" class="text-center">Nothing found!</td></tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                        <div
                                                            class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                                            <a href="{{ route('banks.create') }}"
                                                               class="btn btn-success mr-1 mb-1 waves-effect waves-light">
                                                                Add
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit socail form ends -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('script')
    <script src="{{asset('theme/user/app-assets/js/scripts/pages/app-user.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/js/scripts/navs/navs.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/js/scripts/extensions/swiper.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/extensions/swiper.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/vendors.min.js')}}"></script>

    <script>
        function set_authenticator(secret) {
            $.ajax({
                url: "{{route('complete-registration')}}",
                type: 'POST',
                data: {secret: secret, "_token": csrf},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    if (data.status == 100) {
                        swal('Success', data['msg'], 'success');
                        setTimeout(function () {
                            location.reload();
                        }, 5000)
                    } else {
                        swal('Unsuccess', data['msg'], 'error');
                        setTimeout(function () {
                            location.reload();
                        }, 5000)
                    }
                }
            });
        }
    </script>
@endsection
