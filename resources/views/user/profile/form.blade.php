@extends('templates.user.master_page')
@section('title_browser')

    {{__('titlePage.title-minePage')}}

@endsection
@section('content')
    <div class="row settings">
        <div class="col-md-12 col-lg-12">
            <div class="col-md-12">
                @include('general.flash_message')
                <div class="alert alert-warning py-3 text-center">
                    <span>{{__('attributes.level')}} {{ convertStepToPersian(auth()->user()->step_complate) }}</span>
                    <a class="btn btn-warning text-white" href="{{ route('step.verify-mobile') }}">{{__('attributes.authentication')}}</a>
                </div>
            </div>
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade" id="settings-profile" role="tabpanel"
                     aria-labelledby="settings-profile-tab">
                    <livewire:user.profile.form :user="$user"/>
                </div>
                @if($user->assets[0])
                    <div class="tab-pane fade show active" id="settings-wallet" role="tabpanel"
                         aria-labelledby="settings-wallet-tab">
                        <div class="wallet">
                            <div class="row">
                                <div class="col-md-12 col-lg-4">
                                    <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                                        @php($i=1)
                                        @foreach($user->assets as $row)
                                            <a class="nav-link d-flex justify-content-between align-items-center {{ ($i==1)?'active':'' }}"
                                               data-toggle="pill"
                                               href="#{{ $row->unit }}" role="tab" aria-selected="true">
                                                <div class="d-flex">
                                                    <img
                                                        src="{{ asset("theme/user/assets/img/icon/".$row->logo.".svg") }}"
                                                        alt="btc">
                                                    <div>
                                                        <h2>
                                                            @if($row->unit=='rial')
                                                                {{__('attributes.Rile')}}
                                                            @else
                                                                {{__('attributes.USDT')}}
                                                            @endif
                                                        </h2>
                                                        <p>{{ strtoupper($row->unit) }}</p>
                                                    </div>
                                                </div>
                                                <div>
                                                    @if($row->unit == 'rial')<h3 >{{ number_format($row->amount) }} </h3>@else <h3 class="usdt_balance">0</h3> @endif
                                                    <p class="text-left"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                                </div>
                                            </a>
                                            @php($i++)
                                        @endforeach
                                        {{--  <a class="nav-link d-flex justify-content-between align-items-center"
                                             data-toggle="pill"
                                             href="#coinBTC" role="tab" aria-selected="true">
                                              <div class="d-flex">
                                                  <img src="{{ asset('theme/user/assets/img/icon/usdt.svg') }}"
                                                       alt="usdt">
                                                  <div>
                                                      <h2>تتر</h2>
                                                      <p>USDT</p>
                                                  </div>
                                              </div>
                                              <div>
                                                  <h3>0</h3>
                                                  <p class="text-left"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                              </div>
                                          </a>
                                                       <a class="nav-link d-flex justify-content-between align-items-center" data-toggle="pill"
                                                          href="#coinBNB" role="tab" aria-selected="true">
                                                           <div class="d-flex">
                                                               <img src="{{ asset("theme/user/assets/img/icon/rls.svg") }}" alt="btc">
                                                               <div>
                                                                   <h2>BNB</h2>
                                                                   <p>Binance</p>
                                                               </div>
                                                           </div>
                                                           <div>
                                                               <h3>35.4842458</h3>
                                                               <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                                           </div>
                                                       </a>
                                                       <a class="nav-link d-flex justify-content-between align-items-center" data-toggle="pill"
                                                          href="#coinTRX" role="tab" aria-selected="true">
                                                           <div class="d-flex">
                                                               <img src="assets/img/icon/6.png" alt="btc">
                                                               <div>
                                                                   <h2>TRX</h2>
                                                                   <p>Tron</p>
                                                               </div>
                                                           </div>
                                                           <div>
                                                               <h3>4.458941</h3>
                                                               <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                                           </div>
                                                       </a>
                                                       <a class="nav-link d-flex justify-content-between align-items-center" data-toggle="pill"
                                                          href="#coinEOS" role="tab" aria-selected="true">
                                                           <div class="d-flex">
                                                               <img src="assets/img/icon/2.png" alt="btc">
                                                               <div>
                                                                   <h2>EOS</h2>
                                                                   <p>Eosio</p>
                                                               </div>
                                                           </div>
                                                           <div>
                                                               <h3>33.478951</h3>
                                                               <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                                           </div>
                                                       </a>
                                                       <a class="nav-link d-flex justify-content-between align-items-center" data-toggle="pill"
                                                          href="#coinXMR" role="tab" aria-selected="true">
                                                           <div class="d-flex">
                                                               <img src="assets/img/icon/7.png" alt="btc">
                                                               <div>
                                                                   <h2>XMR</h2>
                                                                   <p>Monero</p>
                                                               </div>
                                                           </div>
                                                           <div>
                                                               <h3>99.465975</h3>
                                                               <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                                           </div>
                                                       </a>
                                                       <a class="nav-link d-flex justify-content-between align-items-center" data-toggle="pill"
                                                          href="#coinKCS" role="tab" aria-selected="true">
                                                           <div class="d-flex">
                                                               <img src="assets/img/icon/4.png" alt="btc">
                                                               <div>
                                                                   <h2>KCS</h2>
                                                                   <p>Kstarcoin</p>
                                                               </div>
                                                           </div>
                                                           <div>
                                                               <h3>114.57564</h3>
                                                               <p class="text-right"><i class="icon ion-md-lock"></i> 0.0000000</p>
                                                           </div>
                                                       </a>--}}
                                    </div>
                                </div>
                                @if($user->assets[0])
                                    <div class="col-md-12 col-lg-8">
                                        <div class="tab-content">
                                            @php($i=1)
                                            @foreach($user->assets as $row)
                                                <div class="tab-pane fade {{ ($i==1)?'show active':'' }}"
                                                     id="{{ $row->unit }}" role="tabpanel">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <h5 class="card-title">{{__('attributes.balance')}}</h5>
                                                            <ul>
                                                                <li class="d-flex justify-content-between align-items-center">
                                                                    <div class="d-flex align-items-center">
                                                                        <i class="icon ion-md-cash"></i>
                                                                        <h2>{{__('attributes.total')}}</h2>
                                                                    </div>
                                                                    <div>

                                                                        @if($row->unit == 'rial')<h3 >{{ number_format($row->amount) }} </h3>@else <h3 class="usdt_balance"></h3> @endif                                                                    </div>
                                                                </li>
                                                                {{--      <li class="d-flex justify-content-between align-items-center">
                                                                          <div class="d-flex align-items-center">
                                                                              <i class="icon ion-md-checkmark"></i>
                                                                              <h2>Available Margin</h2>
                                                                          </div>
                                                                          <div>
                                                                              <h3>1.334 ETH</h3>
                                                                          </div>
                                                                      </li>--}}
                                                            </ul>
                                                            <a href="#">
                                                                <button class="btn green"> {{__('attributes.deposit')}}</button>
                                                            </a>
                                                            <a  class="tag" onclick="tag('{{route('withdraw.index',$row->unit)}}')">
                                                                <button class="btn red">{{__('attributes.withdraw')}}</button>
                                                            </a>
                                                            @if($row->unit == 'usdt')
                                                                <a   class="tag" onclick="tag('{{route('swap.show',['unit'=>'usdt'])}}');">
                                                                    <button class="btn btn-info">{{__('attributes.exchange')}}</button>
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        @if($row->unit=='rial' && url('#rial'))
                                                            <div class="card-body">
                                                                <form method="post" action="{{ route('payment') }}">
                                                                    @csrf
                                                                    <h5 class="card-title">{{__('attributes.depositRile')}}</h5>
                                                                    <div class="row wallet-address">
                                                                        <div class="col-md-8">
                                                                            {{--            <p>واریزs to this address are unlimited. Note that you may
                                                                                            not be able to withdraw all
                                                                                            of your
                                                                                            funds at once if you deposit more than your daily
                                                                                            withdrawal limit.</p>--}}
                                                                            <div class="input-group">
                                                                                <input type="text" chars="int"
                                                                                       class="form-control amount-rial"
                                                                                       required
                                                                                       name="amount">
                                                                                <div class="input-group-prepend">
                                                                                    <button type="submit"
                                                                                            class="btn btn-primary">
                                                                                        {{__('attributes.pay')}}
                                                                                    </button>
                                                                                    <input type="hidden" name="mode"
                                                                                           value="asset">
                                                                                </div>
                                                                            </div>
                                                                            @error('amount') <span
                                                                                class="help-block text-danger">{{ $message }}</span> @enderror
                                                                        </div>
                                                                        {{--       <div class="col-md-4">
                                                                                   <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                                               </div>--}}
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        @else
                                                            <div class="card-body">
                                                                <h5 class="card-title">{{__('attributes.address')}}</h5>
                                                                <div class="row wallet-address">
                                                                    <div class="col-md-8">
                                                                        {{--                                                                        <p>واریزs to this address are unlimited. Note--}}
                                                                        {{--                                                                            that--}}
                                                                        {{--                                                                            you may--}}
                                                                        {{--                                                                            not be able to withdraw all--}}
                                                                        {{--                                                                            of your--}}
                                                                        {{--                                                                            funds at once if you deposit more than your--}}
                                                                        {{--                                                                            daily--}}
                                                                        {{--                                                                            withdrawal limit.</p>--}}
                                                                        <div class="input-group">

                                                                            @if($usdt_asset->token != NULL)
                                                                                <?php $token = json_decode($usdt_asset->token, true) ?>
                                                                                <div class="input-group">
                                                                                    <input type="text"
                                                                                           class="form-control"
                                                                                           value="{{ $token['address']['base58']}}"
                                                                                           id="usdt_address" readonly >
                                                                                    <div class="input-group-prepend">
                                                                                        <button class="btn btn-primary"
                                                                                                onclick="copy('usdt_address')">
                                                                                            COPY
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            @else
                                                                                <div class="input-group">
                                                                                    <button id="wallet"
                                                                                            data-href="{{route('createAccount',['unit'=>'usdt'])}}"
                                                                                            onclick="create_account('usdt')"
                                                                                            class="btn btn-success btn-block link-button">
                                                                                        {{__('attributes.createWallet')}}
                                                                                    </button>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <img src="assets/img/qr-code-dark.svg"
                                                                             alt="qr-code">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <h5 class="card-title">{{__('attributes.last')}}</h5>
                                                            <div class="wallet-history">
                                                                <table class="table">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>{{__('attributes.row')}}</th>
                                                                        <th>{{__('attributes.date')}}</th>
                                                                        <th>{{__('attributes.condition')}}</th>
                                                                        <th>{{__('attributes.price')}}</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    @foreach($finance_transactions as $finance_transaction)
                                                                        <tr>
                                                                            <td>{{$loop->iteration}}</td>
                                                                            <td>{{jdate_from_gregorian($finance_transaction->created_at,'H:i:s Y-m-d' )}}</td>
                                                                            <td>
                                                                                <i class="icon ion-md-checkmark-circle-outline green">@if($finance_transaction->type == 1) <button class="btn btn-danger btn-sm">برداشت</button>@elseif($finance_transaction->type == 2)<button class="btn btn-danger btn-sm">واریز</button>@endif</i>
                                                                            </td>
                                                                            <td>{{$finance_transaction->amount / 1000000}}</td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif

                                                </div>

                                        </div>
                                        {{-- <div class="tab-pane fade {{ ($i==1)?'show active':'' }}" id="{{ $row->unit }}" role="tabpanel">
                                             <div class="card">
                                                 <div class="card-body">
                                                     <h5 class="card-title">Balances</h5>
                                                     <ul>
                                                         <li class="d-flex justify-content-between align-items-center">
                                                             <div class="d-flex align-items-center">
                                                                 <i class="icon ion-md-cash"></i>
                                                                 <h2>Total Equity</h2>
                                                             </div>
                                                             <div>
                                                                 <h3>{{ number_format($row->amount) }} {{ strtoupper($row->unit) }}</h3>
                                                             </div>
                                                         </li>
                                                         --}}{{--      <li class="d-flex justify-content-between align-items-center">
                                                                   <div class="d-flex align-items-center">
                                                                       <i class="icon ion-md-checkmark"></i>
                                                                       <h2>Available Margin</h2>
                                                                   </div>
                                                                   <div>
                                                                       <h3>1.334 ETH</h3>
                                                                   </div>
                                                               </li>--}}{{--
                                                     </ul>
                                                     <a href="#">
                                                         <button class="btn green">واریز</button>
                                                     </a>
                                                     <a href="#">
                                                         <button class="btn red">برداشت</button>
                                                     </a>
                                                     @if($row->unit == 'usdt')
                                                         <a href="{{route('swap.show','usdt')}}">
                                                             <button class="btn btn-info">اکسچنج</button>
                                                         </a>
                                                     @endif
                                                 </div>
                                             </div>
                                             <div class="card">
                                                 <div class="card-body">
                                                     <h5 class="card-title">Wallet واریز Address</h5>
                                                     <div class="row wallet-address">
                                                         <div class="col-md-8">
                                                             <p>واریزs to this address are unlimited. Note that you may
                                                                 not be able to withdraw all
                                                                 of your
                                                                 funds at once if you deposit more than your daily
                                                                 withdrawal limit.</p>
                                                             <div class="input-group">
                                                                 <input type="text" class="form-control"
                                                                        value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">
                                                                 <div class="input-group-prepend">
                                                                     <button class="btn btn-primary">COPY</button>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-4">
                                                             <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="card">
                                                 <div class="card-body">
                                                     <h5 class="card-title">Latest Transactions</h5>
                                                     <div class="wallet-history">
                                                         <table class="table">
                                                             <thead>
                                                             <tr>
                                                                 <th>No.</th>
                                                                 <th>Date</th>
                                                                 <th>Status</th>
                                                                 <th>Amount</th>
                                                             </tr>
                                                             </thead>
                                                             <tbody>
                                                             <tr>
                                                                 <td>1</td>
                                                                 <td>25-04-2019</td>
                                                                 <td>
                                                                     <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                 </td>
                                                                 <td>4.5454334</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>2</td>
                                                                 <td>25-05-2019</td>
                                                                 <td>
                                                                     <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                 </td>
                                                                 <td>0.5484468</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>3</td>
                                                                 <td>25-06-2019</td>
                                                                 <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                 </td>
                                                                 <td>2.5454545</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>4</td>
                                                                 <td>25-07-2019</td>
                                                                 <td>
                                                                     <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                 </td>
                                                                 <td>1.45894147</td>
                                                             </tr>
                                                             <tr>
                                                                 <td>3</td>
                                                                 <td>25-08-2019</td>
                                                                 <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                 </td>
                                                                 <td>2.5454545</td>
                                                             </tr>
                                                             </tbody>
                                                         </table>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>--}}
                                        @php($i++)
                                        @endforeach
                                        {{--                    <div class="tab-pane fade" id="coinBTC" role="tabpanel">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Balances</h5>
                                                                        <ul>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-cash"></i>
                                                                                    <h2>Total Equity</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>5.5894 BTC</h3>
                                                                                </div>
                                                                            </li>
                                                                            --}}{{--          <li class="d-flex justify-content-between align-items-center">
                                                                                          <div class="d-flex align-items-center">
                                                                                              <i class="icon ion-md-checkmark"></i>
                                                                                              <h2>Available Margin</h2>
                                                                                          </div>
                                                                                          <div>
                                                                                              <h3>2.480 BTC</h3>
                                                                                          </div>
                                                                                      </li>--}}{{--
                                                                        </ul>
                                                                        <button class="btn green">واریز</button>
                                                                        <button class="btn red">برداشت</button>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Wallet واریز Address</h5>
                                                                        <div class="row wallet-address">
                                                                            <div class="col-md-8">
                                                                                <p>واریزs to this address are unlimited. Note that you may
                                                                                    not be able to withdraw all
                                                                                    of your
                                                                                    funds at once if you deposit more than your daily
                                                                                    withdrawal limit.</p>
                                                                                <div class="input-group">
                                                                                    <input type="text" class="form-control"
                                                                                           value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">
                                                                                    <div class="input-group-prepend">
                                                                                        <button class="btn btn-primary">COPY</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Latest Transactions</h5>
                                                                        <div class="wallet-history">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>Date</th>
                                                                                    <th>Status</th>
                                                                                    <th>Amount</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>1</td>
                                                                                    <td>25-04-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>4.5454334</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2</td>
                                                                                    <td>25-05-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>0.5484468</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-06-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>4</td>
                                                                                    <td>25-07-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>1.45894147</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-08-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="coinBNB" role="tabpanel">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Balances</h5>
                                                                        <ul>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-cash"></i>
                                                                                    <h2>Total Equity</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>7.342 BNB</h3>
                                                                                </div>
                                                                            </li>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-checkmark"></i>
                                                                                    <h2>Available Margin</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>0.332 BNB</h3>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <button class="btn green">واریز</button>
                                                                        <button class="btn red">برداشت</button>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Wallet واریز Address</h5>
                                                                        <div class="row wallet-address">
                                                                            <div class="col-md-8">
                                                                                <p>واریزs to this address are unlimited. Note that you may
                                                                                    not be able to withdraw all
                                                                                    of your
                                                                                    funds at once if you deposit more than your daily
                                                                                    withdrawal limit.</p>
                                                                                <div class="input-group">
                                                                                    <input type="text" class="form-control"
                                                                                           value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">
                                                                                    <div class="input-group-prepend">
                                                                                        <button class="btn btn-primary">COPY</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Latest Transactions</h5>
                                                                        <div class="wallet-history">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>Date</th>
                                                                                    <th>Status</th>
                                                                                    <th>Amount</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>1</td>
                                                                                    <td>25-04-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>4.5454334</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2</td>
                                                                                    <td>25-05-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>0.5484468</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-06-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>4</td>
                                                                                    <td>25-07-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>1.45894147</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-08-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="coinTRX" role="tabpanel">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Balances</h5>
                                                                        <ul>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-cash"></i>
                                                                                    <h2>Total Equity</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>4.3344 TRX</h3>
                                                                                </div>
                                                                            </li>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-checkmark"></i>
                                                                                    <h2>Available Margin</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>1.453 TRX</h3>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <button class="btn green">واریز</button>
                                                                        <button class="btn red">برداشت</button>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Wallet واریز Address</h5>
                                                                        <div class="row wallet-address">
                                                                            <div class="col-md-8">
                                                                                <p>واریزs to this address are unlimited. Note that you may
                                                                                    not be able to withdraw all
                                                                                    of your
                                                                                    funds at once if you deposit more than your daily
                                                                                    withdrawal limit.</p>
                                                                                <div class="input-group">
                                                                                    <input type="text" class="form-control"
                                                                                           value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">
                                                                                    <div class="input-group-prepend">
                                                                                        <button class="btn btn-primary">COPY</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Latest Transactions</h5>
                                                                        <div class="wallet-history">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>Date</th>
                                                                                    <th>Status</th>
                                                                                    <th>Amount</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>1</td>
                                                                                    <td>25-04-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>4.5454334</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2</td>
                                                                                    <td>25-05-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>0.5484468</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-06-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>4</td>
                                                                                    <td>25-07-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>1.45894147</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-08-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="coinEOS" role="tabpanel">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Balances</h5>
                                                                        <ul>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-cash"></i>
                                                                                    <h2>Total Equity</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>33.35 EOS</h3>
                                                                                </div>
                                                                            </li>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-checkmark"></i>
                                                                                    <h2>Available Margin</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>4.445 EOS</h3>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <button class="btn green">واریز</button>
                                                                        <button class="btn red">برداشت</button>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Wallet واریز Address</h5>
                                                                        <div class="row wallet-address">
                                                                            <div class="col-md-8">
                                                                                <p>واریزs to this address are unlimited. Note that you may
                                                                                    not be able to withdraw all
                                                                                    of your
                                                                                    funds at once if you deposit more than your daily
                                                                                    withdrawal limit.</p>
                                                                                <div class="input-group">
                                                                                    <input type="text" class="form-control"
                                                                                           value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">
                                                                                    <div class="input-group-prepend">
                                                                                        <button class="btn btn-primary">COPY</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Latest Transactions</h5>
                                                                        <div class="wallet-history">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>Date</th>
                                                                                    <th>Status</th>
                                                                                    <th>Amount</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>1</td>
                                                                                    <td>25-04-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>4.5454334</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2</td>
                                                                                    <td>25-05-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>0.5484468</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-06-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>4</td>
                                                                                    <td>25-07-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>1.45894147</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-08-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="coinXMR" role="tabpanel">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Balances</h5>
                                                                        <ul>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-cash"></i>
                                                                                    <h2>Total Equity</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>34.333 XMR</h3>
                                                                                </div>
                                                                            </li>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-checkmark"></i>
                                                                                    <h2>Available Margin</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>2.354 XMR</h3>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <button class="btn green">واریز</button>
                                                                        <button class="btn red">برداشت</button>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Wallet واریز Address</h5>
                                                                        <div class="row wallet-address">
                                                                            <div class="col-md-8">
                                                                                <p>واریزs to this address are unlimited. Note that you may
                                                                                    not be able to withdraw all
                                                                                    of your
                                                                                    funds at once if you deposit more than your daily
                                                                                    withdrawal limit.</p>
                                                                                <div class="input-group">
                                                                                    <input type="text" class="form-control"
                                                                                           value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">
                                                                                    <div class="input-group-prepend">
                                                                                        <button class="btn btn-primary">COPY</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Latest Transactions</h5>
                                                                        <div class="wallet-history">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>Date</th>
                                                                                    <th>Status</th>
                                                                                    <th>Amount</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>1</td>
                                                                                    <td>25-04-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>4.5454334</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2</td>
                                                                                    <td>25-05-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>0.5484468</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-06-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>4</td>
                                                                                    <td>25-07-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>1.45894147</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-08-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane fade" id="coinKCS" role="tabpanel">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Balances</h5>
                                                                        <ul>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-cash"></i>
                                                                                    <h2>Total Equity</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>86.577 KCS</h3>
                                                                                </div>
                                                                            </li>
                                                                            <li class="d-flex justify-content-between align-items-center">
                                                                                <div class="d-flex align-items-center">
                                                                                    <i class="icon ion-md-checkmark"></i>
                                                                                    <h2>Available Margin</h2>
                                                                                </div>
                                                                                <div>
                                                                                    <h3>5.78 KCS</h3>
                                                                                </div>
                                                                            </li>
                                                                        </ul>
                                                                        <button class="btn green">واریز</button>
                                                                        <button class="btn red">برداشت</button>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Wallet واریز Address</h5>
                                                                        <div class="row wallet-address">
                                                                            <div class="col-md-8">
                                                                                <p>واریزs to this address are unlimited. Note that you may
                                                                                    not be able to withdraw all
                                                                                    of your
                                                                                    funds at once if you deposit more than your daily
                                                                                    withdrawal limit.</p>
                                                                                <div class="input-group">
                                                                                    <input type="text" class="form-control"
                                                                                           value="Ad87deD4gEe8dG57Ede4eEg5dREs4d5e8f4e">
                                                                                    <div class="input-group-prepend">
                                                                                        <button class="btn btn-primary">COPY</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <img src="assets/img/qr-code-dark.svg" alt="qr-code">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <h5 class="card-title">Latest Transactions</h5>
                                                                        <div class="wallet-history">
                                                                            <table class="table">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th>No.</th>
                                                                                    <th>Date</th>
                                                                                    <th>Status</th>
                                                                                    <th>Amount</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td>1</td>
                                                                                    <td>25-04-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>4.5454334</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>2</td>
                                                                                    <td>25-05-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>0.5484468</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-06-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>4</td>
                                                                                    <td>25-07-2019</td>
                                                                                    <td>
                                                                                        <i class="icon ion-md-checkmark-circle-outline green"></i>
                                                                                    </td>
                                                                                    <td>1.45894147</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>3</td>
                                                                                    <td>25-08-2019</td>
                                                                                    <td><i class="icon ion-md-close-circle-outline red"></i>
                                                                                    </td>
                                                                                    <td>2.5454545</td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>--}}
                                    </div>
                            </div>
                            @endif
                        </div>
                    </div>
            </div>
            @endif
            {{--  <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                  <div class="card">
                      <div class="card-body">
                          <h5 class="card-title">Notifications</h5>
                          <div class="settings-notification">
                              <ul>
                                  <li>
                                      <div class="notification-info">
                                          <p>Update price</p>
                                          <span>Get the update price in your dashboard</span>
                                      </div>
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="notification1">
                                          <label class="custom-control-label" for="notification1"></label>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="notification-info">
                                          <p>2FA</p>
                                          <span>Unable two factor authentication service</span>
                                      </div>
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="notification2"
                                                 checked>
                                          <label class="custom-control-label" for="notification2"></label>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="notification-info">
                                          <p>Latest news</p>
                                          <span>Get the latest news in your mail</span>
                                      </div>
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="notification3">
                                          <label class="custom-control-label" for="notification3"></label>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="notification-info">
                                          <p>Email Service</p>
                                          <span>Get security code in your mail</span>
                                      </div>
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="notification4"
                                                 checked>
                                          <label class="custom-control-label" for="notification4"></label>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="notification-info">
                                          <p>Phone Notify</p>
                                          <span>Get transition notification in your phone </span>
                                      </div>
                                      <div class="custom-control custom-switch">
                                          <input type="checkbox" class="custom-control-input" id="notification5"
                                                 checked>
                                          <label class="custom-control-label" for="notification5"></label>
                                      </div>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div class="card settings-profile">
                      <div class="card-body">
                          <h5 class="card-title">Create API Key</h5>
                          <div class="form-row">
                              <div class="col-md-6">
                                  <label for="generateKey">Generate key name</label>
                                  <input id="generateKey" type="text" class="form-control"
                                         placeholder="Enter your key name">
                              </div>
                              <div class="col-md-6">
                                  <label for="rewritePassword">Confirm password</label>
                                  <input id="rewritePassword" type="password" class="form-control"
                                         placeholder="Confirm your password">
                              </div>
                              <div class="col-md-12">
                                  <input type="submit" value="Create API key">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-body">
                          <h5 class="card-title">Your API Keys</h5>
                          <div class="wallet-history">
                              <table class="table">
                                  <thead>
                                  <tr>
                                      <th>No.</th>
                                      <th>Key</th>
                                      <th>Status</th>
                                      <th>Action</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <tr>
                                      <td>1</td>
                                      <td>zRmWVcrAZ1C0RZkFMu7K5v0KWC9jUJLt</td>
                                      <td>
                                          <div class="custom-control custom-switch">
                                              <input type="checkbox" class="custom-control-input" id="apiStatus1"
                                                     checked>
                                              <label class="custom-control-label" for="apiStatus1"></label>
                                          </div>
                                      </td>
                                      <td><i class="icon ion-md-trash"></i></td>
                                  </tr>
                                  <tr>
                                      <td>2</td>
                                      <td>Rv5dgnKdmVPyHwxeExBYz8uFwYQz3Jvg</td>
                                      <td>
                                          <div class="custom-control custom-switch">
                                              <input type="checkbox" class="custom-control-input" id="apiStatus2">
                                              <label class="custom-control-label" for="apiStatus2"></label>
                                          </div>
                                      </td>
                                      <td><i class="icon ion-md-trash"></i></td>
                                  </tr>
                                  <tr>
                                      <td>3</td>
                                      <td>VxEYIs1HwgmtKTUMA4aknjSEjjePZIWu</td>
                                      <td>
                                          <div class="custom-control custom-switch">
                                              <input type="checkbox" class="custom-control-input" id="apiStatus3">
                                              <label class="custom-control-label" for="apiStatus3"></label>
                                          </div>
                                      </td>
                                      <td><i class="icon ion-md-trash"></i></td>
                                  </tr>
                                  <tr>
                                      <td>4</td>
                                      <td>M01DueJ4x3awI1SSLGT3CP1EeLSnqt8o</td>
                                      <td>
                                          <div class="custom-control custom-switch">
                                              <input type="checkbox" class="custom-control-input" id="apiStatus4">
                                              <label class="custom-control-label" for="apiStatus4"></label>
                                          </div>
                                      </td>
                                      <td><i class="icon ion-md-trash"></i></td>
                                  </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>--}}

        </div>
    </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(async function () {
            var usdt = document.getElementsByClassName('usdt_balance');
            if(usdt &&  "{{!empty($token)}}")
            {
                var contract = await tronWeb.contract()
                    .at("TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");

                var result = await contract.balanceOf("{{!empty($token['address']['base58']) ? $token['address']['base58'] : ' '}}").call();
                console.log(result / 1000000);
                $('.usdt_balance').html(result / 1000000) ;
            }
        });

        function separate(Number)
        {
            Number+= '';
            Number= Number.replace(',', '');
            x = Number.split('.');
            y = x[0];
            z= x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(y))
                y= y.replace(rgx, '$1' + ',' + '$2');
            return y+ z;
        }

        function tag(address) {
            if ("{{isset($token['address']['base58'])}}") {
                $('.tag').attr('href', address);
            } else {

                    swal({
                        title: "برای ورود ابتدا کیف پول خود را بسازید.",
                        text: 'برای ساخت کیف پول روی دکمه زیر کلیک کنید',
                        icon: "success",
                        buttons: true,
                        successMode: true,
                    }).then(async (willDelete) => {
                        if (willDelete) {
                            swal("موفق! درخواست شما با موفقیت ثبت شد!", {
                                icon: "success",
                            });
                            create_account('usdt')
                        }
                    });
                    // swal('ناموفق', 'لطفا ابتدا کیف پول خود را بسازید!! سپس میتوانید اقدام به تبدیل ارز نمایید.', 'error')

            }
        }



        function create_account(unit) {
            const fullNode = 'https://api.trongrid.io';
            const solidityNode = 'https://api.trongrid.io';
            const eventServer = 'https://api.trongrid.io';

            const tronWeb = new TronWeb(fullNode, solidityNode, eventServer);
            var account = tronWeb.createAccount();
            var res = Promise.resolve(account);
            res.then(function (v) {
                var account = v;

                $.ajax({
                    url: '{{ route('createAccount', '') }}' + "/" + unit,
                    type: 'post',
                    data: {account: account, "_token": "{{ csrf_token() }}"},
                    success: function (data) {
                        if (data.status == '100') {
                            swal('موفق', data.msg, 'success');
                            setTimeout(function () {
                                window.location.reload()
                            }, 2000)
                        } else {
                            swal('ناموفق', data.msg, 'danger');
                        }

                    }
                });
            }, function (e) {
                console.error(e); // TypeError: Throwing
            });
        }

        function copy(input) {
            var copyText = document.getElementById(input);
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
            swal('موفق', 'ادرس شما با موفقیت کپی شد', 'success');
        }


        $(document).ready(function () {
            $(document).on('keyup', '.amount-rial', function () {
                var val = $(this).val();
                $(this).val(numberFormat(val))
            })
        })
    </script>

@endsection
