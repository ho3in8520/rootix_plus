@extends('templates.user.master_page')
@section('title_browser')
    {{ ucfirst($type) }}
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <style>
        .vertical-line {
            width: 1px;
            height: 40px;
            background: #e1e1e1;
            margin: auto;
        }

        .swal-modal .swal-text {
            text-align: center;
        }
    </style>
@endsection
@section('content')
    @if($type=='shares')
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <h4 class="page-title m-0">BTT to USDT</h4>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end page-title-box -->
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row px-3 swap-section">
                            @csrf
                            <div class="form-group col-sm-5">
                                <label>BTT <sub>inventory: {{ $shares_amount }} btt</sub></label>
                                <div class="input-group">
                                    <input class="form-control" name="btt-amount" max="{{ $shares_amount }}">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-info all-amount">$</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 d-flex flex-row">
                                <span class="vertical-line"></span>
                            </div>
                            <div class="form-group col-sm-5">
                                <label>USDT</label>
                                <input class="form-control usdt-val" disabled value="">
                            </div>
                            <div class="col-12">
                                <form action="" method="post">
                                    @csrf
                                    <button type="button" class="btn btn-success swap_buy">Swap BTT to USDT
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-12 mb-1">
                        <h4 class="page-title m-0">Your {{ ucfirst($type) }}</h4>
                        <a class="btn btn-success float-right" href="{{ route("user.{$type}.create") }}">Buy
                            New {{ ucfirst($type) }}</a>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if ($cancel_invest)
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-{{ $cancel_invest['date'] === true?'success':'warning' }} text-right">
                            cancellation fee for today
                            <b>{{ $cancel_invest['date'] === true?0:$cancel_invest['percent'].'%' }}</b>
                        </div>
                    </div>
                </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <div class="content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>value</th>
                                    <th>percent</th>
                                    <th>Profit</th>
                                    <th>Created at</th>
                                    <th>Last Update</th>
                                    <th>Operation</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if (count($invest_shares) > 0)
                                    @foreach($invest_shares as $key=>$item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $item->initial_value }} USDT</td>
                                            <td>{{ $item->percent}}%</td>
                                            <td class="text-success font-weight-bold">{{ $item->profit }} {{ $type=='invest'?'ctr':'btt' }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->updated_at }}</td>
                                            <td>
                                                <a class="btn btn-info btn-sm"
                                                   href="{{ route("user.$type.logs",['invest_shares'=>$item->id]) }}"><i
                                                        class="fa fa-eye"></i></a>
                                                @if ($type=='invest')
                                                    @if($item->status == 1)
                                                        <a href="{{ route('user.invest.add_to_share',['invest_shares'=>$item->id]) }}"
                                                           class="font-14 m-0 btn btn-success btn-sm confirm-box"
                                                           data-val="{{ $item->initial_value }}"
                                                        >
                                                            Add To Share
                                                        </a>
                                                        <a class="btn btn-danger btn-sm confirm-box"
                                                           href="{{ route('user.invest.cancel',['invest_shares'=>$item->id]) }}"
                                                           data-val="{{ $item->initial_value }}"
                                                        >Cancel</a>
                                                    @else
                                                        <span class="text-danger"
                                                              style="cursor: auto">Canceled</span>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="text-center">No row</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        @if($cancel_invest)
        $(".confirm-box").click(function (event) {
            event.preventDefault();
            let confirm_box = $(this);
            let cancellation_fee = '{{ $cancel_invest['date'] === true?0:$cancel_invest['percent'] }}';
            let val = $(this).data('val');
            let cancellation_price = ((100 - cancellation_fee) / 100) / val;
            let text = 'It is not possible to return after cancellation Or Add To Share! \n\r';
            text += `percent of cancellation: ${cancellation_fee}% \n\r`
            text += `total price of cancellation: ${cancellation_price.toFixed(4)} usdt \n\r`;
            swal({
                title: "Are You Sure?",
                text: text,
                icon: "warning",
                buttons: true,
                dangerMode: true,

            }).then((willDelete) => {
                if (willDelete == true) {
                    window.location.replace(confirm_box.attr('href'));
                }
            });
        });
        @endif

        $("input[name=btt-amount]").keyup(function () {
            let amount = $(this).val();
            if (amount > 0) {
                let form_ = {
                    'action': '{{ route('assets.price_on_usdt',['btt']) }}',
                    'method': 'post',
                    'data': {'_token': '{{ csrf_token() }}'},
                    'isObject': true,
                }
                ajax(form_, set_currency_price, {'amount': amount});
            } else {
                $('.usdt-val').val('')
            }
        })

        function set_currency_price(response, params) {
            let amount = params['amount'];
            $('.usdt-val').val(amount * response)
        }

        $(".swap_buy").click(function () {
            let amount = $(".swap-section input[name=btt-amount]").val();
            if (amount > 0) {
                swal({
                    title: 'Are you sure?',
                    text: 'swap btt to usdt',
                    icon: "warning",
                    buttons: ["Cancel", true],
                }).then(function (then) {
                    if (then != null) {
                        let data = {
                            'amount': $(".swap-section input[name=btt-amount]").val(),
                            '_token': '{{ csrf_token() }}'
                        };
                        let form = {
                            'action': '{{ route('assets.convert_to_usdt',['btt']) }}',
                            'method': 'post',
                            'isObject': true,
                            'data': data
                        };
                        ajax(form)
                    }
                })
            } else {
                swal('Ops!', 'amount not valid', 'warning');
            }
        });

        $(".all-amount").click(function () {
            let all_amount = $("input[name='btt-amount']").attr('max');
            $("input[name='btt-amount']").val(all_amount).keyup();
        });

    </script>
@endsection
