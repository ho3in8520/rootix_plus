@extends('templates.user.master_page')
@section('title_browser')
    Buy New {{ ucfirst($type) }}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-12 my-1">
                        <h4 class="page-title m-0">Buy New {{ ucfirst($type) }}</h4>
                        <a class="btn btn-info float-right"
                           href="{{ route("user.{$type}.index") }}">Your {{ ucfirst($type) }}</a>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>

    <!-- Form To Buy Share -->
    <div class="row">
        <div class="card col-12">
            <div class="card-body">
                <h3>Profit {{ ucfirst($type) }}</h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Profit <sub>(daily)</sub></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($profits) > 0)
                            @foreach($profits as $item)
                                <tr>
                                    <td>{{ strtoupper($item->name)  }}</td>
                                    <td>{{ $item->extra_field1 }}
                                        - {{ $item->extra_field2 == -1 ? '∞':$item->extra_field2-1 }}</td>
                                    <td><b>{{ $item->extra_field3 }}%</b></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3" class="text-center">No Row</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!-- Form To Buy Share -->

    <!-- User Assets Inventory -->
    <div class="row">
        <div class="card col-12">
            <div class="card-body">
                <h3>Inventory <sub style="font-size: 11px;">(The only currencies you can buy stocks with)</sub></h3>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>name</th>
                            <th>unit</th>
                            <th>inventory</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($USDT_asset)
                            <tr>
                                <td>{{ $USDT_asset->name }}</td>
                                <td>{{ $USDT_asset->unit }}</td>
                                <td><b>{{ $USDT_asset->amount }}</b></td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="3" class="text-center">No Row</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><!-- User Assets Inventory -->

    <!-- Form To Buy Share -->
    <div class="row">
        <div class="card col-12">
            <div class="card-body">
                <h3 class="mt-1 mb-3">Buy</h3>
                @if(count($profits) > 0)
                    @if ($USDT_asset->amount >= min($profits->pluck('extra_field1')->toArray()))
                        <form class="form row" method="post" action="{{ route("user.{$type}.store") }}">
                            @csrf
                            <div class="form-group col-md-6">
                                <label>Asset Type</label>
                                <select class="form-control">
                                    @if(count($profits) > 0)
                                        @foreach($profits->pluck('name')->unique() as $unit)
                                            <option>{{ strtoupper($item->name) }}</option>
                                        @endforeach
                                    @else
                                        <option selected disabled>No Options</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                @php
                                    $min_max= min($profits->pluck('extra_field2')->toArray());
                                    $is_infinite= $min_max==-1?true:false;
                                    $max= $is_infinite==true?$USDT_asset->amount:max($profits->pluck('extra_field2')->toArray()) ;
                                    $max= $max > $USDT_asset->amount?$USDT_asset->amount:$max;
                                    $min= min($profits->pluck('extra_field1')->toArray());
                                @endphp
                                <label>Amount <sub class="max-value">max: <b>{{ $is_infinite==true?$max:$max-1 }}</b>,
                                        min:<b>{{ $min }}</b></sub></label>
                                <div class="input-group">
                                    <input class="form-control" type="text" name="amount" placeholder="Example: 1"
                                           min="1" max="{{ $is_infinite==true?$max:$max-1 }}">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-info all-amount">$</button>
                                    </div>
                                </div>
                                <span class="text-danger error-text d-none"></span>
                                <span class="text-success percent-profit">Daily Profit : <b>%</b></span>
                            </div>
                            <div class="form-group col-12">
                                <button type="submit" class="btn btn-secondary ajaxStore buy" disabled>Buy</button>
                            </div>
                        </form>
                    @else
                        <div class="alert alert-warning text-center">
                            Your inventory is not enough
                        </div>
                    @endif
                @else
                    <div class="alert alert-warning text-center">
                        Not Found {{ ucfirst($type) }}
                    </div>
                @endif
            </div>
        </div>
    </div><!-- Form To Buy Share -->
@endsection
@section('script')
    @if(count($profits) > 0 && $USDT_asset->amount >= min($profits->pluck('extra_field1')->toArray()))
        <script>
            var USDT_amount = '{{ $USDT_asset->amount }}';
            var min = parseFloat('{{ $min }}');
            var max = parseFloat('{{ $max }}');
            var is_infinite = '{{ $is_infinite }}'

            // when user enter number in amount this function calculate profit and show under input
            $("input[name='amount']").keyup(function () {
                // $(".buy").attr('disabled', true).addClass('btn-secondary').removeClass('btn-info');
                let val = parseFloat($(this).val());
                if (val == '') {
                    $(".percent-profit b").text(`%`);
                    return;
                }
                $(".error-text").addClass('d-none').removeClass('d-block');
                $(".percent-profit").hide();
                console.log(is_infinite, val, max)
                if ((is_infinite == true && val > max) || (is_infinite == false && val >= max)) {
                    $(".error-text").text(`maximum value: ${max}`).addClass('d-block').removeClass('d-none');
                    return;
                }
                if (val < min) {
                    $(".error-text").text(`minimum value: ${min}`).addClass('d-block').removeClass('d-none');
                    return;
                }
                $(".percent-profit").show();
                let percent = '';
                @foreach($profits as $item)
                    ext2 = {{ $item->extra_field2 }};
                ext2 = ext2 == -1 ? max : ext2;
                ext1 = {{ $item->extra_field1 }};

                if (val >= ext1 && val <= ext2)
                    percent = {{ $item->extra_field3 }};
                @endforeach
                if (percent != '')
                    $(".buy").removeAttr('disabled').removeClass('btn-secondary').addClass('btn-info')
                $(".percent-profit b").text(`${percent}%`);
            });
            $(".all-amount").click(function () {
                let all_amount = $("input[name='amount']").attr('max');
                all_amount = all_amount;
                $("input[name='amount']").val(all_amount).keyup();
            })
        </script>
    @endif
@endsection
