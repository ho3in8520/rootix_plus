@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-Authentication')}}
@endsection
@section('style')
    {{--    <link rel="stylesheet" type="text/css" href="https://nobitex.ir/styles.64d4e435.css?__uncache=5%2F13%2F2021%2C%209%3A31%3A12%20PM" />--}}
    <style>
        .progress-container[data-v-822b901a] {
            width: 100%;
            margin: 0 auto 2rem
        }

        .progressbar[data-v-822b901a] {
            counter-reset: step;
            padding: 0
        }

        .progressbar li[data-v-822b901a] {
            list-style-type: none;
            width: 20%;
            float: right;
            font-size: 14px;
            font-weight: 700;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            color: #7d7d7d
        }

        .progressbar li[data-v-822b901a]:before {
            width: 30px;
            height: 30px;
            content: counter(step);
            counter-increment: step;
            line-height: 30px;
            border: 2px solid #7d7d7d;
            display: block;
            text-align: center;
            margin: 0 auto 10px;
            border-radius: 50%;
            background-color: #fff
        }

        .progressbar li[data-v-822b901a]:after {
            width: 100%;
            height: 2px;
            content: "";
            position: absolute;
            background-color: #7d7d7d;
            top: 15px;
            right: -50%;
            z-index: -1
        }

        .progressbar li[data-v-822b901a]:first-child:after {
            content: none
        }

        .progressbar li.active[data-v-822b901a] {
            color: #55b776
        }

        .progressbar li.active[data-v-822b901a]:before {
            border-color: #55b776
        }

        .progressbar li.active + li[data-v-822b901a]:after {
            background-color: #55b776
        }

        .progressbar li.pending[data-v-822b901a] {
            color: #ff7f30
        }

        .progressbar li.pending[data-v-822b901a]:before {
            border-color: #fbc133
        }

        .progressbar li.pending + li[data-v-822b901a]:after {
            background: #fbc133
        }

        .select2-container--default .select2-selection--single {
            height: 38px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            text-align: center;
            margin-top: 6px;
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid body mt-3"><!---->
        <div data-v-7fcc824e="" class="page-content">
            <div data-v-7fcc824e="" class="row">
                <div data-v-822b901a="" data-v-7fcc824e="" class="progress-container clearfix">
                    <ul data-v-822b901a="" class="progressbar d-flex justify-content-center">
                        <li data-v-822b901a="" class="active">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.phone')}}</p> <!----></li>
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.further')}}</p> <!----></li>
                        <li data-v-822b901a="" class="">
                            <!----> <p
                                data-v-822b901a="" class="">{{__('attributes.Authentication.bank')}}</p> <!----></li>
                        {{--<li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.uploadDocuments')}}</p>
                            <!----></li>--}}
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.identity')}}</p> <!----></li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            @if(!empty($user->reject_reason))
                                <div class="col-md-12">
                                    <div class="alert alert-danger text-right">
                                        {{ $user->reject_reason }}
                                    </div>
                                </div>
                            @endif
                            <form method="post" action="{{ route('step.send-email') }}">
                                @csrf
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="alert alert-warning text-left">
                                            {{__('attributes.Authentication.description')}}
                                        </div>
                                    </div>
                                    <div class="col-md-6 mx-auto ">
                                        <div class="input-group">
                                            <label class="m-auto">{{__('attributes.Authentication.phoneNum')}}
                                                :</label>
                                            <fieldset class="form-group position-relative input-divider-right mb-0">
                                                <input type="text" class="form-control text-right float-right"
                                                       id="iconLeft4"
                                                       disabled
                                                       value="{{ $user->mobile }}">
                                                <div class="form-control-position">
                                                    98+
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="alert alert-warning text-left">
                                            <strong>{{__('attributes.Authentication.activationLink')}}</strong>
                                        </div>
                                    </div>

                                    <div class="col-md-7 mx-auto">

                                        <div class="row input-group mx-auto">
                                            <label class="my-auto">{{__('attributes.Authentication.email')}}</label>
                                            <fieldset class="col form-group mb-0">
                                                <input type="text" class="form-control text-right float-right"
                                                       disabled
                                                       value="{{ $user->email }}">
                                                <input type="hidden" name="email" value="{{ $user->email }}">
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <button class="btn btn-success ajaxStore"
                                                type="button">{{__('attributes.Authentication.sendLink')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection
