@extends('templates.user.master_page')
@section('title_browser')

    {{__('titlePage.title-Authentication')}}

@endsection
@section('style')
    {{--    <link rel="stylesheet" type="text/css" href="https://nobitex.ir/styles.64d4e435.css?__uncache=5%2F13%2F2021%2C%209%3A31%3A12%20PM" />--}}
    <style>
        .progress-container[data-v-822b901a] {
            width: 100%;
            margin: 0 auto 2rem
        }

        .progressbar[data-v-822b901a] {
            counter-reset: step;
            padding: 0
        }

        .progressbar li[data-v-822b901a] {
            list-style-type: none;
            width: 20%;
            float: right;
            font-size: 14px;
            font-weight: 700;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            color: #7d7d7d
        }

        .progressbar li[data-v-822b901a]:before {
            width: 30px;
            height: 30px;
            content: counter(step);
            counter-increment: step;
            line-height: 30px;
            border: 2px solid #7d7d7d;
            display: block;
            text-align: center;
            margin: 0 auto 10px;
            border-radius: 50%;
            background-color: #fff
        }

        .progressbar li[data-v-822b901a]:after {
            width: 100%;
            height: 2px;
            content: "";
            position: absolute;
            background-color: #7d7d7d;
            top: 15px;
            right: -50%;
            z-index: -1
        }

        .progressbar li[data-v-822b901a]:first-child:after {
            content: none
        }

        .progressbar li.active[data-v-822b901a] {
            color: #55b776
        }

        .progressbar li.active[data-v-822b901a]:before {
            border-color: #55b776
        }

        .progressbar li.active + li[data-v-822b901a]:after {
            background-color: #55b776
        }

        .progressbar li.pending[data-v-822b901a] {
            color: #ff7f30
        }

        .progressbar li.pending[data-v-822b901a]:before {
            border-color: #fbc133
        }

        .progressbar li.pending + li[data-v-822b901a]:after {
            background: #fbc133
        }

        .select2-container--default .select2-selection--single {
            height: 38px;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            text-align: center;
            margin-top: 6px;
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid body mt-3"><!---->
        <div data-v-7fcc824e="" class="page-content">
            <div data-v-7fcc824e="" class="row">
                <div data-v-822b901a="" data-v-7fcc824e="" class="progress-container clearfix">
                    <ul data-v-822b901a="" class="progressbar d-flex justify-content-center">
                        <li data-v-822b901a="" class="active">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.phoneNum')}}</p> <!----></li>
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.further')}}</p> <!----></li>
                        <li data-v-822b901a="" class="">
                            <!----> <p
                                data-v-822b901a="" class="">{{__('attributes.Authentication.bank')}}</p> <!----></li>
                        {{--<li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.uploadDocuments')}}</p>
                            <!----></li>--}}
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.identity')}}</p> <!----></li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            @if(!empty($user->reject_reason))
                                <div class="col-md-12">
                                    <div class="alert alert-danger text-right">
                                        {{ $user->reject_reason }}
                                    </div>
                                </div>
                            @endif
                            <form action="{{ route('step.send-verify') }}" method="post" onsubmit="event.preventDefault()">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-md-12">
                                        <div class="alert alert-warning text-left">
                                            {{__('attributes.Authentication.description')}}
                                        </div>
                                    </div>
                                    @if(!empty($result['sms']))
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <label class="m-auto">{{__('attributes.Authentication.phoneNum')}}
                                                    :</label>
                                                <fieldset class="form-group position-relative input-divider-right mb-0">
                                                    <input type="text" data-type="number" class="form-control text-right float-right"
                                                           id="iconLeft4" name="number" disabled
                                                           value="{{ $result['sms']['user']->mobile }}">
                                                    <div class="form-control-position">
                                                        98+
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-md-7 row justify-content-center time-expire mt-2" style="display: none">
                                            <div class="input-group">
                                                <label class="m-auto">کد تایید: </label>
                                                <fieldset class="form-group position-relative input-divider-right mb-0">
                                                    <div class="code-verify">
                                                        <input type='text' data-type="number" class='form-control text-center' maxlength='5'
                                                               name='code'
                                                               placeholder="مثال: 12345"
                                                               style='letter-spacing: 5px'>
                                                    </div>
                                                    <div class="form-control-position time " style="border-right: solid 1px gainsboro"></div>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="col-md-7 row mt-1">
                                            <button class="btn btn-success button-verify ml-auto"
                                                    type="button">{{__('attributes.Authentication.submit')}}
                                            </button>
                                        </div>
                                    @else
                                        <div class="col-md-7">
                                            <div class="input-group">
                                                <label class="m-auto">{{__('attributes.Authentication.phoneNum')}}
                                                    :</label>
                                                <fieldset class="form-group position-relative input-divider-right mb-0">
                                                    <input type="text" data-type="number" class="form-control text-right float-right"
                                                           id="iconLeft4" name="number"
                                                           placeholder="مثال: 9131234569">
                                                    <div class="form-control-position">
                                                        98+
                                                    </div>
                                                </fieldset>

                                                {{--<input type="text" class="form-control" maxlength="10" name="number"
                                                       style="letter-spacing:5px">
                                                <div class="input-group-append">
                                                    <input type="text" class="disabled form-control" disabled value="+98">
                                                    <select class="form-control js-example-templating px-0 mb-0" name="country">
                                                        @foreach($countries as $item)
                                                            <option
                                                                value="{{ $item->code }}" {{ ($item->code=='IR')?'selected':'' }}>
                                                                +{{ $item->phonecode }} </option>
                                                        @endforeach
                                                    </select>
                                                </div>--}}
                                            </div>
                                        </div>
                                        <div class="col-md-7 row justify-content-center time-expire mt-2" style="display: none">
                                            <div class="input-group">
                                                <label class="m-auto">کد تایید: </label>
                                                <fieldset class="form-group position-relative input-divider-right mb-0">
                                                    <div class="code-verify"><!-- Js Complate --></div>
                                                    <div class="form-control-position time " style="border-right: solid 1px gainsboro"></div>
                                                </fieldset>
                                            </div>

                                            {{--<label
                                                class="mt-2 d-none label-code my-auto">{{__('attributes.Authentication.verificationCode')}}</label>
                                            <div class="col-md-3 code-verify">
                                            </div>--}}
                                        </div>
                                        <div class="col-md-7 row justify-content-center mt-1">
                                            <button class="btn btn-success sendCode ml-auto"
                                                    type="button">{{__('attributes.Authentication.sendCode')}}
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
        @section('script')
            <script>
                @if(!empty($result['sms']))
                time(`{{ $result['sms']['time'] }}`);
                @endif
                function formatState(state) {
                    if (!state.id) {
                        return state.text;
                    }
                    var baseUrl = `{{asset('theme/user/assets/img/countries')}}`;
                    var $state = $(
                        '<span><img style="width: 22px" src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.svg" class="img-flag" /> ' + state.text + '</span>'
                    );
                    return $state;
                };

                $(".js-example-templating").select2({
                    width: '100px',
                    templateResult: formatState,
                });

                $("input[name='number']").change(function () {
                    $(".help-block").html("");
                    $('span.error-mobile').remove();
                    $(".sendCode").removeAttr('disabled');
                    $("input[name='number']").css({'border-color': '#A6A9AE'});
                    var inputVal = $(this).val();
                    var characterReg = /^(0)?9\d{9}$/;
                    if (!characterReg.test(inputVal)) {
                        $(this).after('<span class="danger error-mobile" style="margin-left: 90px">فرمت شماره موبایل صحیح نیست</span>');
                        $("input[name='number']").css({'border-color': '#bc0005'});
                        $(".sendCode").attr('disabled', true)
                    }
                });

                $(document).on("click", '.sendCode', function () {
                    var elem = $(this);
                    var text_btn= elem.text();
                    elem.text('در حال ارسال SMS ...');
                    var number = $("input[name='number']").val();
                    var country = $("select[name='country']").val();
                    $.post(`{{ route('step.send-verify') }}`, {
                        _token: `{{ csrf_token() }}`,
                        number: number,
                        country: country,
                        mode: 1
                    }).done(function (response) {
                        if (response.status == 100) {
                            swal(response.msg, '', 'success');
                            $("input[name='number']").attr('disabled', true);
                            $(".code-verify").html("<input type='text' class='form-control text-center' placeholder='کد تایید را وارد کنید' maxlength='5' name='code' style='letter-spacing: 5px'>")
                            $("select[name='country']").prop("disabled", true);
                            $(".label-code").removeClass("d-none");
                            $(elem).removeClass("sendCode");
                            $(elem).addClass("button-verify");
                            $(elem).html("ثبت");
                            time(response.time)

                        } else if (response.status == 300) {
                            time(response.time)
                        } else {
                            swal(response.msg, '', 'error')
                            elem.text(text_btn);
                        }
                    })
                })

                $(document).on("click", '.button-verify', function () {
                    var elem = $(this);
                    elem.text('در حال بررسی ...');
                    var code = $("input[name='code']").val();
                    $.post(`{{ route('step.send-verify') }}`, {
                        _token: `{{ csrf_token() }}`,
                        code: code,
                        mode: 2
                    }).done(function (response) {
                        if (response.status == 100) {
                            swal(response.msg, '', 'success');
                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        } else {
                            swal(response.msg, '', 'error')
                        }
                    })
                })

                function time(time) {
                    $(".time-expire").show(100);
                    var minutes = 1;
                    var seconds = minutes * time;

                    function convertIntToTime(num) {
                        var mins = Math.floor(num / 60);
                        var secs = num % 60;
                        var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
                        return (timerOutput);
                    }

                    var countdown = setInterval(function () {
                        var current = convertIntToTime(seconds);
                        $('.time').html(current);

                        if (seconds == 0) {
                            clearInterval(countdown);
                        }
                        seconds--;
                        if (seconds >= 0) {
                        } else {
                            $("input[name='number']").attr('disabled',false);
                            $('h5').hide();
                            $('.time').html('<img src="{{ asset('theme/user/assets/img/icon/reload.svg') }}" class="sendCode" style="width:80%;cursor:pointer" title="ارسال مجدد"></img>');
                        }
                    }, 1000);
                }


            </script>
@endsection
