@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-ticket')}}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card " style="background-color: #f9f7f7">
                <div class="card-body">
                    <h5 class="card-title">{{__('attributes.ticket.tickets')}}</h5>

                    <div class="col-md-12 text-right mb-3 p-0">
                        <a href="{{route('ticket.create')}}" class="btn btn-success">{{__('attributes.ticket.new')}}</a>
                    </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>{{__('attributes.ticket.row')}}</th>
                                    <th>{{__('attributes.ticket.title')}}</th>
                                    <th>{{__('attributes.ticket.department')}}</th>
                                    <th>{{__('attributes.ticket.condition')}}</th>
                                    <th>{{__('attributes.ticket.date')}}</th>
                                    <th>{{__('attributes.ticket.operation')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($tickets->count() > 0)
                                @foreach($tickets as $ticket)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$ticket->title}}</td>
                                        <td>{{\Spatie\Permission\Models\Role::where('id',$ticket->role_id)->value('name')}}</td>
                                        <td>
                                            @switch($ticket->status)
                                                @case(1)
                                                <span  class="badge badge-warning">
                                                {{__('attributes.ticket.pending')}}
                                                </span>
                                                @break
                                                @case(2)
                                                <span  class="badge badge-success">
                                                {{__('attributes.ticket.read')}}
                                                </span>
                                                @break
                                                @case(3)
                                                <span  class="badge badge-info">
                                                {{__('attributes.ticket.referred')}}
                                                </span>
                                                @break
                                                @case(4)
                                                <span  class="badge badge-danger">
                                                    {{__('attributes.ticket.closed')}}
                                                </span>
                                                @break
                                                @default
                                                <span class="badge badge-danger">
                                                 {{__('attributes.ticket.unknown')}}
                                                </span>
                                        @endswitch

                                        <td>{{jdate_from_gregorian($ticket->created_at,'H:i:s Y-m-d ')}}</td>
                                        <td>
                                            <a class="btn btn-info btn-sm default btn-sm" title="{{__('attributes.ticket.show')}}"
                                               href="{{route('ticket.show',$ticket->id)}}" style="padding: 0.5rem 0.5rem">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center">Nothing found!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <div class="text-center col-md-2 mx-auto">
                                {!! $tickets->links() !!}
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
@endsection
