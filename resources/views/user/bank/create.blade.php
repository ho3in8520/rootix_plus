@extends('templates.user.master_page')
@section('title_browser')
    {{ isset($bank) && $bank?__('titlePage.title-bankEdit'):__('titlePage.title-bankNew') }}
@endsection

@section('content')
    <div class="settings mtb15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 m-auto col-12 row">
                    <div class="row col-12 bg-dark-2 text-light"
                         style="min-height: 200px; margin-bottom: -50px; padding-bottom: 40px; border-radius: 3px;">
                        <div class="col-12 row align-items-end m-auto">
                            <div class="col row  m-auto">
                                <div class="">
                                    <img src="{{asset('theme/user/assets/img/add-icon.png')}}" alt="logo" width="90"
                                         height="80">
                                </div>
                                <h3 class="col my-3 d-flex flex-row align-items-center text-white">{{ isset($bank)?__('titlePage.title-bankEdit'):__('titlePage.title-bankNew')  }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8 text-dark ">
                        <div class="card">
                            <div class="card-body">
                                <div class="card overflow-hidden">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <form class="col-10 mx-auto" method="post"
                                                  action=" {{ isset($bank) ? route('banks.update',$bank):route('banks.store') }}">
                                                @csrf
                                                @if(isset($bank))
                                                    @method('put')
                                                @endif
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label class="py-1">{{__('attributes.bank.bankName')}}
                                                            <sub class="text-danger">*</sub>
                                                        </label>
                                                        <select name="name" class="form-control custom-select">
                                                            <option disabled value="" selected>{{__('attributes.bank.bankSelect')}}
                                                            </option>
                                                            @foreach(banks_name() as $item)
                                                                <option
                                                                    value="{{ $item }}" name="name"
                                                                    @if(isset($bank) && $bank->name == $item) selected @endif>{{ $item }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="py-1">{{__('attributes.bank.cardNum')}}
                                                            <sub class="text-danger">*</sub>
                                                        </label>
                                                        <input type="number" name="card_number"
                                                               value="{{ isset($bank) ? $bank->card_number: '' }}"
                                                               class="form-control"
                                                               placeholder="{{__('attributes.bank.example')}}: 1235414525688745">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="py-1">{{__('attributes.bank.accountNumber')}}
                                                            <sub class="text-danger">*</sub>
                                                        </label>
                                                        <input type="number" name="account_number"
                                                               value="{{ isset($bank) ? $bank->account_number: '' }}"
                                                               class="form-control"
                                                               placeholder="{{__('attributes.bank.example')}}: 123456789101">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="py-1">{{__('attributes.bank.shaba')}}
                                                            <sub class="text-danger">*</sub><br>
                                                            <sub class="text-primary">{{__('attributes.bank.shabaIR')}}</sub>
                                                        </label>
                                                        <input type="text" name="sheba_number"
                                                               value="{{ isset($bank) ? $bank->sheba_number: '' }}"
                                                               class="form-control"
                                                               placeholder="{{__('attributes.bank.example')}}: IR421234567891234567891255">
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button"
                                                                class="btn btn-info d-block mx-auto px-4 ajaxStore">{{ isset($bank) ? __('attributes.bank.accountEdit'):__('attributes.bank.accountNew') }}</button>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 card ">
                        <div class="card-body">
                            <p class="alert alert-info">
                                Custom Text
                            </p>
                            <div class="video mx-auto">
                                <video style="width: 100%;" height="240" controls>
                                    <source src="movie.mp4" type="video/mp4">
                                    <source src="movie.ogg" type="video/ogg">
                                    Your browser not support
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
