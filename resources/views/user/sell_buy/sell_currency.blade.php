@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-buySell')}}
@endsection
@section('content')
    <style>
        .item-sell-package {
            transition: all .35s;
        }
        .item-sell-package:hover {
            transform: scale(1.01);
        }
    </style>
    <div class="row settings">
        <div class="col-md-12 col-lg-10 mx-auto row">
            <div class="col-md-12">
                @include('general.flash_message')
            </div>
            <div class="tab-content col-md-4 item-sell-package" id="v-pills-tabContent" style="margin-top: 45px;">
                <div class="card bg-success">
                    <div class="card-header" style="margin-top: -60px">
                        <img class="d-block mx-auto" src="http://127.0.0.1:8000/theme/landing/images/P2P.jpg" width="85" height="85" style="border-radius: 50%;/* box-shadow: -2px 0px 1px #767676, 2px 0px 1px #767676; */">
                    </div>
                    <div class="card-body">
                        <h3 class="text-center text-center text-white">فروش مستقیم P2P</h3>
                        <hr>
                        <p class="text-justify font-weight-bold line-height-2 text-white">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها .
                        </p>
                        <ul style="list-style: none;" class="mt-2 pl-0 card p-2 bg-white">
                            <li class="pb-1 text-success">
                                <i class="fa fa-check text-cuccess"></i>
                                <span>این یک متن تستی برای اوکی بودن است</span>
                            </li>
                            <li class=" text-danger">
                                <i class="fa fa-close text-danger"></i>
                                <span>این یک متن تستی برای غلط بودن است</span>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-white d-block mx-auto waves-effect waves-light">فروش مستقیم به کاربران P2P</a>
                    </div>
                </div>
            </div>
            <div class="tab-content col-md-4 item-sell-package" id="v-pills-tabContent" style="margin-top: 45px;">
                <div class="card bg-info">
                    <div class="card-header" style="margin-top: -60px">
                        <img class="d-block mx-auto" src="http://127.0.0.1:8000/theme/landing/images/P2P.jpg" width="85" height="85" style="border-radius: 50%;/* box-shadow: -2px 0px 1px #767676, 2px 0px 1px #767676; */">
                    </div>
                    <div class="card-body">
                        <h3 class="text-center text-center text-white">فروش مستقیم P2P</h3>
                        <hr>
                        <p class="text-justify font-weight-bold line-height-2 text-white">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها .
                        </p>
                        <ul style="list-style: none;" class="mt-2 pl-0 card p-2 bg-white">
                            <li class="pb-1 text-success">
                                <i class="fa fa-check text-cuccess"></i>
                                <span>این یک متن تستی برای اوکی بودن است</span>
                            </li>
                            <li class=" text-danger">
                                <i class="fa fa-close text-danger"></i>
                                <span>این یک متن تستی برای غلط بودن است</span>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-white d-block mx-auto waves-effect waves-light">فروش مستقیم به کاربران P2P</a>
                    </div>
                </div>
            </div>
            <div class="tab-content col-md-4 item-sell-package" id="v-pills-tabContent" style="margin-top: 45px;">
                <div class="card bg-warning">
                    <div class="card-header" style="margin-top: -60px">
                        <img class="d-block mx-auto" src="http://127.0.0.1:8000/theme/landing/images/P2P.jpg" width="85" height="85" style="border-radius: 50%;/* box-shadow: -2px 0px 1px #767676, 2px 0px 1px #767676; */">
                    </div>
                    <div class="card-body">
                        <h3 class="text-center text-center text-white">فروش مستقیم P2P</h3>
                        <hr>
                        <p class="text-justify font-weight-bold line-height-2 text-white">
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها .
                        </p>
                        <ul style="list-style: none;" class="mt-2 pl-0 card p-2 bg-white">
                            <li class="pb-1 text-success">
                                <i class="fa fa-check text-cuccess"></i>
                                <span>این یک متن تستی برای اوکی بودن است</span>
                            </li>
                            <li class=" text-danger">
                                <i class="fa fa-close text-danger"></i>
                                <span>این یک متن تستی برای غلط بودن است</span>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-white d-block mx-auto waves-effect waves-light">فروش مستقیم به کاربران P2P</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>

    </script>

@endsection
