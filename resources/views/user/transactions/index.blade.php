@extends('templates.user.master_page')
@section('title_browser') Transactions List @endsection
@section('content')

    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Transactions List
                    </div>
                    <hr class="mb-0">
                    <div class="card-body pt-0">
                        <form method="get" action="">
                            @csrf
                            <div class="row mt-4">
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="type">Deposit / Withdraw</label>
                                    <select id="type" name='type' class="form-select form-control">
                                        <option value="">All</option>
                                        <option value="1" {{ request()->type=='1'?'selected':'' }}>Withdraw</option>
                                        <option value="2" {{ request()->type=='2'?'selected':'' }}>Deposit</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="unit">Currency</label>
                                    <select id="unit" name="unit" class="form-select form-control">
                                        <option value="">All</option>
                                        <option value="usdt" {{ request()->unit=='usdt'?'selected':'' }}>usdt</option>
                                        <option value="rial" {{ request()->unit=='rial'?'selected':'' }} >rial</option>
                                        <option value="jst" {{ request()->unit=='jst'?'selected':'' }} >jst</option>
                                        <option value="win" {{ request()->unit=='win'?'selected':'' }} >win</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>Amount</label>
                                    <div class="input-group">
                                        <input class="form-control" name="amount_from" placeholder="From"
                                               value="{{ request()->amount_from?request()->amount_from:'' }}">
                                        <input class="form-control" name="amount_to" placeholder="Until"
                                               value="{{ request()->amount_to?request()->amount_to:'' }}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" name="date_from" placeholder="From"
                                               value="{{ request()->date_from?request()->date_from:'' }}">
                                        <input type="date" class="form-control" name="date_to" placeholder="Until"
                                               value="{{ request()->date_to?request()->date_to:'' }}">
                                    </div>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>Destination Address: </label>
                                    <div class="input-group">
                                        <input class="form-control" name="destination_address"
                                               value="{{ request()->destination_address?request()->destination_address:'' }}">
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-info search-ajax">جستجو</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped text-center table-bordered">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Currency</td>
                                    <td>Amount</td>
                                    <td>Tracking Code</td>
                                    <td>Type</td>
                                    <td>Description</td>
                                    <td>Destination Address</td>
                                    <td>Date</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($transactions) > 0)
                                    @foreach($transactions as $transaction)
                                        <tr>
                                            <td>{{ index($transactions,$loop) }}</td>
                                            @if ($transaction->financeable instanceof \App\Models\Asset)
                                                <td>{{ $transaction->financeable->unit }}</td>
                                            @else
                                                <td>-</td>
                                            @endif
                                            <td>{{ $transaction->amount }}</td>
                                            <td>
                                                @if($transaction->tracking_code)
                                                    <input id="tracking_{{$transaction->id}}" type="hidden"
                                                           class="form-control"
                                                           value="{{$transaction->tracking_code}}" disabled>
                                                    <button type="button" data-copy="tracking_{{$transaction->id}}"
                                                            class="btn btn-sm btn-primary copy-btn"><i
                                                            class="fa fa-copy"></i></button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <span class="badge badge-{{ $transaction->type==1?'danger':'success' }}">
                                                    {{ $transaction->type==1?'withdraw':'Deposit' }}
                                                </span>
                                            </td>
                                            <td>
                                                {{$transaction->description}}
                                            </td>
                                            <td>
                                                @if($transaction->extra_field1)
                                                    <input id="destination_{{$transaction->id}}" type="hidden"
                                                           value="{{$transaction->extra_field1}}">
                                                    <span class="font-12 mr-2">{{$transaction->extra_field1}}</span>
                                                    <button type="button" data-copy="destination_{{$transaction->id}}"
                                                            class="btn btn-sm btn-info copy-btn"
                                                            data-destination="{{$transaction->extra_field1}}"><i
                                                            class="fa fa-copy"></i></button>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                {{ $transaction->created_at }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <th colspan="8">Noting Found</th>
                                @endif
                                </tbody>

                            </table>
                            {!! $transactions->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $(document).on('click', ".copy-btn", function () {
            let elm = document.getElementById($(this).data('copy'));
            if (copyToClipboard(elm) == true) {
                swal('The text was successfully copied', '', 'success');
            }
        });
    </script>


@endsection
