@extends('templates.user.master_page')
@section('title_browser')
    پیغام ها
@endsection
@section('content')
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive text-center">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>عنوان</td>
                                <td>متن</td>
                                <td>تاریخ</td>
                                <td>عملیات</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if($notifications->total() >= 1)
                            @foreach($notifications as $item)
                                @php
$class=null;
if (!$item->viewed){
    $class="font-weight-bold";
}

@endphp                                <tr>
                                    <td class="{{ $class }}">{{ index($notifications,$loop) }}</td>
                                    <td class="{{ $class }}">{{$item->title}}</td>
                                    <td class="{{ $class }}">{!! \Illuminate\Support\Str::limit($item->description,20) !!}</td>
                                    <td class="{{ $class }}">{{\Carbon\Carbon::parse($item->started_at)->format('Y-m-d')}}</td>
                                        <td><a title="مشاهده" href="{{route('notification.show',base64_encode($item->id))}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a></td>
                                </tr>
                            @endforeach
                                @else
                            <tr>
                                <th colspan="5" class="font-weight-bold text-center">موردی یافت نشد!</th>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                        {!! $notifications->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
