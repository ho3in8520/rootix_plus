@extends('templates.admin.master_page')
@section('title_browser')
    لیست ارزها
@endsection
@section('after-title')
    <div class="text-zero top-right-button-container">
        <a href="{{ route('currencies.create') }}" class="btn btn-primary btn-lg top-right-button mr-1">افزودن جدید</a>
    </div>
@stop
<style>
    .rtl.rounded .card-img-left {
        margin: auto;
    }
</style>
@section('content')
    <section id="basic-form-layouts">
        <form class="row mt-2" method="get" action="{{ route('currencies.index') }}">
            <div class="form-group col-md-4">
                <label>نام کامل</label>
                <input type="text" class="form-control" name="full_name" value="{{ request()->full_name }}">
            </div>
            <div class="form-group col-md-4">
                <label>نامک</label>
                <input type="text" class="form-control" name="exclusive_name" value="{{ request()->exclusive_name }}">
            </div>
            <div class="form-group col-md-4">
                <label>وضعیت</label>
                <select class="form-control custom-control" name="status">
                    <option value="1" {{ (request()->query('status') == '1')? 'selected':''  }}>فعال</option>
                    <option value="0" {{ (request()->query('status') == '0')? 'selected':''  }}>غیرفعال</option>
                </select>
            </div>
            <div class="form-group col-12">
                <button class="btn btn-primary search-ajax">جستجو</button>
            </div>
        </form>
        <hr>
        <div class="row">
            <table class="col-12 table table-striped">
                <thead>
                <tr>
                    <th></th>
                    <th>آیکن</th>
                    <th>نام کامل</th>
                    <th>نامک</th>
                    <th>قیمت</th>
                    <th>تاریخ ایجاد</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($currencies as $i=>$currency)
                    <tr>
                        <td>{{ $i+1 }}</td>
                        <td><img src="{{ getImage($currency->files[0]->path) }}" width="65" height="65"></td>
                        <td>{{ $currency->full_name }}</td>
                        <td>{{ $currency->exclusive_name }}</td>
                        <td>{{ number_format($currency->price) }}</td>
                        <td>{{ jdate_from_gregorian($currency->created_at,'Y/m/d') }}</td>
                        <td>@if($currency->status==1) <span class="badge badge-success">فعال</span> @else <span
                                class="badge badge-danger">غیرفعال</span> @endif</td>
                        <td>
                            <a href="{{ route('currencies.edit',$currency) }}" class="btn btn-primary btn-sm">ویرایش</a>
                            <form action="{{ route('currencies.destroy',$currency->id) }}" method="post">
                                @csrf
                                {{ method_field('delete') }}
                                <button type="button" class="btn btn-danger ajaxStore mt-1 btn-sm" >
                                    حذف
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $currencies->appends(request()->query())->render('admin.paginate1') }}
    </section>

@endsection
@section('script')
    <script src="{{ asset('general/js/popup.js') }}"></script>
@stop
