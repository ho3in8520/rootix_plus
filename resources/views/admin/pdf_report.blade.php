<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<style>
    body {
        font-family: DejaVu Sans, sans-serif;
    }

    table {
        width: 100%;
    }

    table, th, td {
        border: 1px solid gray;
        border-collapse: collapse;
        text-align: center;
    }

    th, td {
        padding: 5px;
    }

    div {
        text-align: center;
    }
</style>
<body>
<div class="container mt-5">
    <h4 style="text-align: center">لیست کاربران</h4>
    <div>
        <table>
            @if(isset($data) && count($data)>0)
                <thead>
                <tr>
                    @foreach($data[0] as $key => $item)
                        <th>{{$key}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($data as $item)
                    <tr>
                        @foreach($item as $value)

                            <td>
                                <p class="list-item-heading">{{$value}}</p>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
                @else
                    <tr>لیست خالی می باشد.</tr>
                @endif
                </tbody>
        </table>
    </div>
</div>

<script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>

</html>
