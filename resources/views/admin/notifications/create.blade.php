@extends('templates.admin.master_page')
@section('title_browser')
    ایجاد پیغام
@endsection
@section('style')
    <style>
        button {
            overflow: hidden !important;
        }
        #ckeditor{
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('notifications.store')}}" method="post">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="example-text-input" class="">عنوان</label>
                                <input type="text" name="title" class="form-control">
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="example-text-input" class="">توضیحات</label>
                                <textarea id="ckeditor" name="description" rows="10" cols="125">
            </textarea>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-2 col-md-3 col-sm-6 mt-3">
                                <div class="form-check">
                                    <input class="receiver" type="radio" name="receiver" id="exampleRadios1" value="0" checked>
                                    <label class="form-check-label" for="exampleRadios1">
                                        همه کاربران
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="receiver" type="radio" name="receiver" id="exampleRadios2" value="1">
                                    <label class="form-check-label" for="exampleRadios2">
                                        کاربر خاص
                                    </label>
                                </div>

                            </div>
                            <div class="form-group col-lg-3 col-md-4 col-sm-6 d-none">
                                <label for="example-text-input" class="">کاربران</label>
                                <select id="users" type="text" name="user_id" class="form-control users">
                                    <option value="">انتخاب کنید...</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-4 col-sm-6">
                                <label for="example-text-input">رنگ پیغام</label>
                                <select name="color" class="form-control">
                                    <option value="info">info</option>
                                    <option value="warning">warning</option>
                                    <option value="danger">danger</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6">
                                <label for="example-text-input" readonly>تاریخ</label>
                                <sub class="text-danger">درصورت تمایل به ارسال در زمان حاضر این فیلد را پر نکنید.</sub>
                                <input name="started_at" class="form-control datepicker" readonly>
                            </div>
                            <div class="form-group col-lg-1 col-md-4 col-sm-6">
                                <label for="example-text-input">آیکون</label>
                                <span class="input-group-prepend">
            <button name="icon" class="btn btn-danger" data-icon="" role="iconpicker"></button>
        </span>

                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-info btn-lg ajaxStore">ایجاد پیغام</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('change','.receiver',function (){
                var val=$(this).val();
                var elem=$(".users").closest(".form-group");
                if (val==1) {
                    elem.removeClass('d-none');
                }else {
                    elem.addClass('d-none');
                }
            })
            $('.users').select2({
                width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: `{{ route('notifications.get_user') }}`,
                    dataType: 'json',
                    type: "get",
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.email,
                                    id: item.id,
                                }
                            })
                        };
                    }
                }
            });
        });

        CKEDITOR.replace('ckeditor',
            {
                customConfig: 'config.js',
                toolbar: 'simple'
            });
    </script>
@endsection
