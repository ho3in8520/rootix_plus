@extends('templates.admin.master_page')
@section('title_browser')
    پیغام ها
@endsection
@section('content')
    <div class="row m-2 justify-content-end">
        <div class="col-4 text-left">
                        <a href="{{route('notifications.create')}}" class="btn btn-success btn-lg text-white">ایجاد پیغام</a>
                    <!-- end col -->
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive text-center">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <td>عنوان</td>
                                <td>متن</td>
                                <td>نوع پیغام</td>
                                <td>تاریخ</td>
                                <td>وضعیت پیغام</td>
                                <td>عملیات</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($notifications as $item)
                                @php
                                    $notif_type=null;
                                    $notif_status=null;
                                        if(\Carbon\Carbon::parse($item->started_at)->isPast())
                                             $notif_status='ارسال شده';
                                        else
                                             $notif_status='در انتظار ارسال';

                                            switch ($item->color){
                                                    case 'info':
                                                        $notif_type='عادی';
                                                    break;
                                                    case 'warning':
                                                        $notif_type='هشدار';
                                                    break;
                                                    case 'danger':
                                                        $notif_type='اخطار';
                                                    break;
                                            }
                                @endphp
                                <tr>
                                    <td>{{$item->title}}</td>
                                    <td>{!! \Illuminate\Support\Str::limit($item->description,20) !!}</td>
                                    <td><span class="btn btn-sm btn-{{$item->color}} p-1">{{$notif_type}}</span></td>
                                    <td>{{\Carbon\Carbon::parse($item->started_at)->format('Y-m-d')}}</td>
                                    <td>{{$notif_status}}</td>
                                    @if(\Carbon\Carbon::parse($item->started_at)->isPast())
                                        <td>-</td>
                                    @else
                                        <td><a href="{{route('notifications.edit',$item->id)}}" class="btn btn-sm btn-success"><i class="fa fa-edit"></i></a></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $notifications->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
