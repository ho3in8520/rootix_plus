@extends('templates.admin.master_page')
@section('title_browser')
    ویرایش پیغام
@endsection
@section('style')
    <style>
        .iconpicker {
            overflow: hidden !important;
        }

        #ckeditor {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <h3 class="page-title m-0">ویرایش پیغام </h3>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title-box -->
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('notifications.update',$notification)}}" method="post">
                        @csrf
                        @method('put')
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="example-text-input" class="">عنوان</label>
                                <input type="text" name="title" class="form-control" value="{{$notification->title}}">
                            </div>

                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="example-text-input" class="">توضیحات</label>
                                <textarea id="ckeditor" name="description" rows="10" cols="125">
                                    {{$notification->description}}
            </textarea>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-2 col-md-3 col-sm-6 mt-3">
                                <div class="form-check">
                                    <input class="receiver" type="radio" name="receiver" id="exampleRadios1"
                                           value="0" {{ ($notification->user_id==0)?'checked':'' }}>
                                    <label class="form-check-label" for="exampleRadios1">
                                        همه کاربران
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="receiver" type="radio" name="receiver" id="exampleRadios2"
                                           value="1" {{ ($notification->user_id!=0)?'checked':'' }}>
                                    <label class="form-check-label" for="exampleRadios2">
                                        کاربر خاص
                                    </label>
                                </div>

                            </div>
                            @if($notification->user_id)
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="example-text-input" class="">کاربران</label>
                                    <select type="text" name="user_id" class="form-control users">
                                        <option value="{{ $notification->user_id }}"
                                                selected="selected">{{ $notification->user }}</option>
                                    </select>
                                </div>
                            @else
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 d-none">
                                    <label for="example-text-input" class="">کاربران</label>
                                    <select id="users" type="text" name="user_id" class="form-control users">
                                        <option value="">انتخاب کنید...</option>
                                    </select>
                                </div>
                            @endif
                            <div class="form-group col-lg-2 col-md-4 col-sm-6">
                                <label for="example-text-input">رنگ پیغام</label>
                                <select name="color" class="form-control">
                                    <option @if($notification->color == 'info') selected @endif value="info">info
                                    </option>
                                    <option @if($notification->color == 'warning') selected @endif value="warning">
                                        warning
                                    </option>
                                    <option @if($notification->color == 'danger') selected @endif value="danger">
                                        danger
                                    </option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-6">
                                <label for="example-text-input" readonly>تاریخ</label>
                                <sub class="text-danger">درصورت تمایل به ارسال در زمان حاضر این فیلد را پر نکنید.</sub>
                                <input name="started_at" class="form-control datePicker"
                                       value="{{$notification->started_at}}" readonly>
                            </div>
                            <div class="form-group col-lg-1 col-md-4 col-sm-6">
                                <label for="example-text-input">آیکون</label>
                                <span class="input-group-prepend">
            <button name="icon" class="btn btn-danger iconpicker" data-icon="{{ $notification->icon }}"
                    role="iconpicker"></button>
        </span>

                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-success btn-lg ajaxStore">ویرایش پیغام</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            setIconPicker()
            $(document).on('change', '.receiver', function () {
                var val = $(this).val();
                var elem = $(".users").closest(".form-group");
                if (val == 1) {
                    elem.removeClass('d-none');
                } else {
                    elem.addClass('d-none');
                }
            })
            $('.users').select2({
                width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: `{{ route('notifications.get_user') }}`,
                    dataType: 'json',
                    type: "get",
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.email,
                                    id: item.id,
                                }
                            })
                        };
                    }
                }
            });
        });
        CKEDITOR.replace('ckeditor',
            {
                customConfig: 'config.js',
                toolbar: 'simple'
            })
    </script>
@endsection
