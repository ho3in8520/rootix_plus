@if ($paginator->hasPages())
    <div class="row view-pager">
        <div class="col-sm-12">
            <div class="text-center">
                <div class="dataTables_paginate paging_simple_numbers" id="datatableRows_paginate">
                    <ul class="pagination pagination-sm d-flex flex-row justify-content-center">
                        @if (!$paginator->onFirstPage())
                            <li class="paginate_button page-item previous" id="datatableRows_previous"><a
                                    href="{{ $paginator->previousPageUrl() }}"
                                    aria-controls="datatableRows"
                                    data-dt-idx="0"
                                    tabindex="0"
                                    class="page-link prev"><i
                                        class="simple-icon-arrow-left"></i></a></li>
                        @endif

                        @foreach ($elements as $element)
                                {{-- Array Of Links --}}
                                @if (is_array($element))
                                    @foreach ($element as $page => $url)
                                        @if ($page == $paginator->currentPage())
                                            <li class="paginate_button page-item  active"><span class="page-link">{{ $page }}</span></li>
                                        @else
                                            <li class="paginate_button page-item ">
                                                <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                @endif
                        @endforeach
                        @if ($paginator->hasMorePages())
                            <li class="paginate_button page-item next" id="datatableRows_next"><a href="{{ $paginator->nextPageUrl() }}"
                                                                                                  aria-controls="datatableRows"
                                                                                                  data-dt-idx="5"
                                                                                                  tabindex="0"
                                                                                                  class="page-link next"><i
                                        class="simple-icon-arrow-right"></i></a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif
