@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت کاربران
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-4">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-2">
                        <div class="card-body">
                            <form method="get" action="" class="row">
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام: </label>
                                    <input type="text" class="form-control" name="first_name" value="{{ request()->first_name }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام خانوادگی: </label>
                                    <input type="text" class="form-control" name="last_name" value="{{ request()->last_name }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>موبایل: </label>
                                    <input type="number" class="form-control" name="mobile" value="{{ request()->mobile }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>ایمیل: </label>
                                    <input type="text" class="form-control" name="email" value="{{ request()->email }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>کد ملی: </label>
                                    <input type="number" class="form-control" name="national_code" value="{{ request()->national_code }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>جنسیت: </label>
                                    <select class="form-control custom-select" name="gender">
                                        <option value="" {{ !request()->gender?'selected':'' }}>همه</option>
                                        <option value="1" {{ request()->gender=='1'?'selected':'' }}>مرد</option>
                                        <option value="0" {{ request()->gender=='0'?'selected':'' }}>زن</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>تاریخ تولد: </label>
                                    <input type="text" class="form-control date-picker" name="birth_day" value="{{ request()->birth_day }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>وضعیت پنل: </label>
                                    <select class="form-control custom-select" name="status">
                                        <option value="" {{ !request()->status?'selected':'' }}>همه</option>
                                        <option value="1" {{ request()->status=='1'?'selected':'' }}>فعال</option>
                                        <option value="0" {{ request()->status=='0'?'selected':'' }}>غیرفعال</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <button type="submit" class="btn btn-success float-right">جستجو</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="row col-12 p-2 justify-content-end">
                                    <a href="{{route('user.index',request()->all()+['export' => 'pdf'])}}" type="button" class="btn btn-sm btn-info mr-2" title="دانلود pdf">
                                        <i class="fa fa-file-pdf"></i>
                                    </a>
                                    <a href="{{route('user.index',request()->all()+['export'=>'excel'])}}" type="button" class="btn btn-sm btn-success" title="دانلود اکسل">
                                        <i class="fa fa-file-excel"></i>
                                    </a>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>نام ونام خانوادگی</th>
                                        <th>ایمیل</th>
                                        <th>کد ملی</th>
                                        <th>سطح کاربر</th>
                                        <th>احراز هویت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>
                                                <p class="list-item-heading">{{ (empty($user->first_name))?'تکمیل نشده':$user->first_name . ' ' . $user->last_name}}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{$user->email}}</p>
                                            </td>
                                            <td>
                                                <p class="text-muted">{{ (empty($user->first_name))?'تکمیل نشده':$user->national_code}}</p>
                                            </td>
                                            <td>
                                                <span
                                                    class="badge badge-danger text-white">سطح  {{ $user->step_complate}}</span>
                                            </td>
                                            <td>
                                                <a title="مراحل احراز هویت" class="btn btn-info btn-sm"
                                                   href="{{route('user.authenticate',$user->id)}}"
                                                   style="color: #fff;cursor: pointer"><i class="fa fa-user" style="vertical-align: inherit;"></i> </a>
                                            </td>
                                            <td>
                                                <a title="نمایش اطلاعات کاربر" class="btn btn-info btn-sm"
                                                   href="{{route('user.show',$user->id)}}"
                                                   style="color: #fff;cursor: pointer"><i class="fa fa-eye" style="vertical-align: inherit;"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $users->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
