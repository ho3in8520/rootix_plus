@if(count($result) > 0)
    @foreach($result as $item)
        <div class="row row-btt">
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" name="percent[]" value="{{ number_format(floor($item->extra_field3*100)/100, 2) }}" onkeyup="($(this).val(numberFormat($(this).val())))" class="form-control loan_max_amount" placeholder="مقدار کارمزد">
                    <input type="hidden" name="id[]" value="{{ $item->id }}">
                </div>
            </div>
            <div class="col-md-3">
                <hr>
            </div>
            <div class="col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control loan_max_amount" placeholder="از" name="from[]" onkeyup="($(this).val(numberFormat($(this).val())))" value="{{ number_format(floor($item->extra_field1*100)/100, 2) }}">
                    <input type="text" class="form-control loan_max_amount" placeholder="تا" name="to[]" onkeyup="($(this).val(numberFormat($(this).val())))" value="{{ number_format(floor($item->extra_field2*100)/100, 2) }}">
                </div>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-sm btn-info add"><i class="simple-icon-plus"></i>
                </button>
                <button type="button" class="btn btn-sm btn-danger remove"><i class="simple-icon-minus"></i></button>
            </div>
        </div>
    @endforeach
@else
    <div class="row row-btt">
        <div class="col-md-2">
            <div class="form-group">
                <input type="text" name="percent[]" onkeyup="($(this).val(numberFormat($(this).val())))" class="form-control" placeholder="مقدار کارمزد">
                <input type="hidden" name="id[]">
            </div>
        </div>
        <div class="col-md-3">
            <hr>
        </div>
        <div class="col-md-3">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="از" name="from[]" onkeyup="($(this).val(numberFormat($(this).val())))">
                <input type="text" class="form-control" placeholder="تا" name="to[]" onkeyup="($(this).val(numberFormat($(this).val())))">
            </div>
        </div>
        <div class="col-md-2">
            <button type="button" class="btn btn-sm btn-info add"><i class="simple-icon-plus"></i>
            </button>
            <button type="button" class="btn btn-sm btn-danger remove"><i
                    class="simple-icon-minus"></i></button>
        </div>
    </div>
@endif

