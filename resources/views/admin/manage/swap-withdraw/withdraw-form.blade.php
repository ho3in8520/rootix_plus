<div class="form-group col-md-6">
    <div class="input-group ">
        <label class="col-12">حداقل برداشت</label>
        <input name="min_withdraw" type="text" class="form-control" style="width: 200px" onkeyup="($(this).val(numberFormat($(this).val())))" value="{{ number_format(floor($min_mithdraw *100)/100, 2) }}">
    </div>
</div>
<div class="form-group col-md-6">
    <div class="input-group ">
        <label class="col-12">کارمزد برداشت</label>
        <input name="fee_withdraw" type="text" class="form-control" style="width: 200px" onkeyup="($(this).val(numberFormat($(this).val())))" value="{{ number_format(floor($fee_mithdraw *100)/100, 2) }}">
    </div>
</div>
