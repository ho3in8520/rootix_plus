@extends('templates.admin.master_page')
@section('title_browser')
    تیکت ها
@endsection
@section('style')
    <style>
        .view-pager {
            display: none;
        }

        .view-filter {
            display: none;
        }
    </style>
@endsection
@section('content')
    <section id="basic-form-layouts">
        <div class="row">
            <div class="col-12 mb-4">
                <form class="row mt-2" method="get" action="{{ route('ticket.admin.index') }}">
                    @csrf
                    <div class="form-group col-md-4">
                        <label>شماره تیکت</label>
                        <input type="text" class="form-control" name="ticket_id" value="{{ request()->ticket_id }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>ایمیل</label>
                        <input type="text" class="form-control" name="email" value="{{ request()->email }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>کد کاربری</label>
                        <input type="text" class="form-control" name="code" value="{{ request()->code }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>عنوان</label>
                        <input type="text" class="form-control" name="title" value="{{ request()->title }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>فوریت</label>
                        <select class="form-control custom-control" name="priority">
                            <option></option>
                            <option value="1" {{ (request()->query('priority') == '1')? 'selected':''  }}>کم</option>
                            <option value="2" {{ (request()->query('priority') == '2')? 'selected':''  }}>متوسط</option>
                            <option value="3" {{ (request()->query('priority') == '3')? 'selected':''  }}>زیاد</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>وضعیت</label>
                        <select class="form-control custom-control" name="status">
                            <option></option>
                            <option value="1" {{ (request()->query('status') == '1')? 'selected':''  }}>در حال بررسی
                            </option>
                            <option value="2" {{ (request()->query('status') == '2')? 'selected':''  }}>پاسخ داده شده
                            </option>
                            <option value="3" {{ (request()->query('status') == '3')? 'selected':''  }}>ارجاع داده شده
                            </option>
                            <option value="4" {{ (request()->query('status') == '4')? 'selected':''  }}>بسته شده
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>همراه فایل</label>
                        <select class="form-control custom-control" name="file">
                            <option></option>
                            <option value="1" {{ (request()->query('file') == '1')? 'selected':''  }}>فعال</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label> تاریخ شروع</label>
                        <input type="text" class="form-control observer-example" name="date_from"
                               value="{{ request()->date_from }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>تاریخ پایان</label>
                        <input type="text" class="form-control observer-example" name="date_to"
                               value="{{ request()->date_to }}">
                    </div>
                    <div class="form-group col-12">
                        <button class="btn btn-primary search-ajax">جستجو</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-12 mb-4">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped ">
                            <thead>
                            <tr>
                                <th>تیکت آیدی</th>
                                <th>تاریخ</th>
                                <th>ایمیل</th>
                                <th>عنوان</th>
                                <th>فوریت</th>
                                <th>وضعیت</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($tickets) > 0)
                            @foreach($tickets as $ticket)
                                <tr>
                                    <td>
                                        <p class="list-item-heading">{{$ticket->id}}</p>
                                    </td>
                                    <td>
                                        <p class="list-item-heading">{{jdate_from_gregorian($ticket->created_at,'H:i:s  Y-m-d')}}</p>
                                    </td>
                                    @foreach($ticket->user as $user)
                                        <td>
                                            <p class="list-item-heading">{{$user->email}}</p>
                                        </td>
                                    @endforeach
                                    <td>
                                        <p class="text-muted">{{$ticket->title}}</p>
                                    </td>
                                    <td>
                                        <p class="text-muted">
                                            @switch($ticket->priority)
                                                @case(1)
                                                <button class="btn btn-info">
                                                    کم
                                                </button>
                                                @break
                                                @case(2)
                                                <button class="btn btn-primary">
                                                    متوسط
                                                </button>
                                                @break
                                                @case(3)
                                                <button class="btn btn-danger">
                                                    زیاد
                                                </button>
                                                @break
                                                @default
                                                نا معلوم
                                            @endswitch
                                        </p>
                                    </td>
                                    <td>
                                        @switch($ticket->status)
                                            @case(1)
                                            <button class="btn btn-warning">
                                                بررسی نشده
                                            </button>
                                            @break
                                            @case(2)
                                            <button class="btn btn-primary">
                                                پاسخ داده شده
                                            </button>
                                            @break
                                            @case(3)
                                            <button class="btn btn-danger">
                                                ارجاع داده شده
                                            </button>
                                            @break
                                            @case(4)
                                            <button class="btn btn-danger">
                                                بسته شده
                                            </button>
                                            @break
                                            @default
                                            نا معلوم
                                        @endswitch
                                    </td>
                                    <td>
                                        <div class="glyph">
                                            <a class="btn btn-info btn-sm default btn-sm" title="نمایش"
                                               href="{{route('ticket.admin.show',$ticket->id)}}">
                                                <i class="glyph-icon simple-icon-eye"></i>
                                            </a>
                                            <a class="btn btn-danger btn-sm default btn-sm" title="نمایش"
                                               href="{{route('ticket.admin.ban',$ticket->id)}}">
                                                <i class="glyph-icon simple-icon-ban"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                                @else
                            <tr>
                                <th class="text-center" colspan="6">موردی یافت نشد!</th>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                        </div>
                        <div class="text-center  mx-auto">
                            {{$tickets->appends(request()->query())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $('.observer-example').persianDatepicker({
            observer: true,
            format: 'YYYY-MM-DD',
            altField: '.observer-example-alt',
            initialValue: false,
            locale: 'en'
        });
    </script>
@endsection
