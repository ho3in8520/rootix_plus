@extends('templates.admin.master_page')
@section('title_browser')
    {{ request()->type == 'invest'? 'Invest (usdt -> ctr)':'Shares (usdt -> btt)' }}
@endsection
@section('style')
    <style>
        .swal-text {
            direction: ltr;
        }
    </style>
@endsection
@section('content')
    <!-- Setting to buy shares & invest -->
    <div class="row">
        <div class="card col-12 my-1">
            <div class="card-body">
                <h6 class="mb-3">تنظیمات سود</h6>
                <form class="form row container-row data-form" method="post"
                      action="{{ route('admin.invest_shares.inviter_profit.store',['type'=> request()->type]) }}">
                    @csrf
                    <div class="form-group col-lg-4 col-md-5">
                        <label>نوع ارز</label>
                        <select class="form-control" name="unit">
                            <option value="ctr" {{ $profit_inviter->extra_field1=='ctr'?'selected':'' }}>ctr</option>
                            <option value="btt" {{ $profit_inviter->extra_field1=='btt'?'selected':'' }}>btt</option>
                            <option value="usdt" {{ $profit_inviter->extra_field1=='usdt'?'selected':'' }}>usdt</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-md-5">
                        <label>درصد سود</label>
                        <input type="text" class="form-control" name="percent"
                               value="{{ $profit_inviter->extra_field2 }}">
                    </div>
                    <div class="form-group col-lg-4 col-md-2 d-flex">
                        <button type="submit" class="btn btn-info float-left ajaxStore mt-auto">ثبت</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- Setting to buy shares & invest -->

    @if (request()->type == 'invest')
        <!-- Setting Cancel & add to shares invest -->
        <div class="row">
            <div class="card col-12 my-1">
                <div class="card-body">
                    <h6 class="mb-3">تنظیمات انصراف یا add to share</h6>
                    <form class="form row container-row data-form"
                          action="{{ route('admin.invest_shares.cancel_invest') }}" method="post">
                        @csrf
                        <div class="form-group col-lg-4 col-md-5">
                            <label>تاریخ انصراف invest</label>
                            <input type="date" name="date" value="{{ $cancel_invest['date'] }}" class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-5">
                            <label>درصد خسارت در صورت انصراف invest در تاریخ غیرمجاز</label>
                            <input type="number" min="0" name="percent" value="{{ $cancel_invest['percent'] }}"
                                   class="form-control">
                        </div>
                        <div class="form-group col-lg-4 col-md-2 d-flex">
                            <button type="submit" class="btn btn-info float-left ajaxStore mt-auto">ثبت</button>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- Setting Cancel & add to shares invest -->
    @endif


    <!-- Form To Buy Share -->
    <div class="row">
        <div class="card col-12 my-1">
            <div class="card-body">
                <p class="alert alert-warning font-weight-bold">عدد 1- به معنی مثبت بینهایت میباشد. بهتر است هنگام تعیین
                    بازه ها عدد حداکثر اخرین بازه 1- باشد</p>
                <form class="form row container-row data-form" method="post"
                      action="{{ route('admin.invest_shares.store',['type'=> request()->type]) }}">
                    @csrf
                    @foreach($invest_shares as $key=>$item)
                        <input type="hidden" name="id[]" value="{{ is_null($item) ? '': $item->id }}">
                        <div class="row col-12 extended-row">
                            <div class="row col-md-11 col-10">
                                <div class="form-group col-sm-4">
                                    <label>حداقل</label>
                                    <input class="form-control min" type="text" name="extra_field1[]"
                                           value="{{ is_null($item) ? '': $item->extra_field1 }}">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>حداکثر
                                        <sub>برای عدم وجود حداکثر عدد منفی یک (-1) را وارد کنید</sub>
                                    </label>
                                    <input class="form-control max" type="text" name="extra_field2[]"
                                           value="{{ is_null($item) ? '': $item->extra_field2 }}">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>درصد سود</label>
                                    <input class="form-control" type="text" name="extra_field3[]" min="0"
                                           value="{{ is_null($item) ? '': $item->extra_field3 }}">
                                </div>
                            </div>
                            <div class="form-group col-md-1 col-2 d-flex flex-row align-items-end">
                                <label></label>
                                <button class="btn btn-danger btn-sm remove-row" type="button">حذف</button>
                                <input class="remove-row-input" type="hidden"
                                       value="{{ is_null($item) ? '':route('admin.invest_shares.destroy',[$item->id,'type'=>request()->type]) }}">
                            </div>
                        </div>
                    @endforeach
                    <div class="form-group col-12">
                        <button type="submit" class="btn btn-info float-right ajaxStore">ثبت</button>
                        <button type="button" class="btn btn-success float-right mx-1 new-row">+ سطر جدید</button>
                    </div>
                </form>
                <form class="d-none remove-row-form" action="" method="post">
                    @method('delete')
                    @csrf
                    <button type="button" class="d-none ajaxStore" oncl></button>
                </form>
            </div>
        </div>
    </div><!-- Form To Buy Share -->
@endsection
@section('script')
    <script>
        $(".data-form .ajaxStore").click(function (event) {
            event.preventDefault();
            let inputs = $('.data-form .extended-row .min,.extended-row .max').map(function () {
                return parseInt($(this).val());
            }).get();
            var max_numbers = Math.max.apply(Math, inputs);
            var numbers = [];
            var conflict = [];
            $('.data-form input.max, input.min').removeClass('input-wrong');
            $('.data-form .extended-row').each(function () {
                let min = parseInt($(this).find('.min').val());
                let max = parseInt($(this).find('.max').val());
                let max2 = max;
                max = max == -1 ? max_numbers : max;
                numbers = numbers.concat(range(min, max));
                if (min > max) {
                    $(this).find('input.max, input.min').addClass('input-wrong', 'solid 1px #d2322d');
                    conflict.push(`[${min}, ${max2}]`);
                } else if (array_unique(numbers).length != numbers.length) {
                    $(this).find('input.max, input.min').addClass('input-wrong', 'solid 1px #d2322d');
                    conflict.push(`[${min}, ${max2}]`);
                }
            });

            if (conflict.length > 0) {
                let msg = 'ابتدا مشکل تداخل بازه های زیر را  حل کنید ' + "\n";
                msg += conflict.join(', ');
                // swal('error', msg, 'error')
                // return false;
            }
        });
        $(".data-form").delegate('.remove-row', 'click', function (event) {
            swal({
                title: "آیا مطمئن هستید؟",
                text: "بعد از حذف امکان بازگردانی مورد حذف شده وجود ندارد!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        let action = $(this).siblings('.remove-row-input').val();
                        if (!action) {
                            if ($(".extended-row").length > 1)
                                $(this).closest('.extended-row').remove();
                            else
                                swal('اوپس !', 'امکان حذف این سطر وجود ندارد', 'warning')
                        } else {
                            $(".remove-row-form").attr('action', action).find('.ajaxStore').click();
                        }
                    }
                });
        })
    </script>
@stop
