<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Dore jQuery</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    @include('templates.admin.layout.header')
    <link rel="stylesheet" href="{{asset('theme/admin/css/vendor/bootstrap-float-label.min.css')}}"/>
    {!! htmlScriptTagJsApi() !!}
</head>

<body class="background show-spinner no-footer">
<div class="fixed-background"></div>
<main>
    <div class="container">
        <div class="row h-100">
            <div class="col-12 col-md-10 mx-auto my-auto">
                <div class="card auth-card">
                    <div class="position-relative image-side ">

                        <p class=" text-white h2">جادوی کارِ ما توی جزئیاتشه</p>

                        <p class="white mb-0">
                            برای ورود به سیستم نام کاربری و رمز خود را وارد کنید
                            <br> اگه حساب کاربری نداری نگران نباش، از <a class="white" href="Pages.Auth.Register.html">اینجا</a>
                            میتونی تو سایت اسمتو بویسی
                        </p>
                    </div>
                    <div class="form-side">
                        <a href="Dashboard.Default.html">
                            <span class="logo-single"></span>
                        </a>
                        <h6 class="mb-4">ورود</h6>
                        <form method="post" action="{{ route('admin.login') }}">
                            @csrf
                            <label class="form-group has-float-label mb-4">
                                <input class="form-control" name="email"/>
                                <span>پست الکترونیک</span>
                            </label>

                            <label class="form-group has-float-label mb-4">
                                <input class="form-control" type="password" name="password" placeholder=""/>
                                <span>کلمه عبور</span>
                            </label>
                            <label class="form-group has-float-label mb-4">
                                {!! htmlFormSnippet([
"theme" => "light",
"size" => "normal",
"tabindex" => "3",
"callback" => "callbackFunction",
"expired-callback" => "expiredCallbackFunction",
"error-callback" => "errorCallbackFunction",
]) !!}
                            </label>
                            <div class="d-flex justify-content-between align-items-center">
                                <a href="#">رمز عبورت یادت رفته؟?</a>
                                <button class="btn btn-primary btn-lg btn-shadow ajaxStore" type="button">ورود</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('templates.admin.layout.footer')
<script>
    $(document).ready(function () {
    $("#g-recaptcha-response").addClass('d-none')
    })
</script>
</body>

</html>
