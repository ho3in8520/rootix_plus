<div>
    <!-- Form Starts -->
@if($viewMode=='login')
    <!-- Section Title Starts -->
        <div class="row text-center">
            <h2 class="title-head hidden-xs">{{__('attributes.header1')}} <span>{{__('attributes.header2')}}</span></h2>
            <p class="info-form">{{__('attributes.header')}}</p>
        </div>
        <!-- Section Title Ends -->
        <form wire:submit.prevent="login">
            <!-- Input Field Starts -->
            <div class="form-group">
                <input class="form-control @error('email') input-error-border @enderror" wire:model.defer="email"
                       id="email" placeholder="{{__('attributes.email')}} " type="email"
                       required>
                @error('email') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <!-- Input Field Ends -->
            <!-- Input Field Starts -->
            <div class="form-group">
                <input class="form-control @error('password') input-error-border @enderror" wire:model="password"
                       id="password" placeholder="{{__('attributes.password')}}"
                       type="password" required>
                @error('password') <span class="text-danger">{{ $message }}</span>@enderror
                <a href="{{ route('login.forget_password.form') }}" style="font-size: 13px;">{{__('attributes.forgetPass')}}</a>
            </div>
            <div class="form-group" style="display: none">
                <div class="alert-message"></div>
            </div>
            <!-- Input Field Ends -->
            <!-- Submit Form Button Starts -->

            <!-- Submit Form Button Ends -->
        </form>
@else
    <!-- Section Title Starts -->
        <div class="row text-center">
            <h2 class="title-head hidden-xs">{{__('attributes.header3')}} <span>{{__('attributes.header4')}}</span></h2>
            <p class="info-form">{{__('attributes.header')}}</p>
        </div>
        <!-- Section Title Ends -->
        <form wire:submit.prevent="register">
            <!-- Input Field Starts -->
            <div class="form-group">
                <input class="form-control @error('email') input-error-border @enderror" wire:model.defer="email"
                       id="email" placeholder="{{__('attributes.email')}}" type="email"
                       required>
                @error('email') <span class="text-danger">{{ $message }}</span>@enderror
            </div>

            <div class="form-group">
                <input class="form-control @error('password') input-error-border @enderror" wire:model="password"
                       id="password" placeholder="{{__('attributes.password')}}"
                       type="password" required>
                <span class="text-secondary d-block" style="font-size: 11px;">{{__('attributes.passChar')}}</span>
                <br><span class="text-secondary" style="font-size: 11px;">{{__('attributes.specialChar')}}: !@#$%^&*()</span>
                @error('password') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            @if($passwordStrength>0)
                @php
                    $class='progress-bar-danger';
    if ($passwordStrength >=2 && $passwordStrength <= 3){
        $class='progress-bar-warning';
    }elseif ($passwordStrength >=4){

            $class='progress-bar-success';
}
                @endphp
                <div class="progress">
                    <div class="progress-bar {{ $class }}" role="progressbar"
                         style="width: {{ $passwordStrength * 25}}%" aria-valuenow="{{ $passwordStrength}}"
                         aria-valuemin="0" aria-valuemax="4"></div>
                </div>
            @endif
            <div class="form-group">
                <input class="form-control @error('password_confirmation') input-error-border @enderror"
                       wire:model.defer="password_confirmation" id="password_confirmation" placeholder="{{__('attributes.repeatPassword')}}"
                       type="password" required>
                @error('password_confirmation') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <select class="form-control @error('country') input-error-border @enderror" wire:model.defer="country">
                    <option value="">{{__('attributes.selectCountry')}}</option>
                    @foreach($countries as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                @error('country') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group" style="display: none">
                <div class="alert-message"></div>
            </div>
            <!-- Input Field Ends -->
            <!-- Submit Form Button Starts -->
        {{--            <div class="form-group">--}}
        {{--                <button class="btn btn-primary" type="submit">ثبت اطلاعات</button>--}}
        {{--                <p class="text-center">قبلاً ثبت نام کردید؟<a href="#" wire:click="changeForm('login')">ورود به--}}
        {{--                        سیستم</a>--}}
        {{--            </div>--}}
        <!-- Submit Form Button Ends -->
        </form>
    @endif
    <form wire:submit.prevent="{{ ($viewMode=='login')?'login':'register' }}">
        <div class="form-group text-center" wire:ignore>
            {!! htmlFormSnippet([
    "theme" => "light",
    "size" => "normal",
    "tabindex" => "3",
    "callback" => "callbackFunction",
    "expired-callback" => "expiredCallbackFunction",
    "error-callback" => "errorCallbackFunction",
]) !!}
        </div>
        @error('recaptcha') <span class="text-danger">{{ $message }}</span>@enderror
        <div class="form-group">
            <button class="btn btn-primary"
                    type="submit">{{ ($viewMode=='login')?__('attributes.button-login'):__('attributes.submitRegister') }}</button>
            <p class="text-center"> {{ ($viewMode=='login')?__("attributes.href-checkRegister"):__("attributes.href-checkLogin") }}<a href="#"
                                                                                                           wire:click="changeForm({{ ($viewMode=='login')?"'register'":"'login'" }})">{{ ($viewMode=='login')?__("attributes.doRegister"):__("attributes.href-login" )}}</a>
        </div>
    </form>
    <!-- Form Ends -->
</div>

@section('script')
    <script>
        function callbackFunction() {
            var result = $(".g-recaptcha-response").val();
            Livewire.emit('recaptcha', result)
        }
    </script>
@append
