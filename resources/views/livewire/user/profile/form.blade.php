<div>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">{{__('attributes.header-info')}}</h5>
            <div class="settings-profile">
                <form wire:submit.prevent="submit">
                  {{--  <div class="row px-3">
                        <img src="{{ asset("theme/user/assets/img/avatar.svg") }}" alt="avatar">
                        <div class="custom-file mt-3 mr-3">
                            <input type="file" class="custom-file-input" id="fileUpload">
                            <label class="custom-file-label" for="fileUpload">Choose avatar</label>
                        </div>
                    </div>--}}
                    <div class="form-row mt-4 justify-content-start">
                        <div class="col-md-6">
                            <label for="formFirst">{{__('attributes.name')}}</label>
                            <input id="formFirst" type="text" class="form-control" wire:model.defer="first_name" placeholder="First name" @error("first_name") style="border-color: #dc3545"  @enderror>
                            @error("first_name") <div class="help-block text-danger">{{ $message }}</div>  @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="formLast">{{__('attributes.lastName')}}</label>
                            <input id="formLast" type="text" class="form-control" wire:model.defer="last_name" placeholder="Last name" @error("last_name") style="border-color: #dc3545"  @enderror>
                            @error("last_name") <div class="help-block text-danger">{{ $message }}</div>  @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="emailAddress">{{__('attributes.email')}}</label>
                            <input id="emailAddress" type="text" class="form-control" wire:model.defer="email" placeholder="Enter your email" @error("email") style="border-color: #dc3545"  @enderror>
                            @error("email") <div class="help-block text-danger">{{ $message }}</div>  @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="phoneNumber">{{__('attributes.phone')}}</label>
                            <input id="phoneNumber" type="text" class="form-control"  wire:model.defer="mobile" placeholder="Enter mobile number" @error("mobile") style="border-color: #dc3545"  @enderror>
                            @error("mobile") <div class="help-block text-danger">{{ $message }}</div>  @enderror
                        </div>
                  {{--      <div class="col-md-6">
                            <label for="selectLanguage">Language</label>
                            <select id="selectLanguage" class="custom-select">
                                <option selected>English</option>
                                <option>Mandarin Chinese</option>
                                <option>Spanish</option>
                                <option>Arabic</option>
                                <option>Russian</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="selectCurrency">Currency</label>
                            <select id="selectCurrency" class="custom-select">
                                <option selected>USD</option>
                                <option>EUR</option>
                                <option>GBP</option>
                                <option>CHF</option>
                            </select>
                        </div>--}}
                        <div class="col-md-12">
                            <input type="submit" value="Update">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
{{--    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Security Information</h5>
            <div class="settings-profile">
                <form>
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="currentPass">Current password</label>
                            <input id="currentPass" type="text" class="form-control" placeholder="Enter your password">
                        </div>
                        <div class="col-md-6">
                            <label for="newPass">New password</label>
                            <input id="newPass" type="text" class="form-control" placeholder="Enter new password">
                        </div>
                        <div class="col-md-6">
                            <label for="securityOne">Security questions #1</label>
                            <select id="securityOne" class="custom-select">
                                <option selected>What was the name of your first pet?</option>
                                <option>What's your Mother's middle name?</option>
                                <option>What was the name of your first school?</option>
                                <option>Where did you travel for the first time?</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="securityAnsOne">Answer</label>
                            <input id="securityAnsOne" type="text" class="form-control" placeholder="Enter your answer">
                        </div>
                        <div class="col-md-6">
                            <label for="securityTwo">Security questions #2</label>
                            <select id="securityTwo" class="custom-select">
                                <option selected>Choose...</option>
                                <option>What was the name of your first pet?</option>
                                <option>What's your Mother's middle name?</option>
                                <option>What was the name of your first school?</option>
                                <option>Where did you travel for the first time?</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="securityAnsTwo">Answer</label>
                            <input id="securityAnsTwo" type="text" class="form-control" placeholder="Enter your answer">
                        </div>
                        <div class="col-md-6">
                            <label for="securityThree">Security questions #3</label>
                            <select id="securityThree" class="custom-select">
                                <option selected>Choose...</option>
                                <option>What was the name of your first pet?</option>
                                <option>What's your Mother's middle name?</option>
                                <option>What was the name of your first school?</option>
                                <option>Where did you travel for the first time?</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="securityFore">Answer</label>
                            <input id="securityFore" type="text" class="form-control" placeholder="Enter your answer">
                        </div>
                        <div class="col-md-12">
                            <input type="submit" value="Update">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>--}}
</div>
