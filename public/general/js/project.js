$('.select2').select2({
    dir: 'rtl',
    width: '100%',
});


/////////////// اضافه کردن سطر جدید ////////////
/*
حتما سطری که میخواییم ازش کپی بگیریم باید کلاس extended-row رو داشته باشه
حتما باید اون سطر و باتن توی یه والد باشن و والد هم کلاس container-row رو داشته باشه مهم نیس والد چندم باشه
 */
$(".new-row").click(function () {
    let row = $(this).closest('.container-row')
        .find('.extended-row').last();

    row = row.clone();
    row.find('*').val('');
    row.find('textarea').text('');
    $(this).closest('.container-row')
        .find('.extended-row').last().after(row);

})
/////////////// اضافه کردن سطر جدید ////////////

///////////////ساخت اعداد در رنج ////////////
function range(start, end) {
    var ans = [];
    for (let i = start; i <= end; i++) {
        ans.push(parseInt(i));
    }
    return ans;
}
///////////////ساخت اعداد در رنج ////////////

///////////////یونیک کردن ارایه ////////////
function array_unique(array) {
    return array.filter((v, i, a) => a.indexOf(v) === i);
}
///////////////یونیک کردن ارایه ////////////

var minutes = 1;
var seconds = minutes * 60;

function convertIntToTime(num) {
    var mins = Math.floor(num / 60);
    var secs = num % 60;
    var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
    return (timerOutput);
}

var countdown = setInterval(function () {
    var current = convertIntToTime(seconds);
    $('timer').html(current);
    if (seconds == 0) {
        clearInterval(countdown);
    }
    seconds--;
    if (seconds >= 0) {
    } else {
        $('timer').hide();
        $('#test').show();
        $('#test').html('<span class="ft-refresh-ccw"</span>');
    }
}, 1000);

$(document).on('focus', ".datePicker", function () {
    $(this).datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy/mm/dd",
        showAnim: 'slideDown',
        showButtonPanel: true,
        yearRange: "-100:+10",
    });
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

function parent_form_submit(elm) {
    $(elm).parent('form').submit();
}

// تابع برای فراخوانی ajax
function ajax(form, func_name = null, params = {}) {
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        data: form.serialize(),
        success: function (response) {
            // اگه برای برگشت از ajax خواستیم یه تابعی رو اجرا کنیم
            if (func_name != null)
                func_name(response, params);

            if (response.msg) { //
                if (response.status == 200)
                    swal('موفق', response['msg'], 'success');
                else if (response.status == 500)
                    swal('ناموفق', response['msg'], 'error');
                else if (response.status == 300)
                    swal('ناموفق', response['msg'], 'warning');
            }

        },
        error: function (response) {
            if (func_name != null)
                func_name(response, params);

            swal('اوپس!', 'مشکلی بوجود آمده است لطفا با پشتیبانی سایت در ارتباط باشید', 'error');
        }
    });
}

// تایمر شمارش معکوس
function timer_count(time, class_name, func_after) {
    if (time > 0) {
        let timer = setInterval(function () {
            if (time > 0) {
                $(class_name).text(--time);
            } else {
                clearInterval(timer);
                if (func_after != null) {
                    func_after();
                }
            }
        }, 1000);
    }

}

var Base64 = {
    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    // public method for encoding
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = Base64._utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }
        return output;
    },
    // public method for decoding
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = Base64._utf8_decode(output);
        return output;
    },
    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },
    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}

function swalResponse(status, msg, title = '') {
    switch (status) {
        case 100:
            swal(title, msg, 'success')
            break;
        case 300:
            swal(title, msg, 'warning')
            break;
        case 500:
            swal(title, msg, 'error')
            break;
    }
}

//////////// تابع تبدیل اعداد فارسی به انگلیسی //////
function toEnglishNumber(strNum, elm) {
    var pn = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    var en = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

    var cache = strNum;
    for (var i = 0; i < 10; i++) {
        var regex_fa = new RegExp(pn[i], 'g');
        cache = cache.replace(regex_fa, en[i]);
    }
    $(elm).val(cache);
}

$("input[type=number],input[data-type=number]").keyup(function () {
    toEnglishNumber($(this).val(), $(this));
})
///////////////////////////////////////////
