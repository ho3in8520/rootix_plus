createdModals = [];
function showModal(heading, formContent, size, st = "height:400px;overflow-y: auto !important", buttons = {}) {
    var elId = crDyElId();
    var html = '<div id="' + elId + '" class="modal hide fade in" style="display:none;"><div class="modal-dialog ' + size + '">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<h4 class="modal-title">' + heading + '</h4>';
    html += '<button type="button" class="close" data-dismiss="modal">×</button>';
    html += '</div>'; // header
    html += '<div class="modal-body" style="' + st + '">';
    html += formContent;
    html += '</div>';// body
    html += '<div class="modal-footer">';
    $.each(buttons, function (key, value) {
        var str = "";
        $.each(value.data, function (k, v) {
            str += " data-" + k + "=" + '"' + v + '"';
        });
        html += '<button type="button" id="d" class="' + value.class + '" ' + str + '>';
        html += value.title;
        html += '</button>'; // close button
    });
    html += '<button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>';
    html += '</div>';  // footer
    html += '</div>';  // content
    html += '</div>';  // dialog
    html += '</div>';  // modalWindow
    $(".mdl").append(html);
    $("#" + elId).modal();
    $.each(createdModals, function (key) {
        // $('.modal').on('hidden.bs.modal', function () {
        //     alert();
        //     $('.modal-backdrop').modal('hide');
        // });
        // alert();
    });
    createdModals.push(elId);
    return $("#" + elId);
}
trClick = "";


function generateUrlAjax(elem, url) {
    var data = $(elem).closest('form').serializeArray();
    query = $.param(data);
    changeUrlAjax(url + "?" + query);
}

$(document).ready(function () {
    if (window.location.search) {
        $("div[data-parent='#filter']").addClass("show");
        $("button[data-target='#filter']").after(" <a class='btn btn-danger btn-remove-filters'>حذف فیلتر</a>");
    }
});

$(document).on("click",".btn-remove-filters",function () {
    window.location.href = window.location.href.split('?')[0];
});

function changeUrlAjax(url) {
    // return window.history.pushState("object or string", "Title", url);
    return true;
}

function showData(url = "", data = {}, modalMode = true, element = "#basic-form-layouts", buttons = {}) {
    $.get(url, data, function (response) {
        var viewHtml = JSON.parse(response).FormatHtml;
        if (modalMode) {
            var modal = showModal('', viewHtml, 'modal-dialog modal-lg','height:400px;overflow-y: auto !important', buttons);
            $(modal).find('.select2').parent().find("label").after("<br>");
            $(modal).find('.select2').select2({
                dir: 'rtl',
            });
            // $('.modal').modal('toggle');
        }
        else {
            $(element).html(viewHtml);
            $("button[data-target='#filter']").after(" <a class='btn btn-danger btn-remove-filters'>حذف فیلتر</a>");
            $("input[autofocus]").focus();
            var tmpStr = $("input[autofocus]").val();
            $("input[autofocus]").val('');
            $("input[autofocus]").val(tmpStr);
        }
    });
}

function showIndex(elment) {
    $(elment).html("");
}

$(document).on('click', '.search-ajax', function (e) {
    e.preventDefault();
    var elementClick = $(this);
    var url = $(elementClick).closest('form').attr('action');
    generateUrlAjax(elementClick, url);
    showData(url, $(elementClick).closest('form').serialize(), false);
});

$(document).on('click', '.modal .page-link', function (e) {
    e.preventDefault();
    var elementClick = $(this);
    showData($(elementClick).closest('form').attr('action'), $(elementClick).closest('form').serialize() + "&page=" + $(elementClick).html(), false)
});

$(document).on('click', '#admin-support-form-loaded-data-holder .page-link', function (e) {
    e.preventDefault();
    var elementClick = $(this);
    $.get($(elementClick).attr("href"), function (data) {
        var result = JSON.parse(data);
        $("#admin-support-form-loaded-data-holder").html(result.FormatHtml);
    });
});

$(document).on('click', 'tr.popup-selectable', function () {
    var elementClick = $(this);
    if ($.isFunction(window.customeFunctionAfterSelectRow)) {
        customeFunctionAfterSelectRow(elementClick);
    }
    $(trClick).html($(elementClick).closest('tr').attr('data-name'));
    $(trClick).closest('.form-group').find("input[type=hidden]").val($(elementClick).closest('tr').attr('data-id'));
    $(trClick).closest('.input-group').find("input[type=hidden]").val($(elementClick).closest('tr').attr('data-id'));
    var last_modal = createdModals.pop();
    $('#' + last_modal).modal('toggle');
});


