import {ethers} from "./ethers-5.0.esm.min.js";

if (document.getElementById('withdraw_button') ){
    document.getElementById('withdraw_button').addEventListener('click',async function() {
        var usdt_amount = document.getElementById('usdt_amount').innerText;
        var main_amount =document.getElementById('amount').value;
        var main_amount = main_amount.replace(/,/g,'');
        var regex = /\d+/g;
        var matches = usdt_amount.match(regex);

        if(matches[0]  >= main_amount + 1){
            var unit = $(this).data('unit');

            var withdraw = $(this).data('route-withdraw');
            var send_message_admin = $(this).data('route-send-message');
            var withdraw_from_wallet = $(this).data('route-withdraw-from');
            var dashboard = $(this).data('route-dashboard');
            var csrf = $(this).data('csrf');


            var address_wallet = document.getElementById('address_wallet').value;

            const fullNode = 'https://api.trongrid.io';
            const solidityNode = 'https://api.trongrid.io';
            const eventServer = 'https://api.trongrid.io';

            const tronWeb = new TronWeb(fullNode,solidityNode,eventServer);
            address_wallet = tronWeb.address.toHex(address_wallet);

            if(main_amount == '' || address_wallet == '' ){

                swal('ناموفق','لطفا فیلدها رو پر کنید','error');
                return false;
            }

            const AbiCoder = ethers.utils.AbiCoder;

            const ADDRESS_PREFIX_REGEX = /^(41)/;
            const ADDRESS_PREFIX = "41";

            async function encodeParams(inputs){
                let typesValues = inputs;
                let parameters = '';

                if (typesValues.length == 0)
                    return parameters
                const abiCoder = new AbiCoder();
                let types = [];
                const values = [];

                for (let i = 0; i < typesValues.length; i++) {
                    let {type, value} = typesValues[i];
                    if (type == 'address')
                        value = value.replace(ADDRESS_PREFIX_REGEX, '0x');
                    else if (type == 'address[]')
                        value = value.map(v => toHex(v).replace(ADDRESS_PREFIX_REGEX, '0x'));
                    types.push(type);
                    values.push(value);
                }

                try {
                    parameters = abiCoder.encode(types, values).replace(/^(0x)/, '');
                } catch (ex) {
                    console.log(ex);
                }
                return parameters

            }

            swal({
                title: "آیا شما موافق هستید؟",
                text:  "آیا با برداشت "+main_amount + unit +" موافق هستید؟",
                icon: "success",
                buttons: true,
                successMode: true,
            }).then(async (willDelete) => {
                if (willDelete) {
                    swal("موفق! درخواست شما با موفقیت ثبت شد!", {
                        icon: "success",
                    });

                    var amount = main_amount * 1000000;

                    var inputs = [
                        {type: 'address', value: address_wallet},
                        {type: 'uint256', value: parseInt(amount)},
                    ];

                    var amount2 = 1000000;
                    var inputs2 = [
                        {type: 'address', value: '4167e46d91927744c999ceb6bf625550b7e7f52eb7'},
                        {type: 'uint256', value: parseInt(amount2)}
                    ]
                    let parameters = await encodeParams(inputs);
                    let parameters2 = await encodeParams(inputs2);
                    if (parameters){
                        $.ajax({
                            url: withdraw,
                            type: 'POST',
                            data: {
                                parameters: parameters,
                                parameters2: parameters2,
                                address_wallet:address_wallet,
                                unit: unit,
                                amount: main_amount,
                                "_token": csrf
                            },
                            headers:
                                {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                },
                            success: function (data) {
                                if (data[0] == 'success') {
                                    swal('موفق', data[1], 'success');
                                    setTimeout(function () {
                                        window.location.href = dashboard;
                                    }, 5000);
                                }else if(data[0] == 'error'){
                                    swal('ناموفق', data[1], 'error');
                                    setTimeout(function () {
                                        window.location.href = dashboard;
                                    }, 5000);
                                }
                                else{
                                    swal('ناموفق', 'خطا در ثبت تراکنش', 'error');
                                    setTimeout(function () {
                                        window.location.href = dashboard;
                                    }, 5000);
                                }
                            }
                        });
                    }else{
                        swal("ناموفق!",'خطایی در ارسال بوجود آمده است','error');
                        setTimeout(function () {
                            window.location.href = dashboard;
                        },3000);
                    }
                } else {
                    swal("درخواست شما با موفقیت لغو شد!");
                    setTimeout(function () {
                        window.location.href = dashboard;
                    },3000);

                }
            })

        }else {
            swal("ناموفق!",'موجودی شما کافی نمیباشد','error');
            setTimeout(function () {
                main_amount.value = '';
                address_wallet.value = '';
            },3000);
        }
    });
}
