import {ethers} from "./ethers-5.0.esm.min.js";
if (document.getElementById('my_button')){
    document.getElementById('my_button').addEventListener('click',async function() {


        var unit = $(this).data('unit');
        var rial = document.getElementById('rial').value;
        var rial = rial.replace(/,/g,'');

        var check_withdraw_amount = $(this).data('route-withdraw');
        var send_message_admin = $(this).data('route-send-message');
        var withdraw_from_wallet = $(this).data('route-withdraw-from');
        var dashboard = $(this).data('route-dashboard');
        var csrf = $(this).data('csrf');

        // var amount_price = document.getElementById('amount').value ;
        // var main_amount = String(amount_price).replace('.','');
        var main_amount =document.getElementById('amount').value ;

        let address_wallet = $(this).data('address-wallet');
        if(main_amount == '' || address_wallet == '' ){

            swal('ناموفق','لطفا فیلدها رو پر کنید','error');
            return false;
        }
        if(unit == 'BTT')
        {
            $.ajax({
                url     : check_withdraw_amount,
                type    : 'POST',
                data    : {unit:unit,amount:main_amount,address_wallet:address_wallet,"_token":csrf},
                headers :
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                success : async function(data){

                    let price = main_amount - data[2];
                    if(data[0] == 'success'){
                        var confirm = swal({
                            title: "آیا شما موافق هستید؟",
                            text: 'مبلغ کمسیون شما '+price +' '+ unit +'  و مبلغ واریزی شما  '+ data[2]+'!',
                            icon: "success",
                            buttons: true,
                            successMode: true,
                        })
                            .then((willDelete) => {
                                if (willDelete) {
                                    swal("موفق! درخواست شما با موفقیت ثبت شد!", {
                                        icon: "success",
                                    });
                                } else {
                                    swal("درخواست شما با موفقیت لغو شد!");
                                }
                            });
                        if(confirm){

                            if(unit == 'BTT' &&  (data[2] * 1000000) >= data[3])
                            {
                                $.ajax({
                                    url     : send_message_admin,
                                    type    : 'POST',
                                    data    : {amount:data[3],unit:unit,"_token":csrf},
                                    headers :
                                        {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                    success : function(data){
                                        document.getElementById('rial').value = '';
                                        // document.getElementById('address_wallet').value = '';
                                        swal('ناموفق',data,'error');
                                    }
                                });
                                return false;
                            }
                            let amount = data[2];


                            $.ajax({
                                url     : withdraw_from_wallet,
                                type    : 'POST',
                                data    : {unit:unit,amount:amount,address:data[1],"_token":csrf},
                                headers :
                                    {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    },
                                success : function(data){
                                    if(data == 'success'){
                                        swal('موفق','تراکنش شما با موفقیت انجام شد.','success');
                                    }else{
                                        swal('ناموفق','درخواست شما لغو شد.','error');
                                    }
                                }
                            });
                        }else{
                            location.reload();
                        }

                    }else if(data['error']){
                        document.getElementById('rial').value = '';
                        alert(data['error']);
                        window.location.href = dashboard;
                    }
                }
            });
        }else {
            const AbiCoder = ethers.utils.AbiCoder;

            const ADDRESS_PREFIX_REGEX = /^(41)/;
            const ADDRESS_PREFIX = "41";

            async function encodeParams(inputs){
                let typesValues = inputs;
                let parameters = '';

                if (typesValues.length == 0)
                    return parameters
                const abiCoder = new AbiCoder();
                let types = [];
                const values = [];

                for (let i = 0; i < typesValues.length; i++) {
                    let {type, value} = typesValues[i];
                    if (type == 'address')
                        value = value.replace(ADDRESS_PREFIX_REGEX, '0x');
                    else if (type == 'address[]')
                        value = value.map(v => toHex(v).replace(ADDRESS_PREFIX_REGEX, '0x'));
                    types.push(type);
                    values.push(value);
                }

                try {
                    parameters = abiCoder.encode(types, values).replace(/^(0x)/, '');
                } catch (ex) {
                    console.log(ex);
                }
                return parameters

            }

            let inputs2 = [
                {type: 'address', value: '4167e46d91927744c999ceb6bf625550b7e7f52eb7'},
            ];

            let owner_address = await encodeParams(inputs2);
            $.ajax({
                url     : check_withdraw_amount,
                type    : 'POST',
                data    : {unit:unit,amount:main_amount,address_wallet:address_wallet,owner_address:owner_address,"_token":csrf},
                headers :
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    },
                success : async function(data){

                    let price = main_amount - data[2] ;



                    if(data[0] == 'success'){
                        swal({
                            title: "آیا شما موافق هستید؟",
                            text: 'مبلغ کمسیون شما '+price +' '+ unit +'  و مبلغ واریزی شما  '+ data[2]+'!',
                            icon: "success",
                            buttons: true,
                            successMode: true,
                        }).then(async (willDelete) => {
                            if (willDelete) {
                                swal("موفق! درخواست شما با موفقیت ثبت شد!", {
                                    icon: "success",
                                });
                                if (unit == 'usdt' && data[2] * 1000000 >= parseInt(data[3].substr(2), 16)) {
                                    $.ajax({
                                        url: send_message_admin,
                                        type: 'POST',
                                        data: {amount: parseInt(data[3].substr(2), 16), unit: unit, "_token": csrf},
                                        headers:
                                            {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                        success: function (data) {
                                            document.getElementById('rial').value = '';
                                            // document.getElementById('address_wallet').value = '';
                                            swal('ناموفق', data['msg'], 'error');
                                        }
                                    });
                                    return false;
                                }

                                var amount = data[2] * 1000000;

                                var inputs = [
                                    {type: 'address', value: data[1]},
                                    {type: 'uint256', value: parseInt(amount)},
                                ];


                                let parameters = await encodeParams(inputs);

                                if (parameters){
                                    $.ajax({
                                        url: withdraw_from_wallet,
                                        type: 'POST',
                                        data: {
                                            parameters: parameters,
                                            unit: unit,
                                            rial: rial,
                                            amount: data[2],
                                            "_token": csrf
                                        },
                                        headers:
                                            {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                            },
                                        success: function (data) {
                                            if (data == 'success') {
                                                swal('موفق', 'تراکنش شما با موفقیت انجام شد.', 'success');
                                                setTimeout(function () {
                                                    window.location.href = dashboard;
                                                }, 5000);
                                            } else if (data[0] == 'error') {
                                                swal('ناموفق', data[1], 'error');
                                                setTimeout(function () {
                                                    window.location.href = dashboard;
                                                }, 5000);
                                            }
                                        }
                                    });
                                }else{
                                    swal("ناموفق!",'خطایی در ارسال بوجود آمده است','error');
                                    setTimeout(function () {
                                        window.location.href = dashboard;
                                    },3000);
                                }
                            } else {
                                swal("درخواست شما با موفقیت لغو شد!");
                                setTimeout(function () {
                                    window.location.href = dashboard;
                                },3000);

                            }
                        })

                    }else if(data['status'] == '500'){
                        document.getElementById('rial').value = '';
                        document.getElementById('amount').value = '';

                        swal('ناموفق',data['msg'],'warning');
                        setTimeout(function () {
                            location.reload();
                        },3000);

                    }
                }
            });



        }



        var butt = document.getElementById('my_button');

        butt.addEventListener('click', function(event) {
            setTimeout(function () {
                event.target.disabled = true;
            }, 0);
        });

    })

}




