const openBtn = document.querySelector(".open-market-information");

openBtn.addEventListener("click", () => {
  const marketInformation = document.querySelector(".market-information");
  const marketInformations = document.querySelector(".market-informations");
  const marketInformationBtn = document.querySelector(
    ".market-information-btn"
  );

  const attributeValue = marketInformations.getAttribute("toggle");
  const parseAttributeValueToString = String(attributeValue);

  if (parseAttributeValueToString === "closed") {
    marketInformations.setAttribute("toggle", "open");
    marketInformations.style.height = `47px`;
  } else {
    marketInformations.setAttribute("toggle", "closed");
    marketInformations.style.height = `${
      marketInformation.offsetHeight + marketInformationBtn.offsetHeight + 50 
    }px`;
  }
});
