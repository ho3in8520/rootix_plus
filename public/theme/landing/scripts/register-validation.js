const email = document.getElementById("email");
const checkEmail = document.getElementById("check-email");

email.addEventListener("input", emailValidation);

function emailValidation() {
  const emailPattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;

  if (email.value.match(emailPattern) && email.value.trim().length !== 0) {
    checkEmail.classList.add("active");
    return true;
  } else {
    checkEmail.classList.remove("active");
    return false;
  }
}

const password = document.getElementById("password");
const checkPassword = document.getElementById("check-password");
const strengthMeter = document.getElementById("password-progress");

password.addEventListener("input", updateStrengthMeter);
updateStrengthMeter();

function updateStrengthMeter() {
  const passwordProgress = document.querySelector(".password-progress");
  let strength = 100;
  const weaknesses = calculatePasswordStrength(password.value);

  weaknesses.forEach((weakness) => {
    if (weakness == null) return;
    strength -= weakness.deduction;
    const messageElement = document.createElement("div");
    messageElement.innerText = weakness.message;
  });
  strengthMeter.style.width = `${strength}%`;

  if (strength < 30) {
    passwordProgress.removeAttribute("data-before");
    checkPassword.classList.remove("active");
  }

  if (strength >= 30 && strength <= 50) {
    passwordProgress.setAttribute("data-before", "متوسط");
    checkPassword.classList.add("active");
  }

  if (strength >= 50 && strength <= 100) {
    passwordProgress.setAttribute("data-before", "قوی");
    checkPassword.classList.add("active");
  }
}

function calculatePasswordStrength(password) {
  const weaknesses = [];
  weaknesses.push(lengthWeakness(password));
  weaknesses.push(lowercaseWeakness(password));
  weaknesses.push(uppercaseWeakness(password));
  weaknesses.push(numberWeakness(password));
  weaknesses.push(specialCharactersWeakness(password));
  weaknesses.push(repeatCharactersWeakness(password));
  return weaknesses;
}

function lengthWeakness(password) {
  const length = password.length;

  if (length <= 5) {
    return {
      message: "Your password is too short",
      deduction: 40,
    };
  }

  if (length <= 10) {
    return {
      message: "Your password could be longer",
      deduction: 15,
    };
  }
}

function uppercaseWeakness(password) {
  return characterTypeWeakness(password, /[A-Z]/g, "uppercase characters");
}

function lowercaseWeakness(password) {
  return characterTypeWeakness(password, /[a-z]/g, "lowercase characters");
}

function numberWeakness(password) {
  return characterTypeWeakness(password, /[0-9]/g, "numbers");
}

function specialCharactersWeakness(password) {
  return characterTypeWeakness(
    password,
    /[^0-9a-zA-Z\s]/g,
    "special characters"
  );
}

function characterTypeWeakness(password, regex, type) {
  const matches = password.match(regex) || [];

  if (matches.length === 0) {
    return {
      message: `Your password has no ${type}`,
      deduction: 20,
    };
  }

  if (matches.length <= 2) {
    return {
      message: `Your password could use more ${type}`,
      deduction: 5,
    };
  }
}

function repeatCharactersWeakness(password) {
  const matches = password.match(/(.)\1/g) || [];
  if (matches.length > 0) {
    return {
      message: "Your password has repeat characters",
      deduction: matches.length * 10,
    };
  }
}

const passwordRepeat = document.getElementById("password-again");
const checkPasswordRepeat = document.getElementById("check-password-again");

passwordRepeat.addEventListener("input", passwordRepeatIsValid);
function passwordRepeatIsValid() {
  if (
    password.value === passwordRepeat.value &&
    passwordRepeat.value.trim().length !== 0
  ) {
    checkPasswordRepeat.classList.add("active");
    return true;
  } else {
    checkPasswordRepeat.classList.remove("active");
    return false;
  }
}

const referralCode = document.getElementById("referral-code");
const form = document.querySelector(".register-form");

form.addEventListener("submit", (e) => {

  if (emailValidation() && passwordRepeatIsValid()) {
    email.value = "";
    password.value = "";
    passwordRepeat.value = "";
    referralCode.value = "";

    strength = 0;

    checkEmail.classList.remove("active");
    checkPassword.classList.remove("active");
    checkPasswordRepeat.classList.remove("active");
  }
});
