
const menuIcon = document.querySelector(".res-menu__bar-icon");
const menu = document.querySelector(".container-menu ul");
menuIcon.addEventListener("click", () => {
  menu.classList.toggle("active");
});

const menuItems = [...document.querySelectorAll(".main-top-menu")];
const underMenus = [...document.querySelectorAll(".under-menu")];
const arrows = [...document.querySelectorAll(".down-arrow")];
menuItems.map((item, index) => {
  item.addEventListener("click", () => {
    underMenus[index].classList.toggle("active");
    item.classList.toggle("active");
    arrows[index].classList.toggle("fa-angle-up");
  });
});

document.addEventListener("click", (evt) => {
  let targetElement = evt.target; // clicked element

  do {
    if (targetElement == menu || targetElement == menuIcon) {
      return;
    }
    
    targetElement = targetElement.parentNode;
  } while (targetElement);

  menu.classList.remove("active");

});