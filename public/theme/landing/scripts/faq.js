const faqBtns = [...document.querySelectorAll(".faq")];

faqBtns.map((faqBtn) => {
  faqBtn.addEventListener("click", () => {
    faqBtn.classList.toggle("active");
    const faqArrow = faqBtn.querySelector(".faq-arrow");
    const faqInfoBtn = faqBtn.querySelector(".faq-info button");
    
    if (faqArrow.classList.contains("faq-arrow-down")) {
      const arrowUp = `<svg xmlns="http://www.w3.org/2000/svg" class="faq-arrow-up faq-arrow" width="15" height="8" viewBox="0 0 18 11.115">
      <path id="Icon_material-expand-more" data-name="Icon material-expand-more" d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z" transform="translate(27 24) rotate(180)" opacity="0.7"/>
    </svg>
    `;
      faqInfoBtn.innerHTML = arrowUp;
    } else if (faqArrow.classList.contains("faq-arrow-up")) {
      const arrowDown = `<svg
      xmlns="http://www.w3.org/2000/svg"
      width="15"
      class="faq-arrow-down faq-arrow"
      height="8"
      viewBox="0 0 18 11.115"
    >
      <path
        id="Icon_material-expand-more"
        data-name="Icon material-expand-more"
        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
        transform="translate(-9 -12.885)"
        opacity="0.7"
      />
    </svg>`;
      faqInfoBtn.innerHTML = arrowDown;
    }
  });
});
