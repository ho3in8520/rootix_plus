const forwardBtns = [...document.querySelectorAll(".comment__forward-btn")];

forwardBtns.map((editBtn) => {
  editBtn.addEventListener("click", () => {
    clearChild();
    createCommentInfo(editBtn);
  });
});

function clearChild() {
  const topCommentForm = document.querySelector(".top-comment-form");

  topCommentForm.innerHTML = "";
}

function getCommentInfo(editBtn) {
  const commentContainer =
    editBtn.parentElement.parentElement.parentElement.parentElement;

  return {
    name: commentContainer.querySelector(".comment__writer").textContent,
    text: commentContainer.querySelector(".comment__text").textContent,
  };
}

function createCommentInfo(editBtn) {
  const forwardInfo = document.querySelector(".forward-info");
  const commentInput = document.querySelector(".comment__input");
  const commentInfo = getCommentInfo(editBtn);
  const nameEl = document.querySelector(".forward-comment__name");
  const textEl = document.querySelector(".forward-comment__text");

  const { name, text } = commentInfo;
  nameEl.innerHTML = name;
  textEl.innerHTML = text;

  commentInput.placeholder = `متن پیام در پاسخ به ${name}`;
}
